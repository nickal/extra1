/*
 Navicat MySQL Data Transfer

 Source Server         : Localhost@Root
 Source Server Version : 50163
 Source Host           : localhost
 Source Database       : extra

 Target Server Version : 50163
 File Encoding         : utf-8

 Date: 12/03/2014 10:07:25 AM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ex_admin`
-- ----------------------------
DROP TABLE IF EXISTS `ex_admin`;
CREATE TABLE `ex_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `ex_admin`
-- ----------------------------
BEGIN;
INSERT INTO `ex_admin` VALUES ('1', 'haibing', '123456', '2014-11-26 21:27:08', '2014-11-26 21:27:10');
COMMIT;

-- ----------------------------
--  Table structure for `ex_catlog`
-- ----------------------------
DROP TABLE IF EXISTS `ex_catlog`;
CREATE TABLE `ex_catlog` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `parentId` int(11) NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `ex_catlog`
-- ----------------------------
BEGIN;
INSERT INTO `ex_catlog` VALUES ('1', '男装', '0', '2014-11-27 10:05:02', '2014-11-27 10:05:05'), ('2', '女装', '0', '2014-11-27 10:05:20', '2014-11-27 10:05:21'), ('3', '羽绒服', '1', '2014-11-27 10:05:43', '2014-11-27 10:05:45');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
