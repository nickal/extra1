    项目开发环境搭建
目录
1.	Java环境配置
2.	Maven环境配置
3.	Tomcat环境设置
4.	Eclipse设置
5.	安装Mysql
6.	安装Navicat for Mysql
 
1.	Java环境配置
a)	下载JavaEE
http://www.oracle.com/technetwork/java/javaee/downloads/index.html 
b)	环境变量设置（根据自己安装实际路径设置）
JAVA_HOME=D:\glassfish4\jdk7
JRE_HOME=D:\glassfish4\jdk7\jre
Path +=D:\glassfish4\jdk7\bin
classpath=D:\glassfish4\jdk7\lib\dt.jar;D:\glassfish4\jdk7\lib\tools.jar
c)	测试
DOS命令行中输入java和javac，出现帮助和提示信息，说明安装成功
2.	Maven环境配置
a)	下载Maven
http://maven.apache.org/download.cgi
b)	环境变量配置
       MAVEN_HOME= D:\Softwares\apache-maven-3.2.2
       PATH+=;%MAVEN_HOME%\bin;

c)	测试
       DOS命令行中输入mvn –v，出现帮助和提示信息说明安装成功
3.	Tomcat环境配置
a)	Tomcat下载（建议下载Tomcat7）
http://tomcat.apache.org/download-70.cgi
b)	环境变量配置
      CATALINA_BASE=C:\tomcat
      CATALINA_HOME=C:\tomcat
PATH+=%CATALINA_HOME%\lib;%CATALINA_HOME%\bin
c)	测试
i.	启动Tomcat：在CMD命令下输入命令：startup，出现如下对话框，表明服务启动成功；或者，右键点击桌面上的“我的电脑”->“管理”->“服务和应用程序”->“服务”，找到“Apache     Tomcat”服务，右键点击该服务，选择“属性”，将“启动类型”由“手动”改成“自动”；
ii.	打开浏览器，在地址栏中输入http://localhost:8080回车，如果看到Tomcat自带的一个JSP页面，说明你的Tomcat已搭建成功。
4.	Eclipse设置
a)	Eclipse IDE for Java EE Developers（建议下载最新版4.4.1）
       http://www.eclipse.org/downloads/ 
b)	在Eclipse中配置maven，window->preference->maven->user settings->browser
D:\maven\apache-maven-3.0.5\apache-maven-3.0.5\conf\settings.xml
c)	在Eclipse中配置Tomcat，Windw- > Preferences -> Server -> Runtime Environments，添加Tomcat环境
5.	Mysql安装
下载地址：http://www.mysql.com/downloads/  (建议5.6版本)
正常安装，可参见教程：http://jingyan.baidu.com/article/4b07be3c67853c48b380f311.html
6.	Navicat安装
地址：http://www.navicat.com.cn/download
下载Navicat for MySQL,正常安装。

