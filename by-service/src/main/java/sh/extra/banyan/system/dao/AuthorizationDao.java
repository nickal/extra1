package sh.extra.banyan.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import sh.extra.banyan.common.dao.AbstractDao;
import sh.extra.banyan.system.domain.model.Authorization;

import com.google.common.collect.Maps;

@Repository
public class AuthorizationDao extends AbstractDao{

    public void add(Authorization vo){
        writeSqlSession.insert(sql(),vo);
    }
    
    public List<Authorization> search(int start,int size){
        Map<String,Object> params = Maps.newHashMap();
        params.put("start", start);
        params.put("size", size);
        return list(sql(),params);
    }

    public int searchTotalSize() {
        return size(sql(),null);
    }

    public List<Authorization> loadAll() {
        return list(sql(),null);
    }

    public void update(int id, String path) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("id", id);
        params.put("path", path);
        writeSqlSession.update(sql(),params);
    }
    
    public void deleteById(int id){
    	
        writeSqlSession.delete(sql(), id);
    }
}
