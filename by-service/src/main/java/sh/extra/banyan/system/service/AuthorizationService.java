package sh.extra.banyan.system.service;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import sh.extra.banyan.common.exception.BYServiceException;
import sh.extra.banyan.system.dao.AuthorizationDao;
import sh.extra.banyan.system.domain.model.Authorization;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

@Service
public class AuthorizationService {

    private List<Authorization> authorizations = Lists.newArrayList();
    private Map<String,Authorization> authorizationMaps = Maps.newHashMap();
    
    @Inject
    private AuthorizationDao authorizationDao;
    
    @PostConstruct
    public void init(){
        List<Authorization> authorizations = authorizationDao.loadAll();
        this.authorizations.clear();//q清除list中所有元素
        this.authorizations.addAll(authorizations);//加入现有的list 权限列表
        authorizationMaps.clear();
        for(Authorization auth : this.authorizations){
            String path = auth.getPath();
            String[] pathes = path.split(";");
            for(String p : pathes){
                authorizationMaps.put(p, auth);
            }
        }
    }
    
    /**
     * 检查是否有相应权限
     * 
     * 权限在初始化时加载到内存
     * @param path 请求路径
     * @param authId 
     */
    public void checkPathAuthorization(String path,List<Integer> authIds){
        Authorization auth = authorizationMaps.get(path);
        if(auth == null || CollectionUtils.isEmpty(authIds) 
                || !authIds.contains(auth.getId()) ){   //Admin表中的anthorizations与 auth表中的id相对应
            throw new BYServiceException("您没有相应权限!");
        }
    }

    public List<Authorization> loadAdd() {
        return authorizations;
    }
    
    public List<Authorization> search(int start,int size){
        return authorizationDao.search(start, size);
    }
    
    public int searchTotalSize(){
        return authorizationDao.searchTotalSize();
    }
    
    public void saveOrUpdate(Authorization auth){
        if(auth.getId() > 0){
            authorizationDao.update(auth.getId(),auth.getPath());
        }else{
            authorizationDao.add(auth);
        }
        init();
    }
    
    public void delete(int id){
        
          authorizationDao.deleteById(id);
          init();
        }
    
    } 

    
    

