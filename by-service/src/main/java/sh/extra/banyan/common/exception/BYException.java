package sh.extra.banyan.common.exception;

public class BYException extends RuntimeException{
    
    private static final long serialVersionUID = -8958002631092994278L;

    private int code;
    
    public BYException(int code,String msg,Throwable e){
        super(msg,e);
        this.code = code;
    }
    
    public BYException(int code,String msg){
        super(msg);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

}
