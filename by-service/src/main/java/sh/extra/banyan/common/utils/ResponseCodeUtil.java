package sh.extra.banyan.common.utils;

import java.util.ResourceBundle;

public class ResponseCodeUtil {

	public final static ResourceBundle rb = ResourceBundle
			.getBundle("codes");

	public static String getMsgByCode(int code) {
		return rb.getString(""+code);
	}

}
