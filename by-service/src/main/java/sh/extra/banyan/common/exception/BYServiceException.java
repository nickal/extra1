package sh.extra.banyan.common.exception;

import sh.extra.banyan.common.Constants;

public class BYServiceException extends BYException{
    private static final long serialVersionUID = 7130862567570137310L;

    public static final int CODE = Constants.RESPONSE_ERROR_SERVICE;
    
    public BYServiceException(String msg,Throwable e){
        super(CODE,msg,e);
    }
    
    public BYServiceException(int code,String msg,Throwable e){
        super(code,msg,e);
    }
    
    public BYServiceException(String msg){
        super(CODE,msg);
    }
    
    public BYServiceException(int code ,String msg){
        super(code,msg);
    }

}
