package sh.extra.banyan.common.utils;

import sh.extra.banyan.admin.domain.type.AdminType;
import sh.extra.banyan.item.domain.type.BatchType;
import sh.extra.banyan.item.domain.type.BrandType;
import sh.extra.banyan.item.domain.type.CataType;
import sh.extra.banyan.item.domain.type.ColorType;
import sh.extra.banyan.item.domain.type.DesignerType;
import sh.extra.banyan.item.domain.type.FloralType;
import sh.extra.banyan.item.domain.type.MaterialType;
import sh.extra.banyan.item.domain.type.ModelType;
import sh.extra.banyan.item.domain.type.MonthType;
import sh.extra.banyan.item.domain.type.SexType;
import sh.extra.banyan.item.domain.type.SizeType;
import sh.extra.banyan.member.domain.type.UserType;

public class Utils {

    public static UserType[] getUserTypes(){
        return UserType.values();
    }
    
    public static BatchType[] getBatchType(){
        return BatchType.values();
    }
    
    public static BrandType[] getBrandType(){
        return BrandType.values();
    }
    
    public static CataType[] getCataType(){
        return CataType.values();
    }
    
    public static ColorType[] getColorType(){
        return ColorType.values();
    }
    
    public static DesignerType[] getDesignerType(){
        return DesignerType.values();
    }
    
    public static FloralType[] getFloralType(){
        return FloralType.values();
    }
    
    public static MaterialType[] getMaterialType(){
        return MaterialType.values();
    }
    
    public static ModelType[] getModelType(){
        return ModelType.values();
    }
    
    public static MonthType[] getMonthType(){
        return MonthType.values();
    }
    
    public static SexType[] getSexType(){
        return SexType.values();
    }
    
    public static SizeType[] getSizeType(){
        return SizeType.values();
    }
    
    public static AdminType[] getAdminType(){
        return AdminType.values();
    }
}
