package sh.extra.banyan.common.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Task {

	/**
	 * 是否锁住任务执行。如果锁住，表示任务未执行完不执行新的任务。
	 * @return
	 */
	boolean lock() default true;
	
}
