package sh.extra.banyan.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 来自网页的Ajax请求标示
 * 
 * @author haibing.wang
 * @version $Id: WebAjax.java, v 0.1 2014年11月3日 下午10:48:50 haibing.wang Exp $
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebAjax {
	
    /**
     * 是否需要登录才能访问
     * @return
     */
	boolean token() default false;//token
}
