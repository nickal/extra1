package sh.extra.banyan.common.utils;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;


public class RequestUtils {
	public static final String USER_SESSION_ID	= "_user_id";
	
	public static int checkLogin(HttpServletRequest request){
		return checkLogin(request,USER_SESSION_ID);
	}
	
	public static void allowUser(HttpServletRequest request,int uid){
		allowUser(request,USER_SESSION_ID, uid);
	}
	
	public static int checkLogin(HttpServletRequest request,String param){
		Integer uid = (Integer)request.getSession().getAttribute(param);
		return uid == null ? 0 : uid;
	}
	
	public static void allowUser(HttpServletRequest request,String param,int uid){
		request.getSession().setAttribute(param, uid);
	}
	
	public static boolean isLogin(HttpServletRequest request){
		return isLogin(request,USER_SESSION_ID);
	}
	
	public static boolean isLogin(HttpServletRequest request,String param){
		return checkLogin(request,param) > 0;
	}
	
	public static String decode(String name){
		if(StringUtils.isNotBlank(name)){
			try {
				return new String(name.getBytes("ISO-8859-1"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	public static void logout(HttpServletRequest request) {
		logout(request,USER_SESSION_ID);	
	}
	
	public static void logout(HttpServletRequest request,String param) {
		request.getSession().removeAttribute(USER_SESSION_ID);	
	}
}
