package sh.extra.banyan.common.exception;

import sh.extra.banyan.common.Constants;

public class BYTokenException extends BYException{
    private static final long serialVersionUID = 7130862567570137310L;

    public static final int CODE = Constants.RESPONSE_ERROR_TOKEN;
    
    public BYTokenException(String msg,Throwable e){
        super(CODE,msg,e);
    }
    
    public BYTokenException(int code,String msg,Throwable e){
        super(code,msg,e);
    }
    
    public BYTokenException(String msg){
        super(CODE,msg);
    }

}
