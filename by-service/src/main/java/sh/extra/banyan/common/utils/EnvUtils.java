package sh.extra.banyan.common.utils;

/**
 * 当前环境监测工具类
 * 
 * @author haibing.wang
 * @version $Id: EnvUtils.java, v 0.1 2014年11月7日 上午9:47:41 haibing.wang Exp $
 */
public class EnvUtils {

    private static final String ENV_KEY = "env";
    
    /**
     * 开发环境或者 不是online & 不是 test
     * 
     * @return
     */
    public static boolean isDev(){
        return ("dev".equals(PropertyUtil.getString(ENV_KEY))) || (!isOnline()  && !isTest());
    }
    
    public static boolean isOnline(){
        return "online".equals(PropertyUtil.getString(ENV_KEY));
    }
    
    public static boolean isTest(){
        return "test".equals(PropertyUtil.getString(ENV_KEY));
    }
}
