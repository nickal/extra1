package sh.extra.banyan.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * 客户端接口标示
 * @author haibing.wang
 * @version $Id: Api.java, v 0.1 2014年11月3日 下午10:47:59 haibing.wang Exp $
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Api {
	
    /**
     * 是否需要登录才能访问
     * @return
     */
	boolean token() default false;//token
}
