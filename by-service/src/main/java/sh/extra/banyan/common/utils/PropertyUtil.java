package sh.extra.banyan.common.utils;

import java.util.ResourceBundle;

public class PropertyUtil {

	public final static ResourceBundle rb = ResourceBundle
			.getBundle("constants");

	public static String getConstantsValue(String key) {
		return rb.getString(key);
	}
	
	public static boolean getBoolean(String key){
		return "true".equals(rb.getString(key));
	}
	
	public static Integer getIntegerValue(String key) {
		return new Integer(getConstantsValue(key));
	}

	public static String getString(String key) {
		return rb.getString(key);
	}

}
