package sh.extra.banyan.common;

public class Constants {

    /**
     * ====API接口参数====
     */
    public static final int RESPONSE_SUCCESS = 0;                   // 成功
    public static final int RESPONSE_ERROR_ARGUMENT = 1000;         // 参数异常
    public static final int RESPONSE_ERROR_SERVICE = 2000;          // 服务异常
    public static final int RESPONSE_ERROR_TOKEN = 3000;            // token无效
    public static final int RESPONSE_ERROR_APP = 4000;              // 未知异常
    
    public static class ERROR_SERVICE{
        public static final int USER_NOT_EXIST_ERROR = 2001;
    }
    
    /**
     * 权限常量
     * @author haibing.wang
     * @version $Id: Constants.java, v 0.1 2014年11月22日 上午9:49:35 haibing.wang Exp $
     */
    public static class Authorization{
        public static final int 商品搜索 = 1;
        public static final int 商品详情 = 2;
        public static final int 创建商品 = 3;
        public static final int 商品分类 = 4;
    }
}
