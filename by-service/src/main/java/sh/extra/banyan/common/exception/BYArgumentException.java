package sh.extra.banyan.common.exception;

import sh.extra.banyan.common.Constants;

public class BYArgumentException extends BYException{
    private static final long serialVersionUID = 7130862567570137310L;

    public static final int CODE = Constants.RESPONSE_ERROR_ARGUMENT;
    
    public BYArgumentException(String msg,Throwable e){
        super(CODE,msg,e);
    }
    
    public BYArgumentException(int code,String msg,Throwable e){
        super(code,msg,e);
    }
    
    public BYArgumentException(String msg){
        super(CODE,msg);
    }
    
    public BYArgumentException(int code ,String msg){
        super(code,msg);
    }

}
