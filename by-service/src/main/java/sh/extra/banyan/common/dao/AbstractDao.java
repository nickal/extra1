package sh.extra.banyan.common.dao;

import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;

public class AbstractDao {
    @Resource
    protected SqlSessionTemplate writeSqlSession;

//  @Resource
//  protected SqlSessionTemplate readSqlSession;

    protected String sql() {
        String sqlId = new RuntimeException().getStackTrace()[1].getMethodName();
        return getClass().getCanonicalName() + '.' + sqlId;
    }
    
    protected <T> List<T> list(String sqlId,Object params){
        List<T> result = null ;
        if(params == null )
            result = writeSqlSession.selectList(sqlId);
        else 
            result = writeSqlSession.selectList(sqlId,params);
        
        if(result == null){
            Collections.emptyMap();
        }
        return result;
    }
    
    protected int size(String sqlId,Object params){
        Integer result = null;
        if(params ==null){
            result = writeSqlSession.selectOne(sqlId);
        }else{
            result = writeSqlSession.selectOne(sqlId,params);
        }
        return result == null ? 0: result;
    }
}
