package sh.extra.banyan.common.utils;

import java.io.CharArrayWriter;
import java.io.PrintWriter;

public class ExceptionUtils {

    public static String ex2Str(Throwable ex) {
        CharArrayWriter caw = new CharArrayWriter();
        ex.printStackTrace(new PrintWriter(caw, true));
        return caw.toString();
    }
}
