package sh.extra.banyan.common.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.springframework.stereotype.Service;

import sh.extra.banyan.common.exception.BYServiceException;

import com.alibaba.fastjson.JSON;
import com.qiniu.api.auth.AuthException;
import com.qiniu.api.auth.digest.Mac;
import com.qiniu.api.config.Config;
import com.qiniu.api.io.IoApi;
import com.qiniu.api.io.PutExtra;
import com.qiniu.api.io.PutRet;
import com.qiniu.api.rs.PutPolicy;

@Service
public class ImageService {
	
	private static final String BUKET = "extra";
	private static final String IMAGE_DOMAIN = "http://7sbpt7.com1.z0.glb.clouddn.com/";
	private static final String AK = "2KiSM6dYbgAOjnDsWO_tFa7-A11zJ-jLZR1WUAkb";
	private static final String SK = "WZIEFQBC3NkYLo0r8OtYK5rmakg6_AyAQ2JtSPpk";
	
	private static final Logger logger = Logger.getLogger(ImageService.class);

	
	public String upload(String localFile){
		try {
			return upload(new FileInputStream(localFile));
		} catch (FileNotFoundException e) {
			logger.error(e);
			throw new BYServiceException("本地图片不存在");
		}
	}
	
	public String upload(InputStream inputStream){

		Config.ACCESS_KEY = AK;
        Config.SECRET_KEY = SK;
        Mac mac = new Mac(Config.ACCESS_KEY, Config.SECRET_KEY);
        // 请确保该bucket已经存在
        PutPolicy putPolicy = new PutPolicy(BUKET);
        String uptoken;
		try {
			uptoken = putPolicy.token(mac);
		} catch (AuthException | JSONException e) {
			logger.error(e);
			throw new BYServiceException("图片上传服务器无法访问");
		}
        PutExtra extra = new PutExtra();
        String key = System.currentTimeMillis()+"";
        PutRet ret = IoApi.Put(uptoken, key, inputStream, extra);
        if(ret.statusCode != 200){
        	logger.error(JSON.toJSONString(ret));
        	throw new BYServiceException("图片上传服务器无法访问");
        }
        return IMAGE_DOMAIN+key;
	}
}
