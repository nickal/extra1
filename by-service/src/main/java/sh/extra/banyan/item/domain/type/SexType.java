package sh.extra.banyan.item.domain.type;

public enum SexType {

    成男(1),
    成女(0);
    
    private int value;
    
    private SexType(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static SexType fromValue(int value){
        SexType[] types = values();
        for(SexType type : types){
            if(type.getValue() == value){
                return type;
            }
        }
        return null;
    }
}
