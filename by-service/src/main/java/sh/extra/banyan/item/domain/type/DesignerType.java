package sh.extra.banyan.item.domain.type;

public enum DesignerType {

    YAN("AY"),
    LU("JL");
    
    private String value;
    
    private DesignerType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static DesignerType fromValue(String value){
        DesignerType[] types = values();
        for(DesignerType type : types){
            if(type.getValue().equals(value)){
                return type;
            }
        }
        return null;
    }
}
