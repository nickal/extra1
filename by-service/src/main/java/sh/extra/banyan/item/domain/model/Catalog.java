package sh.extra.banyan.item.domain.model;

import java.util.Date;
import java.util.List;

/**
 * 商品分类
 * 
 * @author haibing.wang
 * @version $Id: Catlog.java, v 0.1 2014年11月26日 下午9:06:08 haibing.wang Exp $
 */
public class Catalog {

    private int id;
    private String name;
    private int parentId;
    private int count;
    
    private Date createTime;
    private Date updateTime;
    
    private List<Catalog> catlogs;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getParentId() {
        return parentId;
    }
    public void setParentId(int parentId) {
        this.parentId = parentId;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public List<Catalog> getCatlogs() {
        return catlogs;
    }
    public void setCatlogs(List<Catalog> catlogs) {
        this.catlogs = catlogs;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    
}
