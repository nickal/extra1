package sh.extra.banyan.item.domain.vo;

import org.apache.commons.lang.StringUtils;

import sh.extra.banyan.common.exception.BYArgumentException;
import sh.extra.banyan.item.domain.type.BatchType;
import sh.extra.banyan.item.domain.type.BrandType;
import sh.extra.banyan.item.domain.type.CataType;
import sh.extra.banyan.item.domain.type.ColorType;
import sh.extra.banyan.item.domain.type.DesignerType;
import sh.extra.banyan.item.domain.type.MaterialType;
import sh.extra.banyan.item.domain.type.ModelType;
import sh.extra.banyan.item.domain.type.SexType;

/**
 * 商品货号规则
 * 
 * @author haibing.wang
 * @version $Id: ItemNo.java, v 0.1 2014年12月7日 下午11:37:18 haibing.wang Exp $
 */
public class ItemNo {

    private int styleNo;                //款号
    private String brand;               //品牌(1位)
    private String cata;                //种类(1位)
    private int model;                  //款型（1位）
    private String designer;            //设计师(2位)
    private String month;               //月份序号(2位)
    private int sex;
    private String no;                  //货号(2位) -> 花色
    private String material;            //材质(1位)
    private int batch;                  //批次
    private String color;               //颜色(3位)
    
    public int getStyleNo() {
        return styleNo;
    }
    public void setStyleNo(int styleNo) {
        this.styleNo = styleNo;
    }
    
    public BrandType getEBrand(){
        return BrandType.fromValue(brand);
    }
    
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    public CataType getECata(){
        return CataType.fromValue(cata);
    }
    public String getCata() {
        return cata;
    }
    public void setCata(String cata) {
        this.cata = cata;
    }
    
    public ModelType getEModel(){
        return ModelType.fromValue(model);
    }
    public int getModel() {
        return model;
    }
    public void setModel(int model) {
        this.model = model;
    }
    
    public DesignerType getEDesigner(){
        return DesignerType.fromValue(designer);
    }
    public String getDesigner() {
        return designer;
    }
    public void setDesigner(String designer) {
        this.designer = designer;
    }
    
    public String getMonth() {
        return month;
    }
    public void setMonth(String month) {
        this.month = month;
    }
    
    public SexType getESex(){
        return SexType.fromValue(sex);
    }
    public int getSex() {
        return sex;
    }
    public void setSex(int sex) {
        this.sex = sex;
    }
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }
    
    public MaterialType getEMaterial() {
        return MaterialType.fromValue(material);
    }
    
    public String getMaterial() {
        return material;
    }
    public void setMaterial(String material) {
        this.material = material;
    }
    
    public BatchType getEBatch() {
        return BatchType.fromValue(batch);
    }
    public int getBatch() {
        return batch;
    }
    public void setBatch(int batch) {
        this.batch = batch;
    }
    
    public ColorType getEColor() {
        return null;//ColorType.fromValue(color);
    }
    
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    
    public String toString(){
        return brand + cata + model + designer + month + sex + no + material + batch + color;
    }
    
    public static ItemNo parse(String no){
        if(StringUtils.isBlank(no) || no.length() != 15){
            throw new BYArgumentException("货号必须是15位!");
        }
        
        ItemNo item = new ItemNo();
        item.setBrand(no.substring(0,1));
        item.setCata(no.substring(1,2));
        item.setModel(Integer.valueOf(no.substring(2,3)));
        item.setDesigner(no.substring(3,5));
        item.setMonth(no.substring(5,7));
        item.setSex(Integer.valueOf(no.substring(7,8)));
        item.setNo(no.substring(8,10));
        item.setMaterial(no.substring(10, 11));
        item.setBatch(Integer.valueOf(no.substring(11, 12)));
        item.setColor(no.substring(12, 15));
        
        return item;
    }
    
    public static void main(String[] args) {
        ItemNo no = ItemNo.parse("XJ2AYA2101E1919");
        System.out.println(no.toString());
    }
}
