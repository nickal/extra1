package sh.extra.banyan.item.domain.type;

/**
 * 花色
 * @author haibing.wang
 * @version $Id: FloralType.java, v 0.1 2014年12月30日 上午12:00:11 haibing.wang Exp $
 */
public enum FloralType {

    印花("00"),
    纯色("01"),
    刺绣("02");
    
    private String value;
    
    private FloralType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static FloralType fromValue(String value){
        FloralType[] types = values();
        for(FloralType type : types){
            if(type.getValue().equals(value)){
                return type;
            }
        }
        return null;
    }
}
