package sh.extra.banyan.item.domain.model;

import java.util.Date;

import sh.extra.banyan.item.domain.type.BatchType;
import sh.extra.banyan.item.domain.type.BrandType;
import sh.extra.banyan.item.domain.type.CataType;
import sh.extra.banyan.item.domain.type.ColorType;
import sh.extra.banyan.item.domain.type.DesignerType;
import sh.extra.banyan.item.domain.type.FloralType;
import sh.extra.banyan.item.domain.type.MaterialType;
import sh.extra.banyan.item.domain.type.ModelType;
import sh.extra.banyan.item.domain.type.SexType;
import sh.extra.banyan.item.domain.type.SizeType;

/**
 * 商品
 * 
 * @author haibing.wang
 * @version $Id: Item.java, v 0.1 2014年11月8日 下午6:49:31 haibing.wang Exp $
 */
public class Item {

    private int id;
    private String no;                  // 货号
    private String styleNo;             // 款号
    private String shopCode;            // 店内码
    
    private String name;                // 名称
    private String description;         // 描述
    private String pics;                // 图片
    
    private String brand;               // 品牌
    private String cata;                // 种类
    private int model;                  // 款型（种类）
    private String designer;            // 设计师
    private String month;               // 月份序号
    private int sex;                    // 性别
    private String floral;              // 花色
    private String material;            // 材质
    private int batch;                  // 批次
    private int color;                  // 颜色
    
    private int size;                   // 尺码
    private int count;                  // 库存
    private int price;                  // 价格
    
    private Date createTime;
    private Date updateTime;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getPics() {
        return pics;
    }
    public void setPics(String pics) {
        this.pics = pics;
    }
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public int getColor() {
        return color;
    }
    public void setColor(int color) {
        this.color = color;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public String getStyleNo() {
        return styleNo;
    }
    public void setStyleNo(String styleNo) {
        this.styleNo = styleNo;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getCata() {
        return cata;
    }
    public void setCata(String cata) {
        this.cata = cata;
    }
    public int getModel() {
        return model;
    }
    public void setModel(int model) {
        this.model = model;
    }
    public String getDesigner() {
        return designer;
    }
    public void setDesigner(String designer) {
        this.designer = designer;
    }
    public String getMonth() {
        return month;
    }
    public void setMonth(String month) {
        this.month = month;
    }
    public int getSex() {
        return sex;
    }
    public void setSex(int sex) {
        this.sex = sex;
    }
    public String getFloral() {
        return floral;
    }
    public void setFloral(String floral) {
        this.floral = floral;
    }
    public String getMaterial() {
        return material;
    }
    public void setMaterial(String material) {
        this.material = material;
    }
    public int getBatch() {
        return batch;
    }
    public void setBatch(int batch) {
        this.batch = batch;
    }
    public String getShopCode() {
        return shopCode;
    }
    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }
    
    public BrandType getEBrand(){
        return BrandType.fromValue(brand);
    }
    
    public CataType getECata(){
        return CataType.fromValue(cata);
    }
    
    public ModelType getEModel(){
        return ModelType.fromValue(model);
    }
    
    public DesignerType getEDesigner(){
        return DesignerType.fromValue(designer);
    }
    
    public SexType getESex(){
        return SexType.fromValue(sex);
    }
    
    public MaterialType getEMaterial() {
        return MaterialType.fromValue(material);
    }
    
    public BatchType getEBatch() {
        return BatchType.fromValue(batch);
    }
    
    public ColorType getEColor() {
        return ColorType.fromValue(color);
    }
    
    public SizeType getESize(){
        return SizeType.fromValue(size);
    }
    public FloralType getEFloral(){
        return FloralType.fromValue(floral);
    }
}
