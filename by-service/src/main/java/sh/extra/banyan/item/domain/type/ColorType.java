package sh.extra.banyan.item.domain.type;

public enum ColorType {

    无(-1),
    红(0),
    黑(1),
    灰(2);
    
    private int value;
    
    private ColorType(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static ColorType fromValue(int value){
        ColorType[] types = values();
        for(ColorType type : types){
            if(type.getValue() == value){
                return type;
            }
        }
        return null;
    }
}
