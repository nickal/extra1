package sh.extra.banyan.item.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import sh.extra.banyan.common.dao.AbstractDao;
import sh.extra.banyan.item.domain.model.Item;

import com.google.common.collect.Maps;

@Repository
public class ItemDao extends AbstractDao{

    public List<Item> search(String keyword,int type,int start,int size){
        Map<String,Object> params = Maps.newHashMap();
        params.put("keyword", keyword);
        params.put("type", type);
        params.put("start", start);
        params.put("size", size);
        return list(sql(),params);
    }

    public int searchTotalSize(String keyword, int type) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("keyword", keyword);
        params.put("type", type);
        Integer result = writeSqlSession.selectOne(sql(),params);
        return result == null ? 0 : result;
    }

    public void add(Item item) {
        writeSqlSession.insert(sql(),item);
    }
}
