package sh.extra.banyan.item.service;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import sh.extra.banyan.item.dao.CatalogDao;
import sh.extra.banyan.item.domain.model.Catalog;

/**
 * 商品分类目录服务
 * 
 * @author haibing.wang
 * @version $Id: CatlogService.java, v 0.1 2014年11月26日 下午10:14:39 haibing.wang Exp $
 */
@Service
public class CatalogService {

    @Inject
    private CatalogDao catlogDao;
    
    public List<Catalog> loadAll(){
        List<Catalog> catas = loadCatlogs();
        List<Catalog> result = Lists.newArrayList();
        for(Catalog log:catas){
            addChild(log, result);
        }
        return result;
    }
    
    private void addChild(Catalog child,List<Catalog> list){
        list.add(child);
        if(CollectionUtils.isEmpty(child.getCatlogs())){
            return;
        }
        for(Catalog catalog : child.getCatlogs()){
            addChild(catalog,list);
        }
    }
    
    public List<Catalog> loadCatlogs(){
        List<Catalog> cats = catlogDao.loadAll();
        Map<Integer,List<Catalog>> maps = Maps.newHashMap();
        
        for(Catalog cat : cats){
            
            List<Catalog> list = maps.get(cat.getParentId());
            if(list == null){
                list = Lists.newArrayList();
                maps.put(cat.getParentId(), list);
            }
            list.add(cat);
        }
        
        List<Catalog> result = maps.get(0);
        
        if(result == null){
            result = Lists.newArrayList();
        }
        buildCatlogs(result,maps);
        return result;
    }
    
    private void buildCatlogs(List<Catalog> result,Map<Integer,List<Catalog>> maps){
        if(result == null){
            return;
        }
        for(Catalog cat : result){
            List<Catalog> list = maps.get(cat.getId());
            cat.setCatlogs(list);
            buildCatlogs(list, maps);
        }
    }

    public void add(int pid, String name) {
        catlogDao.add(pid,name);
    }
    
    public void saveOrUpdate(Catalog cata){
        
    }
}
