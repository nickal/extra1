package sh.extra.banyan.item.domain.type;

public enum MaterialType {

    针织("A"),
    丝("B"),
    麻("C"),
    羊毛("D"),
    尼龙("E"),
    真皮("F"),
    仿皮("G");
    
    private String value;
    
    private MaterialType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static MaterialType fromValue(String value){
        MaterialType[] types = values();
        for(MaterialType type : types){
            if(type.getValue().equals(value)){
                return type;
            }
        }
        return null;
    }
}
