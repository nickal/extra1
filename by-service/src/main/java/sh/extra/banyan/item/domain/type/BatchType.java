package sh.extra.banyan.item.domain.type;

public enum BatchType {
    无(-1),
    批样(0),
    首批(1);
    
    private int value;
    
    private BatchType(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static BatchType fromValue(int value){
        BatchType[] types = values();
        for(BatchType type : types){
            if(type.getValue() == value){
                return type;
            }
        }
        return null;
    }
}
