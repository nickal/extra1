package sh.extra.banyan.item.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;

import sh.extra.banyan.common.dao.AbstractDao;
import sh.extra.banyan.item.domain.model.Catalog;

@Repository
public class CatalogDao extends AbstractDao{

    public List<Catalog> loadAll(){
        return list(sql(),null);   
    }

    public void add(int pid, String name) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("pid", pid);
        params.put("name", name);
        writeSqlSession.insert(sql(),params);
    }
}
