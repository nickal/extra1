package sh.extra.banyan.item.domain.type;

public enum CataType {

    Tee("T"),
    Jack("J");
    
    private String value;
    
    private CataType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static CataType fromValue(String value){
        CataType[] types = values();
        for(CataType type : types){
            if(type.getValue().equals(value)){
                return type;
            }
        }
        return null;
    }
}
