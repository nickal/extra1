package sh.extra.banyan.item.domain.type;

/**
 * 保留   0
    XXS 1
    XS  2
    S   3
    M   4
    L   5
    XL  6
    XXL 7
    XXXL    8
    保留  9
 * 
 * @author haibing.wang
 * @version $Id: SizeType.java, v 0.1 2015年1月2日 下午12:50:42 haibing.wang Exp $
 */
public enum SizeType {

    XXS(1),
    XS(2),
    S(3),
    M(4),
    L(5),
    XL(6),
    XXL(7),
    XXXL(8);
    
    private int value;
    
    private SizeType(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static SizeType fromValue(int value){
        SizeType[] types = values();
        for(SizeType type : types){
            if(type.getValue() == value){
                return type;
            }
        }
        return null;
    }
}
