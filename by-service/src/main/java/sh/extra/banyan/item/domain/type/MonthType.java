package sh.extra.banyan.item.domain.type;

public enum MonthType {

    eXtra("X");
    
    private String value;
    
    private MonthType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static MonthType fromValue(String value){
        MonthType[] types = values();
        for(MonthType type : types){
            if(type.getValue().equals(value)){
                return type;
            }
        }
        return null;
    }
}
