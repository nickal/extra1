package sh.extra.banyan.item.domain.vo;

/**
 * 店内码
 * 
 * @author haibing.wang
 * @version $Id: ShopCode.java, v 0.1 2014年12月7日 下午11:39:48 haibing.wang Exp $
 */
public class ShopCode {

    private String prefix;          //前缀 3位
    private String retain;          //保留 2位
    private int sex;                //性别 1位
    private int color;              //颜色 1位
    private int size;               //尺码 1位
    private String styleNo;         //款号 3位
    
    public String getPrefix() {
        return prefix;
    }
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    public String getRetain() {
        return retain;
    }
    public void setRetain(String retain) {
        this.retain = retain;
    }
    public int getSex() {
        return sex;
    }
    public void setSex(int sex) {
        this.sex = sex;
    }
    public int getColor() {
        return color;
    }
    public void setColor(int color) {
        this.color = color;
    }
    public int getSize() {
        return size;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public String getStyleNo() {
        return styleNo;
    }
    public void setStyleNo(String styleNo) {
        this.styleNo = styleNo;
    }
    
}
