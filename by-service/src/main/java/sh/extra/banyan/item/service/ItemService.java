package sh.extra.banyan.item.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import sh.extra.banyan.item.dao.ItemDao;
import sh.extra.banyan.item.domain.model.Item;

@Service
public class ItemService {

    @Inject
    private ItemDao itemDao;
    
    public int searchTotalSize(String keyword, int type) {
        return itemDao.searchTotalSize(keyword, type);
    }

    public List<Item> search(String keyword, int type, int start, int size) {
        return itemDao.search(keyword, type, start, size);
    }

    public void create(Item vo) {
        // TODO: 校验
        
        Item item = new Item();
        item.setBatch(vo.getBatch());
        item.setBrand(vo.getBrand());
        item.setCata(vo.getCata());
        item.setColor(vo.getColor());
        item.setCount(vo.getCount());
        item.setDescription(vo.getDescription());
        item.setDesigner(vo.getDesigner());
        item.setFloral(vo.getFloral());
        item.setMaterial(vo.getMaterial());
        item.setModel(vo.getModel());
        item.setMonth(vo.getMonth());
        item.setName(vo.getName());
        item.setNo(vo.getNo());
        item.setPics(vo.getPics());
        item.setPrice(vo.getPrice());
        item.setSex(vo.getSex());
        item.setShopCode(vo.getShopCode());
        item.setSize(vo.getSize());
        item.setStyleNo(vo.getStyleNo());
        itemDao.add(item);
    }

}
