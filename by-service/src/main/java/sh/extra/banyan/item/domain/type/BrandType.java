package sh.extra.banyan.item.domain.type;

public enum BrandType {

    eXtra("X");
    
    private String value;
    
    private BrandType(String value){
        this.value = value;
    }
    
    public String getValue(){
        return value;
    }
    
    public static BrandType fromValue(String value){
        BrandType[] types = values();
        for(BrandType type : types){
            if(type.getValue().equals(value)){
                return type;
            }
        }
        return null;
    }
}
