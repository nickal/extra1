package sh.extra.banyan.item.domain.type;

public enum ModelType {

    无(-1),
    短袖(0),
    七分袖(1),
    长袖(2),
    无袖(3);
    
    private int value;
    
    private ModelType(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static ModelType fromValue(int value){
        ModelType[] types = values();
        for(ModelType type : types){
            if(type.getValue() == value){
                return type;
            }
        }
        return null;
    }
}
