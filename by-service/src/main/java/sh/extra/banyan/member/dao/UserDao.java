package sh.extra.banyan.member.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;

import sh.extra.banyan.common.dao.AbstractDao;
import sh.extra.banyan.member.domain.model.User;
import sh.extra.banyan.member.domain.type.UserType;

@Repository
public class UserDao extends AbstractDao{

    public List<User> search(String keyword,UserType type,int start,int size){
        Map<String,Object> params = Maps.newHashMap();
        params.put("keyword", keyword);
        params.put("type", type.getType());
        params.put("start", start);
        params.put("size", size);
        return list(sql(),params);
    }

    public int searchTotalSize(String keyword, UserType type) {
        Map<String,Object> params = Maps.newHashMap();
        params.put("keyword", keyword);
        params.put("type", type.getType());
        Integer result = writeSqlSession.selectOne(sql(),params);
        return result == null ? 0 : result;
    }
}
