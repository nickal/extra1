package sh.extra.banyan.member.domain.type;

/**
 * 用户的类型
 * 
 * @author haibing.wang
 * @version $Id: UserType.java, v 0.1 2014年12月7日 下午11:11:55 haibing.wang Exp $
 */
public enum UserType {

    全部(0),
    官方(1),
    微信(2),
    京东(3);
    
    private int type;
    
    private UserType(int type){
        this.type = type;
    }
    
    public int getType(){
        return type;
    }

    public static UserType fromType(int type) {
        UserType[] types = values();
        for(UserType t : types){
            if(t.getType() == type){
                return t;
            }
        }
        return null;
    }
    
}
