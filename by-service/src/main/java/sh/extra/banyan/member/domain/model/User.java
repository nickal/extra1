package sh.extra.banyan.member.domain.model;

import java.util.Date;

import sh.extra.banyan.member.domain.type.UserType;

/**
 * 会员
 * 
 * @author haibing.wang
 * @version $Id: User.java, v 0.1 2014年12月7日 下午11:10:11 haibing.wang Exp $
 */
public class User {

    private int id;
    
    private String name;
    private String login;
    private String password;
    
    private int type;
    
    private int credit;             // 积分
    
    private Date updateTime;
    private Date createTime;
    
    private String weixinId;
    
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public String getWeixinId() {
        return weixinId;
    }
    public void setWeixinId(String weixinId) {
        this.weixinId = weixinId;
    }
    
    public UserType getEType(){
        return UserType.fromType(type);
    }
    public int getCredit() {
        return credit;
    }
    public void setCredit(int credit) {
        this.credit = credit;
    }
    
}
