package sh.extra.banyan.member.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import sh.extra.banyan.member.dao.UserDao;
import sh.extra.banyan.member.domain.model.User;
import sh.extra.banyan.member.domain.type.UserType;
import sh.extra.banyan.member.domain.vo.UserVo;

/**
 * 用户服务
 * 
 * @author haibing.wang
 * @version $Id: UserService.java, v 0.1 2014年12月7日 下午11:13:23 haibing.wang Exp $
 */
@Service
public class UserService {

    @Inject
    private UserDao userDao;
    
    public List<UserVo> search(String keyword,UserType type,int start,int size){
        List<User> users = userDao.search(keyword,type,start, size);
        List<UserVo> result = Lists.newArrayList();

        for(User user : users){
            result.add(new UserVo(user));
        }
        return result;
    }

    public int searchTotalSize(String keyword,UserType type) {
        return userDao.searchTotalSize(keyword,type);
    }
}
