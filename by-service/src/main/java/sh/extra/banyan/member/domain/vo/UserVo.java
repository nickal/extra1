package sh.extra.banyan.member.domain.vo;

import java.util.Date;

import sh.extra.banyan.member.domain.model.User;
import sh.extra.banyan.member.domain.type.UserType;

public class UserVo {

    private User user;

    public UserVo(User user) {
        super();
        this.user = user;
    }

    public int getId() {
        return user.getId();
    }

    public String getName() {
        return user.getName();
    }

    public String getLogin() {
        return user.getLogin();
    }

    public int getType() {
        return user.getType();
    }

    public Date getCreateTime() {
        return user.getCreateTime();
    }

    public void setCreateTime(Date createTime) {
        user.setCreateTime(createTime);
    }

    public UserType getEType() {
        return user.getEType();
    }
    
}
