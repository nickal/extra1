package sh.extra.banyan.product.domain.type;

public enum StyleStatus {

    有效(1);
    
    private int value;
    
    private StyleStatus(int value){
        this.value = value;
    }
    
    public int getValue(){
        return value;
    }
    
    public static StyleStatus fromValue(int value){
        StyleStatus[] types = values();
        for(StyleStatus type : types){
            if(type.getValue() == value){
                return type;
            }
        }
        return null;
    }
}
