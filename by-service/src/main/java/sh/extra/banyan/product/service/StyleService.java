package sh.extra.banyan.product.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import sh.extra.banyan.product.dao.StyleDao;
import sh.extra.banyan.product.domain.model.Style;
import sh.extra.banyan.product.domain.type.StyleStatus;

@Service
public class StyleService {

    @Inject
    private StyleDao styleDao;
    
    public void create(Style vo){
        Style style = new Style();
        
        style.setNo(vo.getNo());
        style.setDescrible(vo.getDescrible());
        style.setStatus(StyleStatus.有效.getValue());
        style.setPic(vo.getPic());
        styleDao.add(vo);
    }
    
    public List<Style> search(int start,int size){
        return styleDao.search(start, size);
    }
    
    public int searchTotalSize(){
        return styleDao.searchTotalSize();
    }

    public List<Style> loadAll() {
        return styleDao.loadAll();
    }
    
}
