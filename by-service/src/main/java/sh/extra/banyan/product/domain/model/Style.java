package sh.extra.banyan.product.domain.model;

import java.util.Date;

import sh.extra.banyan.product.domain.type.StyleStatus;

public class Style {

    private int id;
    private String no;                  // 款号
    private String describle;           // 描述
    private int status;
    private String pic;
    
    private Date createTime;
    private Date updateTime;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNo() {
        return no;
    }
    public void setNo(String no) {
        this.no = no;
    }
    public String getDescrible() {
        return describle;
    }
    public void setDescrible(String describle) {
        this.describle = describle;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return updateTime;
    }
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public int getStatus() {
        return status;
    }
    public StyleStatus getEStatus(){
        return StyleStatus.fromValue(status);
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    public String getPic() {
        return pic;
    }
    public void setPic(String pic) {
        this.pic = pic;
    }
    
}
