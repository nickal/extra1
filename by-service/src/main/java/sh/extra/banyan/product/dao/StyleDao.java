package sh.extra.banyan.product.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.google.common.collect.Maps;

import sh.extra.banyan.common.dao.AbstractDao;
import sh.extra.banyan.product.domain.model.Style;

@Repository
public class StyleDao extends AbstractDao{

    public void add(Style style){
        writeSqlSession.insert(sql(),style);
    }
    
    public List<Style> search(int start,int size){
        Map<String,Object> params = Maps.newHashMap();
        params.put("start", start);
        params.put("size", size);
        return list(sql(),params);
    }

    public int searchTotalSize() {
        return size(sql(),null);
    }

    public List<Style> loadAll() {
        return list(sql(),null);
    }
}
