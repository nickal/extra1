package sh.extra.banyan.admin.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import sh.extra.banyan.admin.domain.model.Admin;
import sh.extra.banyan.common.dao.AbstractDao;

import com.google.common.collect.Maps;

@Repository
public class AdminDao extends AbstractDao{

    public Admin getByName(String name){
        return writeSqlSession.selectOne(sql(),name);
    }
    
    public List<Admin> search(int start,int size){
        Map<String,Object> params = Maps.newHashMap();
        params.put("start", start);
        params.put("size", size);
        return list(sql(),params);
    }

    public int searchTotalSize() {
        return size(sql(),null);
    }

    public Admin getById(int id) {
        return writeSqlSession.selectOne(sql(),id);
    }
    
    public void add(Admin admin) {
        writeSqlSession.insert(sql(),admin);
    }
    
    public void updateAuth(int id,String authorizations){
        Map<String,Object> params = Maps.newHashMap();
        params.put("id", id);
        params.put("authorizations", authorizations);
        writeSqlSession.update(sql(),params);
    }
}
