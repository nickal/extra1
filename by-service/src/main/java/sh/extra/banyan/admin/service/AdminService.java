package sh.extra.banyan.admin.service;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import sh.extra.banyan.admin.dao.AdminDao;
import sh.extra.banyan.admin.domain.model.Admin;
import sh.extra.banyan.common.exception.BYServiceException;
import sh.extra.banyan.common.utils.LoggerUtils;
import sh.extra.banyan.system.service.AuthorizationService;

import com.google.common.collect.Lists;

@Service
public class AdminService {

    private static final Logger logger = Logger.getLogger(AdminService.class);
    
    @Inject
    private AdminDao adminDao;
    
    @Inject
    private AuthorizationService authorizationService;
    /**
     * 鉴权
     * @param admin
     * @param authorization
     */
    public void authorization(Admin admin, String path) {
        List<Integer> authIds = admin.getAuthors();
        authorizationService.checkPathAuthorization(path, authIds);
    }

    public Admin login(String name,String password){
        Admin admin = adminDao.getByName(name);
        if(admin == null){
            throw new BYServiceException("用户不存在");
        }
        if(!admin.getPassword().equals(password)){
            throw new BYServiceException("密码错误");
        }
        setUpAuth(admin);
        return admin;
    }

    public int searchTotalSize() {
        return adminDao.searchTotalSize();
    }

    public List<Admin> search(int start, int size) {
        List<Admin> result = adminDao.search(start, size);
        for(Admin admin :result){
            setUpAuth(admin);
        }
        return result;
    }

    public Admin getById(int id) {
        Admin admin = adminDao.getById(id);
        setUpAuth(admin);
        return admin;
    }
    //将表中的authorization的字符串 变成List
    private void setUpAuth(Admin admin){
        String auth = admin.getAuthorizations();
        List<Integer> authors = Lists.newArrayList();
        admin.setAuthors(authors);
        if(StringUtils.isEmpty(auth)){
            return;
        }
        String[] auths = auth.split(",");
        for(String at : auths){
            try{
                authors.add(Integer.valueOf(at));
            }catch(Exception e){
                LoggerUtils.error(logger,e, "管理员【{0}-{1}】权限解析错误",admin.getId(),admin.getName());
            }
        }
    }

    public void create(Admin admin) {
        // TODO :校验
        if(admin.getAuthorStrs() != null){
            String auth = StringUtils.join(admin.getAuthorStrs(),",");
            admin.setAuthorizations(auth);
        }else{
            admin.setAuthorizations("");
        }
        adminDao.add(admin);
    }
    
   public void updateAuth(int id,String authorizations){
	   adminDao.updateAuth(id, authorizations);
   }
}
