package sh.extra.banyan.admin.domain.type;

/**
 * 用户的类型
 * 
 * @author haibing.wang
 * @version $Id: UserType.java, v 0.1 2014年12月7日 下午11:11:55 haibing.wang Exp $
 */
public enum AdminType {

    高级管理员(0),
    普通(1);
    
    private int type;
    
    private AdminType(int type){
        this.type = type;
    }
    
    public int getType(){
        return type;
    }

    public static AdminType fromType(int type) {
        AdminType[] types = values();
        for(AdminType t : types){
            if(t.getType() == type){
                return t;
            }
        }
        return null;
    }
    
}
