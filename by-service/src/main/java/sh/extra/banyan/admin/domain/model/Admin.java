package sh.extra.banyan.admin.domain.model;

import java.util.Date;
import java.util.List;

import sh.extra.banyan.admin.domain.type.AdminType;

/**
 * 管理员
 * @author haibing.wang
 * @version $Id: Admin.java, v 0.1 2014年11月22日 上午9:46:31 haibing.wang Exp $
 */
public class Admin {

    private long id;
    private String name;
    private String avatar;
    private int sex;
    private String password;
    private Date createTime;
    private Date updateTime;
    private int type;
    private String authorizations;
    
    private List<Integer> authors;
    private String[] authorStrs;
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getAuthorizations() {
        return authorizations;
    }

    public void setAuthorizations(String authorizations) {
        this.authorizations = authorizations;
    }

    public List<Integer> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Integer> authors) {
        this.authors = authors;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String[] getAuthorStrs() {
        return authorStrs;
    }

    public void setAuthorStrs(String[] authorStrs) {
        this.authorStrs = authorStrs;
    }
    
    public AdminType getEType(){
        return AdminType.fromType(type);
    }
}
