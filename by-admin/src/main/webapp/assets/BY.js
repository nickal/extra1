function AjaxLink() {
			$('#ajax-content a.ajax-link').click(function() {
				var url = $(this).attr('href');
				window.location.hash = url;
				LoadAjaxContent(url);
				return false;
			});
		}

		//
		//  Function for load content from url and put in $('.ajax-content') block
		//
		function LoadAjaxContent(url) {
			$('.preloader').show();
			$.ajax({
				mimeType : 'text/html; charset=utf-8', // ! Need set mimeType only when run from local file
				url : url,
				type : 'GET',
				success : function(data) {
					$('#ajax-content').html(data);
					//				$('#ajax-content').innerHTML = data;
					$('.preloader').hide();
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(errorThrown);
				},
				dataType : "html",
				async : false
			});
		}

		// ajax form 提交
		// form 标签的ID-> ajax-form
		function AjaxForm(){
			$('#ajax-content #ajax-form .submit').click(function(){
				var ajaxForm =$('#ajax-content #ajax-form');
				var url = ajaxForm.attr('action');
				ajaxForm.ajaxSubmit({
					mimeType: 'text/html; charset=utf-8', // ! Need set mimeType only when run from local file
					url: url,
					type: 'POST',
					success: function(data) {
						$('#ajax-content').html(data);
						$('.preloader').hide();
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(errorThrown);
					},
					dataType: "html",
					async: false
			     });
			});
		}
		
		// 图片上传
		// root 根标签
		function InitImageUpload(root){
			var d = 0;
			//  控制只能创建一个div
			if(d == 0){
				var d = document.createElement("div");
				$(root).append(d);
				d.innerHTML = "<form  id='image-uploadform' style='display:none' action='image/upload.html' " +
						"enctype='multipart/form-data' method='post'><input id='image-upload-btn' type='file' name='file' >" +
						"<input style='display:none' id='imgType' name='type'/></form>";
				d++;
			}
			
			var image;
			//  图片被点击
		    $(".image-pic").each(function(){
				$(this).click(function(){
					image = this;
					$('#imgType').val($(this).attr('imgType'));
					$("#image-upload-btn").click();
				});
			});
		    
		    //  图片被选择
			$("#image-upload-btn").on("change",function(){
				var type = $('#imgType').val();
				var form = $("#image-uploadform");
				form.ajaxSubmit({
		           dataType: "json",
				   data:{
				   type:type
				   },
		           success:  function (data) {
		              if(data.code == 0){
		            	  var imageUrl = data.data;
		            	  var _callback_fun = undefined;
		        		  if(image){
		            	  	  _callback_fun = $(image).attr('callback');
		            	  }
		        		  //回调处理返回图片,在修改img之前调用
		        		  if(_callback_fun){
		        			  window[_callback_fun](imageUrl,image);
		        		  }
		        		  if(image){
		            		  $(image).attr("src",imageUrl);
		        		  }
		        	  }else{
		        		  alert(data.msg);
		        	  }
		           },
		           error: function (data, status, e){//服务器响应失败处理函数
		                 alert(e);
		              }
		        });
				  $(this).val("");
		    });
		}

		$(document).ready(function() {
			var ajax_url = location.hash.replace(/^#/, '');
			if (ajax_url.length < 1) {
				ajax_url = 'main.html';
			}
			LoadAjaxContent(ajax_url);
			
			$('a.ajax-link').on('click', function (e) {
				if ($(this).attr('href') == '#') {
					return;
				}
				e.preventDefault();
				var url = $(this).attr('href');
				window.location.hash = url;
				LoadAjaxContent(url);
			});
		});