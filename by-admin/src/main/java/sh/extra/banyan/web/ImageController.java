package sh.extra.banyan.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import sh.extra.banyan.common.service.ImageService;
import sh.extra.banyan.common.utils.PropertyUtil;
import sh.extra.banyan.common.utils.ResponseUtils;

@Controller
@RequestMapping("/image")
public class ImageController {

	private final static Logger logger = Logger
			.getLogger(ImageController.class);

	@Inject
	private ImageService imageService;
	
	private static String getExtention(String fileName) {
		int pos = fileName.lastIndexOf(".");
		String ext = fileName.substring(pos);
		if (".gif".equalsIgnoreCase(ext)) {
			return ".jpg";
		} else {
			return ext;
		}
	}
	
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void upload(HttpServletRequest request, HttpServletResponse response,
			String token) {

		MultipartHttpServletRequest mreq = (MultipartHttpServletRequest) request;
		MultipartFile file = mreq.getFile("file");
		String filename = file.getOriginalFilename();
		String imgtype = getExtention(filename);
		try {
			String acceptType = PropertyUtil
					.getConstantsValue("image.acceptType");
			if (acceptType.contains(imgtype.toLowerCase())) {
				String remotefilename = imageService.upload(file.getInputStream());
				ResponseUtils.writeSuccessReponse(request, response,remotefilename);
			} else {
				ResponseUtils.writeAppErrorResponse(request, response,
						"不支持的图片格式");
			}
		} catch (Exception ex) {
			logger.error(ex);
			ResponseUtils.writeServiceErrorResponse(request, response, "系统繁忙");
		}
	}
}

