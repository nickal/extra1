package sh.extra.banyan.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sh.extra.banyan.common.Constants.Authorization;
import sh.extra.banyan.common.utils.RequestUtils;
import sh.extra.banyan.member.domain.type.UserType;
import sh.extra.banyan.member.service.UserService;
import sh.extra.banyan.web.aop.Web;

@Controller
@RequestMapping("/user")
public class UserController {

    @Inject
    private UserService userService;
    
    @RequestMapping("/search")
    @Web
    public String search(HttpServletRequest request,HttpServletResponse response,Model model,
                         String keyword,
                     @RequestParam(value="type",defaultValue="0")int type,
                     @RequestParam(value="start",defaultValue="0")int start,
                     @RequestParam(value="size",defaultValue="10")int size){
        if(keyword == null){
            keyword = "";
        }else{
            keyword = RequestUtils.decode(keyword);
        }
        
        if(start < 0){
            start = 0;
        }
        
        if(size < 0 || size > 20){
            size = 10;
        }
        UserType userType = UserType.fromType(type);
        int total = userService.searchTotalSize(keyword, userType);
        if(total > 0){
            model.addAttribute("users", userService.search(keyword,userType,start, size));
        }
        model.addAttribute("total", total);
        model.addAttribute("type", type);
        model.addAttribute("keyword", keyword);
        model.addAttribute("start", start);
        model.addAttribute("size", size);
        return "/member/user_list";
    }
}
