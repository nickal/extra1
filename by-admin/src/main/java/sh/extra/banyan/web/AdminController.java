package sh.extra.banyan.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sh.extra.banyan.admin.domain.model.Admin;
import sh.extra.banyan.admin.service.AdminService;
import sh.extra.banyan.common.Constants.Authorization;
import sh.extra.banyan.system.service.AuthorizationService;
import sh.extra.banyan.web.aop.Web;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Inject
    private AdminService adminService;
    
    @Inject
    private AuthorizationService authorizationService;
    @Web
    @RequestMapping("/profile")
    public String profile(HttpServletRequest request,HttpServletResponse response,Model model){
        
        return "/admin/profile";
    }
  //*******************************//  
    @Web
    @RequestMapping("/authorization")
    public String authorization(HttpServletRequest request,HttpServletResponse response,Model model,
                                int id){
        Admin admin = adminService.getById(id);
        model.addAttribute("admin", admin);
        model.addAttribute("auths", authorizationService.loadAdd());
        return "/admin/authorization";
    }
    //********************************//
    @Web
    @RequestMapping("/update")
    public String updeteAuth(HttpServletRequest request,HttpServletResponse response,Model model,
    		@RequestParam(value="id",defaultValue="0")int id,String authorizations){
    	adminService.updateAuth(id, authorizations);
        
        return "redirect:/index#admin/search.html";
    }
    
    @Web
    @RequestMapping("/search")
    public String search(HttpServletRequest request,HttpServletResponse response,Model model,
                         @RequestParam(value="start",defaultValue="0")int start,
                         @RequestParam(value="size",defaultValue="10")int size){
        int total = adminService.searchTotalSize();
        if(total > 0){
            model.addAttribute("admins", adminService.search(start, size));
        }
        model.addAttribute("total", total);
        model.addAttribute("start", start);
        model.addAttribute("size", size);
        return "/admin/admin_list";
    }
    
    @Web
    @RequestMapping("/add")
    public String add(HttpServletRequest request,HttpServletResponse response,Model model){
        model.addAttribute("auths", authorizationService.loadAdd());
        return "/admin/admin_add";
    }
    
    @Web
    @RequestMapping("/doAdd")
    public String doAdd(HttpServletRequest request,HttpServletResponse response,Model model,
                        Admin admin){
        adminService.create(admin);
        return "redirect:/admin/search";
    }
}
