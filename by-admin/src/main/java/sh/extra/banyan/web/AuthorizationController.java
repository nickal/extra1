package sh.extra.banyan.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sh.extra.banyan.system.domain.model.Authorization;
import sh.extra.banyan.system.service.AuthorizationService;
import sh.extra.banyan.web.aop.Web;

@Controller
@RequestMapping("/authorization")
public class AuthorizationController {

    @Inject
    private AuthorizationService authorizationService;
    
    @Web
    @RequestMapping("/search")
    public String search(HttpServletRequest request,HttpServletResponse response,Model model,
                         @RequestParam(value="start",defaultValue="0")int start,
                         @RequestParam(value="size",defaultValue="10")int size){
        int total = authorizationService.searchTotalSize();
        if(total > 0){
            model.addAttribute("auths", authorizationService.search(start, size));
        }
        model.addAttribute("total", total);
        model.addAttribute("start", start);
        model.addAttribute("size", size);
        return "/system/authorization_list";
    }
    
    @Web
    @RequestMapping("/add")
    public String add(HttpServletRequest request,HttpServletResponse response,Model model){
    	
        return "/system/authorization_add";
    }
    
    @Web
    @RequestMapping("/doAdd")
    public String doAdd(HttpServletRequest request,HttpServletResponse response,Model model,
                        Authorization auth){
    	if(!(auth.getName().isEmpty()||auth.getPath().isEmpty()))
            authorizationService.saveOrUpdate(auth);
        return "redirect:/authorization/search";
    }
    
    
    @Web
    @RequestMapping("/doDelete")
    public String doDelete(HttpServletRequest request,HttpServletResponse response,Model model,
    		@RequestParam(value="id",defaultValue="0")int id){
              authorizationService.delete(id);
           return "redirect:/authorization/search";
    }
    
}
