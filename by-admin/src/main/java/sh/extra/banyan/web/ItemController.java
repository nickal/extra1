package sh.extra.banyan.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import sh.extra.banyan.common.Constants.Authorization;
import sh.extra.banyan.common.utils.RequestUtils;
import sh.extra.banyan.common.utils.ResponseUtils;
import sh.extra.banyan.item.domain.model.Item;
import sh.extra.banyan.item.domain.vo.ItemNo;
import sh.extra.banyan.item.service.ItemService;
import sh.extra.banyan.product.service.StyleService;
import sh.extra.banyan.web.aop.Web;
import sh.extra.banyan.web.aop.WebAjax;

@Controller
@RequestMapping("/item")
public class ItemController {

    @Inject
    private ItemService itemService;
    
    @Inject
    private StyleService styleService;
    
    @Web
    @RequestMapping("/search")
    public String search(HttpServletRequest request,HttpServletResponse response,Model model,
                         String keyword,
                     @RequestParam(value="type",defaultValue="0")int type,
                     @RequestParam(value="start",defaultValue="0")int start,
                     @RequestParam(value="size",defaultValue="10")int size){
        if(keyword == null){
            keyword = "";
        }else{
            keyword = RequestUtils.decode(keyword);
        }
        
        if(start < 0){
            start = 0;
        }
        
        if(size < 0 || size > 20){
            size = 10;
        }
        
        int total = itemService.searchTotalSize(keyword, type);
        if(total > 0){
            model.addAttribute("items", itemService.search(keyword,type,start, size));
        }
        model.addAttribute("total", total);
        model.addAttribute("type", type);
        model.addAttribute("keyword", keyword);
        model.addAttribute("start", start);
        model.addAttribute("size", size);
        
        return "/item/item_list";
    }
    
    @Web
    @RequestMapping("/detail")
    public String detail(HttpServletRequest request,HttpServletResponse response,Model model){
        return "/item/item_detail";
    }
    
    @Web
    @RequestMapping("/add")
    public String add(HttpServletRequest request,HttpServletResponse response,Model model){
        model.addAttribute("styles", styleService.loadAll());
        return "/item/item_add";
    }
    
    @Web
    @RequestMapping("/doAdd")
    public String doAdd(HttpServletRequest request,HttpServletResponse response,Model model,
                        Item item){
        itemService.create(item);
        return "redirect:/item/search";
    }
    
    /**
     * 解析货号
     * @param request
     * @param response
     * @param model
     * @return
     */
    @Web
    @RequestMapping(value="/itemno/parse",method=RequestMethod.GET)
    public void ajaxParseItemNo(HttpServletRequest request,HttpServletResponse response,String no){
        ResponseUtils.writeSuccessReponse(request, response, ItemNo.parse(no));
    }
}
