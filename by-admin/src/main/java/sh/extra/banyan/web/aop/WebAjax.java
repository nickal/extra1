package sh.extra.banyan.web.aop;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 认证 (authentication) 和授权 (authorization) 的区别
 * 
 * 你要登机，你需要出示你的 passport 和 ticket，passport 是为了证明你张三确实是你张三，这就是 authentication；而机票是为了证明你张三确实买了票可以上飞机，这就是 authorization。
 * 
 * <hr>
 * 【注意】被注解的方法必须执行如下参数规则：
 * <br>
 * 1、第一、二参数分别为
 *  HttpServletRequest
 *  HttpServletResponse
 * <br>
 * 2、如果不是ajax请求，第三个参数:
 *  Model 
 * @author haibing.wang
 * @version $Id: WebAjax.java, v 0.1 2014年11月3日 下午10:48:23 haibing.wang Exp $
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebAjax {
	
    /**
     * 是否需要认证。证明你是谁，类似护照
     * 默认值 true
     * @return
     */
	boolean authentication() default true;
	
	/**
	 * 需要的权限，前提是认证。证明你有相应的权限，类似机票。
	 * @return
	 */
	boolean authorization() default true;
	
}
