package sh.extra.banyan.web;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import sh.extra.banyan.common.Constants.Authorization;
import sh.extra.banyan.item.domain.model.Catalog;
import sh.extra.banyan.item.service.CatalogService;
import sh.extra.banyan.web.aop.Web;

@Controller
@RequestMapping("/catalog")
public class CatalogController {
    
    @Inject
    private CatalogService catalogService;

    @Web
    @RequestMapping("/search")
    public String search(HttpServletRequest request,HttpServletResponse response,Model model){
        List<Catalog> catalog = catalogService.loadAll();
        model.addAttribute("catas", catalog);
        return "/item/catalog_list";
    }
    
    @Web
    @RequestMapping("/add")
    public String search(HttpServletRequest request,HttpServletResponse response,Model model,
                         String name,int pid){
        catalogService.add(pid,name);
        return search(request, response, model);
    }
}
