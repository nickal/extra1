package sh.extra.banyan.web;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import sh.extra.banyan.product.domain.model.Style;
import sh.extra.banyan.product.service.StyleService;
import sh.extra.banyan.web.aop.Web;

@Controller
@RequestMapping("/style")
public class StyleController {

    @Inject
    private StyleService styleService;
    
    @Web
    @RequestMapping("/search")
    public String search(HttpServletRequest request,HttpServletResponse response,Model model,
                         @RequestParam(value="start",defaultValue="0")int start,
                         @RequestParam(value="size",defaultValue="10")int size){
        int total = styleService.searchTotalSize();
        if(total > 0){
            model.addAttribute("styles", styleService.search(start, size));
        }
        model.addAttribute("total", total);
        model.addAttribute("start", start);
        model.addAttribute("size", size);
        return "/product/style_manger";
    }
    
    @Web
    @RequestMapping("/add")
    public String add(HttpServletRequest request,HttpServletResponse response,Model model){
        return "/product/style_add";
    }
    
    @Web
    @RequestMapping("/doAdd")
    public String doAdd(HttpServletRequest request,HttpServletResponse response,Model model,
                      Style style){
        styleService.create(style);
        return "redirect:/style/search";
    }
}
