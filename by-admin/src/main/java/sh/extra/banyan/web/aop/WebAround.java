package sh.extra.banyan.web.aop;

import java.lang.reflect.Method;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import sh.extra.banyan.admin.domain.model.Admin;
import sh.extra.banyan.admin.domain.type.AdminType;
import sh.extra.banyan.admin.service.AdminService;
import sh.extra.banyan.common.exception.BYArgumentException;
import sh.extra.banyan.common.exception.BYServiceException;
import sh.extra.banyan.common.exception.BYTokenException;
import sh.extra.banyan.common.utils.ResponseUtils;

@Component
@Aspect
public class WebAround {

    public static final String ADMIN_KEY = "__admin";
    private final static Logger logger = Logger.getLogger(WebAround.class);
    
    @Inject
    private AdminService adminService;
    
    @Around("@annotation(sh.extra.banyan.web.aop.WebAjax)")
    public void ajax(ProceedingJoinPoint point) {
        Method method = ((MethodSignature) point.getSignature()).getMethod();
        WebAjax web = method.getAnnotation(WebAjax.class);
        
        boolean authorization = web.authorization();
        boolean authentication = web.authentication();
        around(point,authentication,authorization,true);
    }
    
    @Around("@annotation(sh.extra.banyan.web.aop.Web)")
    public String web(ProceedingJoinPoint point){
        Method method = ((MethodSignature) point.getSignature()).getMethod();
        Web web = method.getAnnotation(Web.class);
        
        boolean authorization = web.authorization();
        boolean authentication = web.authentication();
        return around(point,authentication,authorization,false);
    }
    
    public String around(ProceedingJoinPoint point,boolean authentication,boolean authorization,boolean ajax) {
        return doAround(point,authentication,authorization,ajax);
    }
    
    private String doAround(ProceedingJoinPoint point,boolean authentication,boolean authorization,boolean ajax) {
        
        Object[] args = point.getArgs();
        HttpServletRequest request = (HttpServletRequest) args[0];
        HttpServletResponse response = (HttpServletResponse) args[1];
        Model model = null;
        if(!ajax){// 非ajax请求的第三个参数为model
            model = (Model) args[2];
        }
        
        try{
            if(authentication){
                Admin admin = (Admin)request.getSession().getAttribute(ADMIN_KEY);
                if(admin == null){
                    throw new BYTokenException("请登录");
                }
                
                if(authorization){
                    if(admin.getType() != AdminType.高级管理员.getType() ){// 超级管理员拥有所有权限
                        String path = request.getRequestURI();
                        path = StringUtils.substringBefore(path, ".");
                        if(path.contains("/admin")){
                            path = StringUtils.substringAfter(path,"/admin");
                        }
                        adminService.authorization(admin,path);
                    }
                }
            }
            
            if (ajax) {
                point.proceed(point.getArgs());
                return "";
            } else {
                return point.proceed(point.getArgs()).toString();
            }
        }catch(BYArgumentException e){
            logger.error(e);
            if (ajax) {
                ResponseUtils.writeParamErrorResponse(request, response, e.getCode(),
                    e.getMessage());
                return "";
            } else {
                model.addAttribute("msg", e.getMessage());
                return "/error";
            }
        }catch (BYServiceException e) {
            logger.error(e);
            if (ajax) {
                ResponseUtils.writeServiceErrorResponse(request, response, e.getCode(),
                    e.getMessage());
                return "";
            } else {
                model.addAttribute("msg", e.getMessage());
                return "/error";
            }
        }catch (BYTokenException e) {
            logger.error(e);
            if (ajax) {
                ResponseUtils.writeTokenErrorResponse(request, response, e.getMessage());
                return "";
            } else {
                return "/login";
            }
        }catch(Throwable e){
            logger.error(e);
            if (ajax) {
                ResponseUtils.writeAppErrorResponse(request, response,"服务繁忙");
                return "";
            } else {
                model.addAttribute("msg", "服务繁忙");
                return "/error";
            }
        }
        
    }
}
