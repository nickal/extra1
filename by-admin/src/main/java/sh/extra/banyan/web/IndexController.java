package sh.extra.banyan.web;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import sh.extra.banyan.admin.domain.model.Admin;
import sh.extra.banyan.admin.service.AdminService;
import sh.extra.banyan.item.domain.model.Catalog;
import sh.extra.banyan.item.service.CatalogService;
import sh.extra.banyan.web.aop.Web;
import sh.extra.banyan.web.aop.WebAround;

@Controller
@RequestMapping("")
public class IndexController {
    
    @Inject
    private AdminService adminService;

    @Inject
    private CatalogService catlogService;
    
    @Web(authorization=false)
    @RequestMapping("/index")
    public String index(HttpServletRequest request,HttpServletResponse response,Model model){
        if(request.getSession().getAttribute(WebAround.ADMIN_KEY) != null){
            
            List<Catalog> catlogs = catlogService.loadCatlogs();
            model.addAttribute("catlogs",catlogs);
            
            
            return "/index";
        }
        return "/login";
    }
    
    @Web(authorization=false)
    @RequestMapping("/main.html")
    public String main(HttpServletRequest request,HttpServletResponse response,Model model){
        return "/main";
    }
    
    @RequestMapping("/error")
    public String error(HttpServletRequest request,HttpServletResponse response,Model model){
        return "/error";
    }
    
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public String login(HttpServletRequest request,HttpServletResponse response,Model model){
        return "/login";
    }
    
    @Web(authentication=false)
    @RequestMapping(value="/login",method=RequestMethod.POST)
    public String doLogin(HttpServletRequest request,HttpServletResponse response,Model model,
                          String name,String password){
        Admin admin = adminService.login(name, password);
        request.getSession().setAttribute(WebAround.ADMIN_KEY, admin);
        return "redirect:/index";
    }
    
    @Web(authentication=false)
    @RequestMapping(value="/logout")
    public String logout(HttpServletRequest request,HttpServletResponse response,Model model){
        request.getSession().removeAttribute(WebAround.ADMIN_KEY);
        return "/login";
    }
}
