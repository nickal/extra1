$(function() {

	var interval,
		lastInt,
		startInt = 2,
		$slider = $('#bong-slider'),
		$ctrlBlock = $('#bong-slider-control .bong-control-block'),
		$chooseColor = $('#bong-color-control'),
		$colorItem = $('a', $chooseColor),
		$chosenColor = $('#bong-II-wristband');

	function autoPlay() {
		if (startInt == 0) {
			startInt = 2;
		} else {
			startInt -= 1;
		}

		$slider.attr('class', 'bong-slider s' + startInt);
		$ctrlBlock.eq(startInt).addClass('cur').siblings().removeClass('cur');
	}

	function play(event) {
		if (!interval) {
			interval = setInterval(autoPlay, 4000);
		}
	}

	function pause(event) {
		if (interval) {
			clearInterval(interval);
			interval = null;
		}
	}

	$colorItem.on('click', function(event) {
		event.stopPropagation();
		event.preventDefault();

		var color = $(this).attr('class');

		if (!/cur/g.test(color)) {
			$colorItem.removeClass('cur');
			$(this).addClass('cur');
			$chosenColor.find('img').removeClass('cur');
			$chosenColor.find('img.'+ color).addClass('cur');
		}
	});

	$ctrlBlock.hover(
		function() {
			var index = $(this).index();
			$slider.attr('class', 'bong-slider s' + index);
			$(this).addClass('cur').siblings().removeClass('cur');
			pause();
		}, function() {
			play();
		}
	);

	$('.bong-sports').one('mouseover', function(){
		event.stopPropagation();
		interval = setInterval(autoPlay, 4000);
	});

});