(function(){window.require=function(){}
window.Banana={"initializers":{},"globals":{},"models":{},"collections":{},"controllers":{},"views":{"delivery":{},"factories":{},"coming_soon":{},"components":{},"content_page":{},"products":{},"about":{},"help":{},"collections":{},"subheros":{},"invite":{},"account":{"credit_cards":{},"addresses":{}},"application":{"chrome":{},"modals":{},"static":{}},"users":{},"open_studio":{},"home":{},"orders":{},"checkout":{}}}}).call(this)
!function(){var e=function(){var e={},t={},n=function(){return function(){}},a=function(e,a){var r={"id":e,"exports":{}}
a(r.exports,n(),r)
var i=t[e]=r.exports
return i},r=function(n){if(t.hasOwnProperty(n))return t[n]
if(e.hasOwnProperty(n))return a(n,e[n])
throw new Error('Cannot find module "'+n+'"')}
r.register=function(t,n){e[t]=n}
return r}()
e.register("chaplin/application",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u,h
h=e("underscore")
i=e("backbone")
s=e("chaplin/dispatcher")
c=e("chaplin/views/layout")
o=e("chaplin/composer")
p=e("chaplin/lib/router")
l=e("chaplin/lib/event_broker")
u=e("chaplin/mediator")
a.exports=r=function(){function e(e){null==e&&(e={})
this.initialize(e)}e.extend=i.Model.extend
h.extend(e.prototype,l)
e.prototype.title=""
e.prototype.dispatcher=null
e.prototype.layout=null
e.prototype.router=null
e.prototype.composer=null
e.prototype.started=!1
e.prototype.initialize=function(e){null==e&&(e={})
if(this.started)throw new Error("Application#initialize: App was already started")
this.initRouter(e.routes,e)
this.initDispatcher(e)
this.initLayout(e)
this.initComposer(e)
this.initMediator()
return this.start()}
e.prototype.initDispatcher=function(e){return this.dispatcher=new s(e)}
e.prototype.initLayout=function(e){var t
null==e&&(e={})
null==(t=e.title)&&(e.title=this.title)
return this.layout=new c(e)}
e.prototype.initComposer=function(e){null==e&&(e={})
return this.composer=new o(e)}
e.prototype.initMediator=function(){return u.seal()}
e.prototype.initRouter=function(e,t){this.router=new p(t)
return"function"==typeof e?e(this.router.match):void 0}
e.prototype.start=function(){this.router.startHistory()
this.started=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}
e.prototype.disposed=!1
e.prototype.dispose=function(){var e,t,n,a
if(!this.disposed){t=["dispatcher","layout","router","composer"]
for(n=0,a=t.length;a>n;n++){e=t[n]
null!=this[e]&&this[e].dispose()}this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return e}()})
e.register("chaplin/mediator",function(t,n,a){"use strict"
var r,i,o,s,l,c,p=[].slice
r=e("backbone")
c=e("underscore")
s=e("chaplin/lib/support")
l=e("chaplin/lib/utils")
o={}
o.subscribe=r.Events.on
o.unsubscribe=r.Events.off
o.publish=r.Events.trigger
o._callbacks=null
i=o._handlers={}
o.setHandler=function(e,t,n){return i[e]={"instance":n,"method":t}}
o.execute=function(){var e,t,n,a,r
a=arguments[0],e=2<=arguments.length?p.call(arguments,1):[]
r=!1
if("object"==typeof a){r=a.silent
n=a.name}else n=a
t=i[n]
if(t)return t.method.apply(t.instance,e)
if(!r)throw new Error("mediator.execute: "+n+" handler is not defined")}
o.removeHandlers=function(e){var t,n,a,r
e||(o._handlers={})
if(l.isArray(e))for(a=0,r=e.length;r>a;a++){n=e[a]
delete i[n]}else for(n in i){t=i[n]
t.instance===e&&delete i[n]}}
l.readonly(o,"subscribe","unsubscribe","publish","setHandler","execute","removeHandlers")
o.seal=function(){return s.propertyDescriptors&&Object.seal?Object.seal(o):void 0}
l.readonly(o,"seal")
a.exports=o})
e.register("chaplin/dispatcher",function(t,n,a){"use strict"
var r,i,o,s,l,c
c=e("underscore")
r=e("backbone")
s=e("chaplin/mediator")
l=e("chaplin/lib/utils")
o=e("chaplin/lib/event_broker")
a.exports=i=function(){function e(){this.initialize.apply(this,arguments)}e.extend=r.Model.extend
c.extend(e.prototype,o)
e.prototype.previousRoute=null
e.prototype.currentController=null
e.prototype.currentRoute=null
e.prototype.currentParams=null
e.prototype.currentQuery=null
e.prototype.initialize=function(e){null==e&&(e={})
this.settings=c.defaults(e,{"controllerPath":"controllers/","controllerSuffix":"_controller"})
return this.subscribeEvent("router:match",this.dispatch)}
e.prototype.dispatch=function(e,t,n){var a,r,i=this
t=t?c.extend({},t):{}
n=n?c.extend({},n):{}
null==n.query&&(n.query={})
n.forceStartup!==!0&&(n.forceStartup=!1)
return!n.forceStartup&&(null!=(a=this.currentRoute)?a.controller:void 0)===e.controller&&(null!=(r=this.currentRoute)?r.action:void 0)===e.action&&c.isEqual(this.currentParams,t)&&c.isEqual(this.currentQuery,n.query)?void 0:this.loadController(e.controller,function(a){return i.controllerLoaded(e,t,n,a)})}
e.prototype.loadController=function(e,t){var n,a
n=e+this.settings.controllerSuffix
a=this.settings.controllerPath+n
return("undefined"!=typeof define&&null!==define?define.amd:void 0)?require([a],t):setTimeout(function(){return t(require(a))},0)}
e.prototype.controllerLoaded=function(e,t,n,a){var r,i
this.nextPreviousRoute=this.currentRoute
i=c.extend({},this.nextPreviousRoute)
null!=this.currentParams&&(i.params=this.currentParams)
i.previous&&delete i.previous
this.nextCurrentRoute=c.extend({},e,{"previous":i})
r=new a(t,this.nextCurrentRoute,n)
return this.executeBeforeAction(r,this.nextCurrentRoute,t,n)}
e.prototype.executeAction=function(e,t,n,a){if(this.currentController){this.publishEvent("beforeControllerDispose",this.currentController)
this.currentController.dispose(n,t,a)}this.currentController=e
this.currentParams=n
this.currentQuery=a.query
e[t.action](n,t,a)
return e.redirected?void 0:this.publishEvent("dispatcher:dispatch",this.currentController,n,t,a)}
e.prototype.executeBeforeAction=function(e,t,n,a){var r,i,o,s=this
r=e.beforeAction
i=function(){if(!(e.redirected||s.currentRoute&&t===s.currentRoute)){s.previousRoute=s.nextPreviousRoute
s.currentRoute=s.nextCurrentRoute
s.nextPreviousRoute=s.nextCurrentRoute=null
return s.executeAction(e,t,n,a)}s.nextPreviousRoute=s.nextCurrentRoute=null
e.dispose()}
if(r){if("function"!=typeof r)throw new TypeError("Controller#beforeAction: function expected. Old object-like form is not supported.")
o=e.beforeAction(n,t,a)
return o&&o.then?o.then(i):i()}i()}
e.prototype.disposed=!1
e.prototype.dispose=function(){if(!this.disposed){this.unsubscribeAllEvents()
this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return e}()})
e.register("chaplin/composer",function(t,n,a){"use strict"
var r,i,o,s,l,c,p
p=e("underscore")
r=e("backbone")
l=e("chaplin/mediator")
c=e("chaplin/lib/utils")
o=e("chaplin/lib/composition")
s=e("chaplin/lib/event_broker")
a.exports=i=function(){function e(){this.initialize.apply(this,arguments)}e.extend=r.Model.extend
p.extend(e.prototype,s)
e.prototype.compositions=null
e.prototype.initialize=function(e){null==e&&(e={})
this.compositions={}
l.setHandler("composer:compose",this.compose,this)
l.setHandler("composer:retrieve",this.retrieve,this)
return this.subscribeEvent("dispatcher:dispatch",this.cleanup)}
e.prototype.compose=function(e,t,n){return"function"==typeof t?n||t.prototype.dispose?t.prototype instanceof o?this._compose(e,{"composition":t,"options":n}):this._compose(e,{"options":n,"compose":function(){var e,n
this.item=new t(this.options)
e=this.item.autoRender
n=void 0===e||!e
return n&&"function"==typeof this.item.render?this.item.render():void 0}}):this._compose(e,{"compose":t}):"function"==typeof n?this._compose(e,{"compose":n,"options":t}):this._compose(e,t)}
e.prototype._compose=function(e,t){var n,a,r,i
if("function"!=typeof t.compose&&null==t.composition)throw new Error("Composer#compose was used incorrectly")
if(null!=t.composition)n=new t.composition(t.options)
else{n=new o(t.options)
n.compose=t.compose
t.check&&(n.check=t.check)}a=this.compositions[e]
r=!1
if(a&&a.check(n.options))a.stale(!1)
else{a&&a.dispose()
i=n.compose(n.options)
r="function"==typeof(null!=i?i.then:void 0)
n.stale(!1)
this.compositions[e]=n}return r?i:this.compositions[e].item}
e.prototype.retrieve=function(e){var t
t=this.compositions[e]
return t&&!t.stale()?t.item:void 0}
e.prototype.cleanup=function(){var e,t,n
n=this.compositions
for(t in n){e=n[t]
if(e.stale()){e.dispose()
delete this.compositions[t]}else e.stale(!0)}}
e.prototype.dispose=function(){var e,t,n
if(!this.disposed){this.unsubscribeAllEvents()
l.removeHandlers(this)
n=this.compositions
for(t in n){e=n[t]
e.dispose()}delete this.compositions
this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return e}()})
e.register("chaplin/controllers/controller",function(t,n,a){"use strict"
var r,i,o,s,l,c,p=[].slice,u={}.hasOwnProperty
c=e("underscore")
r=e("backbone")
o=e("chaplin/lib/event_broker")
l=e("chaplin/lib/utils")
s=e("chaplin/mediator")
a.exports=i=function(){function e(){this.initialize.apply(this,arguments)}e.extend=r.Model.extend
c.extend(e.prototype,r.Events)
c.extend(e.prototype,o)
e.prototype.view=null
e.prototype.redirected=!1
e.prototype.initialize=function(){}
e.prototype.beforeAction=function(){}
e.prototype.adjustTitle=function(e){return s.execute("adjustTitle",e)}
e.prototype.reuse=function(){var e
e=1===arguments.length?"retrieve":"compose"
return s.execute.apply(s,["composer:"+e].concat(p.call(arguments)))}
e.prototype.compose=function(){throw new Error("Controller#compose was moved to Controller#reuse")}
e.prototype.redirectTo=function(e,t,n){this.redirected=!0
return l.redirectTo(e,t,n)}
e.prototype.disposed=!1
e.prototype.dispose=function(){var e,t
if(!this.disposed){for(t in this)if(u.call(this,t)){e=this[t]
if(e&&"function"==typeof e.dispose){e.dispose()
delete this[t]}}this.unsubscribeAllEvents()
this.stopListening()
this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return e}()})
e.register("chaplin/models/collection",function(t,n,a){"use strict"
var r,i,o,s,l,c,p={}.hasOwnProperty,u=function(e,t){function n(){this.constructor=e}for(var a in t)p.call(t,a)&&(e[a]=t[a])
n.prototype=t.prototype
e.prototype=new n
e.__super__=t.prototype
return e}
c=e("underscore")
r=e("backbone")
o=e("chaplin/lib/event_broker")
s=e("chaplin/models/model")
l=e("chaplin/lib/utils")
a.exports=i=function(e){function t(){return t.__super__.constructor.apply(this,arguments)}u(t,e)
c.extend(t.prototype,o)
t.prototype.model=s
t.prototype.serialize=function(){return this.map(l.serialize)}
t.prototype.disposed=!1
t.prototype.dispose=function(){var e,t,n,a
if(!this.disposed){this.trigger("dispose",this)
this.reset([],{"silent":!0})
this.unsubscribeAllEvents()
this.stopListening()
this.off()
t=["model","models","_byId","_byCid","_callbacks"]
for(n=0,a=t.length;a>n;n++){e=t[n]
delete this[e]}this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return t}(r.Collection)})
e.register("chaplin/models/model",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u={}.hasOwnProperty,h=function(e,t){function n(){this.constructor=e}for(var a in t)u.call(t,a)&&(e[a]=t[a])
n.prototype=t.prototype
e.prototype=new n
e.__super__=t.prototype
return e}
p=e("underscore")
r=e("backbone")
c=e("chaplin/lib/utils")
i=e("chaplin/lib/event_broker")
s=function(e,t,n){var a,i,o,s,p,u,h,d
a=c.beget(t)
null==n&&(n={})
n[e.cid]=!0
for(i in t){p=t[i]
if(p instanceof r.Model)a[i]=l(p,e,n)
else if(p instanceof r.Collection){s=[]
d=p.models
for(u=0,h=d.length;h>u;u++){o=d[u]
s.push(l(o,e,n))}a[i]=s}}delete n[e.cid]
return a}
l=function(e,t,n){var a
if(e===t||e.cid in n)return null
a="function"==typeof e.getAttributes?e.getAttributes():e.attributes
return s(e,a,n)}
a.exports=o=function(e){function t(){return t.__super__.constructor.apply(this,arguments)}h(t,e)
p.extend(t.prototype,i)
t.prototype.getAttributes=function(){return this.attributes}
t.prototype.serialize=function(){return s(this,this.getAttributes())}
t.prototype.disposed=!1
t.prototype.dispose=function(){var e,t,n,a
if(!this.disposed){this.trigger("dispose",this)
this.unsubscribeAllEvents()
this.stopListening()
this.off()
t=["collection","attributes","changed","_escapedAttributes","_previousAttributes","_silent","_pending","_callbacks"]
for(n=0,a=t.length;a>n;n++){e=t[n]
delete this[e]}this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return t}(r.Model)})
e.register("chaplin/views/layout",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u,h=function(e,t){return function(){return e.apply(t,arguments)}},d={}.hasOwnProperty,m=function(e,t){function n(){this.constructor=e}for(var a in t)d.call(t,a)&&(e[a]=t[a])
n.prototype=t.prototype
e.prototype=new n
e.__super__=t.prototype
return e}
u=e("underscore")
i=e("backbone")
c=e("chaplin/mediator")
p=e("chaplin/lib/utils")
o=e("chaplin/lib/event_broker")
l=e("chaplin/views/view")
r=i.$
a.exports=s=function(e){function t(e){null==e&&(e={})
this.openLink=h(this.openLink,this)
this.globalRegions=[]
this.title=e.title
e.regions&&(this.regions=e.regions)
this.settings=u.defaults(e,{"titleTemplate":function(e){var t
t=e.subtitle?""+e.subtitle+" \u2013 ":""
return t+e.title},"openExternalToBlank":!1,"routeLinks":"a, .go-to","skipRouting":".noscript","scrollTo":[0,0]})
c.setHandler("region:show",this.showRegion,this)
c.setHandler("region:register",this.registerRegionHandler,this)
c.setHandler("region:unregister",this.unregisterRegionHandler,this)
c.setHandler("region:find",this.regionByName,this)
c.setHandler("adjustTitle",this.adjustTitle,this)
t.__super__.constructor.apply(this,arguments)
this.settings.routeLinks&&this.startLinkRouting()}m(t,e)
t.prototype.el="body"
t.prototype.keepElement=!0
t.prototype.title=""
t.prototype.globalRegions=null
t.prototype.listen={"beforeControllerDispose mediator":"scroll"}
t.prototype.scroll=function(){var e
e=this.settings.scrollTo
return e?window.scrollTo(e[0],e[1]):void 0}
t.prototype.adjustTitle=function(e){var t,n=this
null==e&&(e="")
t=this.settings.titleTemplate({"title":this.title,"subtitle":e})
setTimeout(function(){document.title=t
return n.publishEvent("adjustTitle",e,t)},50)
return t}
t.prototype.startLinkRouting=function(){var e
e=this.settings.routeLinks
return e?r?this.$el.on("click",e,this.openLink):this.delegate("click",e,this.openLink):void 0}
t.prototype.stopLinkRouting=function(){var e
e=this.settings.routeLinks
return r?e?this.$el.off("click",e):void 0:this.undelegate("click",e,this.openLink)}
t.prototype.isExternalLink=function(e){var t,n
return"_blank"===e.target||"external"===e.rel||"http:"!==(t=e.protocol)&&"https:"!==t&&"file:"!==t||(n=e.hostname)!==location.hostname&&""!==n}
t.prototype.openLink=function(e){var t,n,a,o,s,l
if(!p.modifierKeyPressed(e)){t=r?e.currentTarget:e.delegateTarget
o="A"===t.nodeName
a=t.getAttribute("href")||t.getAttribute("data-href")||null
if(null!=a&&""!==a&&"#"!==a.charAt(0)){s=this.settings.skipRouting
l=typeof s
if(!("function"===l&&!s(a,t)||"string"===l&&(r?r(t).is(s):i.utils.matchesSelector(t,s)))){n=o&&this.isExternalLink(t)
if(n){if(this.settings.openExternalToBlank){e.preventDefault()
window.open(a)}}else{p.redirectTo({"url":a})
e.preventDefault()}}}}}
t.prototype.registerRegionHandler=function(e,t,n){return null!=t?this.registerGlobalRegion(e,t,n):this.registerGlobalRegions(e)}
t.prototype.registerGlobalRegion=function(e,t,n){this.unregisterGlobalRegion(e,t)
return this.globalRegions.unshift({"instance":e,"name":t,"selector":n})}
t.prototype.registerGlobalRegions=function(e){var t,n,a,r,i,o
o=p.getAllPropertyVersions(e,"regions")
for(r=0,i=o.length;i>r;r++){a=o[r]
for(t in a){n=a[t]
this.registerGlobalRegion(e,t,n)}}}
t.prototype.unregisterRegionHandler=function(e,t){return null!=t?this.unregisterGlobalRegion(e,t):this.unregisterGlobalRegions(e)}
t.prototype.unregisterGlobalRegion=function(e,t){var n,a
n=e.cid
return this.globalRegions=function(){var e,r,i,o
i=this.globalRegions
o=[]
for(e=0,r=i.length;r>e;e++){a=i[e];(a.instance.cid!==n||a.name!==t)&&o.push(a)}return o}.call(this)}
t.prototype.unregisterGlobalRegions=function(e){var t
return this.globalRegions=function(){var n,a,r,i
r=this.globalRegions
i=[]
for(n=0,a=r.length;a>n;n++){t=r[n]
t.instance.cid!==e.cid&&i.push(t)}return i}.call(this)}
t.prototype.regionByName=function(e){var t,n,a,r
r=this.globalRegions
for(n=0,a=r.length;a>n;n++){t=r[n]
if(t.name===e&&!t.instance.stale)return t}}
t.prototype.showRegion=function(e,t){var n
n=this.regionByName(e)
if(!n)throw new Error("No region registered under "+e)
return t.container=""===n.selector?r?n.instance.$el:n.instance.el:n.instance.noWrap?r?r(n.instance.container).find(n.selector):n.instance.container.querySelector(n.selector):n.instance[r?"$":"find"](n.selector)}
t.prototype.dispose=function(){var e,n,a,r
if(!this.disposed){this.stopLinkRouting()
r=["globalRegions","title","route"]
for(n=0,a=r.length;a>n;n++){e=r[n]
delete this[e]}c.removeHandlers(this)
return t.__super__.dispose.apply(this,arguments)}}
return t}(l)})
e.register("chaplin/views/view",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u,h,d,m={}.hasOwnProperty,f=function(e,t){function n(){this.constructor=e}for(var a in t)m.call(t,a)&&(e[a]=t[a])
n.prototype=t.prototype
e.prototype=new n
e.__super__=t.prototype
return e},_=[].indexOf||function(e){for(var t=0,n=this.length;n>t;t++)if(t in this&&this[t]===e)return t
return-1}
d=e("underscore")
i=e("backbone")
p=e("chaplin/mediator")
o=e("chaplin/lib/event_broker")
h=e("chaplin/lib/utils")
r=i.$
c=function(){return Function.prototype.bind?function(e,t){return e.bind(t)}:d.bind?d.bind:void 0}()
u=function(){return r?function(e,t){return e.html(t)}:function(e,t){return e.innerHTML=t}}()
l=function(){return r?function(e){var t
t=r(e.container)
return"function"==typeof e.containerMethod?e.containerMethod(t,e.el):t[e.containerMethod](e.el)}:function(e){var t
t="string"==typeof e.container?document.querySelector(e.container):e.container
return"function"==typeof e.containerMethod?e.containerMethod(t,e.el):t[e.containerMethod](e.el)}}()
a.exports=s=function(e){function t(e){var n,a,i,o,s=this
if(e)for(n in e){a=e[n]
_.call(this.optionNames,n)>=0&&(this[n]=a)}o=this.render
this.render=function(){if(s.disposed)return!1
o.apply(s,arguments)
s.autoAttach&&s.attach.apply(s,arguments)
return s}
this.subviews=[]
this.subviewsByName={}
if(this.noWrap){if(this.region){i=p.execute("region:find",this.region)
null!=i&&(this.el=null!=i.instance.container?null!=i.instance.region?r(i.instance.container).find(i.selector):i.instance.container:i.instance.$(i.selector))}this.container&&(this.el=this.container)}t.__super__.constructor.apply(this,arguments)
this.delegateListeners()
this.model&&this.listenTo(this.model,"dispose",this.dispose)
this.collection&&this.listenTo(this.collection,"dispose",function(e){return e&&e!==s.collection?void 0:s.dispose()})
null!=this.regions&&p.execute("region:register",this)
this.autoRender&&this.render()}f(t,e)
d.extend(t.prototype,o)
t.prototype.autoRender=!1
t.prototype.autoAttach=!0
t.prototype.container=null
t.prototype.containerMethod=r?"append":"appendChild"
t.prototype.regions=null
t.prototype.region=null
t.prototype.stale=!1
t.prototype.noWrap=!1
t.prototype.keepElement=!1
t.prototype.subviews=null
t.prototype.subviewsByName=null
t.prototype.optionNames=["autoAttach","autoRender","container","containerMethod","region","regions","noWrap"]
t.prototype.delegate=function(e,t,n){var a,r,o,s,l,p
if(i.utils)return i.utils.delegate(this,e,t,n)
if("string"!=typeof e)throw new TypeError("View#delegate: first argument must be a string")
if(2===arguments.length)s=t
else{if(3!==arguments.length)throw new TypeError("View#delegate: only two or three arguments are allowed")
p=t
if("string"!=typeof p)throw new TypeError("View#delegate: second argument must be a string")
s=n}if("function"!=typeof s)throw new TypeError("View#delegate: handler argument must be function")
l=function(){var t,n,a,i
a=e.split(" ")
i=[]
for(t=0,n=a.length;n>t;t++){r=a[t]
i.push(""+r+".delegate"+this.cid)}return i}.call(this)
o=l.join(" ")
a=c(s,this)
this.$el.on(o,p||null,a)
return a}
t.prototype._delegateEvents=function(e){var t,n,a,r,o,s,l
if(2===i.View.prototype.delegateEvents.length)return i.View.prototype.delegateEvents.call(this,e,!0)
for(r in e){l=e[r]
a="function"==typeof l?l:this[l]
if(!a)throw new Error("Method '"+l+"' does not exist")
o=r.match(/^(\S+)\s*(.*)$/)
n=""+o[1]+".delegateEvents"+this.cid
s=o[2]
t=c(a,this)
this.$el.on(n,s||null,t)}}
t.prototype.delegateEvents=function(e,t){var n,a,r,i
t||this.undelegateEvents()
if(e)return this._delegateEvents(e)
i=h.getAllPropertyVersions(this,"events")
for(a=0,r=i.length;r>a;a++){n=i[a]
if("function"==typeof n)throw new TypeError("View#delegateEvents: functions are not supported")
this._delegateEvents(n)}}
t.prototype.undelegate=function(e,t,n){var a,r,o,s,l
if(i.utils)return i.utils.undelegate(this,e,t,n)
if(e){if("string"!=typeof e)throw new TypeError("View#undelegate: first argument must be a string")
if(2===arguments.length)"string"==typeof t?l=t:o=t
else if(3===arguments.length){l=t
if("string"!=typeof l)throw new TypeError("View#undelegate: second argument must be a string")
o=n}s=function(){var t,n,r,i
r=e.split(" ")
i=[]
for(t=0,n=r.length;n>t;t++){a=r[t]
i.push(""+a+".delegate"+this.cid)}return i}.call(this)
r=s.join(" ")
return this.$el.off(r,l||null)}return this.$el.off(".delegate"+this.cid)}
t.prototype.delegateListeners=function(){var e,t,n,a,r,i,o,s,l
if(this.listen){s=h.getAllPropertyVersions(this,"listen")
for(i=0,o=s.length;o>i;i++){r=s[i]
for(t in r){n=r[t]
"function"!=typeof n&&(n=this[n])
if("function"!=typeof n)throw new Error("View#delegateListeners: "+(""+n+" must be function"))
l=t.split(" "),e=l[0],a=l[1]
this.delegateListener(e,a,n)}}}}
t.prototype.delegateListener=function(e,t,n){var a
if("model"===t||"collection"===t){a=this[t]
a&&this.listenTo(a,e,n)}else"mediator"===t?this.subscribeEvent(e,n):t||this.on(e,n,this)}
t.prototype.registerRegion=function(e,t){return p.execute("region:register",this,e,t)}
t.prototype.unregisterRegion=function(e){return p.execute("region:unregister",this,e)}
t.prototype.unregisterAllRegions=function(){return p.execute({"name":"region:unregister","silent":!0},this)}
t.prototype.subview=function(e,t){var n,a
a=this.subviews
n=this.subviewsByName
if(e&&t){this.removeSubview(e)
a.push(t)
n[e]=t
return t}return e?n[e]:void 0}
t.prototype.removeSubview=function(e){var t,n,a,r,i,o,s
if(e){o=this.subviews
t=this.subviewsByName
if("string"==typeof e){a=e
s=t[a]}else{s=e
for(r in t){i=t[r]
if(i===s){a=r
break}}}if(a&&s&&s.dispose){s.dispose()
n=h.indexOf(o,s);-1!==n&&o.splice(n,1)
return delete t[a]}}}
t.prototype.getTemplateData=function(){var e,t
e=this.model?h.serialize(this.model):this.collection?{"items":h.serialize(this.collection),"length":this.collection.length}:{}
t=this.model||this.collection
t&&("function"!=typeof t.isSynced||"synced"in e||(e.synced=t.isSynced()))
return e}
t.prototype.getTemplateFunction=function(){throw new Error("View#getTemplateFunction must be overridden")}
t.prototype.render=function(){var e,t,n
if(this.disposed)return!1
n=this.getTemplateFunction()
if("function"==typeof n){t=n(this.getTemplateData())
if(this.noWrap){e=document.createElement("div")
e.innerHTML=t
if(e.children.length>1)throw new Error("There must be a single top-level element when using `noWrap`.")
this.undelegateEvents()
this.setElement(e.firstChild,!0)}else u(r?this.$el:this.el,t)}return this}
t.prototype.attach=function(){null!=this.region&&p.execute("region:show",this.region,this)
if(this.container&&!document.body.contains(this.el)){l(this)
return this.trigger("addedToDOM")}}
t.prototype.disposed=!1
t.prototype.dispose=function(){var e,t,n,a,r,i,o,s
if(!this.disposed){this.unregisterAllRegions()
s=this.subviews
for(a=0,i=s.length;i>a;a++){n=s[a]
n.dispose()}this.unsubscribeAllEvents()
this.off()
if(this.keepElement){this.undelegateEvents()
this.undelegate()
this.stopListening()}else this.remove()
t=["el","$el","options","model","collection","subviews","subviewsByName","_callbacks"]
for(r=0,o=t.length;o>r;r++){e=t[r]
delete this[e]}this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return t}(i.View)})
e.register("chaplin/views/collection_view",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u,h,d,m,f,_=function(e,t){return function(){return e.apply(t,arguments)}},y={}.hasOwnProperty,v=function(e,t){function n(){this.constructor=e}for(var a in t)y.call(t,a)&&(e[a]=t[a])
n.prototype=t.prototype
e.prototype=new n
e.__super__=t.prototype
return e}
f=e("underscore")
i=e("backbone")
s=e("chaplin/views/view")
m=e("chaplin/lib/utils")
r=i.$
p=function(e,t){var n,a,r,o
if(!t)return e
o=[]
for(a=0,r=e.length;r>a;a++){n=e[a]
i.utils.matchesSelector(n,t)&&o.push(n)}return o}
d=function(){return r?function(e,t){return e.toggle(t)}:function(e,t){return e.style.display=t?"":"none"}}()
l=function(){return r?function(e,t){return e.addClass(t)}:function(e,t){return e.classList.add(t)}}()
h=function(){return r?function(e,t,n){return t?l(e,n):e.css("opacity",0)}:function(e,t,n){return t?l(e,n):e.style.opacity=0}}()
c=function(){return r?function(e,t){return e.animate({"opacity":1},t)}:function(e,t){e.style.transition="opacity "+t/1e3+"s"
return e.opacity=1}}()
u=function(){return r?function(e,t,n,a,r){var i,o,s,l,c
s=n>0&&a>n
l=function(e){return 0===e||n===e}
if(!s&&!r){c=l(a)?"append":"prepend"
return e[c](t)}i=e.children(r)
o=i.length
return i[n]!==t?l(o)?e.append(t):0===n?i.eq(n).before(t):i.eq(n-1).after(t):void 0}:function(e,t,n,a,r){var i,o,s,l,c
s=n>0&&a>n
l=function(e){return 0===e||n===e}
if(!s&&!r)return l(a)?e.appendChild(t):e.insertBefore(t,e.firstChild)
i=p(e.children,r)
o=i.length
if(i[n]!==t){if(l(o))return e.appendChild(t)
if(0===n)return e.insertBefore(t,i[n])
c=i[n-1]
return e.lastChild===c?e.appendChild(t):e.insertBefore(t,c.nextElementSibling)}}}()
a.exports=o=function(e){function t(){this.renderAllItems=_(this.renderAllItems,this)
this.toggleFallback=_(this.toggleFallback,this)
this.itemsReset=_(this.itemsReset,this)
this.itemRemoved=_(this.itemRemoved,this)
this.itemAdded=_(this.itemAdded,this)
this.visibleItems=[]
t.__super__.constructor.apply(this,arguments)}v(t,e)
t.prototype.itemView=null
t.prototype.autoRender=!0
t.prototype.renderItems=!0
t.prototype.animationDuration=500
t.prototype.useCssAnimation=!1
t.prototype.animationStartClass="animated-item-view"
t.prototype.animationEndClass="animated-item-view-end"
t.prototype.listSelector=null
t.prototype.$list=null
t.prototype.fallbackSelector=null
t.prototype.$fallback=null
t.prototype.loadingSelector=null
t.prototype.$loading=null
t.prototype.itemSelector=void 0
t.prototype.filterer=null
t.prototype.filterCallback=function(e,t){r&&e.$el.stop(!0,!0)
return d(r?e.$el:e.el,t)}
t.prototype.visibleItems=null
t.prototype.optionNames=s.prototype.optionNames.concat(["renderItems","itemView"])
t.prototype.initialize=function(e){null==e&&(e={})
this.addCollectionListeners()
return null!=e.filterer?this.filter(e.filterer):void 0}
t.prototype.addCollectionListeners=function(){this.listenTo(this.collection,"add",this.itemAdded)
this.listenTo(this.collection,"remove",this.itemRemoved)
return this.listenTo(this.collection,"reset sort",this.itemsReset)}
t.prototype.getTemplateData=function(){var e
e={"length":this.collection.length}
"function"==typeof this.collection.isSynced&&(e.synced=this.collection.isSynced())
return e}
t.prototype.getTemplateFunction=function(){}
t.prototype.render=function(){var e
t.__super__.render.apply(this,arguments)
e=f.result(this,"listSelector")
r?this.$list=e?this.$(e):this.$el:this.list=e?this.find(this.listSelector):this.el
this.initFallback()
this.initLoadingIndicator()
return this.renderItems?this.renderAllItems():void 0}
t.prototype.itemAdded=function(e,t,n){return this.insertView(e,this.renderItem(e),n.at)}
t.prototype.itemRemoved=function(e){return this.removeViewForItem(e)}
t.prototype.itemsReset=function(){return this.renderAllItems()}
t.prototype.initFallback=function(){if(this.fallbackSelector){r?this.$fallback=this.$(this.fallbackSelector):this.fallback=this.find(this.fallbackSelector)
this.on("visibilityChange",this.toggleFallback)
this.listenTo(this.collection,"syncStateChange",this.toggleFallback)
return this.toggleFallback()}}
t.prototype.toggleFallback=function(){var e
e=0===this.visibleItems.length&&("function"==typeof this.collection.isSynced?this.collection.isSynced():!0)
return d(r?this.$fallback:this.fallback,e)}
t.prototype.initLoadingIndicator=function(){if(this.loadingSelector&&"function"==typeof this.collection.isSyncing){r?this.$loading=this.$(this.loadingSelector):this.loading=this.find(this.loadingSelector)
this.listenTo(this.collection,"syncStateChange",this.toggleLoadingIndicator)
return this.toggleLoadingIndicator()}}
t.prototype.toggleLoadingIndicator=function(){var e
e=0===this.collection.length&&this.collection.isSyncing()
return d(r?this.$loading:this.loading,e)}
t.prototype.getItemViews=function(){var e,t,n,a
e={}
if(this.subviews.length>0){a=this.subviewsByName
for(t in a){n=a[t]
"itemView:"===t.slice(0,9)&&(e[t.slice(9)]=n)}}return e}
t.prototype.filter=function(e,t){var n,a,r,i,o,s,l,c,p=this;("function"==typeof e||null===e)&&(this.filterer=e);("function"==typeof t||null===t)&&(this.filterCallback=t)
n=function(){var e
if(p.subviews.length>0)for(e in p.subviewsByName)if("itemView:"===e.slice(0,9))return!0
return!1}()
if(n){c=this.collection.models
for(r=s=0,l=c.length;l>s;r=++s){i=c[r]
a="function"==typeof this.filterer?this.filterer(i,r):!0
o=this.subview("itemView:"+i.cid)
if(!o)throw new Error("CollectionView#filter: no view found for "+i.cid)
this.filterCallback(o,a)
this.updateVisibleItems(o.model,a,!1)}}return this.trigger("visibilityChange",this.visibleItems)}
t.prototype.renderAllItems=function(){var e,t,n,a,r,i,o,s,l,c,p
a=this.collection.models
this.visibleItems=[]
r={}
for(o=0,l=a.length;l>o;o++){n=a[o]
i=this.subview("itemView:"+n.cid)
i&&(r[n.cid]=i)}p=this.getItemViews()
for(e in p)if(y.call(p,e)){i=p[e]
e in r||this.removeSubview("itemView:"+e)}for(t=s=0,c=a.length;c>s;t=++s){n=a[t]
i=this.subview("itemView:"+n.cid)
i?this.insertView(n,i,t,!1):this.insertView(n,this.renderItem(n),t)}return 0===a.length?this.trigger("visibilityChange",this.visibleItems):void 0}
t.prototype.renderItem=function(e){var t
t=this.subview("itemView:"+e.cid)
if(!t){t=this.initItemView(e)
this.subview("itemView:"+e.cid,t)}t.render()
return t}
t.prototype.initItemView=function(e){if(this.itemView)return new this.itemView({"autoRender":!1,"model":e})
throw new Error("The CollectionView#itemView property must be defined or the initItemView() must be overridden.")}
t.prototype.insertView=function(e,t,n,a){var i,o,s,p,d=this
null==a&&(a=!0)
0===this.animationDuration&&(a=!1)
"number"!=typeof n&&(n=this.collection.indexOf(e))
o="function"==typeof this.filterer?this.filterer(e,n):!0
i=r?t.$el:t.el
o&&a&&h(i,this.useCssAnimation,this.animationStartClass)
this.filterer&&this.filterCallback(t,o)
s=this.collection.length
p=r?this.$list:this.list
u(p,i,n,s,this.itemSelector)
t.trigger("addedToParent")
this.updateVisibleItems(e,o)
o&&a&&(this.useCssAnimation?setTimeout(function(){return l(i,d.animationEndClass)},0):c(i,this.animationDuration))
return t}
t.prototype.removeViewForItem=function(e){this.updateVisibleItems(e,!1)
return this.removeSubview("itemView:"+e.cid)}
t.prototype.updateVisibleItems=function(e,t,n){var a,r,i
null==n&&(n=!0)
r=!1
i=m.indexOf(this.visibleItems,e)
a=-1!==i
if(t&&!a){this.visibleItems.push(e)
r=!0}else if(!t&&a){this.visibleItems.splice(i,1)
r=!0}r&&n&&this.trigger("visibilityChange",this.visibleItems)
return r}
t.prototype.dispose=function(){var e,n,a,r
if(!this.disposed){n=["$list","$fallback","$loading","visibleItems"]
for(a=0,r=n.length;r>a;a++){e=n[a]
delete this[e]}return t.__super__.dispose.apply(this,arguments)}}
return t}(s)})
e.register("chaplin/lib/route",function(t,n,a){"use strict"
var r,i,o,s,l,c,p=function(e,t){return function(){return e.apply(t,arguments)}},u={}.hasOwnProperty
c=e("underscore")
r=e("backbone")
o=e("chaplin/lib/event_broker")
i=e("chaplin/controllers/controller")
l=e("chaplin/lib/utils")
a.exports=s=function(){function e(e,t,n,a){var r
this.pattern=e
this.controller=t
this.action=n
this.handler=p(this.handler,this)
this.replaceParams=p(this.replaceParams,this)
this.parseOptionalPortion=p(this.parseOptionalPortion,this)
if("string"!=typeof this.pattern)throw new Error("Route: RegExps are not supported.        Use strings with :names and `constraints` option of route")
this.options=a?c.extend({},a):{}
null!=this.options.name&&(this.name=this.options.name)
if(this.name&&-1!==this.name.indexOf("#"))throw new Error('Route: "#" cannot be used in name')
null==(r=this.name)&&(this.name=this.controller+"#"+this.action)
this.allParams=[]
this.requiredParams=[]
this.optionalParams=[]
if(this.action in i.prototype)throw new Error("Route: You should not use existing controller properties as action names")
this.createRegExp()
"function"==typeof Object.freeze&&Object.freeze(this)}var t,n,a,s
e.extend=r.Model.extend
c.extend(e.prototype,o)
t=/[\-{}\[\]+?.,\\\^$|#\s]/g
n=/\((.*?)\)/g
a=/(?::|\*)(\w+)/g
s=function(e,t){switch(t){case!0:"/"!==e.slice(-1)&&(e+="/")
break
case!1:"/"===e.slice(-1)&&(e=e.slice(0,-1))}return e}
e.prototype.matches=function(e){var t,n,a,r,i,o,s
if("string"==typeof e)return e===this.name
a=0
s=["name","action","controller"]
for(i=0,o=s.length;o>i;i++){n=s[i]
a++
r=e[n]
if(r&&r!==this[n])return!1}t=1===a&&("action"===n||"controller"===n)
return!t}
e.prototype.reverse=function(e,t){var a,r,i,o,c,p,u,h,d,m
e=this.normalizeParams(e)
if(e===!1)return!1
i=this.pattern
d=this.requiredParams
for(c=0,u=d.length;u>c;c++){a=d[c]
o=e[a]
i=i.replace(RegExp("[:*]"+a,"g"),o)}m=this.optionalParams
for(p=0,h=m.length;h>p;p++){a=m[p];(o=e[a])&&(i=i.replace(RegExp("[:*]"+a,"g"),o))}i=i.replace(n,function(e,t){return t.match(/[:*]/g)?"":t})
i=s(i,this.options.trailing)
if(!t)return i
if("object"==typeof t){r=l.queryParams.stringify(t)
return i+=r?"?"+r:""}return i+=("?"===t[0]?"":"?")+t}
e.prototype.normalizeParams=function(e){var t,n,a,r,i,o
if(l.isArray(e)){if(e.length<this.requiredParams.length)return!1
a={}
o=this.requiredParams
for(t=r=0,i=o.length;i>r;t=++r){n=o[t]
a[n]=e[t]}if(!this.testConstraints(a))return!1
e=a}else{null==e&&(e={})
if(!this.testParams(e))return!1}return e}
e.prototype.testConstraints=function(e){var t,n,a
n=this.options.constraints
if(n)for(a in n)if(u.call(n,a)){t=n[a]
if(!t.test(e[a]))return!1}return!0}
e.prototype.testParams=function(e){var t,n,a,r
r=this.requiredParams
for(n=0,a=r.length;a>n;n++){t=r[n]
if(void 0===e[t])return!1}return this.testConstraints(e)}
e.prototype.createRegExp=function(){var e,a=this
e=this.pattern
e=e.replace(t,"\\$&")
this.replaceParams(e,function(e,t){return a.allParams.push(t)})
e=e.replace(n,this.parseOptionalPortion)
e=this.replaceParams(e,function(e,t){a.requiredParams.push(t)
return a.paramCapturePattern(e)})
return this.regExp=RegExp("^"+e+"(?=\\/?(\\?|$))")}
e.prototype.parseOptionalPortion=function(e,t){var n,a=this
n=this.replaceParams(t,function(e,t){a.optionalParams.push(t)
return a.paramCapturePattern(e)})
return"(?:"+n+")?"}
e.prototype.replaceParams=function(e,t){return e.replace(a,t)}
e.prototype.paramCapturePattern=function(e){return":"===e.charAt(0)?"([^/?]+)":"(.*?)"}
e.prototype.test=function(e){var t,n
n=this.regExp.test(e)
if(!n)return!1
t=this.options.constraints
return t?this.testConstraints(this.extractParams(e)):!0}
e.prototype.handler=function(e,t){var n,a,r,i,o,p
t=t?c.extend({},t):{}
if("object"==typeof e){i=l.queryParams.stringify(t.query)
a=e
r=this.reverse(a)}else{p=e.split("?"),r=p[0],i=p[1]
null==i?i="":t.query=l.queryParams.parse(i)
a=this.extractParams(r)
r=s(r,this.options.trailing)}n=c.extend({},a,this.options.params)
o={"path":r,"action":this.action,"controller":this.controller,"name":this.name,"query":i}
return this.publishEvent("router:match",o,n,t)}
e.prototype.extractParams=function(e){var t,n,a,r,i,o,s,l
i={}
a=this.regExp.exec(e)
l=a.slice(1)
for(t=o=0,s=l.length;s>o;t=++o){n=l[t]
r=this.allParams.length?this.allParams[t]:t
i[r]=n}return i}
return e}()})
e.register("chaplin/lib/router",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u,h=function(e,t){return function(){return e.apply(t,arguments)}}
u=e("underscore")
r=e("backbone")
c=e("chaplin/mediator")
i=e("chaplin/lib/event_broker")
o=e("chaplin/lib/history")
s=e("chaplin/lib/route")
p=e("chaplin/lib/utils")
a.exports=l=function(){function e(e){var t
this.options=null!=e?e:{}
this.match=h(this.match,this)
t="file:"!==window.location.protocol
u.defaults(this.options,{"pushState":t,"root":"/","trailing":!1})
this.removeRoot=new RegExp("^"+p.escapeRegExp(this.options.root)+"(#)?")
this.subscribeEvent("!router:route",this.oldEventError)
this.subscribeEvent("!router:routeByName",this.oldEventError)
this.subscribeEvent("!router:changeURL",this.oldURLEventError)
this.subscribeEvent("dispatcher:dispatch",this.changeURL)
c.setHandler("router:route",this.route,this)
c.setHandler("router:reverse",this.reverse,this)
this.createHistory()}e.extend=r.Model.extend
u.extend(e.prototype,i)
e.prototype.oldEventError=function(){throw new Error("!router:route and !router:routeByName events were removed.  Use `Chaplin.utils.redirectTo`")}
e.prototype.oldURLEventError=function(){throw new Error("!router:changeURL event was removed.")}
e.prototype.createHistory=function(){return r.history=new o}
e.prototype.startHistory=function(){return r.history.start(this.options)}
e.prototype.stopHistory=function(){return r.History.started?r.history.stop():void 0}
e.prototype.findHandler=function(e){var t,n,a,i
i=r.history.handlers
for(n=0,a=i.length;a>n;n++){t=i[n]
if(e(t))return t}}
e.prototype.match=function(e,t,n){var a,i,o,l
null==n&&(n={})
if(2===arguments.length&&"object"==typeof t){n=t
i=n.controller,a=n.action
if(!i||!a)throw new Error("Router#match must receive either target or options.controller & options.action")}else{i=n.controller,a=n.action
if(i||a)throw new Error("Router#match cannot use both target and options.controller / options.action")
l=t.split("#"),i=l[0],a=l[1]}u.defaults(n,{"trailing":this.options.trailing})
o=new s(e,i,a,n)
r.history.handlers.push({"route":o,"callback":o.handler})
return o}
e.prototype.route=function(e,t,n){var a,r
if("object"==typeof e){r=e.url
!t&&e.params&&(t=e.params)}t=t?p.isArray(t)?t.slice():u.extend({},t):{}
if(null!=r){r=r.replace(this.removeRoot,"")
a=this.findHandler(function(e){return e.route.test(r)})
n=t
t=null}else{n=n?u.extend({},n):{}
a=this.findHandler(function(n){if(n.route.matches(e)){t=n.route.normalizeParams(t)
if(t)return!0}return!1})}if(a){u.defaults(n,{"changeURL":!0})
a.callback(r||t,n)
return!0}throw new Error("Router#route: request was not routed")}
e.prototype.reverse=function(e,t,n){var a,i,o,s,l,c,p
s=this.options.root
if(null!=t&&"object"!=typeof t)throw new TypeError("Router#reverse: params must be an array or an object")
i=r.history.handlers
for(c=0,p=i.length;p>c;c++){a=i[c]
if(a.route.matches(e)){o=a.route.reverse(t,n)
if(o!==!1){l=s?s+o:o
return l}}}throw new Error("Router#reverse: invalid route specified")}
e.prototype.changeURL=function(e,t,n,a){var i,o
if(null!=n.path&&a.changeURL){o=n.path+(n.query?"?"+n.query:"")
i={"trigger":a.trigger===!0,"replace":a.replace===!0}
return r.history.navigate(o,i)}}
e.prototype.disposed=!1
e.prototype.dispose=function(){if(!this.disposed){this.stopHistory()
delete r.history
this.unsubscribeAllEvents()
c.removeHandlers(this)
this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return e}()})
e.register("chaplin/lib/history",function(t,n,a){"use strict"
var r,i,o,s,l,c,p,u={}.hasOwnProperty,h=function(e,t){function n(){this.constructor=e}for(var a in t)u.call(t,a)&&(e[a]=t[a])
n.prototype=t.prototype
e.prototype=new n
e.__super__=t.prototype
return e}
p=e("underscore")
r=e("backbone")
l=/^[#\/]|\s+$/g
s=/^\/+|\/+$/g
o=/msie [\w.]+/
c=/\/$/
i=function(e){function t(){return t.__super__.constructor.apply(this,arguments)}h(t,e)
t.prototype.getFragment=function(e,t){var n
if(null==e)if(this._hasPushState||!this._wantsHashChange||t){e=this.location.pathname+this.location.search
n=this.root.replace(c,"")
e.indexOf(n)||(e=e.substr(n.length))}else e=this.getHash()
return e.replace(l,"")}
t.prototype.start=function(e){var t,n,a
if(r.History.started)throw new Error("Backbone.history has already been started")
r.History.started=!0
this.options=p.extend({},{"root":"/"},this.options,e)
this.root=this.options.root
this._wantsHashChange=this.options.hashChange!==!1
this._wantsPushState=Boolean(this.options.pushState)
this._hasPushState=Boolean(this.options.pushState&&this.history&&this.history.pushState)
n=this.getFragment()
this.root=("/"+this.root+"/").replace(s,"/")
this._hasPushState?r.$(window).on("popstate",this.checkUrl):this._wantsHashChange&&"onhashchange"in window?r.$(window).on("hashchange",this.checkUrl):this._wantsHashChange&&(this._checkUrlInterval=setInterval(this.checkUrl,this.interval))
this.fragment=n
a=this.location
t=a.pathname.replace(/[^\/]$/,"$&/")===this.root
if(this._wantsHashChange&&this._wantsPushState&&!this._hasPushState&&!t){this.fragment=this.getFragment(null,!0)
this.location.replace(this.root+"#"+this.fragment)
return!0}if(this._wantsPushState&&this._hasPushState&&t&&a.hash){this.fragment=this.getHash().replace(l,"")
this.history.replaceState({},document.title,this.root+this.fragment)}return this.options.silent?void 0:this.loadUrl()}
t.prototype.navigate=function(e,t){var n,a,i
null==e&&(e="")
if(!r.History.started)return!1
t&&t!==!0||(t={"trigger":t})
e=this.getFragment(e)
i=this.root+e
if(this.fragment===e)return!1
this.fragment=e
0===e.length&&"/"!==i&&(i=i.slice(0,-1))
if(this._hasPushState){n=t.replace?"replaceState":"pushState"
this.history[n]({},document.title,i)}else{if(!this._wantsHashChange)return this.location.assign(i)
this._updateHash(this.location,e,t.replace)
a=e!==this.getFragment(this.getHash(this.iframe))
if(null!=this.iframe&&a){t.replace||this.iframe.document.open().close()
this._updateHash(this.iframe.location,e,t.replace)}}return t.trigger?this.loadUrl(e):void 0}
return t}(r.History)
a.exports=r.$?i:r.History})
e.register("chaplin/lib/event_broker",function(t,n,a){"use strict"
var r,i,o=[].slice
i=e("chaplin/mediator")
r={"subscribeEvent":function(e,t){if("string"!=typeof e)throw new TypeError("EventBroker#subscribeEvent: type argument must be a string")
if("function"!=typeof t)throw new TypeError("EventBroker#subscribeEvent: handler argument must be a function")
i.unsubscribe(e,t,this)
return i.subscribe(e,t,this)},"unsubscribeEvent":function(e,t){if("string"!=typeof e)throw new TypeError("EventBroker#unsubscribeEvent: type argument must be a string")
if("function"!=typeof t)throw new TypeError("EventBroker#unsubscribeEvent: handler argument must be a function")
return i.unsubscribe(e,t)},"unsubscribeAllEvents":function(){return i.unsubscribe(null,null,this)},"publishEvent":function(){var e,t
t=arguments[0],e=2<=arguments.length?o.call(arguments,1):[]
if("string"!=typeof t)throw new TypeError("EventBroker#publishEvent: type argument must be a string")
return i.publish.apply(i,[t].concat(o.call(e)))}}
"function"==typeof Object.freeze&&Object.freeze(r)
a.exports=r})
e.register("chaplin/lib/support",function(e,t,n){"use strict"
var a
a={"propertyDescriptors":function(){var e
if("function"!=typeof Object.defineProperty||"function"!=typeof Object.defineProperties)return!1
try{e={}
Object.defineProperty(e,"foo",{"value":"bar"})
return"bar"===e.foo}catch(t){return!1}}()}
n.exports=a})
e.register("chaplin/lib/composition",function(t,n,a){"use strict"
var r,i,o,s,l,c={}.hasOwnProperty
l=e("underscore")
r=e("backbone")
o=e("chaplin/lib/event_broker")
s=Object.prototype.hasOwnProperty
a.exports=i=function(){function e(e){null!=e&&(this.options=l.extend({},e))
this.item=this
this.initialize(this.options)}e.extend=r.Model.extend
l.extend(e.prototype,r.Events)
l.extend(e.prototype,o)
e.prototype.item=null
e.prototype.options=null
e.prototype._stale=!1
e.prototype.initialize=function(){}
e.prototype.compose=function(){}
e.prototype.check=function(e){return l.isEqual(this.options,e)}
e.prototype.stale=function(e){var t,n
if(null==e)return this._stale
this._stale=e
for(n in this){t=this[n]
t&&t!==this&&"object"==typeof t&&s.call(t,"stale")&&(t.stale=e)}}
e.prototype.disposed=!1
e.prototype.dispose=function(){var e,t,n,a,r
if(!this.disposed){for(t in this)if(c.call(this,t)){e=this[t]
if(e&&"function"==typeof e.dispose&&e!==this){e.dispose()
delete this[t]}}this.unsubscribeAllEvents()
this.stopListening()
n=["redirected"]
for(a=0,r=n.length;r>a;a++){t=n[a]
delete this[t]}this.disposed=!0
return"function"==typeof Object.freeze?Object.freeze(this):void 0}}
return e}()})
e.register("chaplin/lib/sync_machine",function(e,t,n){"use strict"
var a,r,i,o,s,l,c,p,u,h
s="unsynced"
i="syncing"
r="synced"
a="syncStateChange"
o={"_syncState":s,"_previousSyncState":null,"syncState":function(){return this._syncState},"isUnsynced":function(){return this._syncState===s},"isSynced":function(){return this._syncState===r},"isSyncing":function(){return this._syncState===i},"unsync":function(){var e
if((e=this._syncState)===i||e===r){this._previousSync=this._syncState
this._syncState=s
this.trigger(this._syncState,this,this._syncState)
this.trigger(a,this,this._syncState)}},"beginSync":function(){var e
if((e=this._syncState)===s||e===r){this._previousSync=this._syncState
this._syncState=i
this.trigger(this._syncState,this,this._syncState)
this.trigger(a,this,this._syncState)}},"finishSync":function(){if(this._syncState===i){this._previousSync=this._syncState
this._syncState=r
this.trigger(this._syncState,this,this._syncState)
this.trigger(a,this,this._syncState)}},"abortSync":function(){if(this._syncState===i){this._syncState=this._previousSync
this._previousSync=this._syncState
this.trigger(this._syncState,this,this._syncState)
this.trigger(a,this,this._syncState)}}}
h=[s,i,r,a]
c=function(e){return o[e]=function(t,n){null==n&&(n=this)
this.on(e,t,n)
return this._syncState===e?t.call(n):void 0}}
for(p=0,u=h.length;u>p;p++){l=h[p]
c(l)}"function"==typeof Object.freeze&&Object.freeze(o)
n.exports=o})
e.register("chaplin/lib/utils",function(t,n,a){"use strict"
var r,i,o,s=[].slice,l=[].indexOf||function(e){for(var t=0,n=this.length;n>t;t++)if(t in this&&this[t]===e)return t
return-1},c={}.hasOwnProperty
o=e("underscore")
r=e("chaplin/lib/support")
i={"beget":function(){var e
if("function"==typeof Object.create)return Object.create
e=function(){}
return function(t){e.prototype=t
return new e}}(),"indexOf":function(){return Array.prototype.indexOf?function(e,t){return e.indexOf(t)}:o.indexOf?o.indexOf:void 0}(),"isArray":Array.isArray||o.isArray,"serialize":function(e){if("function"==typeof e.serialize)return e.serialize()
if("function"==typeof e.toJSON)return e.toJSON()
throw new TypeError("utils.serialize: Unknown data was passed")},"readonly":function(){var e
if(r.propertyDescriptors){e={"writable":!1,"enumerable":!0,"configurable":!1}
return function(){var t,n,a,r,i
t=arguments[0],a=2<=arguments.length?s.call(arguments,1):[]
for(r=0,i=a.length;i>r;r++){n=a[r]
e.value=t[n]
Object.defineProperty(t,n,e)}return!0}}return function(){return!1}}(),"getPrototypeChain":function(e){var t,n,a,r
t=[e.constructor.prototype]
for(;e=null!=(n=null!=(a=e.constructor)?a.__super__:void 0)?n:null!=(r=e.constructor)?r.superclass:void 0;)t.push(e)
return t.reverse()},"getAllPropertyVersions":function(e,t){var n,a,r,o,s,c
a=[]
c=i.getPrototypeChain(e)
for(o=0,s=c.length;s>o;o++){n=c[o]
r=n[t]
r&&l.call(a,r)<0&&a.push(r)}return a},"upcase":function(e){return e.charAt(0).toUpperCase()+e.substring(1)},"escapeRegExp":function(e){return String(e||"").replace(/([.*+?^=!:${}()|[\]\/\\])/g,"\\$1")},"modifierKeyPressed":function(e){return e.shiftKey||e.altKey||e.ctrlKey||e.metaKey},"reverse":function(t,n,a){return e("chaplin/mediator").execute("router:reverse",t,n,a)},"redirectTo":function(t,n,a){return e("chaplin/mediator").execute("router:route",t,n,a)},"queryParams":{"stringify":function(e){var t,n,a,r,o,s,l,p
r=""
o=function(e,t){return null!=t?"&"+e+"="+encodeURIComponent(t):""}
for(a in e)if(c.call(e,a)){s=e[a]
n=encodeURIComponent(a)
if(i.isArray(s))for(l=0,p=s.length;p>l;l++){t=s[l]
r+=o(n,t)}else r+=o(n,s)}return r&&r.substring(1)},"parse":function(e){var t,n,a,r,i,o,s,l,c
i={}
if(!e)return i
r=e.split("&")
for(s=0,l=r.length;l>s;s++){a=r[s]
if(a.length){c=a.split("="),n=c[0],o=c[1]
if(n.length){n=decodeURIComponent(n)
o=decodeURIComponent(o)
t=i[n]
t?t.push?t.push(o):i[n]=[t,o]:i[n]=o}}}return i}}}
"function"==typeof Object.seal&&Object.seal(i)
a.exports=i})
e.register("chaplin",function(t,n,a){a.exports={"Application":e("chaplin/application"),"mediator":e("chaplin/mediator"),"Dispatcher":e("chaplin/dispatcher"),"Controller":e("chaplin/controllers/controller"),"Composer":e("chaplin/composer"),"Composition":e("chaplin/lib/composition"),"Collection":e("chaplin/models/collection"),"Model":e("chaplin/models/model"),"Layout":e("chaplin/views/layout"),"View":e("chaplin/views/view"),"CollectionView":e("chaplin/views/collection_view"),"Route":e("chaplin/lib/route"),"Router":e("chaplin/lib/router"),"EventBroker":e("chaplin/lib/event_broker"),"support":e("chaplin/lib/support"),"SyncMachine":e("chaplin/lib/sync_machine"),"utils":e("chaplin/lib/utils")}})
var t=function(t,n){e.register("backbone",function(e,n,a){a.exports=t})
e.register("underscore",function(e,t,a){a.exports=n})}
if("function"==typeof define&&define.amd)define(["backbone","underscore"],function(n,a){t(n,a)
return e("chaplin")})
else if("object"==typeof module&&module&&module.exports){t(require("backbone"),require("underscore"))
module.exports=e("chaplin")}else{if("function"!=typeof require)throw new Error("Chaplin requires Common.js or AMD modules")
t(window.Backbone,window._||window.Backbone.utils)
window.Chaplin=e("chaplin")}}();(function(){Banana.routes=function(e){e("","home#index")
e("hto","home_try_on#index")
e("hto/account","home_try_on#account")
e("hto/gender","home_try_on#gender")
e("hto/size","home_try_on#size")
e("hto/product/:index","home_try_on#product")
e("hto/checkout/shipping","home_try_on#checkoutShipping")
e("hto/checkout/payment","home_try_on#checkoutPayment")
e("hto/checkout/confirm","home_try_on#checkoutConfirm")
e("hto/thanks","home_try_on#thanks")
e("hometryon/faq","home_try_on#help")
e("hto/no-items","home_try_on#noItems")
e("hto/faq","home_try_on#help")
e("account","account#info")
e("account/info","account#info")
e("account/orders","account#orders")
e("account/returns","account#returns")
e("account/billing","account#billing")
e("account/address","account#address")
e("collections/:permalink","collections#show",{"name":"collection"})
e("redeem","giftcards#redeem")
e("redeem/:token","giftcards#redeem")
e("collections/:permalink/products/:id","products#show")
e("products/:id","products#show")
e("factories","factories#index")
e("factories/:permalink","factories#details")
e("about","pages#about")
e("help","pages#help")
e("invite","pages#invite")
e("coming-soon(/:gender)(/:name)","coming_soon#index",{"trailing":!1})
e("open-studio/:permalink","open_studios#show")
e("checkout/review","checkout#review")
e("checkout/thanks","checkout#thanks")
e("delivery","delivery#index")
e("nycdelivery","delivery#index")
e("nyc-delivery","delivery#index")
return e("landing_page","landing_page#show")}}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.Model=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
_(n.prototype).extend(Chaplin.SyncMachine)
n.prototype.fetch=function(e){var t,a
null==e&&(e={})
a=e.success
e.success=function(e){return function(t,n){"function"==typeof a&&a(t,n)
return e.finishSync()}}(this)
t=e.error
e.error=function(e){return function(n,a){e.trigger("sync_fail")
"function"==typeof t&&t(n,a)
return e.abortSync()}}(this)
this.beginSync()
return n.__super__.fetch.call(this,e)}
n.prototype.fetchOnce=function(e){null==e&&(e={})
return this.isSynced()?void 0:this.fetch(e)}
n.prototype.sync=function(e,t,a){"patch"===e&&(a.type="PUT")
return n.__super__.sync.call(this,e,t,a)}
n.prototype.parse=function(e){return e.code&&e.message?e.data:e}
return n}(Chaplin.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.Collection=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.Model
_(n.prototype).extend(Chaplin.SyncMachine)
n.prototype.fetch=function(e){var t,a
null==e&&(e={})
this.beginSync()
a=e.success
e.success=function(e){return function(t,n){"function"==typeof a&&a(t,n)
return e.finishSync()}}(this)
t=e.error
e.error=function(e){return function(n,a){"function"==typeof t&&t(n,a)
return e.abortSync()}}(this)
return n.__super__.fetch.call(this,e)}
n.prototype.fetchOnce=function(e){null==e&&(e={})
return this.isSynced()?void 0:this.fetch(e)}
n.prototype.parse=Banana.Model.prototype.parse
return n}(Chaplin.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Menu=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Menus=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Menu
n.prototype.url="/api/menus"
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.View=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.autoRender=!0
n.prototype.optionNames=Chaplin.View.prototype.optionNames.concat(["template"])
n.prototype.helpers={"lowerCase":function(e){return e.toLowerCase()},"staticImageUrl":function(e,t){return E.lib.ImageHelper.imageUrl(e,t.hash)},"formatPrice":function(e){_.isObject(e)&&(e=e[E.session.getCountry()])
return"$"+e},"videoUrl":function(e){return E.lib.VideoHelper.videoUrl(e)}}
n.prototype.getTemplateFunction=function(){var e,t
e=function(e){return _.isFunction(e)?e:HandlebarsTemplates[e]||function(){throw new Error('Missing template "'+e+'"')}()}
t=e(this.template)
return function(n){return function(a,r){var i,o,s,l,c,p,u,h,d,m
null==r&&(r={})
r.partials||(r.partials={})
d=Chaplin.utils.getAllPropertyVersions(n,"partials")
for(c=0,u=d.length;u>c;c++){s=d[c]
for(o in s){l=s[o]
r.partials[o]=e(l)}}r.helpers||(r.helpers={})
m=Chaplin.utils.getAllPropertyVersions(n,"helpers")
for(p=0,h=m.length;h>p;p++){i=m[p]
for(o in i){l=i[o]
r.helpers[o]=l}}return t(a,r)}}(this)}
return n}(Chaplin.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Alert=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/alerts"
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
e=$.extend({"dismissible":!0,"flash":!1,"priority":0},e)
this.set("dismissible",e.dismissible)
this.set("flash",e.flash)
return this.set("priority",e.priority)}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Alerts=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Alert
n.prototype.url="/api/alerts"
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.CollectionView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.helpers=Banana.View.prototype.helpers
n.prototype.getTemplateFunction=Banana.View.prototype.getTemplateFunction
n.prototype.toggleLoadingIndicator=function(){var e
e=0===this.collection.length&&this.collection.isSyncing()
return this.$loading.toggle(e)}
return n}(Chaplin.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/alert_view"]=Handlebars.template({"1":function(){return'  <a href="javascript:;" class="alerts__close">&#215</a>\n'},"3":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l='  <h3 class="alerts__heading">\n    '
r=(i=null!=(i=t.title||(null!=e?e.title:e))?i:s,typeof i===o?i.call(e,{"name":"title","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+"\n  </h3>\n"},"5":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l='  <hr class="alerts__divider">\n\n  <p class="alerts__copy">\n    '
r=(i=null!=(i=t.body||(null!=e?e.body:e))?i:s,typeof i===o?i.call(e,{"name":"body","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+"\n  </p>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.dismissible:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.title:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n\n"
r=t["if"].call(e,null!=e?e.body:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/alert_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.AlertView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="alerts__item"
n.prototype.template="banana/templates/components/alert_view"
n.prototype.events={"click .alerts__close":function(){return this.model.destroy()}}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
this.$el.addClass(this.model.get("class"))
return this.subscribeEvent("router:match",function(e){return function(){return e.model.get("flash")?e.model.destroy():void 0}}(this))}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/alert_list_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/alert_list_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.AlertListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.itemView=Banana.views.components.AlertView
n.prototype.template="banana/templates/components/alert_list_view"
n.prototype.tagName="ul"
n.prototype.className="alerts"
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/fixed_navigation_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="fixed-navigation-container">\n  <nav class="navigation"></nav>\n  <div class="cart-container">\n    <a href="/checkout/route" class="cart-trigger drop-down-trigger js-check-order">Shopping Bag <span class="fixed-bag-badge"></span></a>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/fixed_navigation_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/navigation_list_item_view"]=Handlebars.template({"1":function(){return"drop-down-trigger"},"3":function(e,t,n,a){var r,i='  <div class="drop-down-menu hidden drop-down">\n\n    <div class="menu-columns">\n'
r=t["if"].call(e,null!=e?e.hasImage:e,{"name":"if","hash":{},"fn":this.program(4,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.isVertical:e,{"name":"if","hash":{},"fn":this.program(8,a),"inverse":this.program(11,a),"data":a})
null!=r&&(i+=r)
return i+"    </div>\n  </div>\n"},"4":function(e,t,n,a){var r,i='        <ul class="product-spotlights menu-column">\n'
r=t.each.call(e,null!=e?e.allMenus:e,{"name":"each","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"        </ul>\n"},"5":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='            <li data-spotlight-for="menu-'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'" class="'
r=t["if"].call(e,a&&a.index,{"name":"if","hash":{},"fn":this.program(6,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+'">\n              <a href="'+l((i=null!=(i=t.url||(null!=e?e.url:e))?i:s,typeof i===o?i.call(e,{"name":"url","hash":{},"data":a}):i))+'" alt="'+l((i=null!=(i=t.name||(null!=e?e.name:e))?i:s,typeof i===o?i.call(e,{"name":"name","hash":{},"data":a}):i))+'">\n                <img src="'+l((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=e?e.image:e,{"name":"staticImageUrl","hash":{"size":125},"data":a}))+'" />\n              </a>\n            </li>\n'},"6":function(){return"hidden"},"8":function(e,t,n,a){var r,i='        <ul class="product-links menu-column">\n'
r=t.each.call(e,null!=e?e.submenus:e,{"name":"each","hash":{},"fn":this.program(9,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"        </ul>\n"},"9":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'            <li class="'+s((r=null!=(r=t.name_slug||(null!=e?e.name_slug:e))?r:o,typeof r===i?r.call(e,{"name":"name_slug","hash":{},"data":a}):r))+'">\n              <a href="'+s((r=null!=(r=t.url||(null!=e?e.url:e))?r:o,typeof r===i?r.call(e,{"name":"url","hash":{},"data":a}):r))+'">'+s((t.noBreak||e&&e.noBreak||o).call(e,null!=e?e.name:e,{"name":"noBreak","hash":{},"data":a}))+"</a>\n            </li>\n"},"11":function(e,t,n,a){var r,i=""
r=t.each.call(e,null!=e?e.submenus:e,{"name":"each","hash":{},"fn":this.program(12,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"12":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='          <ul class="product-links menu-column">\n            <li><h4><a href="'+l((i=null!=(i=t.url||(null!=e?e.url:e))?i:s,typeof i===o?i.call(e,{"name":"url","hash":{},"data":a}):i))+'">'+l((t.noBreak||e&&e.noBreak||s).call(e,null!=e?e.name:e,{"name":"noBreak","hash":{},"data":a}))+"</a></h4></li>\n\n"
r=t.each.call(e,null!=e?e.submenus:e,{"name":"each","hash":{},"fn":this.program(13,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"          </ul>\n"},"13":function(e,t,n,a){var r,i=t.helperMissing,o=this.escapeExpression,s="function"
return'              <li class="menu-item__'+o((t.slugify||e&&e.slugify||i).call(e,null!=e?e.name:e,{"name":"slugify","hash":{},"data":a}))+'">\n                <a href="'+o((r=null!=(r=t.url||(null!=e?e.url:e))?r:i,typeof r===s?r.call(e,{"name":"url","hash":{},"data":a}):r))+'" data-link-for="menu-'+o((r=null!=(r=t.id||(null!=e?e.id:e))?r:i,typeof r===s?r.call(e,{"name":"id","hash":{},"data":a}):r))+'">\n                  '+o((t.noBreak||e&&e.noBreak||i).call(e,null!=e?e.name:e,{"name":"noBreak","hash":{},"data":a}))+"\n                </a>\n              </li>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<a href="'+l((i=null!=(i=t.url||(null!=e?e.url:e))?i:s,typeof i===o?i.call(e,{"name":"url","hash":{},"data":a}):i))+'" class="top-level-link '
r=t["if"].call(e,null!=e?e.hasDropDown:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='">'+l((t.noBreak||e&&e.noBreak||s).call(e,null!=e?e.name:e,{"name":"noBreak","hash":{},"data":a}))+"</a>\n\n"
r=t["if"].call(e,null!=e?e.hasDropDown:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/navigation_list_item_view"]}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
Banana.views.application.chrome.NavigationListItemView=function(t){function a(){this.flattenSubmenus=e(this.flattenSubmenus,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.tagName="li"
a.prototype.template="banana/templates/application/chrome/navigation_list_item_view"
a.prototype.attach=function(){var e,t
a.__super__.attach.apply(this,arguments)
if(!_.isEmpty(this.model.get("submenus"))){this.$("a").first().addClass("drop-down-indicator")
e=!!(null!=(t=_.first(this.model.get("submenus")))?t.image:void 0)
return new E.lib.MainNavigationDropdown(this.$(".drop-down-trigger"),this.$(".drop-down"),e)}}
a.prototype.flattenSubmenus=function(e){var t
t=_.reject(e.submenus,function(e){return E.delivery.isMarketable()?E.delivery.getApplicableLocation()===E.delivery.Locations.NY?_.contains(E.delivery.Locations.SF.collectionPermalinks,e.url.replace("/collections/","")):E.delivery.getApplicableLocation()===E.delivery.Locations.SF?_.contains(E.delivery.Locations.NY.collectionPermalinks,e.url.replace("/collections/","")):void 0:e.name.indexOf("Everlane Now")>-1})
t=_.reject(t,function(e){return"Everlane for ShopStyle"===e.name&&!$.cookie("show_shopstyle")})
e.submenus=t
return _.flatten([e].concat(_.map(e.submenus,this.flattenSubmenus)))}
a.prototype.hasOnlyOneLevel=function(){return _.every(this.model.get("submenus"),function(){return function(e){return _.isEmpty(e.submenus)}}(this))}
a.prototype.getTemplateData=function(){var e,t
e=a.__super__.getTemplateData.apply(this,arguments)
e.hasDropDown=!_.isEmpty(this.model.get("submenus"))
e.isVertical=this.hasOnlyOneLevel()
e.allMenus=this.flattenSubmenus(this.model.attributes)
e.hasImage=!!(null!=(t=_.first(this.model.get("submenus")))?t.image:void 0)
return e}
return a}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/navigation_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<ul class="navigation-list"></ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/navigation_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.chrome.NavigationView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.itemView=Banana.views.application.chrome.NavigationListItemView
n.prototype.listSelector=".navigation-list"
n.prototype.template="banana/templates/application/chrome/navigation_view"
n.prototype.events={"click .product-links li a":"trackClick"}
n.prototype.trackClick=function(){return E.pub(E.Event.Navigation.LINK_CLICK,{"position":"top"})}
return n}(Banana.CollectionView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.chrome.FixedNavigation=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/application/chrome/fixed_navigation_view"
n.prototype.className="fixed-navigation"
n.prototype.id="fixed-navigation"
n.prototype.events={"click .js-check-order":"checkOrderSize"}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
this.hovercart=new E.lib.HoverCart($(".cart-trigger"),$("#cart-drop-down"),!0)
this.hovercart.sub(this.hovercart.Event.SHOW,function(e){return function(){return e.$el.addClass("active")}}(this))
this.hovercart.sub(this.hovercart.Event.HIDE,function(e){return function(){return e.$el.removeClass("active")}}(this))
e=new Banana.views.application.chrome.NavigationView({"collection":this.collection,"container":this.$(".navigation")})
return this.subview("fixed_navigation_list",e)}
n.prototype.remove=function(){this.hovercart.destroy()
this.hovercart.subs={}
return n.__super__.remove.apply(this,arguments)}
n.prototype.checkOrderSize=function(e){if(0===E.currentOrder.get("lineItems").count()){e.preventDefault()
e.stopPropagation()
return $(e.currentTarget).effect("shake")}}
n.prototype.onScroll=function(e){var t
t="down"===e?0:-50
if("up"===e){$("#cart-drop-down").fadeOut(200)
this.$(".drop-down").fadeOut(200)
this.$(".cart-trigger").removeClass("drop-down-trigger-active")}$("#fixed-navigation").animate({"top":t})
E.config.isStickyDropdown="down"===e
return E.pub("down"===e?E.Event.TopBar.STICK:E.Event.TopBar.UNSTICK)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/navigation_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<li><a href="/account/info">My Account</a></li>\n<li><a href="/account/orders">My Orders</a></li>\n<li><a href="/account/returns">My Returns</a></li>\n<li><a href="/account/billing">Payment Info</a></li>\n<li><a href="/account/address">Shipping Info</a></li>\n<li><a href="/redeem">Redeem Gift</a></li>\n<li><a href="/invite?source=menu">Invite</a></li>\n<li><a href="/logout" class="last">Log Out</a></li>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/navigation_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.NavigationView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="ul"
n.prototype.template="banana/templates/account/navigation_view"
n.prototype.listen={"dispatcher:dispatch mediator":"navigate"}
n.prototype.navigate=function(e,t,n,a){E.pub(E.Event.App.NAVIGATE,e,t,n,a)
return this.renderNewLocation(n.path)}
n.prototype.renderNewLocation=function(e){if(this.$el){this.$("a").parent().removeClass("active")
return this.$('a[href="/'+e+'"]').parent().addClass("active")}}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/account_bar_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"    "+s((r=null!=(r=t.abOfferText||(null!=e?e.abOfferText:e))?r:o,typeof r===i?r.call(e,{"name":"abOfferText","hash":{},"data":a}):r))+"\n"},"3":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c="    "+l((i=null!=(i=t.shippingPolicy||(null!=e?e.shippingPolicy:e))?i:s,typeof i===o?i.call(e,{"name":"shippingPolicy","hash":{},"data":a}):i))+"\n"
r=t["if"].call(e,null!=e?e.canada:e,{"name":"if","hash":{},"fn":this.program(4,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c},"4":function(){return'      <a href="javascript:;" class="geo-change-link">Country: Canada (change)</a>\n'},"6":function(){return'    <li><a href="/admin/dashboard">Admin</a></li>\n'},"8":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'    <li class="account-trigger drop-down-indicator">\n      <a href="/account/info">'+s((r=null!=(r=t.firstName||(null!=e?e.firstName:e))?r:o,typeof r===i?r.call(e,{"name":"firstName","hash":{},"data":a}):r))+'</a>\n      <div class="account-drop-down drop-down-menu account-menu"></div>\n    </li>\n'},"10":function(){return'    <li>\n        <a href="javascript:;" class="login-prompt" data-card="register">Join Now</a>\n        |\n        <a href="javascript:;" class="login-prompt" data-card="sign_in">Log In</a>\n    </li>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<div class="account-message">\n'
r=t["if"].call(e,null!=e?e.hasABOnboardingOffer:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(i+=r)
i+='</div>\n\n<ul class="account-links">\n'
r=t["if"].call(e,null!=e?e.admin:e,{"name":"if","hash":{},"fn":this.program(6,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.currentUser:e,{"name":"if","hash":{},"fn":this.program(8,a),"inverse":this.program(10,a),"data":a})
null!=r&&(i+=r)
return i+'\n  <li class="hover-glyph cart-trigger">\n    <a href="/checkout/route" class="js-check-order">Shopping Bag</a>\n    <div class="hoverable">\n      <div id="bag-ribbon"> </div>\n      <a href="/checkout/route" id="bag-icon" class="js-check-order"></a>\n      <div id="bag-badge"> </div>\n    </div>\n  </li>\n\n\n</ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/account_bar_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/static/geo_prompt_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="geo-prompt" id="geo-prompt">\n\n  <h2 class="country-title subheader">Choose Your Country</h2>\n\n  <a class="everlane-sans country-link" href="http://www.everlane.com?ignore_geo=true">USA</a>\n  <a href="javascript:;" class="currently everlane-sans country-link reveal-modal-close">CAN<a/>\n\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/static/geo_prompt_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.chrome.AccountBarView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/application/chrome/account_bar_view"
n.prototype.events={"click .geo-change-link":"openGeoPrompt","click .js-check-order":"checkOrderSize"}
n.prototype.initialize=function(e){return this.abOnboardingOffer=e.abOnboardingOffer}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=new Banana.views.account.NavigationView({"container":this.$(".account-drop-down")})
this.subview("accountNav",e)
return new E.lib.AccountDropDown(this.$(".account-trigger"),this.$(".account-drop-down"))}
n.prototype.openGeoPrompt=function(){return new Banana.views.components.ModalView({"view":{"class":Banana.View,"template":"banana/templates/application/static/geo_prompt_view"}})}
n.prototype.checkOrderSize=function(e){if(0===E.currentOrder.get("lineItems").count()){e.preventDefault()
e.stopPropagation()
return this.$(".hover-glyph.cart-trigger").effect("shake")}}
n.prototype.getTemplateData=function(){var e,t,a,r,i
e=n.__super__.getTemplateData.apply(this,arguments)
e.canada="CA"===E.session.getCountry()
e.shippingPolicy=E.data.shippingPolicy
e.abOfferText="shipping"===this.abOnboardingOffer?"Free shipping for the next 24 hours":"discount"===this.abOnboardingOffer?"$5.00 credit on first order, expires in 24 hours":void 0
e.hasABOnboardingOffer=this.abOnboardingOffer&&"control"!==this.abOnboardingOffer
e.currentUser=E.chaplinCollections.user
e.firstName=(null!=(t=E.chaplinCollections.user)?t.get("first_name"):void 0)&&""!==(null!=(a=E.chaplinCollections.user)?a.get("first_name"):void 0)?null!=(r=E.chaplinCollections.user)?r.get("first_name"):void 0:"You"
e.admin=(null!=(i=E.chaplinCollections.user)?i.get("roles").indexOf("admin"):void 0)>-1
e.everlaneUser=!!$.cookie("_everlane_user")
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["base/templates/cart"]=Handlebars.template({"1":function(){return'  <div class="cart__sdd clearfix">\n    <h6>1-Hour Delivery</h6>\n    <em class="cart__shipping-explanation">These items will be delivered to you in an hour.</em>\n    <ul class="cart__line-items--sdd"></ul>\n  </div>\n'},"3":function(){return'  <div class="cart__virtual">\n    <h6 class="thirteen">Sending Digitally</h6>\n    <ul class="cart__line-items--virtual"></ul>\n  </div>\n'},"5":function(){return'  <div class="cart__shipping-now clearfix">\n    <h6>Shipping Now</h6>\n    <em class="cart__shipping-explanation">These items will ship separately within 1&mdash;2 days.</em>\n    <ul class="cart__line-items--shipping-now"></ul>\n  </div>\n'},"7":function(){return'  <div class="cart__shipping-later clearfix">\n    <h6>Shipping Later</h6>\n    <ul class="cart__line-items--shipping-later"></ul>\n  </div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.hasSDD:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.hasVirtualShipping:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.hasShippingNow:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.hasShippingLater:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"useData":!0})
return this.HandlebarsTemplates["base/templates/cart"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.checkout.CartView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="base/templates/cart"
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
return this.listenTo(this.collection,"add reset",this.render)}
n.prototype.render=function(){this.hasSDD=this.collection.sdd().length>0
this.hasShippingNow=this.collection.shippingNow().length>0
this.hasShippingLater=this.collection.shippingLater().length>0
this.hasVirtualShipping=this.collection.virtual().length>0
return n.__super__.render.apply(this,arguments)}
n.prototype.attach=function(){var e,t,a,r
n.__super__.attach.apply(this,arguments)
if(this.hasSDD){e=new Banana.views.checkout.LineItemsView({"el":this.$(".cart__line-items--sdd"),"collection":new Banana.Collection(this.collection.sdd())})
this.subview("sddItems",e)}if(this.hasShippingNow){a=new Banana.views.checkout.LineItemsView({"el":this.$(".cart__line-items--shipping-now"),"collection":new Banana.Collection(this.collection.shippingNow())})
this.subview("shippingNowItems",a)}if(this.hasShippingLater){t=new Banana.views.checkout.LineItemsView({"el":this.$(".cart__line-items--shipping-later"),"collection":new Banana.Collection(this.collection.shippingLater())})
this.subview("shippingLaterItems",t)}if(this.hasVirtualShipping){r=new Banana.views.checkout.LineItemsView({"el":this.$(".cart__line-items--virtual"),"collection":new Banana.Collection(this.collection.virtual())})
return this.subview("virtualItems",r)}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.hasSDD=this.hasSDD
e.hasShippingNow=this.hasShippingNow
e.hasShippingLater=this.hasShippingLater
e.hasVirtualShipping=this.hasVirtualShipping
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/hover_cart_footer"]=Handlebars.template({"1":function(){return'  <p class="empty-bag-message sixteen">\n    Your bag is empty.\n  </p>\n'},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"    Shopping Credit: "+s((r=null!=(r=t.creditAmount||(null!=e?e.creditAmount:e))?r:o,typeof r===i?r.call(e,{"name":"creditAmount","hash":{},"data":a}):r))+"<br>\n"},"5":function(){return"    Free U.S. shipping on 2+ items, easy returns\n"},"7":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"    "+s((r=null!=(r=t.shippingPolicy||(null!=e?e.shippingPolicy:e))?r:o,typeof r===i?r.call(e,{"name":"shippingPolicy","hash":{},"data":a}):r))+"\n"},"9":function(){return'  <a href="/checkout/route" class="fancy-button orange checkout-button">Checkout \u2192</a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=""
r=t.unless.call(e,null!=e?e.hasLineItems:e,{"name":"unless","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+='\n<div class="shipping-message">\n'
r=t["if"].call(e,null!=e?e.hasCredit:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.hasFreeShipping:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.program(7,a),"data":a})
null!=r&&(i+=r)
i+="\n\n</div>\n\n"
r=t["if"].call(e,null!=e?e.hasLineItems:e,{"name":"if","hash":{},"fn":this.program(9,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/hover_cart_footer"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.chrome.HoverCartFooterView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/application/chrome/hover_cart_footer"
n.prototype.events={"click .continue-to-checkout":"onContinueToCheckoutClick"}
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
return this.listenTo(this.model.get("lineItems"),"all",this.render)}
n.prototype.getTemplateData=function(){var e,t,a,r
t=n.__super__.getTemplateData.apply(this,arguments)
e=null!=(r=E.session.getCurrentUser())?r.credits_total:void 0
a=E.currentOrder.get("lineItems").deepCount()
t.hasFreeShipping="US"===E.session.getCountry()&&a>1
t.shippingPolicy=E.data.shippingPolicy
t.hasCredit=e&&parseFloat(e.replace("$",""))
t.creditAmount=e
t.hasLineItems=a
return t}
n.prototype.onContinueToCheckoutClick=function(){return E.pub(E.Event.Checkout.CONTINUE_TO_CHECKOUT_CLICK)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/hover_cart"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="cart hovercart-list"></div>\n\n<div class="hovercart-footer clearfix"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/hover_cart"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.chrome.HoverCartView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/application/chrome/hover_cart"
n.prototype.id="cart-drop-down"
n.prototype.className="drop-down-menu hovercart"
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
this.cartView=new Banana.views.checkout.CartView({"collection":E.currentOrder.get("lineItems"),"container":this.$(".cart")})
this.subview("cartView",this.cartView)
this.hoverCartFooterView=new Banana.views.application.chrome.HoverCartFooterView({"model":E.currentOrder,"container":this.$(".hovercart-footer")})
return this.subview("hoverCartFooterView",this.hoverCartFooterView)}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.hasLineItems=E.currentOrder.get("lineItems").deepCount()
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/header_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'  <a href="/open-studio/'+s((r=null!=(r=t.stickerPermalink||(null!=e?e.stickerPermalink:e))?r:o,typeof r===i?r.call(e,{"name":"stickerPermalink","hash":{},"data":a}):r))+'" class="sticker">\n    <img src="'+s((r=null!=(r=t.stickerImage||(null!=e?e.stickerImage:e))?r:o,typeof r===i?r.call(e,{"name":"stickerImage","hash":{},"data":a}):r))+'" />\n  </a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<div class="account-bar"></div>\n\n'
r=t["if"].call(e,null!=e?e.stickerImage:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+'\n<a href="/" class="logotype">Everlane</a>\n\n<nav class="navigation"></nav>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/header_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t},n=[].indexOf||function(e){for(var t=0,n=this.length;n>t;t++)if(t in this&&this[t]===e)return t
return-1}
Banana.views.application.chrome.HeaderView=function(e){function a(){return a.__super__.constructor.apply(this,arguments)}t(a,e)
a.prototype.template="banana/templates/application/chrome/header_view"
a.prototype.container=$(".chrome-header")
a.prototype.hiddenNavRoutes=["/checkout/shipping","/checkout/payment","/checkout/confirm","/checkout/empty_cart","/checkout/sign_in","/landing_page"]
a.prototype.disabledDistractingChromeRoutes=["/checkout/review","/checkout/shipping","/checkout/payment","/checkout/confirm","/checkout/empty_cart","/checkout/sign_in","/landing_page"]
a.prototype.hiddenShoppingBagRoutes=["/landing_page"]
a.prototype.hiddenStickerRoutes=["/orders","/account","/factories"]
a.prototype.initialize=function(){var e,t
e=E.data.menu
this.hiddenStickerRoutes=this.hiddenStickerRoutes.concat(this.disabledDistractingChromeRoutes)
this.listenTo(E,E.Event.App.ROUTE,function(e){return function(t,n){e.maybeHideNavElements(n)
e.maybeHideDistractingChrome(n)
return e.maybeHideSticker(n)}}(this))
"male"===(null!=(t=E.chaplinCollections.user)?t.get("gender"):void 0)&&e.splice(0,2,e[1],e[0])
return this.collection=E.chaplinCollections.menus=new Banana.collections.Menus(_.compact(e))}
a.prototype.attach=function(){a.__super__.attach.apply(this,arguments)
E.chaplinCollections.alerts=new Banana.collections.Alerts
this.initAccountBarView()
this.showUserAlerts()
this.initNavigationView()
this.maybeHideNavElements(window.location.pathname)
this.maybeHideDistractingChrome(window.location.pathname)
this.maybeHideSticker(window.location.pathname)
return this.$el.waypoint(function(e){return function(t){return e.subview("fixedNavigationView")?e.subview("fixedNavigationView").onScroll(t):void 0}}(this),{"offset":-115})}
a.prototype.remove=function(){this.$el.waypoint("destroy")
return a.__super__.remove.apply(this,arguments)}
a.prototype.showUserAlerts=function(){return new Banana.views.components.AlertListView({"collection":E.chaplinCollections.alerts,"container":".chrome-header"})}
a.prototype.initAccountBarView=function(){var e
e=new Banana.views.application.chrome.AccountBarView({"container":this.$(".account-bar"),"abOnboardingOffer":this.abOnboardingOffer})
return this.subview("account_bar",e)}
a.prototype.initNavigationView=function(){var e
if(!this.hasNav){e=new Banana.views.application.chrome.NavigationView({"container":this.$(".navigation"),"collection":this.collection})
this.subview("navigation",e)
this.initHoverCartView()
this.initFixedNavigationView()
return this.hasNav=!0}}
a.prototype.initFixedNavigationView=function(){var e
if(this.subview("fixedNavigationView"))return this.subview("fixedNavigationView")
if(!E.lib.helpers.isMobile()){e=new Banana.views.application.chrome.FixedNavigation({"container":$("#page"),"collection":this.collection,"containerMethod":"prepend"})
return this.subview("fixedNavigationView",e)}}
a.prototype.initHoverCartView=function(){if(this.subview("hoverCartView"))return this.subview("hoverCartView")
this.hoverCartView=new Banana.views.application.chrome.HoverCartView({"container":this.$(".navigation"),"model":E.currentOrder})
this.subview("hoverCartView",this.hoverCartView)
this.hoverCartDecorator=new E.lib.HoverCart(this.$(".cart-trigger"),$("#cart-drop-down"))
return E.pub(E.Event.Cart.INIT)}
a.prototype.maybeHideNavElements=function(e){this.determineNavVisibility(e)
return this.determineShoppingBagVisibility(e)}
a.prototype.maybeHideDistractingChrome=function(e){if(n.call(this.disabledDistractingChromeRoutes,e)>=0){this.hoverCartDecorator.destroy()
this.removeSubview("hoverCartView")
return this.removeSubview("fixedNavigationView")}this.initHoverCartView()
return this.initFixedNavigationView()}
a.prototype.maybeHideSticker=function(e){var t
t=_.find(this.hiddenStickerRoutes,function(t){return e.indexOf(t)>=0})
return t?this.$("a.sticker").hide():this.$("a.sticker").show()}
a.prototype.determineNavVisibility=function(e){return n.call(this.hiddenNavRoutes,e)>=0?this.$(".navigation").hide():this.$(".navigation").show()}
a.prototype.determineShoppingBagVisibility=function(e){if(n.call(this.hiddenShoppingBagRoutes,e)>=0){this.$(".cart-trigger").hide()
return $("#footer").hide()}this.$(".cart-trigger").show()
return $("#footer").show()}
a.prototype.getTemplateData=function(){var e,t,n
e=a.__super__.getTemplateData.apply(this,arguments)
e.stickerImage=null!=(t=E.data.currentSticker)?t.sticker_image.url:void 0
e.stickerPermalink=null!=(n=E.data.currentSticker)?n.permalink:void 0
return e}
return a}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/application/chrome/footer"]=Handlebars.template({"1":function(){return'            <li>\n              <a href="/returns">Returns</a>\n            </li>\n'},"3":function(){return'            <li>\n              <a href="/?mobile=true">View Mobile Site</a>\n            </li>\n'},"5":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      <h5 class="footer__cta-heading">Refer a friend. Earn $25 credit<br/>when they purchase.</h5>\n      <form>\n        <input type="text" value="'+s((r=null!=(r=t.inviteToken||(null!=e?e.inviteToken:e))?r:o,typeof r===i?r.call(e,{"name":"inviteToken","hash":{},"data":a}):r))+'" autocomplete="off" title="copy me" class="share-url fancy-input small" size="44" readonly>\n      </form>\n'},"7":function(){return'      <h5 class="footer__cta-heading">Sign up for early access<br/>to our next collection.</h5>\n      <form accept-charset="UTF-8" action="/users" class="new_user" data-remote="true" id="new_user" method="post">\n        <input type="email" name="user[email]" class="fancy-input small" size="30" placeholder="Enter your email">\n        <input class="fancy-button mid-grey small" data-disable-with="Joining" name="commit" style="margin-left: 5px;" type="submit" value="Join Now">\n      </form>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<div class="row">\n\n  <nav class="footer-nav">\n\n    <ul>\n      <li class="section">\n        <h6 class="footer__column-heading">Help</h6>\n\n        <ul>\n'
r=t["if"].call(e,null!=e?e.isSignedIn:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+='          <li>\n            <a href="/help">Contact / FAQ</a>\n          </li>\n          <li>\n            <a href="/terms">Terms of Service</a>\n          </li>\n          <li>\n            <a href="/privacy">Privacy Policy</a>\n          </li>\n'
r=t["if"].call(e,null!=e?e.isMobileClient:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+='        </ul>\n      </li>\n\n      <li class="section">\n        <h6 class="footer__column-heading">Company</h6>\n\n        <ul>\n          <li>\n            <a href="/factories">Factories</a>\n          </li>\n          <li>\n            <a href="/about">About</a>\n          </li>\n          <li>\n            <a href="/about#jobs">Jobs</a>\n          </li>\n        </ul>\n      </li>\n\n      <li class="section">\n        <h6 class="footer__column-heading">Connect</h6>\n\n        <ul>\n          <li>\n            <a href="http://tumblr.everlane.com" target="_blank">Blog</a>\n          </li>\n          <li>\n            <a href="https://www.facebook.com/everlane" target="_blank">Facebook</a>\n          </li>\n          <li>\n            <a href="https://twitter.com/everlane" target="_blank">Twitter</a>\n        </ul>\n      </li>\n    </ul>\n\n  </nav>\n\n  <div class="signup-module">\n'
r=t["if"].call(e,null!=e?e.isSignedIn:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.program(7,a),"data":a})
null!=r&&(i+=r)
return i+"  </div>\n\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/application/chrome/footer"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.chrome.FooterView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/application/chrome/footer"
n.prototype.tagName="footer"
n.prototype.className="footer"
n.prototype.container=$("#footer")
n.prototype.getTemplateData=function(){var e,t
e=n.__super__.getTemplateData.apply(this,arguments)
e.isSignedIn=E.session.isSignedIn()
e.inviteToken=null!=(t=E.session.getCurrentUser())?t.invite_url:void 0
e.isMobileClient=E.session.isMobileClient()
return e}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t},n=[].indexOf||function(e){for(var t=0,n=this.length;n>t;t++)if(t in this&&this[t]===e)return t
return-1}
Banana.views.application.Layout=function(e){function a(){return a.__super__.constructor.apply(this,arguments)}t(a,e)
a.prototype.regions={"content":"#content","pre_content":"#pre_content","post_content":"#post_content"}
a.prototype.nonChaplinRoutes=["/checkout/shipping","/checkout/payment","/checkout/confirm","/checkout/empty_cart","/checkout/sign_in","/terms","/privacy"]
a.prototype.initialize=function(){var e
if(e=window.location.pathname,n.call(this.nonChaplinRoutes,e)>=0){this.headerView=new Banana.views.application.chrome.HeaderView
this.footerView=new Banana.views.application.chrome.FooterView
return this.listenTo(E,E.Event.App.ROUTE,function(e){return function(t,a){var r,i
if(n.call(e.nonChaplinRoutes,a)<0){null!=(r=e.headerView)&&r.remove()
return null!=(i=e.footerView)?i.remove():void 0}}}(this))}}
return a}(Chaplin.Layout)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.UserInfoSnippetCollection=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.initialize=function(){this.on("add",this.onAdd)
return this.on("change:primary",this.onChangePrimary)}
n.prototype.onAdd=function(e){return this.onChangePrimary(e,e.get("primary"))}
n.prototype.onChangePrimary=function(e,t){var n,a,r,i,o
if(t){i=this.without(e)
o=[]
for(a=0,r=i.length;r>a;a++){n=i[a]
o.push(n.set("primary",!1))}return o}}
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Address=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/addresses"
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Addresses=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Address
n.prototype.url="/api/addresses"
n.prototype.comparator="created_at"
return n}(Banana.collections.UserInfoSnippetCollection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Product=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.idAttribute="permalink"
n.prototype.initialize=function(){var e,t
this.set("main_image",_(this.get("images")).first())
this.set("hover_image",_(this.get("images")).last())
e=new Banana.Collection(_.map(this.get("images"),function(){return function(e){return new Banana.models.ProductAsset({"src":e,"type":"image"})}}(this)))
t=this.get("video")
t&&e.add(new Banana.models.ProductAsset({"urls":t.urls,"type":"video","selected":!1}))
return this.set("assets",e)}
n.prototype.mensTees=function(){return!!this.get("permalink").match(/mens-(crew-|v-|pocket-|summer-pk-)/)}
n.prototype.mensShirts=function(){return this.get("permalink").match(/(mens-(oxford-|poplin-|standard-fit-|slim-fit-|indigo-shirt))/)}
n.prototype.mensSweaters=function(){return this.get("permalink").match(/(seed-stitch-(crew-|vneck-)|mens-(seed-stitch-|french-terry-))/)}
n.prototype.mensJackets=function(){return this.get("permalink").match(/mens-(lightweight-bomber-|knit-blazer-)/)}
n.prototype.isGiftCard=function(){return this.get("permalink").match(/giftcard/)}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Products=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Product
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Grouping=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.ProductAsset=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.type="image"
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Groupings=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Grouping
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Collection=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/collections"
n.prototype.idAttribute="permalink"
n.get=function(e){var t,n,a
a=new $.Deferred
n=E.chaplinCollections.collections
if(t=n.get(e))a.resolve(t)
else if(n.isSyncing())n.synced(function(t){return function(){return t.get(e).done(a.resolve).fail(a.reject)}}(this))
else{t=new Banana.models.Collection({"permalink":e})
t.fetch({"success":function(e){n.add(e)
return a.resolve(e)},"error":a.reject})}return a}
n.prototype.grouping_for=function(e,t){null==t&&(t="shape")
return this.get("groupings")[t].find(function(t){return t.get("products").findWhere({"id":e})})}
n.prototype.set=function(e,t){("products"===e||_.isArray(e.products))&&(e=this.parseRawModels(e))
return n.__super__.set.call(this,e,t)}
n.prototype.parseRawModels=function(e){var t,n
n=new Banana.collections.Products(_.map(e.products,function(){return function(t){var n
t=new Banana.models.Product(t)
if(n=_(e.metadata).findWhere({"id":t.get("metadata_id")})){t.set("description",n.description)
t.set("subheros",n.subheros)
t.set("instagram_images",n.compiled_instagram_images)}t.set("collection_id",e.id)
t.set("collection_permalink",e.permalink)
return t}}(this)))
t={}
_.each(e.groupings,function(){return function(e,a){e=_.map(_.filter(e,function(e){return e.name||"flat"===a}),function(e){var t,r
e=new Banana.models.Grouping(e)
r=e.get("products")
t=_.map(r,function(e){return n.findWhere({"id":e})})
"condensed_product_group"===a&&(t=_.shuffle(t))
t=_.sortBy(t,function(e){return e.attributes.orderable_state in["waitlistable","sold_out"]})
return e.set("products",new Banana.collections.Products(t))})
e=new Banana.collections.Groupings(e)
return t[a]=e}}(this))
e.groupings=t
e.products=n
return e}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Collections=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Collection
n.prototype.url="/api/collections.js"
n.prototype.sync=function(e,t,n){n.url=_.result(this,"url")
n.dataType="jsonp"
n.cache=!0
n.jsonpCallback="callback_2_"+E.env.getCollectionsCacheKey()
if(E.env.getAssetHost()){n.url=E.env.getAssetHost()+n.url
n.data={"platform":E.env.isAdmin()?"admin":"prod"}
console.log("going to cloudfront for collections API: "+n.url,n.data)}return Backbone.sync(e,t,n)}
n.prototype.parse=function(e){return _.map(e.collections,function(t){t.products=_.map(t.products,function(t){return _.findWhere(e.products,{"id":t})})
t.metadata=_.map(t.metadata,function(t){return _.findWhere(e.metadata,{"id":t})})
return t})}
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.ComingSoon=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/upcoming_launches"
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.ComingSoon=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.ComingSoon
n.prototype.url="/api/upcoming_launches"
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.CreditCard=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/credit_cards"
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.CreditCards=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.url="/api/credit_cards"
n.prototype.model=Banana.models.CreditCard
n.prototype.comparator=function(e){return e.attributes.primary?0:1}
return n}(Banana.collections.UserInfoSnippetCollection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.FactoryMetaData=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/factory_meta_data"
n.prototype.COUNTRIES={"CA":"Canada","CN":"China","IN":"India","IT":"Italy","GB":"Scotland","ES":"Spain","TH":"Thailand","TR":"Turkey","US":"USA"}
n.prototype.initialize=function(){this.set("country",this.COUNTRIES[this.get("country")])
return this.set("coords",_.map(this.get("coords").split(/[ ,]+/),function(e){return parseInt(e)}))}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.FactoryMetaData=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.FactoryMetaData
n.prototype.url="/api/factory_meta_data"
n.prototype.comparator="country"
n.prototype.groupByRegions=function(){var e,t
e=this.groupBy("region")
t={}
_.each(e,function(e){return function(n){var a,r,i
i=_.first(n).pick("coords","region"),a=i.coords,r=i.region
return t[r]={"cities":n,"nearby":e.filter(function(e){return e.get("region")!==r&&Math.abs(a[0]-e.get("coords")[0])<100&&Math.abs(a[1]-e.get("coords")[1])<100})}}}(this))
return t}
n.prototype.getHeroImages=function(){return this.map(function(e){return E.lib.ImageHelper.imageUrl(e.main_image)})}
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Homepage=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Homepages=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Homepage
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Order=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/orders"
n.prototype.cancel_return=function(e){var t,n
t=function(t){return function(){return t.trigger("cancel_return",!1,e)}}(this)
n=function(t){return function(){return t.trigger("cancel_return",!0,e)}}(this)
return $.ajax({"type":"POST","url":"/orders/"+this.id+"/return_authorizations/"+e+"/cancel"}).done(t).fail(n)}
n.prototype.cancel=function(e){var t,n
t=function(t){return function(){t.trigger("cancel",!1)
return e.success()}}(this)
n=function(t){return function(){t.trigger("cancel",!0)
return e.error()}}(this)
return $.ajax({"type":"POST","url":"/api/orders/"+this.id+"/cancel"}).done(t).fail(n)}
n.prototype.cancel_remaining=function(e){var t,n
n=_.findWhere(this.attributes.return_units,{"id":e})
t=function(t){return function(a){n.quantity=a.new_quantity
return t.trigger("returnUnitUpdated",e,a.new_quantity)}}(this)
return $.ajax({"type":"POST","url":"/api/orders/"+this.id+"/cancel_return_unit/"+e}).done(t)}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.Orders=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Order
n.prototype.url="/api/orders"
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.collections.PurchasedOrders=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.model=Banana.models.Order
n.prototype.url="/api/orders/purchased"
n.prototype.latest_completed_order=function(){return this.findWhere({"latest_completed_order":!0})}
return n}(Banana.Collection)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.CardView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
if(null==this.cards)throw new Error("CardView requires cards to be implemented on the child class")
if(null==this.cardsContainer)throw new Error("CardView require a cardsContainer on the child class")
this.options=_.extend({},this.options,e)
return this.currentCard=this.options.initialCard}
n.prototype.render=function(){var e,t,a,r,i
n.__super__.render.apply(this,arguments)
r=this.cards
i=[]
for(t in r){a=r[t]
e=new a({"container":this.$(this.cardsContainer),"nextURL":this.options.nextURL,"data":this.options.data})
i.push(this.subview(t,e))}return i}
n.prototype.attach=function(){var e,t,a
n.__super__.attach.apply(this,arguments)
a=this.cards
for(e in a){t=a[e]
this.subview(e).$el.hide()}return this.transitionTo(this.currentCard)}
n.prototype.transitionTo=function(e){var t
t=this.subview(e)
this.subview(this.currentCard).$el.velocity("fadeOut",{"duration":150,"complete":function(){return t.$el.velocity("fadeIn",{"duration":150})}})
return this.currentCard=e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/login_modal"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="login-manager"></div>\n\n<div class="bottom-bar">\n  <p>\n    <a href="javascript:;"></a>\n  </p>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/login_modal"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/facebook_connect"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<a href="javascript:;" class="fancy-button blue facebook-connect" data-frame="loggingIn">\n  <img alt="Fb f" src="//d1zvwc1xfc8qp0.cloudfront.net/assets/fb_f-822a8f283bc1777f49dc0329a64dba0d.png" /> '+s((r=null!=(r=t.text||(null!=e?e.text:e))?r:o,typeof r===i?r.call(e,{"name":"text","hash":{},"data":a}):r))+"\n</a>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/facebook_connect"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.FacebookConnectView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/facebook_connect"
n.prototype.events={"click .facebook-connect":function(e){E.facebookConnect(e)
return this.trigger("fb:connect")}}
n.prototype.textState={"sign_in":"Log in with facebook","register":"Sign up with facebook"}
n.prototype.initialize=function(e){this.text=this.textState[e.text]
return null!=this.text?this.text:this.text=this.textState.sign_in}
n.prototype.getTemplateData=function(e){e=n.__super__.getTemplateData.apply(this,arguments)
e.text=this.text
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/finish_registration"]=Handlebars.template({"1":function(){return'      <label class="fancy-label" for="password">Password</label>\n      <input class="fancy-input password" id="password" name="user[password]" autofocus="true" placeholder="Choose a password" type="password">\n'},"3":function(){return'      <label class="fancy-label" for="full-name">Full name</label>\n      <input class="fancy-input form__full_name" id="full_name" name="full-name" placeholder="Thom Yorke" type="text">\n      <input type="hidden" class="first-name form__first_name" name="user[first_name]">\n      <input type="hidden" class="last-name form__last_name" name="user[last_name]">\n\n      <label class="fancy-label" for="password">Password</label>\n      <input class="fancy-input password" id="password" name="user[password]" placeholder="Choose a password" type="password">\n\n      <label class="fancy-label" for="mobile_number">Mobile number</label>\n      <input class="fancy-input mobile-number" id="mobile_number" name="user[mobile_devices_attributes[0][number]]" placeholder="e.g., (555) 555-5555" type="text">\n\n      <label class="fancy-label" for="gender">What styles are you interested in?</label>\n      <div class="radio-group">\n        <input class="gender" id="female" type="radio" name="user[gender]" value="female">\n        <label for="female">Women</label>\n      </div>\n      <div class="radio-group">\n        <input class="gender" id="male" type="radio" name="user[gender]" value="male">\n        <label for="male">Men</label>\n      </div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<div class="registration-form">\n  <h3>You&#8217;re Almost Done</h3>\n\n  <form class="fancy-form">\n\n'
r=t["if"].call(e,null!=e?e.minimalForm:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(i+=r)
return i+'\n    <button class="registration-button" type="submit">Join Now</button>\n\n    <p class="server-error"></p>\n\n  </form>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/finish_registration"]}).call(this);(function(){var e,t,n
t={"numeral":{"regex":/^\d+$/,"message":"Please enter a whole number"},"number":{"regex":/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/,"message":"Please enter a number"},"email":{"regex":/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,"message":"Please enter a valid email"},"url":{"regex":/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,"message":"Please enter a valid URL"},"us_postal_code":{"regex":/^\d{5}(?:[-\s]\d{4})?$/,"message":"Please enter a valid U.S. ZIP Code"},"us_phone_number":{"regex":/^(\D*\d){10}$/,"message":"Please enter a valid phone number"}}
n={"presence":function(e,t){return t?!_.str.isBlank(e)||"This field is required":!0},"maxLength":function(e,t){return(null!=e?e.length:void 0)<=t?!0:"Maximum length: "+t+" characters"},"minLength":function(e,t){return(null!=e?e.length:void 0)>=t?!0:"Minimum length: "+t+" characters"},"ccNum":function(e){return $.payment.validateCardNumber(e)?!0:"Invalid card number"},"cvc":function(e,t){return $.payment.validateCardCVC(e,$.payment.cardType($(t).val()))?!0:"Invalid CVC"},"cardExpiry":function(e){var t
t=$.payment.cardExpiryVal(e)
return $.payment.validateCardExpiry(t.month,t.year)?!0:"Invalid expiration"}}
e=function(e){return $.trim(e.hasClass("select2-container")?e.select2("val"):e.val())}
E.mixins.Form={"form":"form","getForm":function(){return this.$(this.form)},"getFormValues":function(e){var t,n
e||(e=this.getForm())
e=$(e)
t=e.find(".form__full_name")
if(t.length>0){n=NameParse.parse(t.val())
e.find(".form__first_name").val(n.firstName)
e.find(".form__last_name").val(n.lastName)
t.removeProp("name")}return e.serialize()},"validations":{},"validate":function(a,r){var i
null==r&&(r=this.validations)
a||(a=this.getForm())
a=$(a)
this.clearErrors(a)
i=_.map(r,function(r){return function(i,o){var s,l,c,p,u
if("@"===o){s=a
u=a}else{s=a.find(o)
if(!(s.length>0))return!0
u=e(s)}l=s.hasClass("select2-container")?s.attr("id").split("s2id_")[1]:s.attr("id")
c=a.find("label[for='"+l+"']")
p=!1
if(_.isFunction(i)){p=i.call(r,u)
p!==!0&&r.addError(c,p)}else{p=_.chain(i).map(function(e,a){return"pattern"===a?_.str.isBlank(u)||t[e].regex.test(u)||t[e].message:n[a](u,e)}).find(function(e){return e!==!0}).value()
p&&r.addError(c,p)
p||(p=!0)}return p===!0}}(this))
return _.all(i,function(e){return e===!0})},"attach":function(){var e
E.env.isMobileSite()||this.$(".fancy-select").each(function(e,t){$(t).select2()
return null!=$(t).data("value")?$(t).select2("val",$(t).data("value")):void 0})
e=function(e){return function(){var t,n
n=e.$(".cc_number");(t=$.payment.cardType(n.val()))?e.$("#cc_icons").css("opacity",1).removeClass().addClass(t):e.$("#cc_icons").css("opacity",0)
return e}}(this)
this.$(".cc_expiry").payment("formatCardExpiry")
this.$(".cc_number").payment("formatCardNumber")
this.$(".cc_number").on("keyup",e)
return this.$(".cc_number").on("change",e)},"addError":function(e,t){e.find(".fancy-form__label--error").remove()
return e.append($("<span>").addClass("fancy-form__label--error").html(t))},"clearErrors":function(e){e||(e=this.getForm())
e=$(e)
return e.find(".fancy-form__label--error").remove()}}}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.FinishRegistrationView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/finish_registration"
n.prototype.events={"click .registration-button":"onSubmit"}
n.prototype.validations={"#full_name":{"presence":!0},"#password":{"presence":!0,"minLength":4},"#mobile_number":{"pattern":"us_phone_number"}}
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
this.minimalForm=null!=e?e.minimalForm:void 0
return this.userId=E.chaplinCollections.user.get("id")}
n.prototype.onSubmit=function(e){var t
e.preventDefault()
if(this.validate()){t=new E.lib.ButtonProgressBar({"button":this.$(".registration-button"),"loaderColor":"#B75D48"})
t.start()
return $.ajax({"type":"PATCH","data":this.getFormValues(),"url":"/users/"+this.userId}).done(function(){return function(){E.chaplinCollections.user.set("has_password",!0)
t.stop()
return E.pub(E.Event.Registration.FULL_REGISTRATION)}}(this)).fail(function(e){return function(){t.stop()
return e.$(".server-error").text("Something went wrong on our end, please try again").show()}}(this))}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.id=E.chaplinCollections.user.get("id")
e.minimalForm=this.minimalForm
return e}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$(".mobile-number").formatter({"pattern":"({{999}}) {{999}}-{{9999}}"})}
return n}(Banana.View)
E.mix(Banana.views.users.FinishRegistrationView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/register"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="create-account-frame card">\n  <header>\n    <h3 class="hyphenated-heading" style="margin-bottom: 10px;">become a member</span>\n    <h1 class="everlane-sans">join everlane</h1>\n  </header>\n\n  <p>\n    There&rsquo;s a new way: Designer clothes sell for 8 times what they cost to make.\n    But we skip the middlemen to offer the finest essentials at truly disruptive prices.\n  </p>\n\n  <div class="landing-error facebook"></div>\n\n  <!-- Filled in by RegisterFormView -->\n  <div class="create-account-form"></div>\n\n  <!-- Filled in by FacebookConnectView -->\n  <div class="fb-container"></div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/register"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/full_register_form"]=Handlebars.template({"1":function(){return'      <div class="field">\n        <label for="form__full_name">Full Name</label>\n        <input id="form__full_name" class="form__full_name" type="text" name="user[full_name]">\n        <input type="hidden" class="form__first_name" name="user[first_name]">\n        <input type="hidden" class="form__last_name" name="user[last_name]">\n      </div>\n'},"3":function(){return'      <div class="field register__email-container">\n        <label for="user_email">Email Address</label>\n        <input id="user_email" class="register__email" type="text" name="user[email]">\n      </div>\n'},"5":function(){return'      <div class="field">\n        <label for="user_password">Password</label>\n        <input id="user_password" class="register__password" type="password" name="user[password]">\n      </div>\n'},"7":function(){return'      <div class="field">\n        <label for="user_gender">What styles are you interested in?</label>\n        <div class="register__radio-group">\n          <input class="register__gender" id="user_female" type="radio" name="user[gender]" value="female">\n          <label for="user_female">Women</label>\n        </div>\n        <div class="register__radio-group">\n          <input class="register__gender" id="user_male" type="radio" name="user[gender]" value="male">\n          <label for="user_male">Men</label>\n        </div>\n      </div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<form class="fancy-form register">\n\n  <div class="form_fields clearfix">\n\n'
r=t["if"].call(e,null!=(r=null!=e?e.fields:e)?r.full_name:r,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=(r=null!=e?e.fields:e)?r.email:r,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=(r=null!=e?e.fields:e)?r.password:r,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=(r=null!=e?e.fields:e)?r.gender:r,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+'\n    <div class="field form__button-container">\n      <button type="submit" class="register__join-button default flat-button">Create Account</button>\n    </div>\n\n    <p class="register__server-error hidden"></p>\n\n  </div>\n</form>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/full_register_form"]}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
Banana.views.users.RegisterFormView=function(t){function a(){this.attemptToRegister=e(this.attemptToRegister,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.template="banana/templates/users/full_register_form"
a.prototype.events={"click .register__join-button":"attemptToRegister"}
a.prototype.validations={"#form__full_name":{"presence":!0},"#user_email":{"presence":!0,"pattern":"email"},"#user_password":{"presence":!0,"minLength":3}}
a.prototype.initialize=function(e){a.__super__.initialize.apply(this,arguments)
this.defaultFields={"full_name":!0,"email":!0,"password":!0,"gender":!0}
this.fields=_.extend({},this.defaultFields,e.fields)
this.nextUrl=e.nextUrl||""
return E.sub(E.Event.JOIN_ERROR,function(e){return function(t,n,a){e.trigger("register:error")
if(e.$(".register__email-container").length){e.addError(e.$(".register__email-container"),n)
if(a)return e.$(".register__email").val(a)}}}(this))}
a.prototype.render=function(){return a.__super__.render.apply(this,arguments)}
a.prototype.attach=function(){return a.__super__.attach.apply(this,arguments)}
a.prototype.attemptToRegister=function(e){e.preventDefault()
return this.validate()?this.register(e):void 0}
a.prototype.register=function(e){var t,n
this.trigger("form:submit")
t=this.$(".register")
n=new E.lib.ButtonProgressBar({"button":this.$(e.currentTarget)})
return $.ajax({"url":"/api/users","method":"POST","data":this.getFormValues(t)}).done(function(e){return function(){E.pub(E.Event.Registration.FULL_REGISTRATION)
return window.location=e.nextUrl}}(this)).fail(function(){return function(e){var t,a
n.stop()
t=e.responseJSON.data.email
a=t?"Email "+t[0]:"Something went wrong on our end, please try again"
return E.pub(E.Event.JOIN_ERROR,a)}}(this))}
a.prototype.getTemplateData=function(){var e
e=a.__super__.getTemplateData.apply(this,arguments)
e.fields=this.fields
return e}
return a}(Banana.View)
E.mix(Banana.views.users.RegisterFormView,E.mixins.Form)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.RegisterView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/register"
n.prototype.className="register-card"
n.prototype.initialize=function(e){return this.nextUrl=e.nextURL}
n.prototype.render=function(){return n.__super__.render.apply(this,arguments)}
n.prototype.attach=function(){var e,t
n.__super__.attach.apply(this,arguments)
t=new Banana.views.users.RegisterFormView({"container":this.$(".create-account-form"),"fields":{"full_name":!1,"password":!1,"gender":!1},"nextUrl":this.nextUrl})
this.subview("register_form",t)
e=new Banana.views.users.FacebookConnectView({"container":this.$(".fb-container"),"text":"register"})
this.subview("fb_connect",e)
this.listenTo(t,"form:submit",function(){return this.trigger("form:submit")})
this.listenTo(t,"register:error",function(){return this.trigger("register:error")})
return this.listenTo(e,"fb:connect",function(){return this.trigger("fb:connect")})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/minimal_register"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="create-account-frame card">\n  <header>\n    <h3 class="uppercase-headline" style="margin-bottom: 10px;">Welcome to Everlane</span>\n    <h1 class="medium-serif-headline">Luxury Basics.<br>Low Markups.</h1>\n  </header>\n\n  <p class="paid-landing-text">\n    Sign up now to get free <br>\n    shipping on your first order\n  </p>\n\n  <form class="create-account-form">\n    <div class="form_fields clearfix">\n\n      <div class="field half_width register__email">\n        <label for="register_email"></label>\n        <input class="fancy-input email" placeholder="Enter your email" type="text" id="register_email" autofocus="true">\n      </div>\n\n      <div class="field half_width submit">\n        <label></label>\n        <input class="join-button fancy-button orange small" type="submit" data-disable-with="Joining\u2026" value="Join Now">\n      </div>\n\n    </div>\n  </form>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/minimal_register"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.MinimalRegisterView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/minimal_register"
return n}(Banana.views.users.RegisterView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/sign_in_form"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<form class="fancy-form sign-in">\n  <div class="form_fields clearfix">\n    <div class="field">\n      <label for="login_email">Email Address</label>\n      <input class="sign-in__email" id="login_email" type="text" autofocus="true">\n    </div>\n\n    <div class="field">\n      <label for="login_password">Password</label>\n      <input class="sign-in__password" id="login_password" type="password">\n    </div>\n\n    <div class="field form__button-container">\n      <button class="default flat-button sign-in__login-button" type="submit">Log In</button>\n    </div>\n  </div>\n</form>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/sign_in_form"]}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
Banana.views.users.SignInFormView=function(t){function a(){this.attemptLogin=e(this.attemptLogin,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.template="banana/templates/users/sign_in_form"
a.prototype.events={"click .sign-in__login-button":"attemptLogin"}
a.prototype.validations={"#login_email":{"presence":!0,"pattern":"email"},"#login_password":{"presence":!0,"minLength":3}}
a.prototype.initialize=function(e){this.nextUrl=e.nextUrl
E.sub(E.Event.SIGN_IN_ERROR,function(e){return function(t,n){var a
e.trigger("sign_in:error")
e.clearErrors()
e.addError(e.$('label[for="login_email"]'),n)
return null!=(a=e.progressBar)?a.stop():void 0}}(this))
return E.sub(E.Event.JOIN_ERROR,function(e){return function(){var t
return null!=(t=e.progressBar)?t.stop():void 0}}(this))}
a.prototype.render=function(){return a.__super__.render.apply(this,arguments)}
a.prototype.attach=function(){a.__super__.attach.apply(this,arguments)
this.email=this.$("#login_email")
return this.password=this.$("#login_password")}
a.prototype.attemptLogin=function(e){e.preventDefault()
return this.validate()?this.login(e):void 0}
a.prototype.login=function(e){var t
this.trigger("form:submit")
this.progressBar=new E.lib.ButtonProgressBar({"button":this.$(e.currentTarget)})
t={"session":{"email":this.email.val(),"password":this.password.val()}}
this.nextUrl&&(t.nextURL=this.nextUrl)
return $.ajax({"url":"/login","method":"POST","data":t})}
return a}(Banana.View)
E.mix(Banana.views.users.SignInFormView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/sign_in"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="login-frame card">\n  <header>\n    <h3 class="hyphenated-heading">welcome back</span>\n    <h1 class="everlane-sans">Everlane</h1>\n  </header>\n\n  <div class="landing-error facebook"></div>\n\n\n  <div class="fb-container"></div>\n\n  <span class="hyphenated-heading or">or</span>\n\n  <!-- Error box for loging in, error boxes are mutually exclusive -->\n  <div class="error"></div>\n\n  <!-- Filled in by SignInFormView -->\n  <div class=\'login-form\'></div>\n\n  <p class="forgot-password">Forgot your password? <a href="/reset">Click here</a>.</p>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/sign_in"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.SignInView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/sign_in"
n.prototype.className="login-card"
n.prototype.initialize=function(e){return this.nextUrl=e.nextURL}
n.prototype.attach=function(){var e,t
n.__super__.attach.apply(this,arguments)
t=new Banana.views.users.SignInFormView({"container":this.$(".login-form"),"nextUrl":this.nextUrl})
this.subview("sign_in_form",t)
e=new Banana.views.users.FacebookConnectView({"container":this.$(".fb-container"),"text":"sign_in"})
this.subview("fb_connect",e)
this.listenTo(t,"form:submit",function(){return this.trigger("form:submit")})
this.listenTo(t,"sign_in:error",function(){return this.trigger("sign_in:error")})
return this.listenTo(e,"fb:connect",function(){return this.trigger("fb:connect")})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/users/signing_in"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<div class="logging-in-frame card">\n\n  <header>\n    <h3 class="hyphenated-heading">please wait</h3>\n    <h1 class="everlane-sans">Everlane</h1>\n  </header>\n\n  <p>It&rsquo;s nice to see you.<br>Signing in now.</p>\n\n  <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"preloader.gif",{"name":"staticImageUrl","hash":{},"data":a}))+'" width="70", height="20">\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/users/signing_in"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.SigningInView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/signing_in"
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.users.LoginModalView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/users/login_modal"
n.prototype.className="login-modal"
n.prototype.events={"click .bottom-bar a":function(e){return this.transitionTo($(e.currentTarget).data("card"))}}
n.prototype.cards={"sign_in":Banana.views.users.SignInView,"register":Banana.views.users.RegisterView,"minimal_register":Banana.views.users.MinimalRegisterView,"signing_in":Banana.views.users.SigningInView}
n.prototype.cardsContainer=".login-manager"
n.prototype.bottomBar={"sign_in":{"text":"Join Everlane","toCard":"register"},"register":{"text":"Already a Member? Log In","toCard":"sign_in"},"minimal_register":{"text":"Already a Member? Log In","toCard":"sign_in"}}
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
$.cookie("_everlane_user")&&null==this.currentCard&&(this.currentCard="sign_in")
return null!=this.currentCard?this.currentCard:this.currentCard="register"}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
this.listenTo(this.subview("sign_in"),"form:submit",function(){return this.transitionTo("signing_in")})
this.listenTo(this.subview("sign_in"),"fb:connect",function(){return this.transitionTo("signing_in")})
this.listenTo(this.subview("sign_in"),"sign_in:error",function(){return this.transitionTo("sign_in")})
this.listenTo(this.subview("register"),"form:submit",function(){return this.transitionTo("signing_in")})
this.listenTo(this.subview("register"),"fb:connect",function(){return this.transitionTo("signing_in")})
return this.listenTo(this.subview("register"),"register:error",function(){return this.transitionTo("register")})}
n.prototype.transitionTo=function(e){var t,a
n.__super__.transitionTo.apply(this,arguments)
t=this.bottomBar[e]
if(!t)return this.$(".bottom-bar").hide()
this.$(".bottom-bar").show()
a=this.$(".bottom-bar a")
a.text(t.text)
return a.data("card",t.toCard)}
return n}(Banana.views.components.CardView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.User=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.url="/api/user"
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t},n=[].indexOf||function(e){for(var t=0,n=this.length;n>t;t++)if(t in this&&this[t]===e)return t
return-1}
Banana.Controller=function(e){function a(){return a.__super__.constructor.apply(this,arguments)}t(a,e)
a.prototype.showHeader=!0
a.prototype.showFooter=!0
a.prototype.nonChaplinRoutes=["/checkout/shipping","/checkout/payment","/checkout/confirm","/checkout/empty_cart","/checkout/sign_in","/terms","/privacy"]
a.prototype.initialize=function(e,t){var a
if(a=t.path,n.call(this.nonChaplinRoutes,a)<0){this.showHeader&&this.reuse("application.header",Banana.views.application.chrome.HeaderView)
if(this.showFooter)return this.reuse("application.footer",Banana.views.application.chrome.FooterView)}}
a.prototype.beforeAction=function(){return $("meta[data-chaplin=true]").each(function(e,t){return $(t).attr("content","")})}
a.prototype.adjustMetaTag=function(e,t){var n
if(t){n=$("meta[name='"+e+"']")
n.length||(n=$("<meta name='"+e+"' />").appendTo("head"))
return n.attr({"content":t,"data-chaplin":!0})}}
return a}(Chaplin.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/waitlist_modal_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div id="waitlisted-modal">\n\n  <img alt="Product_waitlist_header" height="224" src="//everlane-2.imgix.net/static/product_waitlist_header.jpg?h=224" />\n\n  <p style="font-size: 16px; line-height: 23px;">\n   We&rsquo;ve added <em>'+s((r=null!=(r=t.display_name||(null!=e?e.display_name:e))?r:o,typeof r===i?r.call(e,{"name":"display_name","hash":{},"data":a}):r))+"</em><br>\n   to your waitlist.\n </p>\n\n <span>\n   We&rsquo;ll email you when it&rsquo;s back in stock.\n </span>\n\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/waitlist_modal_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.WaitlistModalView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/waitlist_modal_view"
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.application.TopLevelView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.hideSticker=!0
n.prototype.requireLogin=!1
n.prototype.region="content"
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
return E.session.isSignedIn()?void 0:this.listenTo(E,E.Event.App.ROUTE,function(){return"email"===E.currentQuery("utm_medium")?new Banana.views.components.ModalView({"view":{"class":Banana.views.users.LoginModalView,"initialCard":"sign_in"}}):void 0})}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
E.lib.LoadingIndicator.stop()
$("#page").removeClass().addClass(this.mainClass)
this.hideSticker?$(".sticker").hide():$(".sticker").show()
this.requireLogin&&!E.session.isSignedIn()&&new Banana.views.components.ModalView({"dismissible":!1,"view":Banana.views.users.LoginModalView})
return!E.session.isSignedIn()||E.chaplinCollections.user.get("has_password")||E.chaplinCollections.user.get("facebook_connected")?void 0:new Banana.views.components.ModalView({"dismissible":!1,"view":{"class":Banana.views.users.FinishRegistrationView,"minimalForm":!!E.currentQuery("reg")},"closeEvents":[E.Event.Registration.FULL_REGISTRATION]})}
n.prototype.openModalOnQueryParams=function(){var e,t,n,a,r,i,o,s
if($.cookie("dismissModal")||E.session.isSignedIn())return!1
e=decodeURIComponent(E.currentQuery("m_options"))
i=_.object(_.map(e.split("|"),function(e){return e.split(":")}))
a={"product":{"click":[$(".groupings"),"a"]}}
n=0===parseInt(null!=i?i.d:void 0)?!1:!0
t=i.w||0
s=1===parseInt(null!=i?i.e:void 0)?!0:!1
r="min"===i.f?"minimal_register":"register"
o=a[i.t]||!1
new Banana.views.components.ModalView({"dismissible":n,"delay":t,"triggerOnExit":s,"eventTrigger":o,"view":{"class":Banana.views.users.LoginModalView,"initialCard":r}})
return $.cookie("dismissModal",1,{"expires":1})}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
E.currentQuery("m_options")&&this.openModalOnQueryParams()
return E.pub(E.Event.App.RENDER,this)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/index_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div id="sidebar" class="column grid_2">\n</div>\n\n<div id="main" class="column grid_10">\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/index_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.regions={"sidebar":"#sidebar","main":"#main"}
n.prototype.template="banana/templates/account/index_view"
n.prototype.mainClass="account"
n.prototype.className="row"
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
return E.session.isSignedIn()?void 0:new Banana.views.components.ModalView({"dismissible":!1,"view":Banana.views.users.LoginModalView})}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/sidebar_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="account-menu sidebar"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/sidebar_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.SidebarView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="sidebar"
n.prototype.template="banana/templates/account/sidebar_view"
n.prototype.render=function(){var e,t
e=n.__super__.render.apply(this,arguments)
t=new Banana.views.account.NavigationView({"container":this.$(".sidebar")})
this.subview("navigation_items",t)
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/info_view"]=Handlebars.template({"1":function(){return'checked="checked"'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div id="account-page" class="account-page clearfix">\n  <div class="column grid_7 prefix_1">\n    <h3>Edit your account information.</h3>\n    <div><p id="user_form_error"></p></div>\n    <form accept-charset="UTF-8" class="checkout_form fancy-form formtastic user">\n      <div class="form_fields clearfix">\n        <div class="field half_width">\n          <label for="user_first_name">First Name</label>\n          <input class="text" id="user_first_name" name="user[first_name]" size="30" type="text" value="'+l((i=null!=(i=t.first_name||(null!=e?e.first_name:e))?i:s,typeof i===o?i.call(e,{"name":"first_name","hash":{},"data":a}):i))+'">\n        </div>\n        <div class="field half_width">\n          <label for="user_last_name">Last Name</label>\n          <input class="text" id="user_last_name" name="user[last_name]" size="30" type="text" value="'+l((i=null!=(i=t.last_name||(null!=e?e.last_name:e))?i:s,typeof i===o?i.call(e,{"name":"last_name","hash":{},"data":a}):i))+'">\n        </div>\n        <div class="field">\n          <label for="user_email">Email Address</label>\n          <input class="text" id="user_email" name="user[email]" placeholder="" size="30" type="text" value="'+l((i=null!=(i=t.email||(null!=e?e.email:e))?i:s,typeof i===o?i.call(e,{"name":"email","hash":{},"data":a}):i))+'">\n        </div>\n        <div class="field">\n          <label for="user_new_password">New Password</label>\n          <input class="text" id="user_new_password" name="user[password]" placeholder="" size="30" type="password">\n        </div>\n\n        <div class="field half_width">\n          <label for="gender_female_Female">Female</label>\n          <input '
r=t["if"].call(e,null!=e?e.female:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+=' id="user_gender_female" name="user[gender]" type="radio" value="female">\n        </div>\n        <div class="field half_width">\n          <label for="gender_male_Male">Male</label>\n          <input '
r=t["if"].call(e,null!=e?e.male:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+' id="user_gender_male" name="user[gender]" type="radio" value="male">\n        </div>\n\n      </div>\n      <div class="form_fields">\n        <div class="field">\n          <input class="fancy-button orange" name="commit" type="submit" value="Save information">\n        </div>\n      </div>\n    </form>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/info_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.InfoView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="main"
n.prototype.template="banana/templates/account/info_view"
n.prototype.events={"submit form":"saveUser"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.male="male"===e.gender
e.female="female"===e.gender
return e}
n.prototype.saveUser=function(e){var t,n,a
e.preventDefault()
a=this.$("input[name=commit]")
t={"first_name":this.$("input[name='user[first_name]']").val(),"last_name":this.$("input[name='user[last_name]']").val(),"email":this.$("input[name='user[email]']").val(),"password":this.$("input[name='user[password]']").val(),"gender":this.$("input:checked[name='user[gender]']").val()}
n=t.password.length>0&&t.password.length<3
this.$("input#user_new_password").toggleClass("error",n)
if(!n){a.disableWith("Saving...")
return this.model.save(t,{"patch":!0,"success":function(e){return function(){e.$("#user_form_error").html("Your account information has been updated")
return a.clearDisableWith()}}(this),"error":function(e){return function(){e.$("#user_form_error").html("Your account information could not be updated")
return a.clearDisableWith()}}(this)})}this.$("#user_form_error").html("Your password must be more than three characters.")}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/addresses/simple_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"  "+s((r=null!=(r=t.company||(null!=e?e.company:e))?r:o,typeof r===i?r.call(e,{"name":"company","hash":{},"data":a}):r))+"<br>\n"},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"  "+s((r=null!=(r=t.extended_address||(null!=e?e.extended_address:e))?r:o,typeof r===i?r.call(e,{"name":"extended_address","hash":{},"data":a}):r))+"<br>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c="<h4>"+l((i=null!=(i=t.full_name||(null!=e?e.full_name:e))?i:s,typeof i===o?i.call(e,{"name":"full_name","hash":{},"data":a}):i))+"</h4>\n"
r=t["if"].call(e,null!=e?e.company:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"+l((i=null!=(i=t.street_address||(null!=e?e.street_address:e))?i:s,typeof i===o?i.call(e,{"name":"street_address","hash":{},"data":a}):i))+"<br>\n"
r=t["if"].call(e,null!=e?e.extended_address:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"\n"+l((i=null!=(i=t.city_line||(null!=e?e.city_line:e))?i:s,typeof i===o?i.call(e,{"name":"city_line","hash":{},"data":a}):i))},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/addresses/simple_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.addresses.SimpleView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/account/addresses/simple_view"
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/modal_view"]=Handlebars.template({"1":function(){return'  <div class="reveal-close-button">&#215;</div>\n'},"3":function(){return'    <div class="reveal-close-button">&#215;</div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="\n"
r=t.unless.call(e,null!=e?e.inSubview:e,{"name":"unless","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+='\n<div class="modal__container">\n'
r=t["if"].call(e,null!=e?e.inSubview:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"</div>\n"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/modal_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.ModalView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.container="#page"
n.prototype.template="banana/templates/components/modal_view"
n.prototype.className="modal__bg"
n.prototype.events={"click":"attemptDismissal","click .reveal-close-button":"attemptDismissal","click .modal__container":"dontPropogate"}
n.prototype.options={"backgroundClass":"modal__bg--defaults","delay":0,"dismissible":!0,"triggerOnExit":!1,"closeButtonInSubview":!0}
n.prototype.initialize=function(e){var t,n,a,r
null==e&&(e={})
this.options=_.extend({},this.options,e)
if(null==this.options.view)throw new Error("ModalView requires a view option")
this.options.view.prototype instanceof Backbone.View&&(this.options.view={"class":this.options.view})
if(!_.isObject(this.options.view)||null==this.options.view["class"])throw new Error("view option must be either a backbone view or object with a class attribute")
null==this.options.closeEvents&&(this.options.closeEvents=[])
this.options.closeEvents.push(E.Event.Exit)
r=this.options.closeEvents
for(n=0,a=r.length;a>n;n++){t=r[n]
E.sub(t,function(e){return function(){return e.dismiss()}}(this))}this.locked=!1
this.options.dismissible||(this.locked=!0)
this.locked&&this.$el.addClass("locked")
this.positionProxy=function(e){return function(t){return e.positionElements(t)}}(this)
this.keypressProxy=function(e){return function(t){return e.handleKeyPresses(t)}}(this)
$(window).on("resize.modal_view",this.positionProxy)
return $(document).on("keydown.modal_view",this.keypressProxy)}
n.prototype.dispose=function(){n.__super__.dispose.apply(this,arguments)
$(window).off("resize.modal_view",this.positionProxy)
return $(document).off("keydown.modal_view",this.keypressProxy)}
n.prototype.attach=function(){var e,t,a,r,i,o
n.__super__.attach.apply(this,arguments)
i=this.options.view["class"]
a=this.options.view.events
o=_.omit(this.options.view,"class","events")
r=new i(_.extend({},o,{"container":this.$el.find(".modal__container")}))
this.subview("modal__container",r)
for(t in a){e=a[t]
this.listenTo(r,t,e)}this.positionElements()
this.$el.addClass(this.options.backgroundClass).hide()
this.$(".modal__container").hide()
return this.options.eventTrigger?_.each(this.options.eventTrigger,function(e){return function(t,n){return t[0].on(""+n+".modal",t[1],$.proxy(e.reveal,e))}}(this)):this.triggerOnExit?ouibounce(!1,{"callback":function(e){return function(){return e.reveal()}}(this)}):this.reveal()}
n.prototype.reveal=function(e){e&&this.dontPropogate(e)
return this.$el.velocity("transition.fadeIn",{"duration":200,"complete":function(e){return function(){return e.$(".modal__container").velocity("transition.fadeIn",{"duration":400})}}(this)})}
n.prototype.dontPropogate=function(e){return e.stopPropagation()}
n.prototype.lock=function(){this.locked=!0
return this.$el.addClass("locked")}
n.prototype.unlock=function(){this.locked=!1
return this.$el.removeClass("locked")}
n.prototype.attemptDismissal=function(){return this.locked?void 0:this.dismiss()}
n.prototype.dismiss=function(){return this.$(".modal__container").velocity("transition.fadeOut",{"duration":200,"complete":function(e){return function(){return e.$el.velocity("transition.fadeOut",{"duration":400})}}(this)})}
n.prototype.positionElements=function(){var e
e=this.$(".modal__container")
return e.css({"top":(window.innerHeight-e.height())/2,"left":(window.innerWidth-e.width())/2})}
n.prototype.handleKeyPresses=function(e){switch(e.which){case 27:return this.attemptDismissal()}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.inSubview=this.options.closeButtonInSubview
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/orders/cancel_modal_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<div id="order-cancel-modal">\n  <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"cancel_order_header.png",{"name":"staticImageUrl","hash":{},"data":a}))+'"></img>\n  <p>Type "cancel" below to confirm.</p>\n  <form id="order-cancel-form" class="clearfix">\n    <input type="text" id="order-cancel-verification">\n    <button type="submit" class="small fancy-button grey inactive" id="cancel-order-submit">Cancel My Order</button>\n  </form>\n  <a class="text-close" href="#">oops, get me out of here</a>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/orders/cancel_modal_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.orders.CancelModalView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/orders/cancel_modal_view"
n.prototype.events={"click .text-close":function(){return this.trigger("modal:close")},"submit #order-cancel-form":"submit","keyup #order-cancel-verification":"modifyColor"}
n.prototype.submit=function(e){var t
e.preventDefault()
this.trigger("form:submit")
t=new E.lib.ButtonProgressBar({"button":this.$("#cancel-order-submit"),"loadingText":"cancelling..."})
t.start()
return this.model.cancel({"success":function(e){return function(){t.stop()
return e.trigger("form:success")}}(this),"error":function(e){return function(){t.stop()
return e.trigger("form:error")}}(this)})}
n.prototype.modifyColor=function(e){var t,n,a,r
t=this.$(e.currentTarget)
a=this.$("#cancel-order-submit")
r=t.val()
n={"c":"#cdcdcd","ca":"#cfc3c3","can":"#d2a9a9","canc":"#d48181","cance":"#d95656","cancel":"#df3939"}
a.css("background-color",n[r]||"#cdcdcd")
return"cancel"===r?a.removeClass("inactive").addClass("red"):a.addClass("inactive")}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.UserInfoSnippetsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
return this.selected=e.selected}
n.prototype.initItemView=function(e){var t
t=n.__super__.initItemView.apply(this,arguments)
if(this.selected){t.observeModel=!1
e.id===this.selected&&(t.selected=!0)}this.listenTo(t,"result",function(e){return function(t){return e.trigger("result",t)}}(this))
return t}
return n}(Banana.CollectionView)}).call(this);(function(){E.mixins.UserInfoSnippet={"tagName":"li","hideControls":!1,"events":{"click .remove":"disable","click .restore":"restore","click .make-primary":"makePrimary"},"initialize":function(e){_.extend(this,_.pick(e,"hideControls"))
return this.listenTo(this.model,"change:primary",this.changePrimaryModel)},"changePrimaryModel":function(){return this.render()},"isSelected":function(){return this.model.get("primary")},"render":function(){if(!this.template)throw new Error(this.toString()+" must implement `template`")
this.$el.html(this.template(this.model.toJSON()))
this.primaryText=this.$(".primary-text")
this.restoreButton=this.$(".restore")
this.removeButton=this.$(".remove")
this.makePrimaryButton=this.$(".make-primary")
this.restoreButton.hide()
this.isSelected()?this.makePrimaryButton.hide():this.primaryText.hide()
if(this.hideControls){this.removeButton.hide()
this.makePrimaryButton.hide()
this.primaryText.hide()}return this},"restore":function(){this.$el.fadeTo(500,1)
this.restoreButton.hide()
this.removeButton.show()
this.isSelected()||this.makePrimaryButton.show()
this.model.set("disabled",!1)
return this.model.save()},"disable":function(){this.$el.fadeTo(300,.3)
this.restoreButton.show()
this.removeButton.hide()
this.makePrimaryButton.hide()
this.model.set("disabled",!0)
return this.model.save()},"makePrimary":function(){this.primaryText.show()
this.removeButton.hide()
this.makePrimaryButton.hide()
this.model.set("primary",!0)
return this.model.save()}}}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.UserInfoSnippetView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.observeModel=!0
n.prototype.hideControls=!1
n.prototype.optionNames=["observeModel","hideControls"]
n.prototype.initialize=function(e){null==e&&(e={})
return e.filterer||(e.filterer=function(e){return!e.attributes.disabled})}
n.prototype.changePrimaryModel=function(){return this.observeModel&&this.render()}
n.prototype.isSelected=function(){return this.observeModel?this.model.get("primary"):this.selected}
n.prototype.makePrimary=function(){return this.trigger("result",this.model)}
return n}(Banana.View)
E.mix(Banana.views.account.UserInfoSnippetView,E.mixins.UserInfoSnippet)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.addresses.ItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template=_.template($(E.templates.address).html())
n.prototype.toString=function(){return"Banana.views.account.addresses.ItemView"}
return n}(Banana.views.account.UserInfoSnippetView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/addresses/list_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<ul class="addresses clearfix">\n  <li class="fallback"><h3 class="fallback">You have no addresses on file</h3></li>\n  <li class="loading">\n    <img class="load-animation" src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"preloader.gif",{"name":"staticImageUrl","hash":{},"data":a}))+'" />\n  </li>\n</ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/addresses/list_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.addresses.ListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/account/addresses/list_view"
n.prototype.itemView=Banana.views.account.addresses.ItemView
n.prototype.listSelector=".addresses.clearfix"
n.prototype.loadingSelector=".loading"
n.prototype.fallbackSelector=".fallback"
n.prototype.initialize=function(){E.chaplinCollections.addresses.fetch()
this.collection=E.chaplinCollections.addresses
return n.__super__.initialize.apply(this,arguments)}
return n}(Banana.views.account.UserInfoSnippetsView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/addresses/new_shipping_address_dialog_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'            <option value="'+s((r=null!=(r=t.id||(null!=e?e.id:e))?r:o,typeof r===i?r.call(e,{"name":"id","hash":{},"data":a}):r))+'">'+s((r=null!=(r=t.name||(null!=e?e.name:e))?r:o,typeof r===i?r.call(e,{"name":"name","hash":{},"data":a}):r))+"</option>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=this.lambda,o=this.escapeExpression,s='<div id="newShippingDialog">\n  <h3 class="subheader">New Shipping Address</h3>\n\n  <div class="status-message"></div>\n\n  <div class="dialog-content">\n\n    <!-- TODO Snip this for address partial -->\n    <form accept-charset="UTF-8" class="checkout_form smaller address_form" id="new_address">\n      <div style="margin:0;padding:0;display:inline">\n        <input name="utf8" type="hidden" value="&#x2713;" />\n      </div>\n\n      <div class="form_fields clearfix">\n        <div class="field half_width">\n          <label for="address_full_name">Full Name</label>\n          <input class="text full_name fancy-input" id="address_full_name" name="address[full_name]" size="30" type="text" value="'+o(i(null!=(r=null!=e?e.user:e)?r.first_name:r,e))+" "+o(i(null!=(r=null!=e?e.user:e)?r.last_name:r,e))+'" />\n\n          <input class="first_name" id="address_first_name" name="address[first_name]" type="hidden" value="'+o(i(null!=(r=null!=e?e.user:e)?r.first_name:r,e))+'" />\n          <input class="last_name" id="address_last_name" name="address[last_name]" type="hidden" value="'+o(i(null!=(r=null!=e?e.user:e)?r.last_name:r,e))+'" />\n        </div>\n\n        <div class="field half_width">\n          <label for="address_company">Organization</label>\n          <input class="text fancy-input" id="address_company" name="address[company]" placeholder="e.g., Apple" size="30" type="text" />\n        </div>\n\n        <div class="field">\n          <label for="address_street_address">Street Address</label>\n          <input class="text fancy-input" id="address_street_address" name="address[street_address]" placeholder="e.g., 555 Main St." size="30" type="text" />\n        </div>\n\n        <div class="field">\n          <input class="text fancy-input" id="address_extended_address" name="address[extended_address]" size="30" type="text" />\n        </div>\n\n        <div class="field half_width">\n          <label for="address_city">City</label>\n          <input class="text fancy-input" id="address_city" name="address[city]" placeholder="e.g., Chicago" size="30" type="text" />\n        </div>\n\n        <div class="field half_width">\n          <label for="address_country">Country</label>\n          <select class="country fancy-select-without-search" id="address_country" name="address[country]" style="width: 100%;">\n            <option value="US">USA</option>\n            <option value="CA">Canada</option>\n          </select>\n        </div>\n\n        <div class="field half_width USA">\n          <label for="address_state_id">State</label>\n          <select class="fancy-select" data-placeholder="Select State" id="state_id" name="address[state_id]" style="width: 100%;">\n            <option value=""></option>\n'
r=t.each.call(e,null!=e?e.usa:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(s+=r)
s+='          </select>\n        </div>\n\n        <div class="field half_width CAN">\n          <label for="address_province_id">Province</label>\n          <select class="fancy-select" data-placeholder="Select Province" id="province_id" name="address[province_id]" style="width: 100%;">\n            <option value=""></option>\n'
r=t.each.call(e,null!=e?e.canada:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(s+=r)
return s+'          </select>\n        </div>\n\n        <div class="field half_width">\n          <label for="address_postal_code">Postal Code</label>\n          <input class="text fancy-input" id="address_postal_code" maxlength="10" name="address[postal_code]" size="10" type="text" />\n        </div>\n      </div>\n\n      <div class="form_fields clearfix">\n        <div class="field">\n          <a href="#" class="fancy-button cancel" style="float: left">Cancel</a>\n          <input class="fancy-button orange" name="commit" style="float: right" type="submit" value="Create Shipping Address" />\n        </div>\n      </div>\n    </form>\n\n    <!-- end snip -->\n\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/addresses/new_shipping_address_dialog_view"]}).call(this);(function(){Banana.globals.usa=E.data.states.usa
Banana.globals.canada=E.data.states.canada}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
Banana.views.account.addresses.NewShippingAddressDialogView=function(t){function a(){this.changeCountry=e(this.changeCountry,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.template="banana/templates/account/addresses/new_shipping_address_dialog_view"
a.prototype.events={"click .cancel":"close","change .country":"changeCountry","submit .address_form":"submitForm"}
a.prototype.attach=function(){a.__super__.attach.apply(this,arguments)
this.$("#address_full_name").select()
this.$(".fancy-select").select2({"formatSelection":function(e){console.log(e)
return e.text}})
this.$(".fancy-select-without-search").select2({"minimumResultsForSearch":-1})
return this.$(".country").change()}
a.prototype.parseName=function(){var e
e=NameParse.parse(this.$(".full_name").val())
this.$(".first_name").val(e.firstName)
this.$(".last_name").val(e.lastName)
this.$(".full_name").removeProp("name")
return e}
a.prototype.submitForm=function(e){var t,n,a,r,i,o,s,l,c,p
e.preventDefault()
this.trigger("form:submit")
o=this.parseName()
a=this.$(".address_form")
s=this.$("input[name=commit]")
s.disableWith("Creating...")
r={}
p=a.serializeArray()
for(l=0,c=p.length;c>l;l++){n=p[l]
r[n.name]=n.value}i=new Banana.models.Address
t={"first_name":r["address[first_name]"],"last_name":r["address[last_name]"],"street_address":r["address[street_address]"],"extended_address":r["address[extended_address]"],"city":r["address[city]"],"postal_code":r["address[postal_code]"],"state_id":r["address[state_id]"],"province_id":r["address[province_id]"],"country":r["address[country]"],"company":r["address[company]"]}
return i.save(t,{"success":function(e){return function(t){return e.trigger("form:success",t)}}(this),"error":function(e){return function(){var t
e.trigger("form:error")
t=e.$(".status-message")
t.html("Hmmm, looks like something is incorrect&hellip;")
t.data("has-error")?t.effect("highlight",750):t.velocity("fadeIn").effect("highlight",{},1500).data("has-error",!0)
return s.clearDisableWith()}}(this)})}
a.prototype.changeCountry=function(e){var t,n
t=this.$(e.currentTarget).select2("val")
this.$(".address_form .USA").toggle("US"===t)
this.$(".address_form .CAN").toggle("CA"===t)
n="US"===t?"ZIP Code":"Postal Code"
return this.$('label[for="address_postal_code"]').text(n)}
a.prototype.getTemplateData=function(){var e
e=a.__super__.getTemplateData.apply(this,arguments)
e.usa=Banana.globals.usa
e.canada=Banana.globals.canada
e.user=E.session.getCurrentUser()
return e}
a.prototype.close=function(){this.$(".address_form .fancy-select").select2("close")
this.$(".address_form .fancy-select-without-search").select2("close")
return E.pub(E.Event.Exit)}
return a}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/addresses/modal_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div id="pickShippingDialog">\n  <h3 class="subheader">Select a Shipping Address</h3>\n\n  <div class="dialog-content">\n    <div class="list"></div>\n\n    <a class="orange add-new-address" href="#">Add New Shipping Address</a>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/addresses/modal_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.addresses.ModalView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/account/addresses/modal_view"
n.prototype.regions={"addresses":".list"}
n.prototype.events={"click .add-new-address":"addNewAddress"}
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
return this.selected=e.selected}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.account.addresses.ListView({"region":"addresses","selected":this.selected})
this.listenTo(e,"result",function(e){return function(t){e.trigger("result",t)
return $(e.container.context).trigger("reveal:close")}}(this))
return this.subview("addresses-modal",e)}
n.prototype.addNewAddress=function(){$(this.container.context).trigger("reveal:close")
return new Banana.views.components.ModalView({"view":Banana.views.account.addresses.NewShippingAddressDialogView})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/orders/item_view"]=Handlebars.template({"1":function(){return"primary"},"3":function(){return'    <div class="shipping-address-details"></div>\n'},"5":function(e){var t,n=this.lambda,a=this.escapeExpression
return"    "+a(n(null!=(t=null!=e?e.user:e)?t.full_name:t,e))+"\n"},"7":function(){return'  <small><a href="javascript:;" class="change-shipping-info">Change Shipping Info</a></small>\n'},"9":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='    <div id="shipment-'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'" class="shipment '
r=t["if"].call(e,null!=e?e.shipped:e,{"name":"if","hash":{},"fn":this.program(10,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='">\n      <span id="shipment-'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'-status">\n        '+l((i=null!=(i=t.stateMessage||(null!=e?e.stateMessage:e))?i:s,typeof i===o?i.call(e,{"name":"stateMessage","hash":{},"data":a}):i))+"\n"
r=t["if"].call(e,null!=e?e.hasSameDayDelivery:e,{"name":"if","hash":{},"fn":this.program(12,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.tracking_url:e,{"name":"if","hash":{},"fn":this.program(14,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="      </span>\n\n"
r=t.each.call(e,null!=e?e.line_items:e,{"name":"each","hash":{},"fn":this.program(16,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"    </div>\n"},"10":function(){return"disabled"},"12":function(){return'          <span class="same-day-delivery">1-hour&nbsp;delivery</span>\n'},"14":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <a style="color: #555;" href="'+s((r=null!=(r=t.tracking_url||(null!=e?e.tracking_url:e))?r:o,typeof r===i?r.call(e,{"name":"tracking_url","hash":{},"data":a}):r))+'">- Tracking info</a>\n'},"16":function(e,t,n,a){var r,i=""
r=this.invokePartial(n.line_item,"        ","line_item",e,void 0,t,n,a)
null!=r&&(i+=r)
return i},"18":function(e,t,n,a){var r,i=""
r=t.each.call(e,null!=e?e.line_items:e,{"name":"each","hash":{},"fn":this.program(19,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"19":function(e,t,n,a){var r,i=""
r=this.invokePartial(n.line_item,"      ","line_item",e,void 0,t,n,a)
null!=r&&(i+=r)
return i},"21":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.has_return_units:e,{"name":"if","hash":{},"fn":this.program(22,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"22":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='      <span id="return-units-header-'+l((i=null!=(i=t.number||(null!=e?e.number:e))?i:s,typeof i===o?i.call(e,{"name":"number","hash":{},"data":a}):i))+'">\n        Returns for this order:\n'
r=t["if"].call(e,null!=e?e["has_receivable_return_units?"]:e,{"name":"if","hash":{},"fn":this.program(23,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="      </span>\n\n"
r=t.each.call(e,null!=e?e.return_units:e,{"name":"each","hash":{},"fn":this.program(25,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t.each.call(e,null!=e?e.orphan_received_return_units:e,{"name":"each","hash":{},"fn":this.program(25,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c},"23":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'          <a href="/orders/'+s((r=null!=(r=t.number||(null!=e?e.number:e))?r:o,typeof r===i?r.call(e,{"name":"number","hash":{},"data":a}):r))+"/return_authorizations/"+s((r=null!=(r=t.last_rma_number||(null!=e?e.last_rma_number:e))?r:o,typeof r===i?r.call(e,{"name":"last_rma_number","hash":{},"data":a}):r))+'?clean=1" id="return-instructions-link-'+s((r=null!=(r=t.number||(null!=e?e.number:e))?r:o,typeof r===i?r.call(e,{"name":"number","hash":{},"data":a}):r))+'">\n            instructions\n          </a>\n'},"25":function(e,t,n,a){var r,i=""
r=this.invokePartial(n.return_unit,"        ","return_unit",e,void 0,t,n,a)
null!=r&&(i+=r)
return i},"27":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.shipped:e,{"name":"if","hash":{},"fn":this.program(28,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"28":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e["returnable?"]:e,{"name":"if","hash":{},"fn":this.program(29,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"29":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <a href="/orders/'+s((r=null!=(r=t.number||(null!=e?e.number:e))?r:o,typeof r===i?r.call(e,{"name":"number","hash":{},"data":a}):r))+'/return_authorizations/new">New&nbsp;Return&nbsp;or&nbsp;Exchange</a>\n'},"31":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.allow_cancel:e,{"name":"if","hash":{},"fn":this.program(32,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"32":function(e,t,n,a){var r,i=""
r=t.unless.call(e,null!=e?e.has_one_hour_delivery_items:e,{"name":"unless","hash":{},"fn":this.program(33,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"33":function(){return'        <a class="cancel-order" href="#">Cancel&nbsp;Order</a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="col one-wide">\n  <strong>'+l((i=null!=(i=t.completed_at||(null!=e?e.completed_at:e))?i:s,typeof i===o?i.call(e,{"name":"completed_at","hash":{},"data":a}):i))+'</strong>\n</div>\n\n<div class="col two-wide">\n  <div class="'
r=t["if"].call(e,null!=e?e.latest_completed_order:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+=' shipping-address">\n'
r=t["if"].call(e,null!=e?e.shipping_address:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.program(5,a),"data":a})
null!=r&&(c+=r)
c+="  </div>\n\n"
r=t["if"].call(e,null!=e?e.latest_completed_order:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='</div>\n\n<div class="col five-wide">\n  <strong class="order-number">\n    Order: '+l((i=null!=(i=t.number||(null!=e?e.number:e))?i:s,typeof i===o?i.call(e,{"name":"number","hash":{},"data":a}):i))+"\n  </strong>\n\n"
r=t.each.call(e,null!=e?e.shipments:e,{"name":"each","hash":{},"fn":this.program(9,a),"inverse":this.program(18,a),"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.show_returns:e,{"name":"if","hash":{},"fn":this.program(21,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='</div>\n\n<div class="col two-wide last">\n  '+l((i=null!=(i=t.display_price||(null!=e?e.display_price:e))?i:s,typeof i===o?i.call(e,{"name":"display_price","hash":{},"data":a}):i))+"<br />\n"
r=t["if"].call(e,null!=e?e.show_returns:e,{"name":"if","hash":{},"fn":this.program(27,a),"inverse":this.program(31,a),"data":a})
null!=r&&(c+=r)
return c+"</div>"},"usePartial":!0,"useData":!0})
return this.HandlebarsTemplates["banana/templates/orders/item_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/orders/line_item"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"  "+s((r=null!=(r=t.size||(null!=e?e.size:e))?r:o,typeof r===i?r.call(e,{"name":"size","hash":{},"data":a}):r))+"\n"},"3":function(e){var t,n=this.lambda,a=this.escapeExpression
return"    <br><em>To:&nbsp;"+a(n(null!=(t=null!=e?e.unit_attributes:e)?t.recipient_name:t,e))+"&nbsp;("+a(n(null!=(t=null!=e?e.unit_attributes:e)?t.recipient_email:t,e))+")</em>\n    <br/>Can&nbsp;not&nbsp;be&nbsp;returned.\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="line_item">\n  ( '+l((i=null!=(i=t.quantity||(null!=e?e.quantity:e))?i:s,typeof i===o?i.call(e,{"name":"quantity","hash":{},"data":a}):i))+" )\n  <b>"+l((i=null!=(i=t.title||(null!=e?e.title:e))?i:s,typeof i===o?i.call(e,{"name":"title","hash":{},"data":a}):i))+"</b> &mdash;\n"
r=t["if"].call(e,null!=e?e.size:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.is_giftcard:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/orders/line_item"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/orders/return_unit"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"  "+s((r=null!=(r=t.size||(null!=e?e.size:e))?r:o,typeof r===i?r.call(e,{"name":"size","hash":{},"data":a}):r))+"\n"},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"    ( "+s((r=null!=(r=t.returned_quantity||(null!=e?e.returned_quantity:e))?r:o,typeof r===i?r.call(e,{"name":"returned_quantity","hash":{},"data":a}):r))+' / <span id="return-unit-quantity-'+s((r=null!=(r=t.id||(null!=e?e.id:e))?r:o,typeof r===i?r.call(e,{"name":"id","hash":{},"data":a}):r))+'">'+s((r=null!=(r=t.quantity||(null!=e?e.quantity:e))?r:o,typeof r===i?r.call(e,{"name":"quantity","hash":{},"data":a}):r))+"</span> received )\n"},"5":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"    ( "+s((r=null!=(r=t.quantity||(null!=e?e.quantity:e))?r:o,typeof r===i?r.call(e,{"name":"quantity","hash":{},"data":a}):r))+" received )\n"},"7":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'    <a href="javascript:;" class="cancel-return-unit" data-id="'+s((r=null!=(r=t.id||(null!=e?e.id:e))?r:o,typeof r===i?r.call(e,{"name":"id","hash":{},"data":a}):r))+'" id="return-unit-cancel-link-'+s((r=null!=(r=t.id||(null!=e?e.id:e))?r:o,typeof r===i?r.call(e,{"name":"id","hash":{},"data":a}):r))+'">cancel remaining</a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="line_item" id="return-unit-'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'">\n  <b>'+l((i=null!=(i=t.title||(null!=e?e.title:e))?i:s,typeof i===o?i.call(e,{"name":"title","hash":{},"data":a}):i))+"</b> &mdash;\n"
r=t["if"].call(e,null!=e?e.size:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="  <br/>\n\n"
r=t["if"].call(e,null!=e?e.id:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.program(5,a),"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e["cancelable?"]:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/orders/return_unit"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.orders.ItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.template="banana/templates/orders/item_view"
n.prototype.className="clearfix order"
n.prototype.listen={"cancel model":"cancelOrder","cancel_return model":"canelReturnComplete","returnUnitUpdated model":"returnUnitUpdated"}
n.prototype.events={"click .cancel-order":"cancel","click .cancel-return":"cancelReturn","click .change-shipping-info":"changeShippingInfo","click .cancel-return-unit":"cancelReturnUnit"}
n.prototype.partials={"line_item":"banana/templates/orders/line_item","return_unit":"banana/templates/orders/return_unit"}
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
return this.show_returns=e.show_returns}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
this.render_shipping_address()
return this}
n.prototype.render_shipping_address=function(){var e
e=new Banana.models.Address(this.model.attributes.shipping_address)
return this.subview("shipping-address",new Banana.views.account.addresses.SimpleView({"model":e,"container":this.$(".shipping-address-details")}))}
n.prototype.getTemplateData=function(){var e,t,a,r,i,o,s,l,c,p,u,h,d
t=n.__super__.getTemplateData.apply(this,arguments)
c=t.shipments
for(i=0,s=c.length;s>i;i++){r=c[i]
r.shipped="shipped"===r.state
r.stateMessage=function(){switch(r.state){case"ready":return"Ships soon"
case"backordered":return"Shipping later"
case"shipped":return"Shipped on "+r.shipped_at}}()
r.hasSameDayDelivery=6===(p=r.fulfillment_center_id)}u=t.returns
for(o=0,l=u.length;l>o;o++){a=u[o]
a.zero=0===a.quantity
a.one=1===a.quantity
a.many=a.quantity>1
a.authorized="authorized"===a.state}t.show_returns=this.show_returns
t.shipped="partially_shipped"===(h=t.state)||"fully_shipped"===h
t.cancel_remaining=_.any(t.shipments,function(e){return e.allow_cancel})
t.last_rma_number=null!=(d=_.last(t.returns))?d.number:void 0
e=_.union(t.return_units,t.orphan_received_return_units)
t.has_return_units=_.any(e,function(e){return e.quantity>0})
return t}
n.prototype.cancel=function(){return new Banana.views.components.ModalView({"view":{"class":Banana.views.orders.CancelModalView,"model":this.model,"events":{"modal:close":function(){return this.dismiss()},"form:success":function(){return this.dismiss()},"form:submit":function(){return this.lock()},"form:error":function(){return this.dismiss()}}}})}
n.prototype.cancelReturn=function(e){var t
e.preventDefault()
t=$(e.target).data("number")
return this.model.cancel_return(t)}
n.prototype.canelReturnComplete=function(e,t){return e?alert("Could not cancel return. Contact support@everlane.com for help."):this.$("#"+t).hide()}
n.prototype.changeShippingInfo=function(){var e,t
t=this.model.attributes.shipping_address.id
e=new Banana.views.components.ModalView({"view":{"class":Banana.views.account.addresses.ModalView,"selected":t}})
return this.listenToOnce(e.subviews[0],"result",function(e){return function(t){e.model.set({"shipping_address":t.attributes})
e.render_shipping_address()
e.$(".shipping-address-details").delay(700).effect("highlight")
return e.model.save({"ship_address_id":t.id})}}(this))}
n.prototype.cancelOrder=function(e){var t,n,a,r,i
if(e)return alert("Sorry, we couldn't cancel because it has already left our warehouse. Contact support@everlane.com for help.")
this.$(".cancel-order").remove()
if(this.model.attributes.shipments.length>0){r=this.model.attributes.shipments
i=[]
for(n=0,a=r.length;a>n;n++){t=r[n]
this.$("#shipment-"+t.id).addClass("disabled")
i.push(this.$("#shipment-"+t.id+"-status").delay(700).html("Canceled").effect("highlight"))}return i}return this.$(".line_item").delay(700).html("Canceled").effect("highlight")}
n.prototype.cancelReturnUnit=function(e){var t
t=$(e.currentTarget).data("id")
return $("#return-unit-"+t).fadeOut(400,function(e){return function(){return e.model.cancel_remaining(t)}}(this))}
n.prototype.returnUnitUpdated=function(e,t){var n,a
a=this.model.attributes
n=_.union(a.return_units,a.orphan_received_return_units)
if(_.every(n,function(e){return 0===e.quantity})){$("#return-instructions-link-"+a.number).fadeOut()
return $("#return-units-header-"+a.number).fadeOut()}if(t){$("#return-unit-quantity-"+e).html(t)
$("#return-unit-cancel-link-"+e).hide()
return $("#return-unit-"+e).fadeIn()}}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/orders/list_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<div class="order-page">\n  <h2 class="orders-header">My Orders</h2>\n\n  <div class="order-table-headers clearfix">\n    <div class="col one-wide">\n      <h3>Date</h3>\n    </div>\n\n    <div class="col two-wide">\n      <h3>Recipient</h3>\n    </div>\n\n    <div class="col five-wide">\n      <h3>Order&nbsp;&amp;&nbsp;Shipment Information</h3>\n    </div>\n\n    <div class="col two-wide last">\n      <h3>Total</h3>\n    </div>\n  </div>\n\n  <ul class="orders">\n    <li class="fallback">\n      <h3 class="fallback">You have no past orders</h3>\n    </li>\n  </ul>\n\n  <div class="loading">\n    <img class="load-animation" src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"preloader.gif",{"name":"staticImageUrl","hash":{},"data":a}))+'" />\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/orders/list_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.orders.ListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="main"
n.prototype.listSelector=".orders"
n.prototype.loadingSelector=".loading"
n.prototype.fallbackSelector=".fallback"
n.prototype.itemView=Banana.views.orders.ItemView
n.prototype.template="banana/templates/orders/list_view"
n.prototype.initialize=function(e){null==e&&(e={})
if(!this.collection){E.chaplinCollections.purchased_orders.fetch()
this.collection=E.chaplinCollections.purchased_orders}this.show_returns=e.show_returns
return n.__super__.initialize.apply(this,arguments)}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
return e}
n.prototype.initItemView=function(){var e
e=n.__super__.initItemView.apply(this,arguments)
e.show_returns=this.show_returns
return e}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/returns_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="order-page">\n  <h2>Returns &amp; Exchanges</h2>\n  <p class="grid_8">\n    If for any reason you wish to exchange an item or return it for a refund,\n    you can start a return by clicking on the "Create Return" link next to the order with the items you would like to return below.\n  </p>\n\n  <p class="grid_8">\n    We accept returns within 90 days of purchase. Please ensure that items are unworn, unwashed,<br> and undamaged.\n  </p>\n\n  <p class="grid_8">\n    To make a gift return or exchange please contact <a href="mailto:support@everlane.com" class="support-email">support@everlane.com</a>.\n  </p>\n</div>\n<div class="orders-subview"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/returns_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.ReturnsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="main"
n.prototype.regions={"orders":".orders-subview"}
n.prototype.template="banana/templates/account/returns_view"
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.orders.ListView({"region":"orders","show_returns":!0,"filterer":function(e){var t
return e.get("returnable")&&("partially_shipped"===(t=e.get("state"))||"fully_shipped"===t)}})
return this.subview("orders",e)}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.credit_cards.ItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template=_.template($(E.templates.creditCard).html())
n.prototype.toString=function(){return"Banana.views.account.credit_cards.item_view"}
return n}(Banana.views.account.UserInfoSnippetView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/credit_cards/list_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<ul class="credit-cards clearfix">\n  <li class="fallback"><h3 class="fallback">You have no credit cards on file</h3></li>\n  <li class="loading">\n    <img class="load-animation" src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"preloader.gif",{"name":"staticImageUrl","hash":{},"data":a}))+'" />\n  </li>\n</ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/credit_cards/list_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.credit_cards.ListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/account/credit_cards/list_view"
n.prototype.itemView=Banana.views.account.credit_cards.ItemView
n.prototype.listSelector=".credit-cards.clearfix"
n.prototype.loadingSelector=".loading"
n.prototype.fallbackSelector=".fallback"
n.prototype.initialize=function(){E.chaplinCollections.credit_cards.fetch()
this.collection=E.chaplinCollections.credit_cards
return n.__super__.initialize.apply(this,arguments)}
return n}(Banana.views.account.UserInfoSnippetsView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/credit_cards/security_modal_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<div id="card-security-code-modal">\n  <h2>What is your Security Code?</h2>\n  <p>A security code is a three or four digit code on your credit card, that is separate from the <br>16-digit number. The location varies on your type of card.</p>\n  <div class="card">\n    <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"mostcardcvv.png",{"name":"staticImageUrl","hash":{"width":"250"},"data":a}))+'"></img>\n    <p class="title">Most cards</p>\n    <p>Three digits printed directly to the right of the signature strip on the back of the card.</p>\n  </div>\n  <div class="card">\n    <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"amexcvv.png",{"name":"staticImageUrl","hash":{"width":"250"},"data":a}))+'"></img>\n    <p class="title">American Express</p>\n    <p>Four digits printed on either the right or left side above the 16-digit number on the front of the card.</p>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/credit_cards/security_modal_view"]}).call(this);(function(){E.mixins.CreditCardForm={"events":{"change .country":"changeCountry","keyup .cc_number":"toggleCardType","change .cc_number":"toggleCardType","click #cvv-info":"openCardSecurityModal","submit #credit_card_form":"submitForm"},"errors":[],"render":function(){this.$(".fancy-select").select2()
this.$(".country").change()
this.$(".cc_number").payment("formatCardNumber").change()
this.$(".cc_cvv").payment("formatCardCVC").change()
this.$(".cc_expiry").payment("formatCardExpiry").change()
this.$(".cc_zip_code").payment("restrictNumeric")
return this},"openCardSecurityModal":function(){return new Banana.views.components.ModalView({"view":{"class":Banana.View,"template":"banana/templates/account/credit_cards/security_modal_view"}})},"toggleCardType":function(){var e,t
t=this.$(".cc_number")
return(e=$.payment.cardType(t.val()))?this.$("#cc_icons").css("opacity",1).removeClass().addClass(e):this.$("#cc_icons").css("opacity",0)},"changeCountry":function(){var e
e=this.$(".country")
this.$(".zip-code-container").toggle("US"===e.select2("val"))
return this.$(".cc_zip_code").prop("readonly","US"!==e.select2("val"))},"validate":function(e){var t,n,a,r,i,o,s,l
t=function(t){return function(n,a){var r
r=e.find("label[for='"+n.attr("id")+"']")
return t.errors.push({"input":n,"label":r,"parent":n.parent(),"message":r.contents()[0].textContent.trim()+" "+a})}}(this)
r=e.find(".cc_number")
n=e.find(".cc_cvv")
a=e.find(".cc_expiry")
l=e.find(".cc_zip_code")
o=e.find(".cc_full_name")
i=a.payment("cardExpiryVal")
s=NameParse.parse(o.val())
s.firstName||(s.firstName=s.initials)
s.firstName||t(o,"is incomplete")
$.payment.validateCardNumber(r.val())||t(r,"is invalid")
$.payment.validateCardCVC(n.val(),$.payment.cardType(r.val()))||t(n,"is invalid")
$.payment.validateCardExpiry(i)||t(a,"is invalid")
e.find(".required").each(function(){return $(this).val().length?void 0:t($(this),"is required")})
"US"===e.find(".country").select2("val")&&5!==l.val().length&&t(l,"is required for US credit cards")
return 0===this.errors.length},"populateHiddenFields":function(e){var t,n,a,r,i
n=e.find(".cc_number")
r=e.find(".cc_full_name")
t=e.find(".cc_expiry")
a=t.payment("cardExpiryVal")
i=NameParse.parse(r.val())
i.firstName||(i.firstName=i.initials)
e.find(".first_name").val(i.firstName)
e.find(".last_name").val(i.lastName)
e.find(".cc_number_hidden").val(n.val().replace(/\s/g,""))
return e.find(".cc_expiry_hidden").val(""+a.month+"/"+a.year)},"showErrors":function(){var e
if(this.errors.length){e="<ul>"
_.each(this.errors,function(t){e+="<li>"+t.message+"</li>"
t.parent.addClass("shake twice")
t.input.addClass("fancy-form__input--error")
return t.label.addClass("fancy-form__label--error")})
e+="</ul>"
return this.$(".status-message").append(e).show()}},"submitForm":function(e,t){var n,a
e.preventDefault()
a=$(e.currentTarget)
this.errors=[]
this.$(".status-message").hide().empty()
this.$(".fancy-form__input--error").removeClass("fancy-form__input--error")
this.$(".fancy-form__label--error").removeClass("fancy-form__label--error")
if(this.validate(a)){n=new E.lib.ButtonProgressBar({"button":this.$("button[type=submit]"),"stopPoint":95,"baseIncrementAmount":10})
this.populateHiddenFields(a)
return this.makeRequest(a,t)}this.showErrors()},"makeRequest":function(e,t){var n,a,r,i,o,s,l
r={}
l=e.serializeArray()
for(o=0,s=l.length;s>o;o++){a=l[o]
r[a.name]=a.value}i=new E.models.CreditCard({"primary":!0,"cvv":r["credit_card[cvv]"],"number":r["credit_card[number]"],"expiration_date":r["credit_card[expiration_date]"],"first_name":r["credit_card[billing_address][first_name]"],"last_name":r["credit_card[billing_address][last_name]"],"zip_code":r["credit_card[billing_address][postal_code]"],"billing_address":{"first_name":r["credit_card[billing_address][first_name]"],"last_name":r["credit_card[billing_address][last_name]"],"postal_code":r["credit_card[billing_address][postal_code]"],"country_code_alpha2":r["credit_card[billing_address][country_code_alpha2]"]}})
n={"error":function(e){return function(t,n){return 422===n.status?e.$(".status-message").html(n.responseText).show():e.$(".status-message").html('Your card could not be added at this time.<br>If this issue persists, try a different card or contact <a href="mailto:support@everlane.com">support@everlane.com<a>.').show()}}(this)}
return i.save(null,_.extend(t,t))}}}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/credit_cards/form_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e){var t,n=this.lambda,a=this.escapeExpression
return'<div id="newCreditCardDialog">\n  <h3 class="subheader">Add a New Payment Method.</h3>\n\n  <div class="status-message hidden">\n  </div>\n\n  <form accept-charset="UTF-8" class="checkout_form formtastic credit_card" id="credit_card_form">\n    <div style="margin:0;padding:0;display:inline">\n      <input name="utf8" type="hidden" value="&#x2713;" />\n    </div>\n\n    <div class="form_fields clearfix">\n      <div class="field half_width">\n        <label for="credit_card_billing_address_full_name">Full Name</label>\n        <input class="text required cc_full_name fancy-input" id="credit_card_billing_address_full_name" size="30" type="text" value="'+a(n(null!=(t=null!=e?e.user:e)?t.first_name:t,e))+" "+a(n(null!=(t=null!=e?e.user:e)?t.last_name:t,e))+'" />\n        <input class="first_name" id="credit_card_billing_address_first_name" name="credit_card[billing_address][first_name]" type="hidden" value="'+a(n(null!=(t=null!=e?e.user:e)?t.first_name:t,e))+'" />\n        <input class="last_name" id="credit_card_billing_address_last_name" name="credit_card[billing_address][last_name]" type="hidden" value="'+a(n(null!=(t=null!=e?e.user:e)?t.last_name:t,e))+'" />\n      </div>\n\n      <div class="field one_third_width right">\n        <label for="credit_card_billing_address_country_code_alpha2">Country</label>\n        <select class="country fancy-select" id="credit_card_billing_address_country_code_alpha2" name="credit_card[billing_address][country_code_alpha2]" style="width: 100%;">\n          <option selected="selected" value="US">USA</option>\n<option value="CA">Canada</option>\n<option value="AU">Australia</option>\n<option value="BR">Brazil</option>\n<option value="DK">Denmark</option>\n<option value="FR">France</option>\n<option value="DE">Germany</option>\n<option value="HK">Hong Kong</option>\n<option value="NL">Netherlands</option>\n<option value="NO">Norway</option>\n<option value="SG">Singapore</option>\n<option value="ES">Spain</option>\n<option value="SE">Sweden</option>\n<option value="GB">United Kingdom</option>\n        </select>\n      </div>\n    </div>\n\n    <div class="form_fields secure-zone clearfix">\n      <div class="field two_thirds_width" id="cc_num_container">\n        <label for="credit_card_number_display">Credit Card Number</label>\n        <input autocompletetype="cc-number" class="text required cc_number fancy-input" id="credit_card_number_display" size="30" type="text" />\n        <input class="cc_number_hidden" id="credit_card_number" name="credit_card[number]" data-encrypted-name="credit_card[number]" type="hidden" />\n        <div id="cc_icons"></div>\n      </div>\n\n      <div class="field one_third_width">\n        <label for="credit_card_cvv">\n          Security Code\n          <span id="cvv-info">?</span>\n        </label>\n\n        <input autocompletetype="cc-csc" class="text required cc_cvv fancy-input" id="credit_card_cvv" name="credit_card[cvv]" size="30" data-encrypted-name="credit_card[cvv]" type="password" />\n      </div>\n\n      <div class="field one_third_width">\n        <label for="credit_card_expiration_display">Expiration Date</label>\n        <input autocompletetype="cc-exp" class="text required cc_expiry fancy-input" id="credit_card_expiration_display" placeholder="MM / YY" size="30" type="text" />\n        <input class="cc_expiry_hidden" id="credit_card_expiration_date" name="credit_card[expiration_date]" data-encrypted-name="credit_card[expiration_date]" type="hidden" />\n      </div>\n\n      <div class="field one_third_width right zip-code-container">\n        <label for="credit_card_billing_address_postal_code">Zip Code</label>\n        <input class="text cc_zip_code fancy-input" id="credit_card_billing_address_postal_code" maxlength="5" name="credit_card[billing_address][postal_code]" size="5" type="text" />\n      </div>\n    </div>\n\n    <div class="form_fields clearfix">\n      <div class="field clearfix">\n        <a href="#" class="fancy-button cancel mid-grey" style="float: left">Back</a>\n        <button type="submit" class="fancy-button orange continue-checkout-button" style="float: right" name="commit">Save Information</button>\n      </div>\n    </div>\n\n  </form>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/credit_cards/form_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.credit_cards.FormView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/account/credit_cards/form_view"
n.prototype.events={"click .cancel":"exit","submit #credit_card_form":function(e){return this.submitForm(e,{"success":function(e){return function(t){return e.trigger("form:success",t)}}(this)})}}
n.prototype.initialize=function(){return n.__super__.initialize.apply(this,arguments)}
n.prototype.render=function(){return n.__super__.render.apply(this,arguments)}
n.prototype.exit=function(){return this.trigger("form:close")}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.user=E.session.getCurrentUser()
return e}
return n}(Banana.View)
E.mix(Banana.views.account.credit_cards.FormView,E.mixins.CreditCardForm)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/credit_cards_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div id="payment-page" class="checkout-page clearfix">\n  <div class="column grid_7 prefix_1">\n\n    <h3>Payment Information</h3>\n    <p><small>Manage and add new payment information.</small></p>\n\n    <div class="list"></div>\n\n    <a href="#" class="add-new-credit-card fancy-button orange">Add New Credit Card</a>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/credit_cards_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.CreditCardsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="main"
n.prototype.template="banana/templates/account/credit_cards_view"
n.prototype.regions={"credit_cards_list":".list"}
n.prototype.events={"click .add-new-credit-card":"openCreditCardForm"}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.account.credit_cards.ListView({"region":"credit_cards_list"})
this.subview("credit_cards_list",e)
return this.listenTo(e,"result",this.makePrimary)}
n.prototype.openCreditCardForm=function(){var e
e=new Banana.views.components.ModalView({"view":{"class":Banana.views.account.credit_cards.FormView,"events":{"form:close":function(){return this.dismiss()},"form:success":function(e){this.trigger("form:success",e)
return this.dismiss()}}}})
this.subview("credit_cards_form",e)
return this.listenTo(e,"form:success",this.addCard)}
n.prototype.addCard=function(e){return E.chaplinCollections.credit_cards.add(e)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/account/addresses_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div id="address-page" class="checkout-page clearfix">\n  <div class="column grid_7 prefix_1">\n    <h3>Shipping Information</h3>\n    <p><small>Manage and add new shipping addresses.</small></p>\n\n    <div class="list"></div>\n\n    <a href="#" class="add-new-address fancy-button orange">Add New Address</a>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/account/addresses_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.account.AddressesView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="main"
n.prototype.template="banana/templates/account/addresses_view"
n.prototype.regions={"addresses_list":".list"}
n.prototype.events={"click .add-new-address":"openAddressForm"}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.account.addresses.ListView({"region":"addresses_list"})
return this.subview("addresses-list",e)}
n.prototype.openAddressForm=function(){var e
e=new Banana.views.components.ModalView({"view":{"class":Banana.views.account.addresses.NewShippingAddressDialogView,"events":{"form:submit":function(){return this.lock()},"form:error":function(){return this.unlock()},"form:success":function(e){this.trigger("form:success",e)
return this.dismiss()}}}})
return this.listenTo(e,"form:success",this.addAddress)}
n.prototype.addAddress=function(e){return E.chaplinCollections.addresses.add(e)}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.AccountController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.beforeAction=function(){n.__super__.beforeAction.apply(this,arguments)
this.reuse("account",Banana.views.account.IndexView)
this.reuse("sidebar",Banana.views.account.SidebarView)
return E.lib.LoadingIndicator.stop()}
n.prototype.info=function(){this.adjustTitle("My Account")
return this.view=new Banana.views.account.InfoView({"model":E.chaplinCollections.user})}
n.prototype.orders=function(){this.adjustTitle("My Orders")
return this.view=new Banana.views.orders.ListView}
n.prototype.returns=function(){this.adjustTitle("My Returns")
return this.view=new Banana.views.account.ReturnsView}
n.prototype.billing=function(){this.adjustTitle("Payment Info")
return this.view=new Banana.views.account.CreditCardsView}
n.prototype.address=function(){this.adjustTitle("Shipping Info")
return this.view=new Banana.views.account.AddressesView}
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/line_item"]=Handlebars.template({"1":function(){return'    <img class="line-item__sdd-banner" src="/assets/sdd-corner.svg">\n'},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'    <a href="/collections/'+s((r=null!=(r=t.collection_permalink||(null!=e?e.collection_permalink:e))?r:o,typeof r===i?r.call(e,{"name":"collection_permalink","hash":{},"data":a}):r))+"/products/"+s((r=null!=(r=t.product_permalink||(null!=e?e.product_permalink:e))?r:o,typeof r===i?r.call(e,{"name":"product_permalink","hash":{},"data":a}):r))+'">\n      <img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.imagePath:e,{"name":"staticImageUrl","hash":{"size":"100"},"data":a}))+'" width="100" height="100" class="line-item__image">\n    </a>\n'},"5":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'    <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,null!=e?e.imagePath:e,{"name":"staticImageUrl","hash":{"size":"100"},"data":a}))+'" width="100" height="100" class="line-item__image">\n'},"7":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <a href="/collections/'+s((r=null!=(r=t.collection_permalink||(null!=e?e.collection_permalink:e))?r:o,typeof r===i?r.call(e,{"name":"collection_permalink","hash":{},"data":a}):r))+"/products/"+s((r=null!=(r=t.product_permalink||(null!=e?e.product_permalink:e))?r:o,typeof r===i?r.call(e,{"name":"product_permalink","hash":{},"data":a}):r))+'">'+s((r=null!=(r=t.title||(null!=e?e.title:e))?r:o,typeof r===i?r.call(e,{"name":"title","hash":{},"data":a}):r))+"</a>\n"},"9":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"        "+s((r=null!=(r=t.title||(null!=e?e.title:e))?r:o,typeof r===i?r.call(e,{"name":"title","hash":{},"data":a}):r))+"\n"},"11":function(){return'        <li class="line-item__annotation line-item__annotation--same-day-delivery">\n          1-hour delivery is available\n        </li>\n'},"13":function(e,t,n,a){var r,i="\n"
r=t["if"].call(e,null!=e?e.preorderable:e,{"name":"if","hash":{},"fn":this.program(14,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.is_digital_giftcard:e,{"name":"if","hash":{},"fn":this.program(16,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"\n"},"14":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'          <li class="line-item__annotation">\n            Ships on '+s((r=null!=(r=t.restockDate||(null!=e?e.restockDate:e))?r:o,typeof r===i?r.call(e,{"name":"restockDate","hash":{},"data":a}):r))+"\n          </li>\n"},"16":function(e){var t,n=this.lambda,a=this.escapeExpression
return'          <li class="line-item__annotation line-item__annotation--gift-card">\n            To: '+a(n(null!=(t=null!=e?e.unit_attributes:e)?t.recipient_name:t,e))+"\n            ("+a(n(null!=(t=null!=e?e.unit_attributes:e)?t.recipient_email:t,e))+")\n          </li>\n"},"18":function(e,t,n,a){var r,i=t.helperMissing,o=""
r=(t.unlessCond||e&&e.unlessCond||i).call(e,null!=e?e.size:e,"One Size",{"name":"unlessCond","hash":{},"fn":this.program(19,a),"inverse":this.noop,"data":a})
null!=r&&(o+=r)
return o},"19":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"        <dl>\n          <dt>Size</dt>\n          <dd>"+s((r=null!=(r=t.size||(null!=e?e.size:e))?r:o,typeof r===i?r.call(e,{"name":"size","hash":{},"data":a}):r))+"</dd>\n        </dl>\n"},"21":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <dd class="line-item__update-quantity">\n          <input type="text" value="'+s((r=null!=(r=t.quantity||(null!=e?e.quantity:e))?r:o,typeof r===i?r.call(e,{"name":"quantity","hash":{},"data":a}):r))+'" class="line-item__update-quantity-field">\n          <span class="fancy-button orange line-item__update-quantity-button">update</span>\n        </dd>\n'},"23":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"        <dd>"+s((r=null!=(r=t.quantity||(null!=e?e.quantity:e))?r:o,typeof r===i?r.call(e,{"name":"quantity","hash":{},"data":a}):r))+"</dd>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="line-item__container--inactive clearfix hidden">\n  <p>Removed <strong>'+l((i=null!=(i=t.title||(null!=e?e.title:e))?i:s,typeof i===o?i.call(e,{"name":"title","hash":{},"data":a}):i))+'</strong> from your bag.</p>\n  <a href="javascript:;" class="line-item__restore fancy-button orange small hidden">undo</a>\n</div>\n\n<div class="line-item__container--active">\n\n'
r=t["if"].call(e,null!=e?e.deliverable:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.collection_permalink:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.program(5,a),"data":a})
null!=r&&(c+=r)
c+='\n  <a href="javascript:;" class="line-item__remove">\xd7</a>\n\n  <div class="line-item__info clearfix">\n\n    <h4 class="line-item__title">\n'
r=t["if"].call(e,null!=e?e.collection_permalink:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.program(9,a),"data":a})
null!=r&&(c+=r)
c+='    </h4>\n\n    <ul class="line-item__annotation-list">\n'
r=t["if"].call(e,null!=e?e.deliverable:e,{"name":"if","hash":{},"fn":this.program(11,a),"inverse":this.program(13,a),"data":a})
null!=r&&(c+=r)
c+="    </ul>\n\n    <!--\n      Handlebars makes this difficult on us, what we are trying to say is:\n      if(!is_giftcard && size !== 'One Size')\n    -->\n"
r=t.unless.call(e,null!=e?e.is_giftcard:e,{"name":"unless","hash":{},"fn":this.program(18,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n    <dl>\n      <dt>Quantity</dt>\n"
r=t.unless.call(e,null!=e?e.is_digital_giftcard:e,{"name":"unless","hash":{},"fn":this.program(21,a),"inverse":this.program(23,a),"data":a})
null!=r&&(c+=r)
return c+'    </dl>\n\n    <dl>\n      <dt>Total</dt>\n      <dd class="line-item__total">$'+l((i=null!=(i=t.total||(null!=e?e.total:e))?i:s,typeof i===o?i.call(e,{"name":"total","hash":{},"data":a}):i))+"</dd>\n    </dl>\n\n  </div>\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/line_item"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.LineItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/components/line_item"
n.prototype.tagName="li"
n.prototype.className="line-item"
n.prototype.events={"click .line-item__remove":"removeFromCart","click .line-item__restore":"restore","click .line-item__update-quantity-button":"updateQuantity","keypress .line-item__update-quantity-field":"handleKeypress"}
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
return this.listenTo(this.model,"change:quantity",this.render)}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.preorderable="preorderable"===this.model.get("variant").orderable_state
e.deliverable=E.delivery.isDeliverable({"lineItem":this.model})
return e}
n.prototype.updateQuantity=function(){var e
e=parseInt(this.$(".line-item__update-quantity-field").val(),10)
this.$(".line-item__update-quantity-button").removeClass("visible")
if(e>0){this.model.save({"quantity":e})
return this.$(".line-item__total").text("$"+this.total())}return this.removeFromCart()}
n.prototype.handleKeypress=function(e){var t
t=[48,49,50,51,52,53,54,55,56,57]
if(_.contains(t,e.which))return this.$(".line-item__update-quantity-button").addClass("visible")
if(13===e.which){e.preventDefault()
return this.updateQuantity()}}
n.prototype.total=function(){return this.model.get("quantity")*this.model.get("price")}
n.prototype.restore=function(){var e
if(!this.model.get("active")){e=this.model.activate()
this.$el.find(".line-item__update-quantity-field").val(this.model.get("quantity"))
this.$el.find(".line-item__restore").hide()
this.$el.find(".line-item__container--inactive").hide()
this.$el.find(".line-item__container--active").css({"opacity":1,"height":"auto"}).show()
return e.done(function(e){return function(){return e.$el.find(".line-item__remove").fadeIn()}}(this))}}
n.prototype.removeFromCart=function(){var e
if(this.model.get("active")){e=this.model.deactivate()
this.$el.find(".line-item__remove").hide()
this.$el.find(".line-item__container--active").css("opacity",0).velocity("slideUp",{"duration":250,"easing":"easeOutQuad"})
this.$el.find(".line-item__container--inactive").fadeIn()
return e.promise().done(function(e){return function(){return e.$el.find(".line-item__restore").fadeIn()}}(this))}}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.checkout.LineItemsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template=function(){return""}
n.prototype.itemView=Banana.views.components.LineItemView
n.prototype.tagName="ul"
n.prototype.className="line-items"
n.prototype.filterer=function(e){return e.get("active")}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/checkout/review_view"]=Handlebars.template({"1":function(){return'\n  <div class="checkout__empty-bag">\n    <!-- TODO Something good here -->\n  </div>\n\n'},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'\n  <div class="checkout-page">\n\n    <div class="column grid_10 prefix_1">\n\n      <div class="reviewcart">\n        <!-- seperated line items will get filled in here -->\n      </div>\n\n      <div class="fake-order-summary">\n        <div id="order-details">\n          <table class="order-summary" id="order_summary">\n            <tr>\n              <td class="label">\n                Number of Items\n              </td>\n              <td class="value cart-count">'+s((r=null!=(r=t.numItemsInCart||(null!=e?e.numItemsInCart:e))?r:o,typeof r===i?r.call(e,{"name":"numItemsInCart","hash":{},"data":a}):r))+'</td>\n            </tr>\n            <tr>\n              <td class="label">\n                Order Subtotal\n              </td>\n              <td class="value subtotal">$'+s((r=null!=(r=t.subtotal||(null!=e?e.subtotal:e))?r:o,typeof r===i?r.call(e,{"name":"subtotal","hash":{},"data":a}):r))+'</td>\n            </tr>\n          </table>\n        </div>\n\n        <a href="/checkout/route" class="fancy-button orange" style="float: right">Continue To Checkout</a>\n        <a href="'+s((r=null!=(r=t.backLink||(null!=e?e.backLink:e))?r:o,typeof r===i?r.call(e,{"name":"backLink","hash":{},"data":a}):r))+'" class="continue-shopping">\u2190 Return to Shop</a>\n      </div>\n    </div>\n\n  </div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r
r=t["if"].call(e,null!=e?e.noItems:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/checkout/review_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.checkout.ReviewView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.template="banana/templates/checkout/review_view"
n.prototype.mainClass="checkout cart"
n.prototype.listen={"change collection":"updateOrderInfo","sync collection":"handleSync"}
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
return this.hideMostRecent=e.hideMostRecent}
n.prototype.attach=function(){var e,t,a
n.__super__.attach.apply(this,arguments)
E.pub(E.Event.Checkout.CART_VIEW)
if(t=E.currentQuery("items_added")){a=t>1?"s":""
E.delivery.isActive()&&E.delivery.isDeliveryCollection(E.currentQuery("collection"))&&(e=$("<a>").attr("href","/collections/"+E.currentQuery("collection")).html("\u2190 Back to Everlane Now").prop("outerHTML"))
return E.showAlert({"title":""+t+" Item"+a+" added to bag","body":e,"class":"alerts__item--add-to-cart","dismissible":!1,"flash":!0,"priority":10})}}
n.prototype.getTemplateData=function(){var e,t
if(0===this.collection.length)return{"noItems":!0}
e="male"===(null!=(t=E.session.getCurrentUser())?t.gender:void 0)?"mens-all":"womens-all"
return{"quantity":this.collection.last().get("quantity"),"numItemsInCart":this.collection.deepCount(),"subtotal":this.collection.total(),"backLink":"/collections/"+(E.currentQuery("collection")||e)}}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
if(e=E.currentQuery("sku")){this.collection.comparator=function(t){return e===t.get("sku")?0:1}
this.collection.sort()}return this.subview("other",new Banana.views.checkout.CartView({"collection":this.collection,"container":this.$(".reviewcart")}))}
n.prototype.updateOrderInfo=function(){this.$(".subtotal").text("$"+this.collection.total())
return this.$(".cart-count").text(this.collection.deepCount())}
n.prototype.handleSync=function(){this.$(".subtotal").text("$"+this.collection.total())
return this.$(".cart-count").text(this.collection.deepCount())}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.CheckoutController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.review=function(){this.adjustTitle("Your Bag")
return this.view=new Banana.views.checkout.ReviewView({"collection":E.currentOrder.get("lineItems")})}
n.prototype.thanks=function(){this.adjustTitle("Thank You")
return this.view=new Banana.views.orders.ConfirmationView}
return n}(Banana.Controller)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Event=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/events"
n.prototype.rsvp=function(){var e,t
e=function(e){return function(){return e.trigger("rsvp",!1)}}(this)
t=function(e){return function(t,n,a){e.trigger("rsvp",!0)
if("Unauthorized"===a){e.trigger("setLoginSession")
return new Banana.views.components.ModalView({"view":Banana.views.users.LoginModalView,"dismissible":!1})}}}(this)
return $.ajax({"type":"POST","url":"/api/events/"+this.id+"/rsvp"}).done(e).fail(t)}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.Subhero=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.url="/api/user"
return n}(Banana.Model)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/grouping"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'  <h3 class="product-subheading serif">'+s((r=null!=(r=t.name||(null!=e?e.name:e))?r:o,typeof r===i?r.call(e,{"name":"name","hash":{},"data":a}):r))+"</h3>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.name:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+'\n<div class="products"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/grouping"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/products_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<ul class="products clearfix"></ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/products_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/color_palette_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i,o=this.lambda,s=this.escapeExpression,l="function",c=t.helperMissing
return'\n  <a class="color-count" href="/collections/'+s(o(null!=(r=null!=(r=null!=e?e.products:e)?r[0]:r)?r.collection_permalink:r,e))+"/products/"+s(o(null!=(r=null!=(r=null!=e?e.products:e)?r[0]:r)?r.permalink:r,e))+'" alt="'+s((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:c,typeof i===l?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+'">\n    '+s(o(null!=(r=null!=(r=null!=(r=null!=e?e.products:e)?r[0]:r)?r.color:r)?r.name:r,e))+"\n  </a>\n\n"},"3":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='\n  <span class="color-count">'+l((i=null!=(i=t.numberOfColors||(null!=e?e.numberOfColors:e))?i:s,typeof i===o?i.call(e,{"name":"numberOfColors","hash":{},"data":a}):i))+' Colors</span>\n\n  <ul class="colors">\n'
r=t.each.call(e,null!=e?e.products:e,{"name":"each","hash":{},"fn":this.program(4,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"  </ul>\n\n"},"4":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c=this.lambda
return'      <li data-product="'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'" title="'+l(c(null!=(r=null!=e?e.color:e)?r.name:r,e))+'">\n        <a href="/collections/'+l((i=null!=(i=t.collection_permalink||(null!=e?e.collection_permalink:e))?i:s,typeof i===o?i.call(e,{"name":"collection_permalink","hash":{},"data":a}):i))+"/products/"+l((i=null!=(i=t.permalink||(null!=e?e.permalink:e))?i:s,typeof i===o?i.call(e,{"name":"permalink","hash":{},"data":a}):i))+'" alt="'+l((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:s,typeof i===o?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+'" style="background-color:#'+l(c(null!=(r=null!=e?e.color:e)?r.hex_value:r,e))+'">\n          '+l((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:s,typeof i===o?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+"\n        </a>\n      </li>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r
r=t["if"].call(e,null!=e?e.showText:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/color_palette_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.ColorPaletteView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="div"
n.prototype.className="clearfix"
n.prototype.template="banana/templates/collections/color_palette_view"
n.prototype.events={"mouseenter .colors li":"onColorHover"}
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
this.showText=1===this.collection.length
return this.collapseColors=this.collection.length>=7}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.collapseColors?void 0:this.makeTipTip()}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
this.colors=this.$(".colors")
this.colorCount=this.$(".color-count")
return this.collapseColors||this.showText?this.colors.hide():this.colorCount.hide()}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.products=_.pluck(this.collection.models,"attributes")
e.numberOfColors=this.collection.length
e.collapseColors=this.collapseColors
e.showText=this.showText
return e}
n.prototype.onColorHover=function(e){var t,n
t=$(e.currentTarget)
n=this.collection.findWhere({"id":t.data("product")})
n.set("active",!0)
E.pub(E.Event.Collections.COLOR_HOVER,n.get("variants"))
return this.collection.each(function(e){return e!==n?e.set("active",!1):void 0})}
n.prototype.toggleColors=function(e){var t,n
null==e&&(e=!1)
if(this.collapseColors){if(e){this.colorCount.hide()
this.colors.show()
n=this.colors.find("li")
t=n.map(function(e,t){return $(t).position().left})
n.css({"position":"absolute","left":this.$el.outerWidth()/2-n.outerWidth()/2})
return n.each(function(e){return function(n,a){return $(a).velocity({"left":t[n]},{"duration":300,"easing":"easeOutBack","complete":function(){return e.makeTipTip()}})}}(this))}this.colors.hide()
return this.colorCount.show()}}
n.prototype.makeTipTip=function(){return this.colors.find("li").tipTip({"delay":0,"enter":function(){return $(this).on("mousedown",function(e){return function(){return $(e).trigger("mouseleave.tipTip")}}(this))}})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/product_image_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.sdd_banner:e,{"name":"if","hash":{},"fn":this.program(2,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"2":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      <img class="product-image__sdd-banner" src="'+s((r=null!=(r=t.sdd_banner||(null!=e?e.sdd_banner:e))?r:o,typeof r===i?r.call(e,{"name":"sdd_banner","hash":{},"data":a}):r))+'">\n'},"4":function(e,t,n,a){var r,i='    <span class="sold-out"></span>\n      <span class="sold-out-text serif thirteen">\n'
r=t["if"].call(e,null!=e?e.waitlistable:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
i+="\n"
r=t["if"].call(e,null!=e?e.sold_out:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"    </span>\n"},"5":function(){return"        waitlist\n"},"7":function(){return"        sold out\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<a href="/collections/'+l((i=null!=(i=t.collection_permalink||(null!=e?e.collection_permalink:e))?i:s,typeof i===o?i.call(e,{"name":"collection_permalink","hash":{},"data":a}):i))+"/products/"+l((i=null!=(i=t.permalink||(null!=e?e.permalink:e))?i:s,typeof i===o?i.call(e,{"name":"permalink","hash":{},"data":a}):i))+'" alt="'+l((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:s,typeof i===o?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+'" class="main-product-link">\n\n'
r=t["if"].call(e,null!=e?e.deliverable:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='\n  <img src="'+l((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{"size":null!=e?e.image_size:e},"data":a}))+'" class="main-image" alt="'+l((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:s,typeof i===o?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+' - Everlane" data-hover-src="'+l((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=e?e.hover_image:e,{"name":"staticImageUrl","hash":{"size":null!=e?e.image_size:e},"data":a}))+'">\n\n'
r=t["if"].call(e,null!=e?e.not_available:e,{"name":"if","hash":{},"fn":this.program(4,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"\n</a>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/product_image_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.ProductImageView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="div"
n.prototype.template="banana/templates/collections/product_image_view"
n.prototype.PRODUCTS_PER_ROW_TO_IMAGE_SIZE={"1":450,"2":450,"3":300,"4":220}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
return this.$el.EverlaneCollectionImage()}
n.prototype.getTemplateData=function(){var e,t,a,r,i
a=this.model.get("images")
r=this.model.get("orderable_state")
e=E.chaplinCollections.collections.findWhere({"id":this.model.get("collection_id")})
i=e.get("products_per_row")
t=n.__super__.getTemplateData.apply(this,arguments)
t.image_size=this.PRODUCTS_PER_ROW_TO_IMAGE_SIZE[i]
t.not_available="waitlistable"===r||"sold_out"===r
t.waitlistable="waitlistable"===r
t.sold_out="sold_out"===r
t.sdd_banner=E.delivery.bannerForCurrentLocation()
!E.delivery.isActive()&&E.delivery.isMarketable()&&E.delivery.isDeliveryCollection(e.get("permalink"))?t.deliverable=E.delivery.isDeliverable({"product":this.model}):E.delivery.isActive()&&(t.deliverable=E.delivery.isDeliverable({"product":this.model}))
return t}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/size_tray_view"]=Handlebars.template({"1":function(e,t,n,a,r){var i,o,s=this.lambda,l=this.escapeExpression,c=t.helperMissing,p="function",u='  <li>\n  <a href="/collections/'+l(s(null!=r[1]?r[1].collection_permalink:r[1],e))+"/products/"+l(s(null!=r[1]?r[1].permalink:r[1],e))+"?size="+l((t.lowerCase||e&&e.lowerCase||c).call(e,null!=e?e.short_name:e,{"name":"lowerCase","hash":{},"data":a}))+'" data-short-name="'+l((o=null!=(o=t.short_name||(null!=e?e.short_name:e))?o:c,typeof o===p?o.call(e,{"name":"short_name","hash":{},"data":a}):o))+'" class="tray-size '
i=t.unless.call(e,null!=e?e.available:e,{"name":"unless","hash":{},"fn":this.program(2,a,r),"inverse":this.noop,"data":a})
null!=i&&(u+=i)
return u+'">\n      '+l((o=null!=(o=t.short_name||(null!=e?e.short_name:e))?o:c,typeof o===p?o.call(e,{"name":"short_name","hash":{},"data":a}):o))+"\n    </a>\n  </li>\n"},"2":function(){return"strikeout"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a,r){var i,o='<p class="add-to-bag">Add to Bag</p>\n'
i=t.each.call(e,null!=e?e.variants:e,{"name":"each","hash":{},"fn":this.program(1,a,r),"inverse":this.noop,"data":a})
null!=i&&(o+=i)
return o},"useData":!0,"useDepths":!0})
return this.HandlebarsTemplates["banana/templates/collections/size_tray_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.SizeTrayView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/collections/size_tray_view"
n.prototype.className="size-tray serif thirteen"
n.prototype.tagName="ul"
n.prototype.events={"click li a.tray-size":"addToCart"}
n.prototype.addToCart=function(e){var t,n,a,r,i
r=$(e.target).data("short-name")
a=this.model.getAttributes()
i=a.variants
t=_.find(i,function(e){return e.short_name===r.toString()})
if(t.available){e.preventDefault()
e.stopPropagation()
E.pub(E.Event.SizeTray.ADD_TO_CART,{"sku":t.sku})
n=E.currentOrder.get("lineItems")
n.addLineItem(1,t,a)
return E.pub(E.Event.Cart.BLINK)}}
n.prototype.show=function(){return this.model.get("permalink").match(/giftcard/)?void 0:this.$el.velocity({"opacity":.7},{"duration":200})}
n.prototype.hide=function(){return this.$el.velocity({"opacity":0},{"duration":200})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/product_view"]=Handlebars.template({"1":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'  <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{},"data":a}))+'" class="promo"></img>\n'},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'\n  <div class="product-image-container"></div>\n\n  <ul class="product-info serif">\n    <li class="product-name">\n      <a href="/collections/'+s((r=null!=(r=t.collection_permalink||(null!=e?e.collection_permalink:e))?r:o,typeof r===i?r.call(e,{"name":"collection_permalink","hash":{},"data":a}):r))+"/products/"+s((r=null!=(r=t.permalink||(null!=e?e.permalink:e))?r:o,typeof r===i?r.call(e,{"name":"permalink","hash":{},"data":a}):r))+'" alt="'+s((r=null!=(r=t.display_name||(null!=e?e.display_name:e))?r:o,typeof r===i?r.call(e,{"name":"display_name","hash":{},"data":a}):r))+'" class="main-product-link">\n        '+s((r=null!=(r=t.name||(null!=e?e.name:e))?r:o,typeof r===i?r.call(e,{"name":"name","hash":{},"data":a}):r))+" &ndash; "+s((t.formatPrice||e&&e.formatPrice||o).call(e,null!=e?e.price:e,{"name":"formatPrice","hash":{},"data":a}))+'\n      </a>\n    </li>\n  </ul>\n\n  <div class="color-palette"></div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r
r=t["if"].call(e,null!=e?e.promo:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/product_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.ProductView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="product column"
n.prototype.template="banana/templates/collections/product_view"
n.prototype.events={"mouseenter":"onMouseEvent","mouseleave":"onMouseEvent"}
n.prototype.initialize=function(){var e,t,a
n.__super__.initialize.apply(this,arguments)
e=this.model.get("collection_id")
this.collectionModel=E.chaplinCollections.collections.findWhere({"id":e})
a=this.collectionModel.get("groupings").product_group
this.productGroup=a.find(function(e){return function(t){return t.get("products").find(function(t){return t===e.model})}}(this))
t=this.collectionModel.get("grouping_type")
if("condensed_product_group"===t){this.productCollection=this.productGroup.get("products")
return this.listenTo(this.productCollection,"change",this.onColorChange)}}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
this.productImageView=new Banana.views.collections.ProductImageView({"container":this.$(".product-image-container"),"model":this.model})
this.subview("image",this.productImageView)
this.colorPaletteView=new Banana.views.collections.ColorPaletteView({"container":this.$(".color-palette"),"collection":this.productCollection||new Banana.collections.Products([this.model])})
this.subview("colors",this.colorPaletteView)
if(!this.productCollection){this.sizeTrayView=new Banana.views.collections.SizeTrayView({"container":this.$(".product-image-container"),"model":this.model})
return this.subview("sizes",this.sizeTrayView)}}
n.prototype.getTemplateData=function(){var e,t
e=n.__super__.getTemplateData.apply(this,arguments)
e.name=null!=(t=this.productGroup)?t.get("name"):void 0
return e}
n.prototype.onMouseEvent=function(e){var t,n,a
t="mouseenter"===e.type
"condensed_product_group"===this.collectionModel.get("grouping_type")&&this.colorPaletteView.toggleColors(t)
if(t){E.pub(E.Event.Collections.PRODUCT_HOVER,this.model.get("variants"))
return null!=(n=this.subview("sizes"))?n.show():void 0}return null!=(a=this.subview("sizes"))?a.hide():void 0}
n.prototype.onColorChange=function(e){if(e.get("active")){this.removeSubview("image")
this.productImageView=new Banana.views.collections.ProductImageView({"container":this.$(".product-image-container"),"model":e})
return this.subview("image",this.productImageView)}}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.ProductsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.animationDuration=0
n.prototype.listSelector=".products"
n.prototype.itemView=Banana.views.collections.ProductView
n.prototype.template="banana/templates/collections/products_view"
return n}(Banana.CollectionView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.GroupingView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/collections/grouping"
n.prototype.className="collection__grouping"
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.collections.ProductsView({"container":this.$(".products"),"collection":this.model.get("products")})
return this.subview("products",e)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/groupings"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="groupings"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/groupings"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.GroupingsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.animationDuration=0
n.prototype.listSelector=".groupings"
n.prototype.itemView=Banana.views.collections.GroupingView
n.prototype.template="banana/templates/collections/groupings"
n.prototype.initialize=function(e){return this.isCategoryDisplay=e.isCategoryDisplay}
n.prototype.initItemView=function(e){return new this.itemView({"id":"group_"+this.collection.indexOf(e),"model":e,"autoRender":!1,"isCategoryDisplay":this.isCategoryDisplay})}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return setTimeout(function(e){return function(){return e.$(".collection__grouping").each(function(e,t){var n
n=$(t)
return n.waypoint({"offset":0===e?"25%":"75%","handler":function(){var e
e=n.attr("id")
$(".collection__side-nav").find("li").removeClass("collection__side-nav-item--active")
return $(".collection__side-nav").find("li#nav_item_"+e).addClass("collection__side-nav-item--active")}})})}}(this),1)}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/side_navigation"]=Handlebars.template({"1":function(e,t,n,a){var r,i=this.lambda,o=this.escapeExpression,s="function",l=t.helperMissing
return'    <li id="nav_item_group_'+o(i(a&&a.index,e))+'" class="collection__side-nav-item">\n      <a href="#group_'+o(i(a&&a.index,e))+'">\n        '+o((r=null!=(r=t.name||(null!=e?e.name:e))?r:l,typeof r===s?r.call(e,{"name":"name","hash":{},"data":a}):r))+"\n      </a>\n    </li>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="\n<h4>\n  The Gift Guide\n</h4>\n\n<ul>\n\n"
r=t.each.call(e,null!=e?e.items:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"\n</ul>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/side_navigation"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.SideNavigationView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="collection__side-nav"
n.prototype.template="banana/templates/collections/side_navigation"
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=$(window)
this.scrollProxy=function(t){return function(){var n,a,r
n=$("#group_0")
if(n.length>0){r=n.offset().top-e.scrollTop()
a=n.offset().left
if(60>r){t.$el.addClass("collection__side-nav--fixed")
return t.$el.css({"left":a-150})}if(r>60){t.$el.removeClass("collection__side-nav--fixed")
return t.$el.css({"left":-150})}}}}(this)
return e.scroll(this.scrollProxy)}
n.prototype.dispose=function(){n.__super__.dispose.apply(this,arguments)
return $(window).off("scroll",this.scrollProxy)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/collections/show"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div class="collection up-'+s((r=null!=(r=t.products_per_row||(null!=e?e.products_per_row:e))?r:o,typeof r===i?r.call(e,{"name":"products_per_row","hash":{},"data":a}):r))+'">\n  <div class="groupings">\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/collections/show"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.collections.ShowView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.template="banana/templates/collections/show"
n.prototype.listen={"change:updated_at model":"render"}
n.prototype.mainClass="collections"
n.prototype.hideSticker=!1
n.prototype.render=function(){var e,t,a,r,i,o,s
n.__super__.render.apply(this,arguments)
a=this.model.get("grouping_type")
e=this.model.get("groupings")
t=e[a]||e.shape
if("condensed_product_group"===a){i=e.product_group
t.each(function(e){var t
t=e.get("products").groupBy(function(e){var t
t=i.find(function(t){return t.get("products").contains(e)})
return t.get("name")})
t=_.map(_.values(t),function(e){return e[0]})
return e.set("products",new Banana.collections.Products(t))})}r=new Banana.views.collections.GroupingsView({"container":this.$(".groupings"),"collection":t})
this.subview("groupings",r)
if("mens-gift-guide"===(s=this.model.get("permalink"))||"womens-gift-guide"===s){o=new Banana.views.collections.SideNavigationView({"container":this.$(".collection"),"collection":t})
return this.subview("sideNav",o)}}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return new E.lib.Slider(this.$(".swipe"))}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/subheros/tee_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<div class="subhero-full-width" style="background-color: #fff; height: 450px; overflow: hidden;">\n  <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"tee_subhero_overlay.png",{"name":"staticImageUrl","hash":{},"data":a}))+'" style="position: absolute; max-width: 1280px; width: 100%; z-index: 2;"></img>\n\n  <video style="margin-top: -90px; max-width: 1280px; width: 100%; z-index: 1; opacity: 0.9;" height="720" width="1280" id="main-video" loop="loop" preload="auto" autoplay controls poster="//d1zvwc1xfc8qp0.cloudfront.net/assets/tee_subhero_poster-cf9f86814564679d5c80e2b9ba3a1f72.jpg">\n    <source src="//dns4jlfurg7hp.cloudfront.net/videos/tee_subhero.ogv" type=\'video/ogg; codecs="theora, vorbis"\'></source>\n    <source src="//dns4jlfurg7hp.cloudfront.net/videos/tee_subhero.mp4" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'></source>\n  </video>\n\n</div>\n<hr class="invisible-divider-full-width">'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/subheros/tee_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.subheros.TeeView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="pre_content"
n.prototype.template="banana/templates/subheros/tee_view"
n.prototype.events={"click .subhero-full-width":"clickVideo"}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$("video").attr("controls",!1)}
n.prototype.clickVideo=function(){return window.location="/tee-factory"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.mobile=E.lib.helpers.isMobile()
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/subheros/elastic_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'<div class="elastic-container">\n  <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{"width":1400},"data":a}))+'" class="elastic">\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/subheros/elastic_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.subheros.ElasticView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/subheros/elastic_view"
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$el.find("img.elastic").reserveSpace({"maxWidth":1400,"maxHeight":450,"minWidth":940,"minHeight":302})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/subheros/elastic_video_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/subheros/elastic_video_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.subheros.ElasticVideoView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/subheros/elastic_video_view"
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/subheros/video_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="vimeo-video-container"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/subheros/video_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/vimeo_view"]=Handlebars.template({"1":function(){return"auto"},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return s((r=null!=(r=t.width||(null!=e?e.width:e))?r:o,typeof r===i?r.call(e,{"name":"width","hash":{},"data":a}):r))+"px"},"5":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return s((r=null!=(r=t.default_height||(null!=e?e.default_height:e))?r:o,typeof r===i?r.call(e,{"name":"default_height","hash":{},"data":a}):r))+"px"},"7":function(){return"1500px"},"9":function(){return'      <div class="play-container">\n        <img alt="Play" class="play-icon" height="45" src="//d1zvwc1xfc8qp0.cloudfront.net/assets/play-f9092967841e496a258b082e106fda82.png" width="45" />\n        <span>Play Video</span>\n      </div>\n'},"11":function(){return'    <a href="javascript:;" class="close">&#215;</a>\n'},"13":function(){return"video-full-width"},"15":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      style="padding-bottom: '+s((r=null!=(r=t.ratio||(null!=e?e.ratio:e))?r:o,typeof r===i?r.call(e,{"name":"ratio","hash":{},"data":a}):r))+'%"\n'},"17":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      style="width: '+s((r=null!=(r=t.width||(null!=e?e.width:e))?r:o,typeof r===i?r.call(e,{"name":"width","hash":{},"data":a}):r))+"px; height: "+s((r=null!=(r=t.height||(null!=e?e.height:e))?r:o,typeof r===i?r.call(e,{"name":"height","hash":{},"data":a}):r))+'px;"\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="vimeo-video"\n     data-component="VimeoVideo"\n     style="width: '
r=t["if"].call(e,null!=e?e.full_width:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(c+=r)
c+=";\n            height: "
r=t["if"].call(e,null!=e?e.default_height:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.program(7,a),"data":a})
null!=r&&(c+=r)
c+='">\n\n  <div class="overlay" style="background-color: '+l((i=null!=(i=t.background_color||(null!=e?e.background_color:e))?i:s,typeof i===o?i.call(e,{"name":"background_color","hash":{},"data":a}):i))+'">\n\n'
r=t["if"].call(e,null!=e?e.show_play_button:e,{"name":"if","hash":{},"fn":this.program(9,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='\n    <div class="poster" style="background-image: url(\''+l((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=e?e.placeholder_frame_path:e,{"name":"staticImageUrl","hash":{},"data":a}))+"')\">\n    </div>\n\n  </div>\n\n"
r=t["if"].call(e,null!=e?e.show_close_button:e,{"name":"if","hash":{},"fn":this.program(11,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='\n  <div class="iframe-container '
r=t["if"].call(e,null!=e?e.full_width:e,{"name":"if","hash":{},"fn":this.program(13,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='"\n'
r=t["if"].call(e,null!=e?e.full_width:e,{"name":"if","hash":{},"fn":this.program(15,a),"inverse":this.program(17,a),"data":a})
null!=r&&(c+=r)
return c+'  >\n\n    <iframe class="vimeo-video"\n      width="'+l((i=null!=(i=t.width||(null!=e?e.width:e))?i:s,typeof i===o?i.call(e,{"name":"width","hash":{},"data":a}):i))+'" height="'+l((i=null!=(i=t.height||(null!=e?e.height:e))?i:s,typeof i===o?i.call(e,{"name":"height","hash":{},"data":a}):i))+'"\n      src="//player.vimeo.com/video/'+l((i=null!=(i=t.video_id||(null!=e?e.video_id:e))?i:s,typeof i===o?i.call(e,{"name":"video_id","hash":{},"data":a}):i))+"?byline=0&amp;color=f1f1f1&amp;api=1&amp;player_id="+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'"\n      id="'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'"\n      frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen>\n    </iframe>\n\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/vimeo_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.VimeoView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/components/vimeo_view"
n.prototype.options={"autoplay":!1,"default_height":null,"full_width":!1,"show_play_button":!0,"show_close_button":!0,"background_color":"#000"}
n.prototype.initialize=function(e){this.options=_.defaults(e,this.options)
return this.options.ratio=this.options.height/this.options.width*100}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return new E.lib.VimeoVideo(this.$('[data-component="VimeoVideo"]'),this.options)}
n.prototype.getTemplateData=function(){return this.options}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.subheros.VideoView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/subheros/video_view"
n.prototype.VIDEO_SOURCES={"VIMEO":"Vimeo","HTML5":"html5","YOUTUBE":"Youtube"}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
this.setAutoplay()
this.model.get("video_source")===this.VIDEO_SOURCES.VIMEO?e=new Banana.views.components.VimeoView({"container":this.$(".vimeo-video-container"),"video_id":parseInt(this.model.get("video_id")),"width":1400,"height":788,"default_height":450,"full_width":!0,"close":!0,"background_color":"#000","placeholder_frame_path":this.model.get("main_image"),"show_play_button":!1,"autoplay":this.autoplay}):this.model.get("video_source")===this.VIDEO_SOURCES.YOUTUBE||console.warn(this.toString+" does not have an a proper `video_source`")
return this.subview("video",e)}
n.prototype.setAutoplay=function(){var e
e="v_"+this.model.get("video_id")
if("once"!==this.model.get("autoplay")||$.cookie(e))return this.autoplay="on"===this.model.get("autoplay")?!0:!1
$.cookie(e,1,{"expires":365})
return this.autoplay=!0}
n.prototype.toString=function(){return"Banana.views.collections.subheros.VideoView"}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.subheros.LookbookSlideView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="lookbook-slide"
n.prototype.template=function(e){return"<img src='"+E.lib.ImageHelper.imageUrl(e.image_url)+"'>"}
n.prototype.events={"click":function(){return this.trigger("slideshow:next")}}
return n}(Banana.View)}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
Banana.views.subheros.LookbookView=function(t){function a(){this.openSlider=e(this.openSlider,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.className="lookbook"
a.prototype.events={"click .elastic-container img.elastic":"openSlider"}
a.prototype.openSlider=function(){var e
e=Handlebars.helpers.staticImageUrl(this.model.get("images")[0],{})
this.imageCollection||(this.imageCollection=new Banana.Collection(_.map(this.model.attributes.images,function(e){return{"image_url":e}})))
return this.loadImage(e,function(e){return function(t){e.lookbookSlidesView=new Banana.views.components.ModalView({"closeButtonInSubview":!1,"backgroundClass":"slideshow__bg","view":{"class":Banana.views.components.SlideshowView,"collection":e.imageCollection,"itemView":Banana.views.subheros.LookbookSlideView,"width":.85*window.innerWidth,"height":.85*window.innerWidth*t.height/t.width}})
return e.subview("lookbook-slides",e.lookbookSlidesView)}}(this))}
a.prototype.loadImage=function(e,t){return $("<img />").attr("src",e).load(function(){return t({"width":this.width,"height":this.height})})}
return a}(Banana.views.subheros.ElasticView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/subheros/delivery"]=Handlebars.template({"1":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='  <div style="padding: 50px 0;\n  text-align: center;\n  background: #F8F8F8;">\n\n    <section>\n\n      <h3 class="delivery-hero__subheading">Everlane Now</h3>\n\n      <h1 class="delivery-hero__heading">\n        1-hour delivery\n      </h1>\n\n      <p class="delivery-hero__main-copy">\n        Open Monday &ndash; Friday, '+l((i=null!=(i=t.openTime||(null!=e?e.openTime:e))?i:s,typeof i===o?i.call(e,{"name":"openTime","hash":{},"data":a}):i))+" &ndash; "+l((i=null!=(i=t.closeTime||(null!=e?e.closeTime:e))?i:s,typeof i===o?i.call(e,{"name":"closeTime","hash":{},"data":a}):i))+"<br>\n        "
r=(i=null!=(i=t.marketingCopy||(null!=e?e.marketingCopy:e))?i:s,typeof i===o?i.call(e,{"name":"marketingCopy","hash":{},"data":a}):i)
null!=r&&(c+=r)
return c+'\n      </p>\n\n    </section>\n\n    <div class="delivery-postal-code-form-container"></div>\n\n    <div class="delivery-hero__postal-code-success">\n      <div class="delivery-hero__postal-code-success-text hidden">\n        Enjoy!\n      </span>\n    </div>\n\n  </div>\n'},"3":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'  <div class="elastic-container">\n    <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{"width":1400},"data":a}))+'" class="elastic">\n  </div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r
r=t["if"].call(e,null!=e?e.showSDDPrompt:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/subheros/delivery"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.subheros.DeliveryView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="pre_content"
n.prototype.template="banana/templates/subheros/delivery"
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=new Banana.views.delivery.PostalCodeFormView({"container":this.$(".delivery-postal-code-form-container")})
this.subview("postalCodeForm",e)
return E.sub(E.Event.Delivery.POSTAL_CODE_CHOSEN,function(e){return function(){return e.$(".delivery-postal-code-form-container").velocity("slideUp",{"complete":function(){return e.$(".delivery-hero__postal-code-success-text").velocity("transition.fadeIn")}})}}(this))}
n.prototype.getTemplateData=function(){var e,t
e=n.__super__.getTemplateData.apply(this,arguments)
t=E.delivery.getApplicableLocation()
e.openTime=t.openTime
e.closeTime=t.closeTime
e.showSDDPrompt=!E.delivery.isActive()&&E.delivery.isMarketable()
e.marketingCopy=t.marketingCopy
return e}
return n}(Banana.View)}).call(this);(function(){var e,t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
e=null
Banana.controllers.CollectionsController=function(t){function a(){return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.subheros={"mens-tees":Banana.views.subheros.TeeView,"sf-everlane-now-womens":Banana.views.subheros.DeliveryView,"sf-everlane-now-mens":Banana.views.subheros.DeliveryView,"ny-everlane-now-womens":Banana.views.subheros.DeliveryView,"ny-everlane-now-mens":Banana.views.subheros.DeliveryView,"video":Banana.views.subheros.VideoView,"elastic":Banana.views.subheros.ElasticView,"lookbook":Banana.views.subheros.LookbookView}
a.prototype.beforeAction=function(t){a.__super__.beforeAction.apply(this,arguments)
return Banana.models.Collection.get(t.permalink).then(function(){return function(t){return e=t}}(this))}
a.prototype.show=function(t,n,a){var r,i,o
r=e
this.adjustTitle(r.get("title"))
this.setMetaDescription(r)
e.get("permalink").match("shopstyle")&&!$.cookie("show_shopstyle")&&$.cookie("show_shopstyle",!0,{"expires":new Date(Date.parse("2015-01-01"))})
E.pub(E.Event.Collections.PAGE_VIEW,e.attributes)
i=function(e){var t,n,i,o,s,l,c
if(e["class"]){_.isArray(e["class"])||(e["class"]=[e["class"]])
e.model||(e.model=r)
s=e["class"]
c=[]
for(t=i=0,o=s.length;o>i;t=++i){n=s[t]
c.push(this["view_"+t]=new n({"model":e.model,"region":"pre_content","autoplay":null!=(l=a.query)?l.autoplay:void 0}))}return c}}
this.view=new Banana.views.collections.ShowView({"model":r})
return!(null!=(o=a.query)?o.ev_nosub:void 0)&&this.preContent(r)?this.reuse("pre_content",{"class":this.preContent(r),"model":new Banana.models.Subhero(_.extend({},r.get("subhero"),{"collection":r}))},i):void 0}
a.prototype.preContent=function(e){var t,n,a,r
t=e.get("permalink")
return e.get("subhero")||this.subheros[t]?this.subheros[t]?this.subheros[t]:this.subheros[null!=(n=e.get("subhero"))?n.subhero_type:void 0]?"video"===(null!=(a=e.get("subhero"))?a.subhero_type:void 0)?"US"===(r=E.session.getCountryCode())||"CA"===r?this.subheros.video:this.subheros.elastic:this.subheros[e.get("subhero").subhero_type]:Banana.views.subheros.ElasticView:!1}
a.prototype.setMetaDescription=function(e){var t,n,a,r,i
n=e.get("products").first()
t=(null!=(i=e.get("description"))?i.length:void 0)?e.get("description"):(a=n.get("description"),r=_.findWhere(a,{"heading":"Quick Description"}))
this.adjustMetaTag("description",null!=r?r.content:void 0)
this.adjustMetaTag("og:description",null!=r?r.content:void 0)
this.adjustMetaTag("og:title",e.get("title"))
return this.adjustMetaTag("og:image","http:"+E.lib.ImageHelper.imageUrl(n.get("main_image")))}
return a}(Banana.Controller)}).call(this);(function(){Banana.globals.months=["January","February","March","April","May","June","July","August","September","October","November","December"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/rsvp_button_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing
r=(i=null!=(i=t.rsvp_html||(null!=e?e.rsvp_html:e))?i:s,typeof i===o?i.call(e,{"name":"rsvp_html","hash":{},"data":a}):i)
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/rsvp_button_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.RsvpButtonView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/components/rsvp_button_view"
n.prototype.events={"click .promo-notify":"rsvp","click .login-prompt":"setLoginSession"}
n.prototype.listen={"rsvp model":"rsvpDone","setLoginSession model":"setLoginSession"}
n.prototype.COOKIE_NAME="waitlist_event_id"
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
this.options=e
return E.session.isSignedIn()&&parseInt($.cookie(this.COOKIE_NAME))===this.model.get("id")?this.model.rsvp():void 0}
n.prototype.rsvpDone=function(e){if(e)return this.container.html('<a href="javascript:;" class="promo-notify">Waitlist</a>')
this.container.html("<span>Thank You.<br>"+this.options.previous_reservation_text+"</span>")
$.removeCookie(this.COOKIE_NAME)
return this.options.reservation_callback?this.options.reservation_callback():void 0}
n.prototype.rsvp=function(){this.container.html("<a>...</a>")
return this.model.rsvp()}
n.prototype.setLoginSession=function(){return $.cookie(this.COOKIE_NAME,this.model.get("id"),{"expires":1})}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.rsvp_html=this.model.get("available")?E.session.isSignedIn()&&this.model.get("reservation")?"<span>"+this.options.previous_reservation_text+"</span>":E.session.isSignedIn()?"<a href='javascript:;' class='promo-notify'>"+this.options.available_text+"</a>":"<a href='javascript:;' class='login-prompt'>"+this.options.logged_out_text+"</a>":"<span>"+this.options.sold_out_text+"</span>"
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/coming_soon/list_item_view"]=Handlebars.template({"1":function(){return'    <span class="button-info">Sign up to shop one day early.</span>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="vertical-center">\n  <h3>'+l((i=null!=(i=t.name||(null!=e?e.name:e))?i:s,typeof i===o?i.call(e,{"name":"name","hash":{},"data":a}):i))+"</h3>\n  <h2>"+l((i=null!=(i=t.date||(null!=e?e.date:e))?i:s,typeof i===o?i.call(e,{"name":"date","hash":{},"data":a}):i))+"</h2>\n  <p>"
r=(i=null!=(i=t.content||(null!=e?e.content:e))?i:s,typeof i===o?i.call(e,{"name":"content","hash":{},"data":a}):i)
null!=r&&(c+=r)
c+='</p>\n  <div class="rsvp-button"></div>\n'
r=t.unless.call(e,null!=(r=null!=e?e.event:e)?r.reservation:r,{"name":"unless","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/coming_soon/list_item_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.coming_soon.ListItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="item"
n.prototype.template="banana/templates/coming_soon/list_item_view"
n.prototype.render=function(){var e,t
n.__super__.render.apply(this,arguments)
this.$el.css("background-image","url("+E.lib.ImageHelper.imageUrl(this.model.get("image"))+")")
this.$el.attr("id",this.model.get("name").replace(/\s+/g,"-").toLowerCase())
t=this.$el.find(".button-info")
e=new Banana.views.components.RsvpButtonView({"model":new Banana.models.Event(this.model.get("event")),"container":this.$(".rsvp-button"),"available_text":"Waitlist","sold_out_text":"This product has launched","logged_out_text":"Waitlist","previous_reservation_text":"You&rsquo;re on the list! We&rsquo;ll notify you a day before launch.","reservation_callback":function(){return t.hide()}})
return this.subview("button",e)}
n.prototype.formatDate=function(e){var t,n,a,r
t=new Date(this.model.get("launches_at"))
a=t.getTimezoneOffset()/60
t.setHours(t.getHours()+a)
r=Banana.globals.months[t.getMonth()].substring(0,3)
n=t.getDate()
return"humanize"===e?r+" "+n:"serialize"===e?r+"-"+n:void 0}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.date=this.formatDate("humanize")
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/coming_soon/list_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<ul class="coming-soon-list"></ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/coming_soon/list_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.coming_soon.ListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.itemView=Banana.views.coming_soon.ListItemView
n.prototype.listSelector=".coming-soon-list"
n.prototype.template="banana/templates/coming_soon/list_view"
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/coming_soon/index_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l='  <div class="coming-soon-list">\n    <div class="no-new-products">\n      <h2>Sorry!</h2>\n      <p>We don\'t currently have any upcoming launches. Check back soon!</p>\n\n      <a href="/collections/'
r=(i=null!=(i=t.gender||(null!=e?e.gender:e))?i:s,typeof i===o?i.call(e,{"name":"gender","hash":{},"data":a}):i)
null!=r&&(l+=r)
l+='s-all">Shop '
r=(i=null!=(i=t.gender||(null!=e?e.gender:e))?i:s,typeof i===o?i.call(e,{"name":"gender","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+"</a>\n    </div>\n  </div>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r
r=t["if"].call(e,null!=e?e.noNewProducts:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/coming_soon/index_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.coming_soon.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.mainClass="coming-soon"
n.prototype.region="pre_content"
n.prototype.template="banana/templates/coming_soon/index_view"
n.prototype.SECTION_PEEK_AMOUNT=30
n.prototype.noNewProducts=!1
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
this.chrome_height=$(".chrome-header").outerHeight()
this.scroll_to=e.scroll_to
this.gender=null!=e?e.gender:void 0
return 0===this.collection.length?this.noNewProducts=!0:void 0}
n.prototype.render=function(){var e,t
n.__super__.render.apply(this,arguments)
if(0!==this.collection.length){e=new Banana.views.coming_soon.ListView({"collection":this.collection,"containerMethod":"after"})
this.subview("list",e)
t=new E.lib.VerticalSlides(e.$el.find("ul"),{"nav":!1,"next":!1,"title":"comingsoon","loadingScreen":$(""),"calculateSectionHeight":function(e){return function(){return $(window).outerHeight()-e.chrome_height-e.SECTION_PEEK_AMOUNT}}(this),"loadedFunction":function(e){return function(){return e.scroll_to?$("#"+e.scroll_to).velocity("scroll"):void 0}}(this)})
t.setSectionHeight()
return $(".vertical-center").center("vertical")}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.noNewProducts=this.noNewProducts
e.gender=this.gender
return e}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.ComingSoonController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.beforeAction=function(){n.__super__.beforeAction.apply(this,arguments)
this.initial_collection=new Banana.collections.ComingSoon
return this.initial_collection.fetchOnce()}
n.prototype.index=function(e){var t,n,a,r,i
a=e.gender
i=e.name?e.name:null
if(a){r="men"===a?"male":"female"
n=this.initial_collection.filter(function(e){var t
t=e.get("gender")
return"unisex"===t||t===r})
t=new Banana.collections.ComingSoon(n)}else t=this.initial_collection
this.adjustTitle("Coming Soon")
return this.view=new Banana.views.coming_soon.IndexView({"collection":t,"scroll_to":i,"gender":a})}
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/delivery/closed"]=Handlebars.template({"1":function(){return"    Sorry, due to bad weather we are closed for the day.<br>\n    We will reopen as soon as we can!\n"},"3":function(e){var t,n=this.lambda,a=this.escapeExpression
return"    Open Monday - Friday, "+a(n(null!=(t=null!=e?e.location:e)?t.openTime:t,e))+" - "+a(n(null!=(t=null!=e?e.location:e)?t.closeTime:t,e))+" "+a(n(null!=(t=null!=e?e.location:e)?t.timeZone:t,e))+"\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<h2 class="delivery-modal__heading">Everlane Now is closed for the day.</h2>\n\n<p class="delivery-modal__copy delivery-modal__copy--closed">\n'
r=t["if"].call(e,null!=(r=null!=e?e.location:e)?r.badWeather:r,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(i+=r)
return i+'</p>\n\n<a href="/" class="flat-button uber">Shop Everlane</a>\n<br>\n<br>\n<a href="javascript:;" class="return-link">Learn more about Everlane Now</a>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/delivery/closed"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.delivery")
Banana.views.delivery.CloseView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/delivery/closed"
n.prototype.className="delivery-modal"
n.prototype.events={"click a":function(){return E.pub(E.Event.Exit)}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.location=E.delivery.Locations.NY
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/delivery/postal_code_search"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<h2 class="delivery-modal__heading">Can&rsquo;t find your zip code?</h2>\n\n<p class="delivery-modal__copy">\n  Which ZIP code do you want to deliver your order to?\n</p>\n\n<form class="fancy-form delivery-enter-zip">\n  <p class="delivery-enter-zip__error"></p>\n\n  <label for="delivery-enter-zip__input"></label>\n  <input id="delivery-enter-zip__input" class="delivery-enter-zip__input" size="30" type="text" placeholder="Enter ZIP Code">\n\n  <br>\n\n  <button type="submit" class="delivery-enter-zip__button" name="commit">\n    Search\n  </button>\n\n</form>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/delivery/postal_code_search"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/delivery/postal_code_map"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="delivery-waitlist-form-wrap">\n  <h2 class="delivery-modal__heading">\n    Sorry, we&rsquo;re not delivering to your area just yet.\n  </h2>\n\n  <p class="delivery-modal__copy">\n    We&rsquo;ll tell you when we expand to your ZIP code.\n  </p>\n\n  <form class="fancy-form delivery-waitlist-form">\n    <p class="delivery-error"></p>\n\n    <div class="field">\n      <label for="delivery-waitlist-form__input--email"></label>\n      <input id="delivery-waitlist-form__input--email" class="delivery-waitlist-form__input delivery-waitlist-form__input--email" size="30" type="text" placeholder="Enter Email" autofocus>\n    </div>\n\n    <div class="field">\n      <label for="delivery-waitlist-form__input--zip"></label>\n      <input id="delivery-waitlist-form__input--zip" class="delivery-waitlist-form__input delivery-waitlist-form__input--zip" size="9" type="text" placeholder="ZIP Code">\n    </div>\n\n    <button type="submit" class="delivery-waitlist-form__button" name="commit">Notify Me</button>\n\n  </form>\n\n</div>\n\n<div class="delivery-waitlist-confirmation">\n  <h2 class="delivery-modal__heading">Thank You</h2>\n  <p class="delivery-modal__copy">\n    We&rsquo;ll notify you as soon as we start delivering to\n    <span class="delivery-waitlist-confirmation__zip"></span>.\n  </p>\n  <a href="/" class="return-link">Return to Everlane</a>\n</div>\n'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/delivery/postal_code_map"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.delivery")
Banana.views.delivery.PostalCodeMapView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/delivery/postal_code_map"
n.prototype.className="delivery-modal"
n.prototype.validations={"#delivery-waitlist-form__input--zip":function(e){return e.length>4||"Please input a valid postal code"},"#delivery-waitlist-form__input--email":{"presence":!0,"pattern":"email"}}
n.prototype.events={"submit form":"recordPostalCode","click .return-link":function(){return E.pub(E.Event.Exit)}}
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
return this.postalCode=e.postalCode}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$(".delivery-waitlist-form__input--zip").val(this.postalCode)}
n.prototype.recordPostalCode=function(e){var t,n,a
E.pub(E.Event.Delivery.BUTTON_CLICK,{"context":"waitlist"})
e.preventDefault()
if(this.validate()){n=this.$(".delivery-waitlist-form__input--zip").val()
t=this.$(".delivery-waitlist-form__button")
a=new E.lib.ButtonProgressBar({"button":t})
a.start()
return $.ajax({"url":"/api/identities","method":"POST","data":{"identity":{"email":this.$(".delivery-waitlist-form__input--email").val(),"postal_code":n}},"success":function(e){return function(){a.stop()
e.$(".delivery-waitlist-confirmation__zip").html(n)
return e.$(".delivery-waitlist-form-wrap").velocity("transition.slideUpOut",{"duration":300,"complete":function(){return e.$(".delivery-waitlist-confirmation").velocity("transition.slideUpIn")}})}}(this)})}}
n.prototype.getTemplateData=function(){return $.extend(n.__super__.getTemplateData.apply(this,arguments),{"postalCode":this.postalCode})}
return n}(Banana.View)
E.mix(Banana.views.delivery.PostalCodeMapView,E.mixins.Form)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.delivery")
Banana.views.delivery.PostalCodeSearchView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/delivery/postal_code_search"
n.prototype.className="delivery-modal"
n.prototype.events={"submit form":"searchPostalCode"}
n.prototype.validations={"#delivery-enter-zip__input":{"presence":!0,"pattern":"us_postal_code"}}
n.prototype.searchPostalCode=function(e){var t,n
E.pub(E.Event.Delivery.BUTTON_CLICK,{"context":"search zip"})
e.preventDefault()
if(this.validate()){n=parseInt(this.$(".delivery-enter-zip__input").val())
if(E.delivery.isValidPostalCode(n)){E.delivery.setPostalCode(n)
t=E.delivery.getApplicableLocation()
E.showAlert({"title":"Everlane delivers to "+n+"!","body":'<a href="/collections/'+t.collectionPermalinks.female+'">Shop Women</a> /'+('<a href="/collections/'+t.collectionPermalinks.male+'">Shop Men</a>'),"flash":!0})
return this.trigger("VALID_POSTAL_CODE")}return new Banana.views.components.ModalView({"view":{"class":Banana.views.delivery.PostalCodeMapView,"postalCode":n}})}}
n.prototype.attach=function(){return n.__super__.attach.apply(this,arguments)}
return n}(Banana.View)
E.mix(Banana.views.delivery.PostalCodeSearchView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/delivery/postal_code_form"]=Handlebars.template({"1":function(e){var t=this.lambda,n=this.escapeExpression
return'        <option value="'+n(t(e,e))+'">'+n(t(e,e))+"</option>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<label for="postal_code_'+l((i=null!=(i=t.select_id||(null!=e?e.select_id:e))?i:s,typeof i===o?i.call(e,{"name":"select_id","hash":{},"data":a}):i))+'"></label>\n\n<div class="delivery-postal-code-form__container">\n\n  <div class="delivery-postal-code-form__buttons">\n    <select\n        id="postal_code_'+l((i=null!=(i=t.select_id||(null!=e?e.select_id:e))?i:s,typeof i===o?i.call(e,{"name":"select_id","hash":{},"data":a}):i))+'"\n        class="fancy-select delivery-postal-code-form__select"\n        placeholder="Select your ZIP code">\n      <option></option>\n'
r=t.each.call(e,null!=e?e.postalCodes:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+'    </select>\n\n    <button type="submit" class="delivery-postal-code-form__button" name="commit" style="width: 260px;" >\n      Shop Everlane Now\n    </button>\n\n    <a href="javascript:;" class="delivery-postal-code-form__no-zip-code">\n      Can&rsquo;t find your ZIP code?\n    </a>\n  </div>\n\n  <ul class="delivery-postal-code-form__gender-buttons hidden">\n    <li>\n      <a class="flat-button uber" href="'+l((i=null!=(i=t.womensUrl||(null!=e?e.womensUrl:e))?i:s,typeof i===o?i.call(e,{"name":"womensUrl","hash":{},"data":a}):i))+'">Shop Women</a>\n    </li>\n    <li>\n      <a class="flat-button uber" href="'+l((i=null!=(i=t.mensUrl||(null!=e?e.mensUrl:e))?i:s,typeof i===o?i.call(e,{"name":"mensUrl","hash":{},"data":a}):i))+'">Shop Men</a>\n    </li>\n  </ul>\n\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/delivery/postal_code_form"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.delivery")
Banana.views.delivery.PostalCodeFormView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/delivery/postal_code_form"
n.prototype.className="delivery-postal-code-form"
n.prototype.tagName="form"
n.prototype.validations={".delivery-postal-code-form__select":{"pattern":"us_postal_code"}}
n.prototype.events={"submit":"submitPostalCode","click .delivery-postal-code-form__no-zip-code":"noPostalCode"}
n.prototype.initialize=function(e){return this.location=null!=e?e.location:void 0}
n.prototype.render=function(){return n.__super__.render.apply(this,arguments)}
n.prototype.attach=function(){return n.__super__.attach.apply(this,arguments)}
n.prototype.submitPostalCode=function(e){var t
E.pub(E.Event.Delivery.BUTTON_CLICK,{"context":"main cta"})
e.preventDefault()
if(this.validate(e.target)){t=$(e.target).find("select").select2("val")
E.delivery.setPostalCode(t)
return this.$(".delivery-postal-code-form__buttons").velocity({"top":-60,"opacity":0},{"easing":"easeInBack","duration":350,"display":"none","complete":function(){return function(){return E.pub(E.Event.Delivery.POSTAL_CODE_CHOSEN)}}(this)})}}
n.prototype.getTemplateData=function(){var e,t
e=n.__super__.getTemplateData.apply(this,arguments)
t=this.location||E.delivery.getApplicableLocation()
e.select_id=_.uniqueId("postal_code_selector")
e.mensUrl="collections/"+t.collectionPermalinks.male
e.womensUrl="collections/"+t.collectionPermalinks.female
e.postalCodes=null!=t?t.postalCodes:void 0
return e}
n.prototype.showGenderButtons=function(){return this.$(".delivery-postal-code-form__gender-buttons").velocity("transition.fadeIn",{"delay":200})}
n.prototype.noPostalCode=function(e){E.pub(E.Event.Delivery.BUTTON_CLICK,{"context":"cant find zip code"})
e.preventDefault()
return this.subview("postal_search",new Banana.views.components.ModalView({"view":Banana.views.delivery.PostalCodeSearchView}))}
return n}(Banana.View)
E.mix(Banana.views.delivery.PostalCodeFormView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/delivery/index"]=Handlebars.template({"1":function(){return"delivery-content__end-image--closed"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=t.helperMissing,o=this.escapeExpression,s='<section class="delivery-hero">\n\n  <h3 class="delivery-hero__subheading">Everlane Now</h3>\n\n  <h1 class="delivery-hero__heading">\n    1-hour delivery\n  </h1>\n\n  <p class="delivery-hero__main-copy">\n    Introducing 1-hour delivery in New York City<br>\n    Open Monday &ndash; Friday, 9am &ndash; 6pm<br>\n    Free for orders of 2+ items\n  </p>\n\n  <div class="delivery-postal-code-form-container"></div>\n\n</section>\n\n<section class="delivery-content">\n\n  <div class="bick">\n\n    <ul class="delivery-columns">\n      <li class="delivery-columns__column">\n        <div class="delivery-columns__sprite icons-shirts"></div>\n        <h3 class="delivery-columns__heading">Shop collection</h3>\n        <p class="delivery-columns__copy">\n          Shop the Everlane Now collection<br>\n          then select <em>1-hour delivery</em><br>\n          before you check out.\n        </p>\n      </li>\n\n      <li class="delivery-columns__column">\n        <div class="delivery-columns__sprite delivery-columns__sprite--short icons-clock"></div>\n        <h3 class="delivery-columns__heading">Relax for 1 hour</h3>\n        <p class="delivery-columns__copy">\n          Hang out for 40 &ndash; 60 minutes.<br>\n          We&rsquo;ll text you when we&rsquo;re<br>\n          5 minutes away.\n        </p>\n      </li>\n\n      <li class="delivery-columns__column">\n        <div class="delivery-columns__sprite delivery-columns__sprite--short icons-postmates"></div>\n        <h3 class="delivery-columns__heading">Receive your order</h3>\n        <p class="delivery-columns__copy">\n          Our friends at <em>Postmates</em> will<br>\n          hand-deliver your package to<br>\n          your door or office.\n        </p>\n      </li>\n    </ul>\n\n    <hr class="delivery-content__divider">\n\n    <p class="delivery-content__big-copy">\n      We believe the future of online<br>\n      is incredibly fast delivery. Get your<br>\n      instant gratification, New York City.\n    </p>\n\n    <div class="delivery-postal-code-form-container"></div>\n\n  <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"delivery_bottom_bg.png",{"name":"staticImageUrl","hash":{},"data":a}))+'" class="delivery-content__end-image '
r=t.unless.call(e,null!=e?e.openForBusiness:e,{"name":"unless","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(s+=r)
return s+'">\n\n</section>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/delivery/index"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.delivery")
E.ns("E.delivery")
Banana.views.delivery.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/delivery/index"
n.prototype.region="pre_content"
n.prototype.mainClass="delivery"
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=[]
if(E.delivery.openForBusiness(E.delivery.Locations.NY)){this.$(".delivery-postal-code-form-container").each(function(t){return function(n,a){var r
r=new Banana.views.delivery.PostalCodeFormView({"container":$(a),"location":E.delivery.Locations.NY})
e.push(r)
return t.subview("postal_code_form_"+n,r)}}(this))
return E.sub(E.Event.Delivery.POSTAL_CODE_CHOSEN,function(){return function(){return _.each(e,function(e){return e.showGenderButtons()})}}(this))}return this.subview("postal_search",new Banana.views.components.ModalView({"view":Banana.views.delivery.CloseView}))}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.openForBusiness=E.delivery.openForBusiness(E.delivery.Locations.NY)
return e}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.DeliveryController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.index=function(){this.adjustTitle("Everlane Now")
return this.view=new Banana.views.delivery.IndexView}
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/sidebar_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div class="content map-results">\n\n\n <!-- <h3 class="subheading">'+s((r=null!=(r=t.country||(null!=e?e.country:e))?r:o,typeof r===i?r.call(e,{"name":"country","hash":{},"data":a}):r))+'</h3> -->\n\n  <h4 class="title nearby-title" style="border-top: 0;">'+s((r=null!=(r=t.region||(null!=e?e.region:e))?r:o,typeof r===i?r.call(e,{"name":"region","hash":{},"data":a}):r))+'</h4>\n\n  <div class="factory-list-items"></div>\n\n\n  <h4 class="title nearby-title" style="padding-top: 20px;">Nearby Factories</h4>\n  <div class="factory-nearby-items"></div>\n\n\n  <div class="sidebar-footer">\n    <a href="#all-factories" class="learn-more see-all">See all Factories</a>\n  </div>\n\n\n\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/sidebar_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/sidebar_item_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<h3 class="subheading">'+s((r=null!=(r=t.location||(null!=e?e.location:e))?r:o,typeof r===i?r.call(e,{"name":"location","hash":{},"data":a}):r))+", "+s((r=null!=(r=t.country||(null!=e?e.country:e))?r:o,typeof r===i?r.call(e,{"name":"country","hash":{},"data":a}):r))+'</h3>\n\n<ul>\n    <li class="item">\n\n      <a href="/factories/'+s((r=null!=(r=t.permalink||(null!=e?e.permalink:e))?r:o,typeof r===i?r.call(e,{"name":"permalink","hash":{},"data":a}):r))+'">\n        <figure class="item-image">\n          <img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.thumbnail:e,{"name":"staticImageUrl","hash":{"height":80,"width":100},"data":a}))+'" />\n        </figure>\n\n        <div class="item-body">\n          <h5 class="title">'+s((r=null!=(r=t.title||(null!=e?e.title:e))?r:o,typeof r===i?r.call(e,{"name":"title","hash":{},"data":a}):r))+'</h5>\n          <span class="learn-more">Learn More&nbsp;&#62;</span>\n        </div>\n      </a>\n    </li>\n</ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/sidebar_item_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.SidebarItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/factories/sidebar_item_view"
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.SidebarView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/factories/sidebar_view"
n.prototype.initialize=function(e){this.factories=e.factories
return this.nearby=e.nearby}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=$(".sidebar")
e.css({"left":-e.outerWidth()})
return e.velocity({"left":0},{"duration":250,"easing":"easeInQuad"})}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
_.each(this.factories,function(e){return function(t){var n
n=new Banana.views.factories.SidebarItemView({"model":t,"container":e.$(".factory-list-items")})
return e.subview("sidebar-"+t.get("permalink"),n)}}(this))
return _.each(this.nearby,function(e){return function(t){var n
n=new Banana.views.factories.SidebarItemView({"model":t,"container":e.$(".factory-nearby-items")})
return e.subview("nearby-"+t.get("permalink"),n)}}(this))}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.region=this.factories[0].get("region")
e.factories=this.factories
e.nearby=this.nearby
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/map_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      <a href="#'+s((r=null!=(r=t.permalink||(null!=e?e.permalink:e))?r:o,typeof r===i?r.call(e,{"name":"permalink","hash":{},"data":a}):r))+'"\n         id="'+s((r=null!=(r=t.permalink||(null!=e?e.permalink:e))?r:o,typeof r===i?r.call(e,{"name":"permalink","hash":{},"data":a}):r))+'"\n         data-location="'+s((r=null!=(r=t.region||(null!=e?e.region:e))?r:o,typeof r===i?r.call(e,{"name":"region","hash":{},"data":a}):r))+'"\n         class="pin" title="'+s((r=null!=(r=t.region||(null!=e?e.region:e))?r:o,typeof r===i?r.call(e,{"name":"region","hash":{},"data":a}):r))+'">\n      </a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=this.lambda,o=this.escapeExpression,s=t.helperMissing,l='<div class="map-view full-screen">\n\n  <div class="sidebar fadein">\n\n    <div class="content featured-content">\n      <div class="intro-block">\n        <h2 class="title intro-title">Our Factories</h2>\n        <p class="description">Every factory has a story. We seek the best from around the world to make our products.\n          <a href="https://twitter.com/intent/tweet?url=https://www.everlane.com/factories&button_hashtag=KnowYourFactories&text=Every factory has a story, and it\'s your right to know it.">\n              #KnowYourFactories\n          </a>\n        </p>\n        <span class="directions">Choose a location on the map</span>\n      </div>\n\n      <div class="factory-list-items">\n        <h3 class="title subheading">Featured Factory</h3>\n\n        <ul>\n          <li class="item">\n            <a href="/factories/'+o(i(null!=(r=null!=e?e.featured:e)?r.permalink:r,e))+'">\n              <figure class="item-image">\n                <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=(r=null!=e?e.featured:e)?r.thumbnail:r,{"name":"staticImageUrl","hash":{"height":80,"width":100},"data":a}))+'" />\n              </figure>\n\n              <div class="item-body">\n                <h5 class="title">'+o(i(null!=(r=null!=e?e.featured:e)?r.title:r,e))+'</h5>\n                <span class="learn-more">Learn More &#62;</span>\n              </div>\n            </a>\n          </li>\n        </ul>\n\n        <div class="sidebar-footer">\n          <a href="#all-factories" class="learn-more see-all">See all Factories</a>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n  <div class="factory-map">\n'
r=t.each.call(e,null!=e?e.items:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(l+=r)
return l+"  </div>\n\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/map_view"]}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
Banana.views.factories.MapView=function(t){function a(){this.setSectionHeight=e(this.setSectionHeight,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.region="pre_content"
a.prototype.template="banana/templates/factories/map_view"
a.prototype.events={"click .pin":"revealInSidebar","click .see-all":"scrollToGridView"}
a.prototype.originalMapWidth=1410
a.prototype.originalMapHeight=783
a.prototype.initialize=function(){this.locations=this.collection.groupByRegions()
this.featured=this.collection.findWhere({"featured":!0})
return $(window).on("resize",this.setSectionHeight)}
a.prototype.remove=function(){$(window).off("resize",this.setSectionHeight)
return a.__super__.remove.apply(this,arguments)}
a.prototype.render=function(){var e
a.__super__.render.apply(this,arguments)
E.lib.ImageLoader.loadImages(this.collection.getHeroImages())
this.setSectionHeight()
this.renderPins()
e=this.$(".pin")
e.each(function(e,t){return setTimeout(function(){return $(t).addClass("pindrop")},Math.ceil(800*Math.random()))})
return e.tipTip()}
a.prototype.scrollToGridView=function(e){e.preventDefault()
E.pub(E.Event.Factories.VIEW_ALL)
return $(".factory-grid").velocity("scroll",{"offset":E.config.fixedNavHeight})}
a.prototype.renderPins=function(){return _.each(this.locations,function(e){return function(t){var n,a
n=_.first(t.cities)
a=n.get("region")
return $("[data-location='"+a+"']").css(e.calculateCoords(n.get("coords")))}}(this))}
a.prototype.calculateCoords=function(e){var t
t=this.$(".factory-map").width()/this.originalMapWidth
return{"left":e[0]*t,"top":e[1]*t}}
a.prototype.revealInSidebar=function(e){var t,n,a,r,i
e.preventDefault()
n=$(e.currentTarget)
this.$(".pin").removeClass("pin-active-state")
n.addClass("pin-active-state")
i=n.data("location")
a=this.locations[i].cities
t=this.locations[i].nearby
E.pub(E.Event.Factories.PIN_CLICK,{"pin":i})
r=this.$(".sidebar")
return r.velocity({"left":-r.outerWidth()},{"duration":250,"easing":"easeOutQuad","complete":function(e){return function(){var n
e.$(".content").remove()
e.removeSubview("sidebar")
n=new Banana.views.factories.SidebarView({"container":$(".sidebar"),"factories":a,"nearby":t})
return e.subview("sidebar",n)}}(this)})}
a.prototype.setSectionHeight=function(){var e,t
e=$(window).outerHeight()-($("header.chrome-header").outerHeight()+80)
this.$(".pin").hide()
this.$(".full-screen").css(e<this.originalMapHeight?{"height":e}:{"height":this.originalMapHeight})
clearTimeout(t)
return t=setTimeout(function(e){return function(){e.$(".pin").show()
return e.renderPins()}}(this),400)}
a.prototype.getTemplateData=function(){var e
e=a.__super__.getTemplateData.apply(this,arguments)
e.featured=this.featured.attributes
return e}
return a}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/grid_item_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<a href="/factories/'+s((r=null!=(r=t.permalink||(null!=e?e.permalink:e))?r:o,typeof r===i?r.call(e,{"name":"permalink","hash":{},"data":a}):r))+'">\n  <figure>\n    <img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.thumbnail:e,{"name":"staticImageUrl","hash":{"height":217,"width":300},"data":a}))+'" />\n\n    <figcaption class="item-body">\n      <span class="subheading">'+s((r=null!=(r=t.location||(null!=e?e.location:e))?r:o,typeof r===i?r.call(e,{"name":"location","hash":{},"data":a}):r))+'</span>\n      <h4 class="title">'+s((r=null!=(r=t.title||(null!=e?e.title:e))?r:o,typeof r===i?r.call(e,{"name":"title","hash":{},"data":a}):r))+"</h4>\n    </figcaption>\n  </figure>\n</a>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/grid_item_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.GridItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="factory column item"
n.prototype.template="banana/templates/factories/grid_item_view"
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/grid_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div id="all-factories" class="factory-grid">\n  <div id="content" class="grid-content">\n    <a href="#all-factories" class="learn-more">\n      <h3 class="title subheading see-all">See all Factories</h3>\n    </a>\n  </div>\n  <ul class="factories-list row"></ul>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/grid_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.GridView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="post_content"
n.prototype.itemView=Banana.views.factories.GridItemView
n.prototype.listSelector=".factories-list"
n.prototype.template="banana/templates/factories/grid_view"
n.prototype.events={"click .see-all":"scrollToGridView"}
n.prototype.scrollToGridView=function(e){e.preventDefault()
E.pub(E.Event.Factories.VIEW_ALL)
return this.$(".factory-grid").velocity("scroll",{"offset":E.config.fixedNavHeight})}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/index_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/index_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.template="banana/templates/factories/index_view"
n.prototype.mainClass="factories"
n.prototype.render=function(){var e,t
n.__super__.render.apply(this,arguments)
t=new Banana.views.factories.MapView({"collection":this.collection})
this.subview("map",t)
e=new Banana.views.factories.GridView({"collection":this.collection})
return this.subview("grid",e)}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/details_header_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div class="factory-details-header">\n\n  <div class="hero elastic-container">\n\n    <nav class="utility-nav">\n      <ul>\n        <li class="nav-item" data-tooltip="Factory Home">\n          <a href="/factories"><span class="utilities-home"></span></a>\n        </li>\n\n        <li class="nav-item" data-tooltip="Previous">\n          <a href="/factories/'+s((r=null!=(r=t.prevFactory||(null!=e?e.prevFactory:e))?r:o,typeof r===i?r.call(e,{"name":"prevFactory","hash":{},"data":a}):r))+'"><span class="utilities-prev"></span></a>\n        </li>\n\n        <li class="nav-item" data-tooltip="Next">\n          <a href="/factories/'+s((r=null!=(r=t.nextFactory||(null!=e?e.nextFactory:e))?r:o,typeof r===i?r.call(e,{"name":"nextFactory","hash":{},"data":a}):r))+'"><span class="utilities-next"></span></a>\n        </li>\n      </ul>\n    </nav>\n\n    <nav class="utility-nav bottom">\n      <ul>\n        <li class="nav-item" data-tooltip="Factory Home">\n          <a href="/factories"><span class="utilities-home"></span></a>\n        </li>\n\n        <li class="nav-item" data-tooltip="Previous">\n          <a href="/factories/'+s((r=null!=(r=t.prevFactory||(null!=e?e.prevFactory:e))?r:o,typeof r===i?r.call(e,{"name":"prevFactory","hash":{},"data":a}):r))+'"><span class="utilities-prev"></span></a>\n        </li>\n\n        <li class="nav-item" data-tooltip="Next">\n          <a href="/factories/'+s((r=null!=(r=t.nextFactory||(null!=e?e.nextFactory:e))?r:o,typeof r===i?r.call(e,{"name":"nextFactory","hash":{},"data":a}):r))+'"><span class="utilities-next"></span></a>\n        </li>\n      </ul>\n    </nav>\n\n    <img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{},"data":a}))+'" class="elastic fadein" />\n  </div>\n\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/details_header_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.DetailsHeaderView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="pre_content"
n.prototype.template="banana/templates/factories/details_header_view"
n.prototype.events={"click .nav-item":"trackNavCLicks"}
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
this.onScrollClosure=function(e){return function(){return e.onScroll()}}(this)
return $(window).on("scroll.factories.details_header_view",this.onScrollClosure)}
n.prototype.remove=function(){n.__super__.remove.apply(this,arguments)
return $(window).off("scroll.factories.details_header_view",this.onScrollClosure)}
n.prototype.onScroll=function(){this.header=this.$(".factory-details-header")
this.bottomOfHeader=this.header.offset().top+this.header.outerHeight()
return this.$(".utility-nav.bottom").toggleClass("show",$(window).scrollTop()>this.bottomOfHeader)}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
this.$el.find("img").reserveSpace()
return this.$(".nav-item").tipTip({"attribute":"data-tooltip","enter":function(){return $(this).on("mousedown",function(e){return function(){return $(e).trigger("mouseleave.tipTip")}}(this))}})}
n.prototype.trackNavCLicks=function(){return E.pub(E.Event.Factories.NAV_CLICK)}
n.prototype.shiftFactoryIndex=function(e){var t,n,a
t=this.model.collection.length-1
e="next"===e?1:-1
n=this.model.collection.indexOf(this.model)
a=n+e
a>t?a=0:0>a&&(a=t)
return this.model.collection.at(a).get("permalink")}
n.prototype.groupfactories=function(){var e,t
t=this.model.collection.models.length
return e=_.pluck(this.model.collection.models,"attributes")}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.factories=_.pluck(this.model.collection.models,"attributes")
e.nextFactory=this.shiftFactoryIndex("next")
e.prevFactory=this.shiftFactoryIndex("prev")
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/factories/details_view"]=Handlebars.template({"1":function(e){var t,n=this.lambda,a=this.escapeExpression
return'        <li class="item ajax">\n          <ul class="horizontal">\n            <li>\n              <span class="icon metadata-weather"></span>\n              <dl class="meta">\n                <dt class="subheading">Weather</dt>\n                <dd><span id="weather">-- </span><sup>o</sup>F</dd>\n              </dt>\n            </li>\n            <li>\n              <span class="icon metadata-time"></span>\n              <dl class="meta">\n                <dt class="subheading">'+a(n(null!=(t=null!=e?e.dateTaime:e)?t.day:t,e))+"</dt>\n                <dd>"+a(n(null!=(t=null!=e?e.dateTaime:e)?t.time:t,e))+"</dd>\n              </dt>\n            </li>\n          </ul>\n        </li>\n"},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <li class="item">\n          <span class="icon metadata-employees"></span>\n          <dl class="meta">\n            <dt class="subheading">Employees</dt>\n            <dd>'+s((r=null!=(r=t.employees||(null!=e?e.employees:e))?r:o,typeof r===i?r.call(e,{"name":"employees","hash":{},"data":a}):r))+"</dd>\n          </dt>\n        </li>\n"},"5":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <li class="item">\n          <span class="icon metadata-calendar"></span>\n          <dl class="meta">\n            <dt class="subheading">Established</dt>\n            <dd>'+s((r=null!=(r=t.established_date||(null!=e?e.established_date:e))?r:o,typeof r===i?r.call(e,{"name":"established_date","hash":{},"data":a}):r))+"</dd>\n          </dt>\n        </li>\n"},"7":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'        <li class="item">\n          <span class="icon metadata-factory"></span>\n          <dl class="meta">\n            <dt class="subheading">Factory Size (sq ft)</dt>\n            <dd>'+s((r=null!=(r=t.factory_size||(null!=e?e.factory_size:e))?r:o,typeof r===i?r.call(e,{"name":"factory_size","hash":{},"data":a}):r))+"</dd>\n          </dt>\n        </li>\n"},"9":function(e,t,n,a){var r,i='        <li class="item">\n          <ul>\n'
r=t.each.call(e,null!=e?e.products:e,{"name":"each","hash":{},"fn":this.program(10,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"          </ul>\n        </li>\n"},"10":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='              <li class="item-second-level">\n'
r=t["if"].call(e,null!=e?e.collection:e,{"name":"if","hash":{},"fn":this.program(11,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='                  <span class="icon product products-'+l((i=null!=(i=t.icon||(null!=e?e.icon:e))?i:s,typeof i===o?i.call(e,{"name":"icon","hash":{},"data":a}):i))+'"></span>\n                  <dl class="meta">\n                    <dt class="subheading">product</dt>\n                    <dd>'+l((i=null!=(i=t.short_name||(null!=e?e.short_name:e))?i:s,typeof i===o?i.call(e,{"name":"short_name","hash":{},"data":a}):i))+"</dd>\n                  </dt>\n                "
r=t["if"].call(e,null!=e?e.collection:e,{"name":"if","hash":{},"fn":this.program(13,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"\n              </li>\n"},"11":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'                <a href="/collections/'+s((r=null!=(r=t.collection||(null!=e?e.collection:e))?r:o,typeof r===i?r.call(e,{"name":"collection","hash":{},"data":a}):r))+"/products/"+s((r=null!=(r=t.permalink||(null!=e?e.permalink:e))?r:o,typeof r===i?r.call(e,{"name":"permalink","hash":{},"data":a}):r))+'">\n'},"13":function(){return"</a>"},"15":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      <section class="description">\n        <p>\n          '+s((r=null!=(r=t.description||(null!=e?e.description:e))?r:o,typeof r===i?r.call(e,{"name":"description","hash":{},"data":a}):r))+"\n        </p>\n      </section>\n"},"17":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'      <img src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,e,{"name":"staticImageUrl","hash":{"width":960},"data":a}))+'" >\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="factory-details clearfix">\n  <aside class="sidebar">\n    <ul>\n\n      <li class="item country">\n        <div class="meta-container">\n          <span class="icon-flag flags-'+l((i=null!=(i=t.country||(null!=e?e.country:e))?i:s,typeof i===o?i.call(e,{"name":"country","hash":{},"data":a}):i))+'"></span>\n          <dl class="meta country">\n            <dt class="subheading">'+l((i=null!=(i=t.country||(null!=e?e.country:e))?i:s,typeof i===o?i.call(e,{"name":"country","hash":{},"data":a}):i))+"</dt>\n            <dd>"+l((i=null!=(i=t.location||(null!=e?e.location:e))?i:s,typeof i===o?i.call(e,{"name":"location","hash":{},"data":a}):i))+"</dd>\n          </dl>\n        </div>\n      </li>\n\n"
r=t["if"].call(e,null!=e?e.dateTaime:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.employees:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.established_date:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.factory_size:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n\n"
r=t["if"].call(e,null!=e?e.products:e,{"name":"if","hash":{},"fn":this.program(9,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='\n    </ul>\n  </aside>\n\n  <div class="content-body">\n    <h4 class="title subheading">'+l((i=null!=(i=t.sub_title||(null!=e?e.sub_title:e))?i:s,typeof i===o?i.call(e,{"name":"sub_title","hash":{},"data":a}):i))+'</h4>\n    <h2 class="title">'+l((i=null!=(i=t.title||(null!=e?e.title:e))?i:s,typeof i===o?i.call(e,{"name":"title","hash":{},"data":a}):i))+"</h2>\n\n"
r=t["if"].call(e,null!=e?e.description:e,{"name":"if","hash":{},"fn":this.program(15,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='\n    <section class="content">\n      '
r=(i=null!=(i=t.body||(null!=e?e.body:e))?i:s,typeof i===o?i.call(e,{"name":"body","hash":{},"data":a}):i)
null!=r&&(c+=r)
c+='\n    </section>\n\n  </div>\n\n  <section class="images">\n'
r=t.each.call(e,null!=e?e.body_images:e,{"name":"each","hash":{},"fn":this.program(17,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"  </section>\n\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/factories/details_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.factories.DetailsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.template="banana/templates/factories/details_view"
n.prototype.mainClass="factories"
n.prototype.initialize=function(){this.extractProducts(_.groupBy(this.model.get("products"),"short_name"))
return this.weather=null}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.getWeather()}
n.prototype.extractProducts=function(e){this.products=[]
return _.each(e,function(e){return function(t){return t[0].short_name?e.products.push({"permalink":t[0].permalink,"short_name":t[0].short_name,"icon":t[0].product_icon,"collection":t[0].collection?"scarf"===t[0].product_icon?null:t[0].collection.permalink:null}):void 0}}(this))}
n.prototype.getWeather=function(){var e,t,n
e=$("#weather")
t=this.model.get("location").split(" ").join("_")
n=this.model.get("US"===this.model.get("country")?"state":"country")
return $.ajax({"url":"/api/weather/"+n+"/"+t}).done(function(t){var n
if(!isNaN(t)){n=Math.round(t).toString()
e.html(n)
return $(".ajax").velocity("transition.slideDownIn")}}).fail()}
n.prototype.formatTime=function(e,t){t.toString().length<2&&(t+="0")
return e>=12?e-12+":"+t+" PM":e+":"+t+" AM"}
n.prototype.calculateTime=function(){var e,t,n,a,r,i
n=new Date
a=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
t=this.model.get("GMT_offset")
i=n.getTimezoneOffset()/60
r=n.getHours()
e=i+t+r
return e>24?{"time":this.formatTime(e-24,n.getUTCMinutes()),"day":a[n.getDay()+1]}:{"time":this.formatTime(e,n.getUTCMinutes()),"day":a[n.getDay()]}}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.factories.DetailsHeaderView({"model":this.model})
return this.subview("header",e)}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.country=this.model.get("country").toLowerCase()
e.products=this.products
e.dateTaime=this.calculateTime()
return e}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.FactoriesController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.beforeAction=function(){n.__super__.beforeAction.apply(this,arguments)
return E.chaplinCollections.factory_meta_data.fetchOnce()}
n.prototype.index=function(){var e
e=E.chaplinCollections.factory_meta_data
this.adjustTitle("Factories")
E.pub(E.Event.Factories.PAGE_VIEW,{"permalink":"/factories"})
return this.view=new Banana.views.factories.IndexView({"collection":e})}
n.prototype.details=function(e){var t,n,a
t=E.chaplinCollections.factory_meta_data
a=e.permalink
n=t.findWhere({"permalink":a})
this.adjustTitle(n.get("title"))
E.pub(E.Event.Factories.PAGE_VIEW,{"permalink":a})
return this.view=new Banana.views.factories.DetailsView({"model":n,"collection":t})}
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/credit_giftcard"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div class="credit-message__message-container">\n\n  <section class="credit-message__amount">\n    <strong><sup>$</sup>'+s((r=null!=(r=t.amount||(null!=e?e.amount:e))?r:o,typeof r===i?r.call(e,{"name":"amount","hash":{},"data":a}):r))+'</strong>\n  </section>\n\n  <section class="credit-message__details">\n    <p class="credit-message__details__message">'+s((r=null!=(r=t.message||(null!=e?e.message:e))?r:o,typeof r===i?r.call(e,{"name":"message","hash":{},"data":a}):r))+'</p>\n    <p class="credit-message__details__sender">&mdash; '+s((r=null!=(r=t.sender||(null!=e?e.sender:e))?r:o,typeof r===i?r.call(e,{"name":"sender","hash":{},"data":a}):r))+'</p>\n  </section>\n\n  <div class="credit-message__shop-container">\n    <a class="credit-message__shop-button" href="javascript:;">Shop Now</a>\n\n    <p class="credit-message__shop-subtext">This credit will be applied at checkout.</p>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/credit_giftcard"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.CreditGiftcardView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/credit_giftcard"
n.prototype.className="credit-message--giftcard"
n.prototype.events={"click .credit-message__shop-button":function(){return E.pub(E.Event.Exit)}}
n.prototype.defautMessage="Enjoy!"
n.prototype.defaultSender="Everlane"
n.prototype.initialize=function(e){var t
this.giftcard=null!=e?e.data.data:void 0
this.amount=Math.floor(null!=(t=this.giftcard)?t.giftcard_amount:void 0)
this.sender=this.giftcard.purchaser_name||this.defaultSender
return this.message=this.giftcard.message||this.defautMessage}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.amount=this.amount
e.sender=this.sender
e.message=this.message
return e}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.CreditPromoView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/credit_giftcard"
n.prototype.className="credit-message--promo"
n.prototype.events={"click .credit-message__shop-button":function(){return E.pub(E.Event.Exit)}}
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
this.giftcard=null!=e?e.data.data:void 0
this.amount=this.giftcard.amount
return this.message=this.giftcard.coupon_redemption_message_subtext}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.amount=this.amount
e.message=this.message+" Enjoy!"
e.sender="Everlane"
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/credit_loading"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="credit-message__loading-container">\n  <p>Wait for it&hellip;</p>\n  <div class="credit-message__loading-bar-container">\n    <div class="credit-message__loading-bar"></div>\n  </div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/credit_loading"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.CreditLoadingView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/credit_loading"
n.prototype.className="credit-message--loading"
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/credit_message"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="credit-message__container"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/credit_message"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.CreditMessageView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/credit_message"
n.prototype.className="credit-message"
n.prototype.cards={"credit_loading":Banana.views.giftcards.CreditLoadingView,"credit_promo":Banana.views.giftcards.CreditPromoView,"credit_giftcard":Banana.views.giftcards.CreditGiftcardView}
n.prototype.cardsContainer=".credit-message__container"
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
this.gift=e.data
return null!=this.currentCard?this.currentCard:this.currentCard="credit_loading"}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
return this.listenTo(this.subview("credit_loading"),"CreditLoadingView:ProgressEnded",function(e){return function(){return e.transitionTo("giftcard"===e.gift.type?"credit_giftcard":"credit_promo")}}(this))}
n.prototype.attach=function(){return n.__super__.attach.apply(this,arguments)}
n.prototype.transitionToProperCard=function(){return this.transitionTo("giftcard"===this.gift.type?"credit_giftcard":"credit_promo")}
n.prototype.transitionTo=function(e){var t,a
if("credit_loading"===e){n.__super__.transitionTo.apply(this,arguments)
a=new E.lib.ProgressBar({"stopPoint":100,"baseIncrementAmount":65,"selector":".credit-message__loading-bar"})
a.start()
return setTimeout(function(e){return function(){a.stop()
return e.transitionToProperCard()}}(this),3e3)}t=this.subview(e)
this.subview(this.currentCard).$el.velocity("transition.flipXOut",{"duration":350,"complete":function(){return t.$el.velocity("transition.flipXIn",{"duration":275})}})
return this.currentCard=e}
return n}(Banana.views.components.CardView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/form"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<form class="gift-code">\n\n  <div class="form_fields clearfix">\n    <div class="field">\n      <input value="'+s((r=null!=(r=t.token||(null!=e?e.token:e))?r:o,typeof r===i?r.call(e,{"name":"token","hash":{},"data":a}):r))+'"\n             type="text"\n             id="giftcard-token"\n             class="fancy-input gift-code__giftcard-token"\n             placeholder="Enter your code" autofocus="true">\n    </div>\n\n    <div class="field submit">\n      <button class="default flat-button gift-code__submit-button" type="submit">Redeem</button>\n    </div>\n  </div>\n\n  <p class="gift-code__error-label"></p>\n\n</form>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/form"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.FormView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/form"
n.prototype.events={"click .gift-code__submit-button":"attemptSubmission"}
n.prototype.validations={"#giftcard-token":{"presence":!0}}
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
return this.token=e.token}
n.prototype.attemptSubmission=function(e){e.preventDefault()
return this.validate()?this.submit(e):void 0}
n.prototype.submit=function(){var e
e=new E.lib.ButtonProgressBar({"button":this.$(".gift-code__submit-button")})
return $.ajax({"type":"POST","data":{"token":this.$(".gift-code__giftcard-token").val().toUpperCase()},"url":"/api/redeem","success":function(){return function(t){e.stop()
return E.pub(E.Event.GiftCards.REDEEMED,t)}}(this),"error":function(t){return function(){e.stop()
t.addError($(".gift-code__error-label"),"Invalid Token")
return E.pub(E.Event.GiftCards.FAILED,t.$el)}}(this)})}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.token=this.token
return e}
n.prototype.attach=function(){return n.__super__.attach.apply(this,arguments)}
return n}(Banana.View)
E.mix(Banana.views.giftcards.FormView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/gift_code"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<p>Enter your code below to redeem.</p>\n<div class="giftcard-redeem__form-container"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/gift_code"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.giftcards.GiftCodeView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/gift_code"
n.prototype.className="giftcard-redeem--redeem-form"
n.prototype.initialize=function(e){n.__super__.initialize.apply(this,arguments)
this.token=e.token
E.sub(E.Event.GiftCards.REDEEMED,function(e){return function(t,n){return e.showCreditMessage(n)}}(this))
return E.sub(E.Event.GiftCards.FAILED,function(e){return function(t,n){return e.showFailedMessage(n)}}(this))}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=new Banana.views.giftcards.FormView({"container":this.$(".giftcard-redeem__form-container"),"className":"giftcard-redeem__form","token":this.token})
return this.subview("giftcard_form_view",e)}
n.prototype.showFailedMessage=function(e){return e.effect("shake")}
n.prototype.showCreditMessage=function(e){"male"===E.session.getCurrentUser().gender?Backbone.history.navigate("/collections/mens-all",!0):Backbone.history.navigate("/collections/womens-all",!0)
return new Banana.views.components.ModalView({"dismissible":!1,"view":{"class":Banana.views.giftcards.CreditMessageView,"data":e}})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/sign_in"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<p>Log in to redeem.</p>\n<div class="giftcard-redeem__form-container">\n  <div class="giftcard-redeem__fb-button"></div>\n</div>\n<p class="giftcard-redeem__login-message">Don&rsquo;t have an account? <a href="javascript:;" class="giftcard-redeem__toggle-login-view" data-to="register">Sign up for Everlane</a>.</p>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/sign_in"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.giftcards.SignInView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/sign_in"
n.prototype.className="giftcard-redeem--signin-form"
n.prototype.attach=function(){var e,t
n.__super__.attach.apply(this,arguments)
t=new Banana.views.users.SignInFormView({"container":this.$(".giftcard-redeem__form-container"),"className":"giftcard-redeem__form"})
this.subview("sign_in_form",t)
e=new Banana.views.users.FacebookConnectView({"container":this.$(".giftcard-redeem__fb-button"),"text":"sign_in"})
this.subview("fb_connect",e)
this.listenTo(t,"form:submit",function(){return this.trigger("form:submit")})
this.listenTo(t,"sign_in:error",function(){return this.trigger("sign_in:error")})
return this.listenTo(e,"fb:connect",function(){return this.trigger("fb:connect")})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/register"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<p>Create your Everlane account.</p>\n<div class="giftcard-redeem__form-container">\n  <div class="giftcard-redeem__fb-button"></div>\n</div>\n<p class="giftcard-redeem__login-message">Already have an account? <a href="javascript:;" class="giftcard-redeem__toggle-login-view" data-to="sign_in">Log in to Everlane</a>.</p>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/register"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.giftcards.RegisterView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/register"
n.prototype.className="giftcard-redeem--register-form"
n.prototype.attach=function(){var e,t
n.__super__.attach.apply(this,arguments)
t=new Banana.views.users.RegisterFormView({"container":this.$(".giftcard-redeem__form-container"),"className":"giftcard-redeem__form"})
this.subview("sign_in_form",t)
e=new Banana.views.users.FacebookConnectView({"container":this.$(".giftcard-redeem__fb-button"),"text":"register"})
this.subview("fb_connect",e)
this.listenTo(t,"form:submit",function(){return this.trigger("form:submit")})
this.listenTo(t,"register:error",function(){return this.trigger("register:error")})
return this.listenTo(e,"fb:connect",function(){return this.trigger("fb:connect")})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/login_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="giftcard-redeem__form-view"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/login_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.LoginView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/login_view"
n.prototype.events={"click .giftcard-redeem__toggle-login-view":function(e){return this.transitionTo($(e.currentTarget).data("to"))}}
n.prototype.cards={"sign_in":Banana.views.giftcards.SignInView,"register":Banana.views.giftcards.RegisterView}
n.prototype.cardsContainer=".giftcard-redeem__form-view"
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
$.cookie("_everlane_user")&&null==this.currentCard&&(this.currentCard="sign_in")
return null!=this.currentCard?this.currentCard:this.currentCard="register"}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
this.listenTo(this.subview("sign_in"),"sign_in:error",function(){return this.transitionTo("sign_in")})
return this.listenTo(this.subview("register"),"register:error",function(){return this.transitionTo("register")})}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return $(".giftcard-redeem").css({"minHeight":"700px"})}
n.prototype.transitionTo=function(e){var t
t=this.subview(e)
this.container.prevObject.find("h1").velocity("transition.fadeOut")
this.container.prevObject.find(".giftcard-redeem__terms").velocity("transition.fadeOut")
this.subview(this.currentCard).$el.velocity("transition.slideDownOut",{"duration":300,"complete":function(e){return function(){t.$el.velocity("transition.slideUpIn",{"duration":300})
e.container.prevObject.find("h1").velocity("transition.fadeIn")
return e.container.prevObject.find(".giftcard-redeem__terms").velocity("transition.fadeIn")}}(this)})
return this.currentCard=e}
return n}(Banana.views.components.CardView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/redeem"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="giftcard-redeem__content-container">\n  <h1>Ready for your gift?</h1>\n  <div class="giftcard-redeem__form-wrapper"></div>\n  <a href="javascript:;" class="giftcard-redeem__terms js-open-terms">Gift card terms and conditions</a>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/redeem"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/giftcards/terms"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="giftcard-terms">\n  <h3>Gift Card Terms</h3>\n\n  <p>\n    Everlane gift cards may only be purchased by residents of the USA or Canada,\n    and can only be redeemed online at everlane.com. No service fees or expiration\n    dates apply. Upon redemption, the holder will receive store credit on his or her\n    Everlane account for the amount stated on the gift card. Cards and credits cannot\n    be redeemed for cash except when required by law. Contact Everlane at support@everlane.com\n    to request replacement of a lost or stolen card or for any questions.\n  </p>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/giftcards/terms"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.giftcards")
Banana.views.giftcards.RedeemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/giftcards/redeem"
n.prototype.className="giftcard-redeem"
n.prototype.region="post_content"
n.prototype.events={"click .js-open-terms":"showTerms"}
n.prototype.initialize=function(e){this.token=e.token
return E.sub(E.Event.GiftCards.REDEEMED,function(e){return function(){return e.giftcardRedeemed}}(this))}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
this.chromeHeight=$(".chrome-header").outerHeight()
this.setViewportHeight()
$(window).on("load",$.proxy(this.setViewportHeight,this))
$(window).on("resize.giftcard.redeem",$.proxy(this.setViewportHeight,this))
e=E.session.isSignedIn()?new Banana.views.giftcards.GiftCodeView({"container":this.$(".giftcard-redeem__form-wrapper"),"token":this.token}):new Banana.views.giftcards.LoginView({"container":this.$(".giftcard-redeem__form-wrapper")})
return this.subview("giftcard-form-container",e)}
n.prototype.remove=function(){$(window).off("resize.giftcard.redeem")
return n.__super__.remove.apply(this,arguments)}
n.prototype.setViewportHeight=function(){return this.$el.css({"height":$(document.body).outerHeight()-this.chromeHeight})}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.isSignedIn=E.session.isSignedIn()
return e}
n.prototype.showTerms=function(e){e.preventDefault()
return new Banana.views.components.ModalView({"view":{"class":Banana.View,"template":"banana/templates/giftcards/terms"}})}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.GiftcardsController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.redeem=function(e){var t
this.adjustTitle("Redeem Gift Card")
t=e.token
return this.view=new Banana.views.giftcards.RedeemView({"token":t})}
return n}(Banana.Controller)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.ContentPage=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.contentPage")
Banana.views.contentPage.BaseView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="content-page"
n.prototype.initialize=function(e){this.$el.attr({"id":"content_page_"+e.model.get("id")})
return n.__super__.initialize.apply(this,arguments)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/content_page/html_page"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l="<style>\n  "
r=(i=null!=(i=t.compiled_styles||(null!=e?e.compiled_styles:e))?i:s,typeof i===o?i.call(e,{"name":"compiled_styles","hash":{},"data":a}):i)
null!=r&&(l+=r)
l+="\n</style>\n\n"
r=(i=null!=(i=t.compiled_content||(null!=e?e.compiled_content:e))?i:s,typeof i===o?i.call(e,{"name":"compiled_content","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l},"useData":!0})
return this.HandlebarsTemplates["banana/templates/content_page/html_page"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.contentPage")
Banana.views.contentPage.HtmlPageView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/content_page/html_page"
return n}(Banana.views.contentPage.BaseView)}).call(this);(function(){E.ns("Banana.views.contentPage")
Banana.views.contentPage.ViewFactory={"getInstance":function(e){var t,n
null==e&&(e={})
if(null==e.model)throw"getInstance must be passed a model option"
t=e.model.get("content_type")
switch(t){case"md":return new Banana.views.contentPage.HtmlPageView(e)
default:if(n=Banana.views.contentPage[t])return new n(e)
throw"Unsupported content type: "+t}}}}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.home.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="post_content"
n.prototype.className="home__index"
n.prototype.template=function(){return""}
n.prototype.events={"click a":"logButtonClicks"}
n.prototype.attach=function(){var e,t
n.__super__.attach.apply(this,arguments)
e=new Banana.models.ContentPage(this.model.get("content_page"))
t=Banana.views.contentPage.ViewFactory.getInstance({"model":e,"container":this.el})
return this.subview("contentPageView",t)}
n.prototype.logButtonClicks=function(e){var t
t=this.$el.find("a").index(e.currentTarget)
return E.pub(E.Event.Home.BUTTON_CLICK,{"homepage":this.containerName,"buttonPosition":t})}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.HomeController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.index=function(){var e
this.adjustTitle("Modern Basics. Radical Transparency.")
E.pub(E.Event.Home.PAGE_VIEW)
e=E.currentQuery("homepage_id")?this._getHomepage({"id":parseInt(E.currentQuery("homepage_id"))}):E.delivery.openForBusiness()?this._getHomepage(E.delivery.getApplicableLocation()===E.delivery.Locations.SF?{"context":"sf-everlane-now"}:{"context":"nyc-everlane-now"}):$.cookie("o_s")?this._getOpenStudio(E.chaplinCollections.homepages.where({"context":"open-studio"})):E.session.isSignedIn()?this._getHomepage("male"===E.session.getCurrentUser().gender?{"context":"logged-in-male"}:{"context":"logged-in-female"}):E.session.getReferredClick()?($("header, footer, .fixed-navigation").hide(),this._getHomepage({"context":"referral"})):this._getHomepage({"context":"logged-out"})
return this.view=new Banana.views.home.IndexView({"model":e})}
n.prototype.homeTryOn=function(){this.adjustTitle("Home Try-On")
$("footer, .fixed-navigation").hide()
return new E.views.hto.IndexView}
n.prototype._getOpenStudio=function(e){var t
if($.cookie("os_location")){t=_.filter(e,function(e){var t
t=e.get("name").split("_")
return t[0]===$.cookie("os_location")})
return t[0]}return this._getHomepage({"context":"open-studio"})}
n.prototype._getHomepage=function(e){return E.chaplinCollections.homepages.where(e)[0]}
return n}(Banana.Controller)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.BaseView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="main"
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["base/hto/templates/pages/checkout/create_account_page"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="login-signin-content">\n  <header>\n    <h2 style="margin-bottom: 0">Create an Everlane account.</h2>\n    <a href="javascript:;" class="skip log-in-link">Already have an account? Log in.</a>\n  </header>\n  <form class="user-form">\n\n    <div class="form_fields clearfix">\n      <div class="field">\n        <label for="user_full_name" class="fancy-label">Full Name</label>\n        <input id="user_full_name" class="fancy-input form__full_name" type="text" name="user[full_name]">\n        <input type="hidden" class="form__first_name" name="user[first_name]">\n        <input type="hidden" class="form__last_name" name="user[last_name]">\n      </div>\n\n      <div class="field">\n        <label for="user_email" class="fancy-label">Email Address</label>\n        <input id="user_email" class="fancy-input" type="text" name="user[email]">\n      </div>\n\n      <div class="field">\n        <label for="user_password" class="fancy-label">Password</label>\n        <input id="user_password" class="fancy-input" type="password" name="user[password]">\n      </div>\n\n      <div class="field">\n        <button type="submit" style="width: 100%" class="default flat-button">Create Account</button>\n      </div>\n\n    </div>\n  </form>\n\n  <form class="hidden login-form">\n    <div class="errors"></div>\n\n    <div class="form_fields clearfix">\n\n      <div class="field half_width">\n        <label for="sign_in_email" class="fancy-label">Email</label>\n        <input id="sign_in_email"  class="fancy-input" type="email" name="email">\n      </div>\n\n      <div class="field half_width">\n        <label for="sign_in_password" class="fancy-label">Password</label>\n        <input if="sign_in_password"  class="fancy-input" type="password" name="password">\n      </div>\n\n      <div class="field">\n        <button type="submit" style="width: 100%" class="default flat-button">Log In</button>\n      </div>\n\n      <span class="hyphenated-heading">or</span>\n\n      <a href="javascript:;" class="fancy-button blue facebook">\n        <img alt="Fb f" src="//d1zvwc1xfc8qp0.cloudfront.net/assets/fb_f-822a8f283bc1777f49dc0329a64dba0d.png" /> Log In with Facebook\n      </a>\n\n    </div>\n  </form>\n</div>\n\n<div class="previous-order-message hidden">\n  <p>\n    Sorry, Home Try-On is only available for new customers.\n  <p>\n\n  <a href="https://www.everlane.com" class="continue-to-everlane">Shop Everlane.com</a>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["base/hto/templates/pages/checkout/create_account_page"]}).call(this);(function(){var e=function(e,t){return function(){return e.apply(t,arguments)}},t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
E.ns("Banana.views.hto")
Banana.views.hto.AccountView=function(t){function a(){this._makeFbPostBackCall=e(this._makeFbPostBackCall,this)
return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.className="hto__page hto__page--account"
a.prototype.template="base/hto/templates/pages/checkout/create_account_page"
a.prototype.events={'click .user-form button[type="submit"]':"createAccount",'click .login-form button[type="submit"]':"logIn","click .facebook":"facebookLogIn","click .log-in-link":"toggleForms"}
a.prototype.validations={"#user_full_name":{"presence":!0},"#user_email, #sign_in_email":{"presence":!0,"pattern":"email"},"#user_password, #sign_in_password":{"presence":!0,"minLength":3}}
a.prototype.attach=function(){return a.__super__.attach.apply(this,arguments)}
a.prototype["continue"]=function(){Chaplin.utils.redirectTo({"url":"/hto/gender"})
return this.dispose()}
a.prototype.toggleForms=function(e){this.$(".user-form, .login-form").toggleClass("hidden")
return $(e.currentTarget).text(this.$(".user-form").hasClass("hidden")?"Create an Account.":"Already have an account? Log in.")}
a.prototype.createAccount=function(e){var t
e.preventDefault()
if(this.validate(".user-form")){t=new E.lib.ButtonProgressBar({"button":$(e.currentTarget),"loadingText":"Logging In..."})
return $.ajax({"url":"/api/users","method":"POST","data":this.getFormValues(".user-form"),"success":function(e){return function(n){t.stop()
E.session.signIn(n)
return e.bootstrapOrders()}}(this),"error":function(e){return function(n){var a,r
t.stop()
a=n.responseJSON.data.email
r=n.responseJSON.data.password
a&&e.addError(e.$("label[for='user_email']"),"Email "+a)
return r?e.addError(e.$("label[for='user_password']"),"Password "+r):void 0}}(this)})}}
a.prototype.logIn=function(e){var t,n,a,r
e.preventDefault()
if(this.validate(".login-form")){n=this.$(".login-form")
t=n.find('input[type="email"]').val()
a=n.find('input[type="password"]').val()
r=new E.lib.ButtonProgressBar({"button":$(e.currentTarget),"loadingText":"Logging In..."})
return $.ajax({"url":"/login","method":"POST","data":{"is_hto_login":!0,"session":{"email":t,"password":a}},"success":function(e){return function(t){e.user=t
r.stop()
E.session.signIn(t)
return e.bootstrapOrders()}}(this),"error":function(e){return function(){r.stop()
return e.addError(e.$(".errors"),"We can&rsquo;t find an account with that email and password.")}}(this)})}}
a.prototype._makeFbPostBackCall=function(e){return $.ajax({"url":"/users","method":"POST","data":_.extend({},e,{"is_hto_login":!0}),"success":function(e){return function(t){E.session.signIn(t)
e.user=t
return e.bootstrapOrders()}}(this),"error":function(e){return function(){progressBar.stop()
return e.$(".errors").html("Something went wrong, please try again")}}(this)})}
a.prototype.facebookLogIn=function(e){var t,n
t=$(e.currentTarget)
n=new E.lib.ButtonProgressBar({"button":t,"loadingText":"Logging In..."})
return FB.getLoginStatus(function(e){return function(t){return"connected"===t.status?e._makeFbPostBackCall(t.authResponse):FB.login(function(t){return e._makeFbPostBackCall(t.authResponse)},{"scope":"email,user_birthday,user_location"})}}(this))}
a.prototype.bootstrapOrders=function(){$.getJSON("/api/orders?current=true",function(){return function(e){E.currentOrder=new E.models.Order(_.findWhere(e.outstanding_orders,{"trial":!1}))
return E.trialOrder=new E.models.Order(_.findWhere(e.outstanding_orders,{"trial":!0}))}}(this))
return this["continue"]()}
return a}(Banana.views.hto.BaseView)
E.mix(Banana.views.hto.AccountView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/hto/cart"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<header>\n  <p class="title">\n    Home Try-On Package\n  </p>\n</header>\n<div class="product-list">\n  <a href="/hto/checkout/shipping" class="flat-button uber continue-to-checkout" style="display: none;">Checkout</a>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/hto/cart"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.hto.CartView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/hto/cart"
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
return this.listenTo(E.trialOrder.get("lineItems"),"add change:active remove",this.maybeShowCheckoutButton)}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
this.maybeShowCheckoutButton()
this.lineItems=new Banana.views.hto.LineItemListView({"collection":E.trialOrder.get("lineItems"),"container":this.$(".product-list"),"containerMethod":"prepend"})
return this.subview("lineItems",this.lineItems)}
n.prototype.maybeShowCheckoutButton=function(){return E.trialOrder.get("lineItems").activeLineItems().length>=5?this.$(".continue-to-checkout").show().css("display","block"):this.$(".continue-to-checkout").hide()}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.CheckoutConfirmView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--checkout-confirm"
n.prototype.template="base/hto/templates/pages/checkout/confirmation_page"
n.prototype.events={"click a.checkout":"checkout"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.address=E.hto.get("address")
e.order=E.trialOrder.toJSON()
e.lineItems=E.trialOrder.get("lineItems").toJSON()
e.creditCard=E.hto.get("payment")
return e}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$(".submit-section").find("a.checkout").is(":offscreen")?this.$(".right").find("a.checkout").show():void 0}
n.prototype.checkout=function(){this.showLoadingIndicator()
return E.trialOrder.complete(function(e){return function(){e.stopLoadingIndicator()
return Chaplin.utils.redirectTo({"url":"/hto/thanks"})}}(this))}
n.prototype.showLoadingIndicator=function(){this.buttonLoadingIndicators=[]
return this.$("a.checkout").each(function(e){return function(t,n){var a
a=new E.lib.ButtonProgressBar({"button":$(n)})
return e.buttonLoadingIndicators.push(a)}}(this))}
n.prototype.stopLoadingIndicator=function(){return _.each(this.buttonLoadingIndicators,function(){return function(e){return e.stop()}}(this))}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["base/hto/templates/pages/checkout/credit_card_page"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e){var t,n=this.lambda,a=this.escapeExpression
return'<header>\n  <h2>\n    Don&rsquo;t worry, your 3-day Try-On is free.<br>\n    We&rsquo;ll only charge you for what you keep.\n  </h2>\n</header>\n\n\n<form accept-charset="UTF-8" class="credit_card" id="credit_card_form">\n    <div style="margin:0;padding:0;display:inline">\n        <input name="utf8" type="hidden" value="&#x2713;" />\n    </div>\n\n    <div class="status-message"></div>\n\n    <div class="form_fields clearfix">\n        <div class="field half_width">\n            <label for="credit_card_billing_address_full_name" class="fancy-form__label">Full Name</label>\n            <input class="text required cc_full_name fancy-form__input" id="credit_card_billing_address_full_name" size="30" type="text" value="'+a(n(null!=(t=null!=e?e.user:e)?t.first_name:t,e))+" "+a(n(null!=(t=null!=e?e.user:e)?t.last_name:t,e))+'" />\n            <input class="first_name" id="credit_card_billing_address_first_name" name="credit_card[billing_address][first_name]" type="hidden" value="'+a(n(null!=(t=null!=e?e.user:e)?t.first_name:t,e))+'" />\n            <input class="last_name" id="credit_card_billing_address_last_name" name="credit_card[billing_address][last_name]" type="hidden" value="'+a(n(null!=(t=null!=e?e.user:e)?t.last_name:t,e))+'" />\n        </div>\n\n        <div class="field one_third_width right">\n            <label for="credit_card_billing_address_country_code_alpha2" class="fancy-form__label">Country</label>\n            <select class="country fancy-select" id="credit_card_billing_address_country_code_alpha2" name="credit_card[billing_address][country_code_alpha2]" style="width: 100%;">\n                <option value="US">USA</option>\n                <option value="CA">Canada</option>\n            </select>\n        </div>\n    </div>\n\n    <div class="form_fields secure-zone clearfix">\n      <div class="field two_thirds_width" id="cc_num_container">\n          <label for="credit_card_number_display" class="fancy-form__label">Credit Card Number</label>\n          <input autocompletetype="cc-number" class="text required cc_number fancy-form__input" id="credit_card_number_display" size="30" type="text" />\n          <input class="cc_number_hidden" id="credit_card_number" name="credit_card[number]" data-encrypted-name="credit_card[number]" type="hidden" />\n          <div id="cc_icons"></div>\n      </div>\n\n      <div class="field one_third_width">\n          <label for="credit_card_cvv" class="fancy-form__label">\n              Security Code\n              <span id="cvv-info">?</span>\n\n          </label>\n\n          <input autocompletetype="cc-csc" class="text required cc_cvv fancy-form__input" id="credit_card_cvv" name="credit_card[cvv]" size="30" data-encrypted-name="credit_card[cvv]" type="password" />\n      </div>\n\n      <div class="field one_third_width">\n          <label for="credit_card_expiration_display" class="fancy-form__label">Expiration Date</label>\n          <input autocompletetype="cc-exp" class="text required cc_expiry fancy-form__input" id="credit_card_expiration_display" placeholder="MM / YY" size="30" type="text" />\n          <input class="cc_expiry_hidden" id="credit_card_expiration_date" name="credit_card[expiration_date]" data-encrypted-name="credit_card[expiration_date]" type="hidden" />\n      </div>\n\n      <div class="field one_third_width right zip-code-container">\n          <label for="credit_card_billing_address_postal_code" class="fancy-form__label">Zip Code</label>\n          <input class="text cc_zip_code fancy-form__input" id="credit_card_billing_address_postal_code" maxlength="5" name="credit_card[billing_address][postal_code]" size="5" type="text" />\n      </div>\n    </div>\n\n    <div class="field clearfix">\n      <button type="submit" name="commit" class="default flat-button">Continue \u279e</button>\n\n      <p style="text-align: center; margin-top: 15px">\n          For security purposes, there will be a $1 authorization charge.<br>\n          This will be refunded once your card has been verified.\n      </p>\n    </div>\n\n\n</form>'},"useData":!0})
return this.HandlebarsTemplates["base/hto/templates/pages/checkout/credit_card_page"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.CheckoutPaymentView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--checkout-payment"
n.prototype.template="base/hto/templates/pages/checkout/credit_card_page"
n.prototype.events={"submit form":"onSubmit"}
n.prototype.validations={"#credit_card_billing_address_full_name":{"presence":!0},"#credit_card_number_display":{"ccNum":!0},"#credit_card_cvv":{"cvc":"#credit_card_number_display"},"#credit_card_expiration_display":{"cardExpiry":!0},"#credit_card_billing_address_postal_code":{"pattern":"us_postal_code"}}
n.prototype.attach=function(){return n.__super__.attach.apply(this,arguments)}
n.prototype.onSubmit=function(e){var t,n
e.preventDefault()
t=$(e.currentTarget)
E.mixins.CreditCardForm.populateHiddenFields(t)
if(this.validate()){n=new E.lib.ButtonProgressBar({"button":this.$("button")})
return E.mixins.CreditCardForm.makeRequest(t,{"success":function(e,t){n.stop()
E.hto.set("payment",t)
return Chaplin.utils.redirectTo({"url":"/hto/checkout/confirm"})},"error":function(){n.stop()
return alert("Something went wrong, please try again.")}})}}
return n}(Banana.views.hto.BaseView)
E.mix(Banana.views.hto.CheckoutPaymentView,E.mixins.Form)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.CheckoutShippingView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--checkout-shipping"
n.prototype.template="base/hto/templates/pages/checkout/create_address_page"
n.prototype.events={"submit form":"createAddress"}
n.prototype.validations={"#email":{"presence":!0,"pattern":"email"},"#address_full_name":{"presence":!0},"#address_street_address":{"presence":!0},"#address_city":{"presence":!0},"#address_state_id":{"presence":!0},"#address_postal_code":{"presence":!0,"pattern":"us_postal_code"}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.user=E.session.getCurrentUser()
e.address=E.hto.get("address")
return e}
n.prototype.attach=function(){return n.__super__.attach.apply(this,arguments)}
n.prototype.createAddress=function(e){var t,n
e.preventDefault()
if(this.validate()){t=$(e.currentTarget).find(".flat-button")
n=new E.lib.ButtonProgressBar({"button":t,"loadingText":"Loading..."})
return $.ajax({"url":"/api/addresses","method":"POST","data":this.getFormValues(),"success":function(){return function(e){n.stop()
E.hto.set("address",e.data)
return Chaplin.utils.redirectTo({"url":"/hto/checkout/payment"})}}(this),"error":function(){return function(){n.stop()
return alert("Something went wrong when creating your address.")}}(this)})}}
return n}(Banana.views.hto.BaseView)
E.mix(Banana.views.hto.CheckoutShippingView,E.mixins.Form)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.GenderView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--gender"
n.prototype.template="base/hto/templates/pages/intro/gender_selection_page"
n.prototype.events={"click a[data-value]":"setGender"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.choices=["Women's","Men's"]
return e}
n.prototype.setGender=function(e){var t
t=$(e.currentTarget).data("value")
return E.hto.set("gender",t)}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/hto/header"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="logo">Everlane</div>\n<div class="back hidden">Back</div>\n\n<div class="hto-progress">\n  <div class="hto-progress-inner"></div>\n</div>\n\n<div class="hto__cart hidden"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/hto/header"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.hto.HeaderView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__main-header"
n.prototype.template="banana/templates/hto/header"
n.prototype.region="header"
n.prototype.events={"click .back":function(){return history.back(-1)}}
n.prototype.changeSize=function(e){return setTimeout(function(t){return function(){return t.$(".hto-progress-inner").css("width",""+e+"%")}}(this),500)}
n.prototype.toggleBack=function(e){return this.$(".back").velocity("stop").velocity(e?"fadeIn":"fadeOut")}
n.prototype.toggleCart=function(e){return this.$(".hto__cart").velocity("stop").velocity(e?"fadeIn":"fadeOut")}
n.prototype.createCart=function(){if(this.subview("cart"))return this.subview("cart")
this.cartView=new Banana.views.hto.CartView({"container":this.$(".hto__cart"),"collection":E.trialOrder.get("lineItems")})
return this.subview("cart",this.cartView)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/hto/help"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<h2>How it Works</h2>\n\n<h3>Can I use Home Try-On if I\u2019m an existing customer?</h3>\n<p>\n  At the moment, this service is for first-time customers only.\n</p>\n\n\n<h3>How many styles can I try on?</h3>\n<p>\n  Your Home Try-On package must include a minimum of 5 items, with the maximum being 15.\n</p>\n\n\n<h3>Can I add more than one size?</h3>\n<p>\n  The focus of Home Try-On is for customers to try out styles rather than sizes.\n  Feel free to email <a href="mailto:support@everlane.com">support@everlane.com</a>\n  to ask about fit or exchange sizes.\n</p>\n\n\n<h3>What if I don\u2019t see the style I want to try on?</h3>\n<p>\n  For the moment, Home Try-On only has a limited selection of our products.\n</p>\n\n\n\n<h2>Your Home Try-On</h2>\n\n\n<h3>When will my Home Try-On package arrive?</h3>\n<p>\n  Our Home Try-On package ships in 1-2 days after you place the order.\n  We\u2019ll notify you when it leaves our warehouse, after that, orders typically take 2-3 business days to arrive.\n  Let <a href="mailto:support@everlane.com">support@everlane.com</a>\n  know if your package hasn\u2019t turned up after a week.\n</p>\n\n\n<h3>Can I wear my Try-On items out of the house?</h3>\n<p>\n  Please do not wear your Home Try-On items out of the house.\n  You\u2019ll be charged for any items that come back ripped or stained.\n</p>\n\n\n<h3>When will I be charged for the items I decide to keep?</h3>\n<p>\n  Once your package arrives, you have 3 business days to start your return.\n  Your credit card is charged for unreturned items on the fourth business day.\n</p>\n\n\n<h3>What if I\u2019m late sending back my package?</h3>\n<p>\n  Contact our Customer Experience team at\n  <a href="mailto:support@everlane.com">support@everlane.com</a>,\n  and let them know you\u2019re running late.\n</p>\n\n\n<h3>What if an item is damaged or defective?</h3>\n<p>\n  Please give our Customer Experience team a call. We\u2019ll take care of you!\n</p>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/hto/help"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.HelpView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--help"
n.prototype.template="banana/templates/hto/help"
n.prototype.events={"click h3":"questionClicked"}
n.prototype.questionClicked=function(e){var t
t=$(e.currentTarget).next("p")
return t.toggle()}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["base/hto/templates/pages/intro/intro_page"]=Handlebars.template({"1":function(){return'    <p class="thank-you-message">\n      Thanks for trying Everlane Home Try-On.<br>\n      <a href="/orders">View the status of your order</a>.<br>\n      Have questions? <a href="/help">Contact our customer support</a>.\n    </p>\n'},"3":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.hasPurchased:e,{"name":"if","hash":{},"fn":this.program(4,a),"inverse":this.program(6,a),"data":a})
null!=r&&(i+=r)
return i},"4":function(){return'      <p class="thank-you-message">\n        Sorry, Home Try-On is only available for new customers.<br>\n        <a href="https://www.everlane.com" class="continue-to-everlane">Shop Everlane.com</a>\n      </p>\n'},"6":function(){return'      <a href="/hto/account" class="default flat-button next">\n        Get Started \u279e\n      </a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=t.helperMissing,o=this.escapeExpression,s='<header>\n  <h1>Introducing</h1>\n  <h2>Home Try-On</h2>\n\n  <p>\n    Try our products from the comfort of your own home.<br>\n    Free shipping and free returns.\n  </p>\n</header>\n\n<div class="good-stuff-container" style="display: none;">\n  <ul class="clearfix">\n\n    <li>\n      <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"hto_icon_1.png",{"name":"staticImageUrl","hash":{},"data":a}))+'" height="100">\n      <b>Pick Styles</b><br>\n      <span>Choose up to 15 different<br>styles from our handpicked selection.</span>\n    </li>\n\n    <li>\n      <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"hto_icon_2.png",{"name":"staticImageUrl","hash":{},"data":a}))+'" height="100">\n      <b>Try-on at Home</b><br>\n      <span>Try them out for 3 business days in the <br> comfort of your home.</span>\n    </li>\n\n    <li>\n      <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"hto_icon_3.png",{"name":"staticImageUrl","hash":{},"data":a}))+'" height="100">\n      <b>Keep What You Like</b><br>\n      <span>Pay for what you keep, <br> and send the rest back for free.</span>\n    </li>\n\n  </ul>\n\n'
r=t["if"].call(e,null!=e?e.hasActiveHto:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(s+=r)
return s+"\n</div>"},"useData":!0})
return this.HandlebarsTemplates["base/hto/templates/pages/intro/intro_page"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--intro"
n.prototype.template="base/hto/templates/pages/intro/intro_page"
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.hasPurchased=E.session.isSignedIn()&&E.session.getCurrentUser().has_purchased
e.hasActiveHto=E.session.isSignedIn()&&E.session.getCurrentUser().has_active_hto
return e}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$(".good-stuff-container").velocity("transition.slideUpIn",{"duration":300,"delay":300})}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.LayoutView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template=function(){return'<div id="hto__header"></div> <div id="hto__main"></div>'}
n.prototype.regions={"main":"#hto__main","header":"#hto__header"}
n.prototype.render=function(){$(".chrome-header").hide()
$(".fixed-navigation").hide()
$("footer").css({"visibility":"hidden"})
return n.__super__.render.apply(this,arguments)}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/hto/line_item"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=t.helperMissing,o=this.escapeExpression,s="function"
return'<div class="line-item">\n  <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,null!=e?e.imagePath:e,{"name":"staticImageUrl","hash":{"size":"100"},"data":a}))+'">\n  <div>\n    <div class="remove">\n      <a href="javascript:;"><span>Remove item</span></a>\n    </div>\n    <p class="name">'+o((r=null!=(r=t.title||(null!=e?e.title:e))?r:i,typeof r===s?r.call(e,{"name":"title","hash":{},"data":a}):r))+'</p>\n    <p class="details">\n      '+o((r=null!=(r=t.size||(null!=e?e.size:e))?r:i,typeof r===s?r.call(e,{"name":"size","hash":{},"data":a}):r))+"\n    </p>\n  </div>\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/hto/line_item"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.hto.LineItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/hto/line_item"
n.prototype.className="hto__item"
n.prototype.events={"click .remove":"onRemove"}
n.prototype.onRemove=function(){return this.model.destroy()}
return n}(Banana.View)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.hto.LineItemListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.itemView=Banana.views.hto.LineItemView
n.prototype.template=function(){return""}
n.prototype.className="hto__item-list"
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
return this.listenTo(E.trialOrder.get("lineItems"),"change:active",function(e){return function(t,n,a){return n?e.itemAdded(t,E.trialOrder.get("lineItems"),a):e.itemRemoved(t)}}(this))}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/hto/no_items"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<p>\n  That&rsquo;s all the products we are currently offering for Home Try-On.<br>\n  However, we will always be adding new products.\n<p>\n\n<a href="/products/0" class="flat-button start-over">Start Over</a>\n<span class="hyphenated-heading">or</span>\n<a href="https://www.everlane.com" class="continue-to-everlane">Shop Everlane.com</a>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/hto/no_items"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.NoItemsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--no-items"
n.prototype.template="banana/templates/hto/no_items"
return n}(Banana.views.hto.BaseView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.ProductView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--product"
n.prototype.template="base/hto/templates/pages/product_choice_page"
n.prototype.events={"click li":"onClick"}
n.prototype.initialize=function(e){this.products=e.products
this.tagline=e.tagline
this.currentIndex=e.currentIndex
return this.listenTo(E.trialOrder.get("lineItems"),"change:active add destroy",function(e){return function(t){var n,a
a=t.get("product_id")
n=$("li[data-id="+a+"]")
n.toggleClass("selected",t.get("active"))
return e.toggleCheckbox(n,t.get("active"))}}(this))}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.products=_.map(this.products,function(e){return e.toJSON()})
e.tagline=this.tagline
e.productName=_.first(this.products).get("name")
e.nextIndex=this.currentIndex+1
return e}
n.prototype.toggleCheckbox=function(e,t){null==t&&(t=!0)
e.find(".checkbox").text(t?"\u2713":"")
return this.$("li.selected").length>0?this.$(".next").removeClass("inactive"):this.$(".next").addClass("inactive")}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
this.$(".products").css({"width":276*this.products.length})
e=E.trialOrder.get("lineItems").activeLineItems()
return _.each(e,function(e){return function(t){var n,a
a=t.get("product_id")
n=e.$("li[data-id="+a+"]")
n.addClass("selected")
return e.toggleCheckbox(n,!0)}}(this))}
n.prototype.onClick=function(e){var t,n
t=$(e.currentTarget)
n=t.hasClass("selected")
return n?this.revert(t):this.select(t)}
n.prototype.revert=function(e){var t,n,a
t=e.data("value")
n=E.hto.products.findWhere({"permalink":t})
a=n.getVariantOfSize(E.hto.get("size"))
return E.trialOrder.get("lineItems").findWhere({"variant_id":a.variant_id}).destroy()}
n.prototype.select=function(e){var t,n,a
t=e.data("value")
n=E.hto.products.findWhere({"permalink":t})
a=n.getVariantOfSize(E.hto.get("size"))
return E.trialOrder.get("lineItems").addLineItem(1,{"id":a.variant_id,"size":a.size,"sku":a.sku},_.omit(n.attributes,"variants"),{"add":!1})}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.SizeView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--size"
n.prototype.template="base/hto/templates/pages/intro/shirt_size_selection_page"
n.prototype.events={"click .selection-box":"selectSize"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.choices="male"===E.hto.get("gender")?["XS","S","M","L","XL"]:["XS","S","M","L"]
return e}
n.prototype.selectSize=function(e){var t
this.$(".selected").removeClass("selected")
this.$(".next.inactive").removeClass("inactive")
t=$(e.currentTarget)
t.addClass("selected")
return E.hto.set("size",t.data("value"))}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["base/hto/templates/pages/checkout/thank_you_page"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<header>\n  <h2>Thanks for trying on Everlane.</h2>\n</header>\n\n<p>\n  We hope you enjoy. Feel free to send us an email at\n  <a href="mailto:support@everlane.com">support@everlane.com</a><br>\n  if you have any questions or just want to give feedback.\n</p>\n\n<hr>\n\n<header>\n  <h3>\n    Tell your friends about our Home Try-On.\n  </h3>\n  <p>\n    Earn $25 credit when they make a purchase.\n  </p>\n</header>\n\n<div class="rewards-container">\n\n  <div class="link-container">\n    <input readonly value="'+s((r=null!=(r=t.shareURL||(null!=e?e.shareURL:e))?r:o,typeof r===i?r.call(e,{"name":"shareURL","hash":{},"data":a}):r))+'" autocomplete="off" class="share-url fancy-input" title="copy me" name="share">\n    <a class="fancy-button twitter" href="https://twitter.com/intent/tweet?'+s((r=null!=(r=t.twitterShareLink||(null!=e?e.twitterShareLink:e))?r:o,typeof r===i?r.call(e,{"name":"twitterShareLink","hash":{},"data":a}):r))+'"><img src="//everlane-2.imgix.net/static/referral_tw.png?q=65&dpr=1.100000023841858"> Tweet</a>\n    <a class="fancy-button facebook"><img src="//everlane.imgix.net/static/referral_fb.png?q=65&dpr=1.100000023841858"> Share</a>\n  </div>\n\n</div>\n\n<a href="https://www.everlane.com/" class="go-to-everlane">Visit Everlane.com</a>'},"useData":!0})
return this.HandlebarsTemplates["base/hto/templates/pages/checkout/thank_you_page"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
E.ns("Banana.views.hto")
Banana.views.hto.ThanksView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="hto__page hto__page--thanks"
n.prototype.template="base/hto/templates/pages/checkout/thank_you_page"
n.prototype.events={"click .facebook":"onFacebookShare"}
n.prototype.initialize=function(){var e
return this.shareURL=(null!=(e=E.session.getCurrentUser())?e.invite_url:void 0)||"https://www.everlane.com"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.shareURL=this.shareURL
e.twitterShareLink=$.param({"url":this.shareURL,"text":"Check out Everlane - high quality basics without traditional retail markups."})
return e}
n.prototype.onFacebookShare=function(){return E.lib.FB.share({"link":this.shareURL,"name":"Everlane","caption":"Try Everlane","description":"Luxury basics without traditional retail markups. "+this.link,"actions":[{"name":"Join Everlane","link":this.shareURL}],"display":"popup"},function(e){if(e){E.lib.FlashMessage("Shared")
return E.pub(E.Event.Invite.SHARE,{"network":"facebook","context":"rewards"})}})}
return n}(Banana.views.hto.BaseView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.HomeTryOnController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.beforeAction=function(){n.__super__.beforeAction.apply(this,arguments)
this.reuse("hto.layout",Banana.views.hto.LayoutView)
return this.reuse("hto.header",Banana.views.hto.HeaderView)}
n.prototype.index=function(){this.adjustTitle("Home Try-On")
this._clearCart()
this.view=new Banana.views.hto.IndexView
this._progress(10)
this._showChrome({"back":!1,"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"index"})}
n.prototype.account=function(){if(E.session.isSignedIn())return this._routeTo("/hto/gender")
this.adjustTitle("Home Try-On")
this._clearCart()
this.view=new Banana.views.hto.AccountView
this._progress(18)
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"account"})}
n.prototype.gender=function(){if(!E.hto.isNewUser())return this._routeTo("/hto")
this.adjustTitle("Home Try-On")
this._clearCart()
this.view=new Banana.views.hto.GenderView
this._progress(25)
this._showChrome({"back":!1,"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"gender"})}
n.prototype.size=function(){if(!E.hto.isNewUser())return this._routeTo("/hto")
this.adjustTitle("Home Try-On")
this._clearCart()
this.view=new Banana.views.hto.SizeView
this._progress(30)
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"size"})}
n.prototype.product=function(e){var t,n,a,r,i,o,s,l
if(!E.hto.isNewUser())return this._routeTo("/hto")
if(null==E.hto.get("gender"))return this._routeTo("/hto/gender")
if(null==E.hto.get("size"))return this._routeTo("/hto/size")
if(!E.hto.hasCartSpace())return this._routeTo("/hto/checkout/shipping")
this.adjustTitle("Home Try-On")
r=parseInt(e.index)
a=E.hto.get("gender")
s=E.hto.get("size")
l=E.hto.getProductsAndCategories(a,s),o=l[0],t=l[1]
if(!(r<t.length))return this._routeTo("/hto/checkout/shipping")
n=t[r]
i=o.findByPermalinks(n.permalinks,{"limit":3,"size":s})
this.view=new Banana.views.hto.ProductView({"products":i,"tagline":n.tagline,"currentIndex":r})
this._progress(35+3*r)
this._showChrome()
this._showCart()
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"product"})}
n.prototype.checkoutShipping=function(){if(!E.hto.hasItemsInCart())return this._routeTo("/hto/no-items")
this.view=new Banana.views.hto.CheckoutShippingView
this._progress(75)
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"shipping"})}
n.prototype.checkoutPayment=function(){if(!E.hto.hasItemsInCart())return this._routeTo("/hto")
if(null==E.hto.get("address"))return this._routeTo("/hto/checkout/shipping")
this.view=new Banana.views.hto.CheckoutPaymentView
this._progress(85)
this._showChrome()
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"payment"})}
n.prototype.checkoutConfirm=function(){if(!E.hto.hasItemsInCart())return this._routeTo("/hto")
if(null==E.hto.get("payment"))return this._routeTo("/hto/checkout/payment")
if(null==E.hto.get("address"))return this._routeTo("/hto/checkout/shipping")
this.view=new Banana.views.hto.CheckoutConfirmView
this._progress(95)
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"confirm"})}
n.prototype.thanks=function(){this.view=new Banana.views.hto.ThanksView
this._progress(100)
this._showChrome({"back":!1,"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"thanks"})}
n.prototype.help=function(){this.view=new Banana.views.hto.HelpView
this._progress(0)
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"help"})}
n.prototype.noItems=function(){this.view=new Banana.views.hto.NoItemsView
this._progress(75)
this._showChrome({"cart":!1})
return E.pub(E.Event.HTO.PAGE_VIEW,{"page":"no-items"})}
n.prototype._progress=function(e){return this.reuse("hto.header",Banana.views.hto.HeaderView).changeSize(e)}
n.prototype._clearCart=function(){var e
e=E.trialOrder.get("lineItems")
if(e.deepCount()>0){e.reset([])
return e.sync()}}
n.prototype._routeTo=function(e){return this.redirectTo({"url":e,"replace":!0})}
n.prototype._showChrome=function(e){var t
null==e&&(e={})
null==e.back&&(e.back=!0)
null==e.cart&&(e.cart=!0)
t=this.reuse("hto.header",Banana.views.hto.HeaderView)
t.toggleBack(e.back)
return t.toggleCart(e.cart)}
n.prototype._showCart=function(){var e
e=this.reuse("hto.header",Banana.views.hto.HeaderView)
return e.createCart()}
return n}(Banana.Controller)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.LandingPageController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.showHeader=!1
n.prototype.showFooter=!1
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/open_studio/index_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i,o=t.helperMissing,s="function",l='    <div class="open-studio-container overlay" style="background-image: url(\''
r=(t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{},"data":a})
null!=r&&(l+=r)
l+="');\">\n      "
r=(i=null!=(i=t.html||(null!=e?e.html:e))?i:o,typeof i===s?i.call(e,{"name":"html","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+"\n    </div>\n"},"3":function(e,t,n,a){var r,i,o=t.helperMissing,s="function",l='    <div class="open-studio-container">\n      <div class="elastic-container">\n        <img src="'
r=(t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{},"data":a})
null!=r&&(l+=r)
l+='" />\n      </div>\n\n      <div class="content">\n        '
r=(i=null!=(i=t.html||(null!=e?e.html:e))?i:o,typeof i===s?i.call(e,{"name":"html","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+'\n        <div class="rsvp-container">\n          <div class="hidden rsvp-buttons">\n            <a href="javascript:;" class="fancy-button rsvp-button" data-status="yes">I\u2019ll Be There</a>\n          </div>\n          <p class="hidden yes-message"><em>Thanks for signing up!</em></p>\n        </div>\n      </div>\n    </div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c="<style>\n  "+l((i=null!=(i=t.css||(null!=e?e.css:e))?i:s,typeof i===o?i.call(e,{"name":"css","hash":{},"data":a}):i))+'\n</style>\n\n<div class="open-studio">\n'
r=t["if"].call(e,null!=e?e.overlay_text:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(c+=r)
return c+"</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/open_studio/index_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.open_studio.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/open_studio/index_view"
n.prototype.region="content"
n.prototype.events={"click .rsvp-button":"attemptToRsvp"}
n.prototype.initialize=function(){this.$rsvp=$('<a href="javascript:;" class="fancy-button rsvp-button" data-status="yes">I\u2019ll Be There</a>')
return this.$signedUp=$('<p class="yes-message"><em>Thanks for signing up!</em></p>')}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$(".rsvp-buttons").append("yes"===this.model.get("rsvp_status")?this.$signedUp:this.$rsvp)}
n.prototype.attemptToRsvp=function(e){var t
t=$(e.currentTarget).data("status")
if(E.session.isSignedIn()){this.model.rsvp(t)
return this.updateButtons(t)}return new Banana.views.components.ModalView({"view":Banana.views.users.LoginModalView})}
n.prototype.updateButtons=function(e){return"yes"===e?this.$(".rsvp-buttons .rsvp-button").velocity("fadeOut",{"complete":function(e){return function(){return e.$signedUp.hide().appendTo(e.$(".rsvp-buttons")).velocity("fadeIn")}}(this)}):void 0}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.OpenStudio=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.urlRoot="/api/open_studios"
n.prototype.idAttribute="permalink"
n.prototype.rsvp=function(e){return $.ajax({"type":"POST","data":{"rsvp":e},"url":"/api/open_studios/"+this.get("permalink")})}
return n}(Banana.Model)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.OpenStudiosController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.beforeAction=function(e){n.__super__.beforeAction.apply(this,arguments)
if(E.data.initial_open_studio)return this.model=new Banana.models.OpenStudio(E.data.initial_open_studio)
this.model=new Banana.models.OpenStudio({"permalink":e.permalink})
return this.model.fetchOnce()}
n.prototype.show=function(e,t,n){var a,r
this.adjustTitle("Open Studio");(null!=(a=n.query)?a.rsvp:void 0)&&this.model.set("status",null!=(r=n.query)?r.rsvp:void 0)
return this.view=new Banana.views.open_studio.IndexView({"model":this.model})}
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/orders/confirmation_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<link href=\'//api.tiles.mapbox.com/mapbox.js/v1.6.2/mapbox.css\' rel=\'stylesheet\' />\n\n<div id="thank-you-container" class="clearfix">\n  <h1 class="serif">Thanks for your order.</h1>\n\n  <p class="thank-you-text">\n    We hope you enjoy. Feel free to send us an email at <a href="mailto:support@everlane.com">support@everlane.com</a>\n    <br>\n    if you have any questions or just want to give feedback.\n  </p>\n\n  <p class="same-day-delivery-text">\n    Our friends at Postmates will deliver your order within the hour.<br>\n    Please email <a href=""></a> if you have any questions.<br>\n    You\'ll get a text when the courier is a few minutes away.\n  </p>\n</div>\n\n<div class="order-page column grid_12 confirmation">\n  <div class="orders-subview"></div>\n</div>\n\n<div id="confirmation-map" class="column grid_12">\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/orders/confirmation_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.orders.ConfirmationView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="content"
n.prototype.template="banana/templates/orders/confirmation_view"
n.prototype.mainClass="orders"
n.prototype.listen={"synced collection":"updateSupportCopy"}
n.prototype.regions={"orders":".orders-subview"}
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
this.model=new Banana.models.Order({"id":"last_purchased"})
this.collection=new Banana.Collection
this.collection.beginSync()
return this.model.fetch().then(function(e){return function(){var t
e.collection.add(e.model)
e.collection.finishSync()
t=new Banana.models.Address(e.model.get("shipping_address"))
$.getScript("//api.tiles.mapbox.com/mapbox.js/v1.6.1/mapbox.js",function(){return e.drawMap(e.stringifyAddress(t),"confirmation-map")})
return e.listenTo(e.model,"change:shipping_address",function(){t=new Banana.models.Address(e.model.get("shipping_address"))
return e.drawMap(e.stringifyAddress(t),"confirmation-map")})}}(this))}
n.prototype.stringifyAddress=function(e){var t,n,a,r,i
n=""
i=["street_address","city_line","country"]
for(a=0,r=i.length;r>a;a++){t=i[a]
n+=" "+e.get(t)}return n}
n.prototype.drawMap=function(e,t,n){var a,r
null==n&&(n=15)
if("undefined"!=typeof L&&null!==L?L.mapbox:void 0){r="everlane.hc5lao21"
a="//a.tiles.mapbox.com/v3/"+r+"/geocode/"
return $.ajax({"url":""+a+encodeURIComponent(e)+".json","dataType":"json","success":function(a){var i,o,s,l,c
if(a.results){c=[a.results[0][0].lat,a.results[0][0].lon],o=c[0],s=c[1]
i=L.mapbox.geocoder(r)
l=L.mapbox.map(t,r,{"scrollWheelZoom":!1,"zoomControl":!1}).setView([o,s],n)
return L.mapbox.featureLayer({"type":"Feature","geometry":{"type":"Point","coordinates":[s,o]},"properties":{"title":e,"marker-size":"large","marker-color":"#3B5998"}}).addTo(l)}}})}}
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.orders.ListView({"region":"orders","collection":this.collection,"show_returns":!1})
return this.subview("orders",e)}
n.prototype.updateSupportCopy=function(){var e
if(this.model.get("has_one_hour_delivery_items")){e=this.model.get("notification_address")
this.$(".same-day-delivery-text a").html(e)
this.$(".same-day-delivery-text a").attr("href","mailto:"+e)
return this.$(".same-day-delivery-text").velocity("fadeIn")}return this.$(".thank-you-text").velocity("fadeIn")}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.OrdersController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.show=function(e){this.adjustTitle("Thank You")
return this.view=new Banana.views.orders.ConfirmationView({"order_number":e.number})}
return n}(Banana.Controller)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/about/jobs_view"]=Handlebars.template({"1":function(e,t,n,a,r){var i,o,s="function",l=t.helperMissing,c=this.escapeExpression,p=this.lambda
return'  <li class="job">\n    <h4>'+c((o=null!=(o=t.text||(null!=e?e.text:e))?o:l,typeof o===s?o.call(e,{"name":"text","hash":{},"data":a}):o))+'</h4>\n    <span class="location">Location: '+c(p(null!=(i=null!=e?e.categories:e)?i.location:i,e))+'</span><br>\n    <a href="https://jobs.lever.co/everlane/'+c((o=null!=(o=t.id||(null!=e?e.id:e))?o:l,typeof o===s?o.call(e,{"name":"id","hash":{},"data":a}):o))+c(p(null!=r[1]?r[1].queryString:r[1],e))+'" target="_blank" class="about_jobs__link serif-italic">Read More</a>\n  </li>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a,r){var i
i=t.each.call(e,null!=e?e.jobs:e,{"name":"each","hash":{},"fn":this.program(1,a,r),"inverse":this.noop,"data":a})
return null!=i?i:""},"useData":!0,"useDepths":!0})
return this.HandlebarsTemplates["banana/templates/about/jobs_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.about.JobsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/about/jobs_view"
n.prototype.initialize=function(e){return this.jobs=e.jobs}
n.prototype.getTemplateData=function(){var e
e=_.sortBy(this.jobs,function(e){var t,n
t=function(e){switch(e){case"Full time":return 0
case"Part Time":return 2
case"Internship":return 3
case"Contracted":return 4
default:return 4}}
n=function(e){var t,n,a,r,i
if(e.match(/Lead|Senior|Junior|Full Time|Head/g)){n=0
i=e.match(/Lead|Senior|Junior|Full Time|Head/g)
for(a=0,r=i.length;r>a;a++){t=i[a]
switch(t){case"Lead":n=0
break
case"Senior":n+=1
break
case"Head":n+=1
break
case"Full time":n+=2
break
case"Junior":n+=3
break
default:n+=4}}return n}return 4}
return n(e.text)+t(e.categories.commitment)})
return{"jobs":e,"queryString":E.currentQuery("lever-source")?"?lever-source="+E.currentQuery("lever-source"):""}}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/about/index_view"]=Handlebars.template({"1":function(){return'      <a class="subscribe-button disabled small fancy-button">Unsubscribe from Updates</a>\n'},"3":function(){return'      <a class="subscribe-button small fancy-button">Subscribe to Job Updates</a>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i=t.helperMissing,o=this.escapeExpression,s='<ul id="about-nav" class="clearfix">\n  <li><a href="#team">Team</a></li>\n  <li><a href="#press">Press</a></li>\n  <li><a href="#open_jobs">Jobs</a></li>\n  <li><a href="#contact">Contact</a></li>\n</ul>\n\n<section id="about">\n\n  <div class="about-text-hero">\n\n    <span class="theres-a">&mdash; introducing &mdash;</span>\n    <span class="new-way serif">Radical Transparency</span>\n    <p class="subheader">\n      Know your factories. Know your costs.<br>\n      Always ask why.\n    </p>\n\n  </div>\n\n'
r=this.invokePartial(n.text_columns,"  ","text_columns",e,void 0,t,n,a)
null!=r&&(s+=r)
s+='</section>\n\n<section id="team">\n\n  <h2 class="section-divider">team</h2>\n\n  <img src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"about_team_072014.jpg",{"name":"staticImageUrl","hash":{},"data":a}))+'" class="hero">\n\n  <blockquote class="team-quote stylized-quotation">\n    They say you should<br>\n    start a business that you wish<br>\n    already existed, so we quit<br>\n    our day jobs.<br>\n  </blockquote>\n\n  <ul class="clearfix">\n\n    <li class="copy-column">\n\n      <p>\n        In the fall of 2010, a then 25-year-old Michael<br>\n        Preysman left his job in venture capital to start<br>\n        his own business.\n      </p>\n\n      <p>\n        He never expected to work in fashion, but a passion<br>\n        for great design and frustration with the lack of<br>\n        innovation in the retail space, led him to build<br>\n        Everlane. He hasn\u2019t looked back.\n      </p>\n\n      <p>\n        While the  team skews young, our employees have cut<br>\n        their teeth at places like Google, Yelp, Gilt Groupe,\n      </p>\n\n    </li>\n\n    <li class="copy-column">\n\n      <p>\n        American Apparel, Marc Jacobs, J.Crew, Goldman\n        Sachs, Pentagram, and The Gap.\n      </p>\n\n      <p>\n        It&rsquo;s a motley group held together by a shared passion<br>\n        for pushing boundaries and challenging conventions.\n      </p>\n\n      <p>\n        We work in an airy new office in San Francisco\'s<br>\n        Mission District. Since our biggest customer base<br>\n        is in New York City, we have a small team on the<br>\n        ground there, as well. It\'s challenging at times,<br>\n        but we visit each other often.\n      </p>\n\n    </li>\n\n  </ul>\n</section>\n\n<section id="press" class="clearfix">\n\n  <h2 class="section-divider">press</h2>\n\n  <ul class="clearfix">\n\n    <li>\n      <a class="sprite-press-sources nyt ir">New York Times</a>\n      <blockquote>\n        &ldquo;If you are buying such basics as T-<br>\n        shirts, belts or tote bags at<br>\n        traditional retailers, you&rsquo;re<br>\n        probably paying too much.&rdquo;\n      </blockquote>\n    </li>\n\n    <li>\n      <a class="sprite-press-sources gq ir">GQ</a>\n      <blockquote>\n        &ldquo;The company designs and produces<br>\n        all of its own goods so you, the<br>\n        customer, get to save a good chunk<br>\n        of dough on buying some cool stuff.&rdquo;\n      </blockquote>\n    </li>\n\n  </ul>\n\n  <ul>\n\n    <li>\n      <a class="sprite-press-sources lucky ir">Lucky Magazine</a>\n      <blockquote>\n        &ldquo;It might seem unlikely<br>\n        that such inexpensive products<br>\n        could be of high quality,<br>\n        but the company has a unique<br>\n        business model on its side:<br>\n        by choosing to sell solely online,<br>\n        it has managed to cut costs<br>\n        to a bare minimum.&rdquo;\n      </blockquote>\n    </li>\n\n    <li>\n      <a class="sprite-press-sources style ir">Style.com</a>\n      <blockquote>\n        &ldquo;We just never understood why the<br>\n        most beautiful and simple products<br>\n        needed to cost so much. We&rsquo;ve just<br>\n        cut out the middlemen so we can<br>\n        take smaller margins without<br>\n        sacrificing on quality at all.&rdquo;\n      </blockquote>\n    </li>\n\n  </ul>\n\n  <ul>\n\n    <li>\n      <a class="sprite-press-sources lat ir">Los Angeles Times</a>\n      <blockquote>\n        &ldquo;When it comes to fashion<br>\n        today it&rsquo;s easy to be overwhelmed<br>\n        by choice and confused about what<br>\n        things really cost. Enter the Web-<br>only\n        Everlane, founded on a<br>\n        less-is-more philosophy.&rdquo;\n      </blockquote>\n    </li>\n\n    <li>\n      <a class="sprite-press-sources glamour ir">Glamour</a>\n      <blockquote>\n        &ldquo;New online shopping site<br>\n        Everlane.com is serving up a<br>\n        selection of cute and classic<br>\n        wardrobe staples for less.&rdquo;\n      </blockquote>\n    </li>\n\n  </ul>\n</section>\n\n<section id="jobs" class="clearfix">\n\n  <h2 class="section-divider">jobs</h2>\n\n  <img id="jobs" src="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"about_jobs_072014.jpg",{"name":"staticImageUrl","hash":{},"data":a}))+'" class="hero">\n\n  <blockquote id="open_jobs" class="stylized-quotation">\n    Dear rule breakers, questioners,<br>\n    straight-A students who<br>\n    skipped class: We want you.\n  </blockquote>\n\n  <div class="subscribe-container">\n'
r=t["if"].call(e,null!=e?e.userIsSubscribed:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
null!=r&&(s+=r)
return s+'  </div>\n\n  <div class="copy-column">\n\n    <h3>Design &amp; Production</h3>\n    <p>\n      Sharp and detail oriented people\n      with an appreciation for craftsmanship\n      and quality.\n    </p>\n\n    <ul class="job-column-1 jobs-list"></ul>\n\n  </div>\n\n  <div class="copy-column">\n\n    <h3>Creative &amp; Marketing</h3>\n\n    <p>\n      Creatives who think outside the box and<br>\n      know how to use both sides\n      of the brain.\n    </p>\n\n    <ul class="job-column-2 jobs-list"></ul>\n\n  </div>\n\n  <div class="copy-column">\n\n    <h3>Engineering &amp; Operations</h3>\n    <p>\n      Engineers and designers with a passion <br>\n      for UX and a love of great design.\n    </p>\n\n    <ul class="job-column-3 jobs-list"></ul>\n\n  </div>\n\n</section>\n\n<section id="contact" class="serif">\n\n  <h2 class="section-divider">contact</h2>\n\n  <blockquote class="stylized-quotation">\n    We&rsquo;re online only,<br>\n    which means we love email.<br>\n    You can also follow us:\n  </blockquote>\n\n  <p class="social-links">\n\n    <a href="http://twitter.com/everlane" class="sprite-social-icons twitter ir">\n      Twitter\n    </a>\n\n    <a href="http://tumblr.everlane.com" class="sprite-social-icons tumblr ir">\n      Tumblr\n    </a>\n\n    <a href="http://facebook.com/everlane" class="sprite-social-icons facebook ir">\n      Facebook\n    </a>\n\n  </p>\n\n  <p>\n    For general inquiries:<br>\n    <a href="mailto:support@everlane.com">support@everlane.com</a>\n  </p>\n\n  <p>\n    For press and media inquiries:<br>\n    <a href="mailto:press@everlane.com">press@everlane.com</a>\n  </p>\n\n  <p>\n    For bulk order inquiries:<br>\n    <a href="mailto:bulk@everlane.com">bulk@everlane.com</a>\n  </p>\n\n  <p>\n    If you want to make a return or<br>\n    an exchange, go to your <a href="/orders">orders page</a><br>\n    to begin the return process.\n  </p>\n\n</section>'},"usePartial":!0,"useData":!0})
return this.HandlebarsTemplates["banana/templates/about/index_view"]}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/about/text_columns"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<ul class="clearfix copy-columns">\n\n  <li class="copy-column">\n\n    <div class="sprite-about-icons machine">&nbsp;</div>\n\n    <h3>Know Your Factories</h3>\n\n    <p>\n      We spend months finding the best factories around the world\u2014the very same ones that produce your favorite designer labels. We visit&nbsp;them often, and build strong personal relationships with the owners.\n    </p>\n\n    <p>\n      This hands-on approach is the most effective way&nbsp;to ensure a factory\'s integrity.\n      As an added assurance we also require stringent workplace compliancy paperwork.\n    </p>\n\n  </li>\n\n  <li class="copy-column">\n\n    <div class="sprite-about-icons men">&nbsp;</div>\n\n    <h3>Know Your Costs</h3>\n\n    <p>\n      We believe customers have the right to know what their products cost to make.\n      At Everlane we&nbsp;reveal our true costs, and then we show you our&nbsp;markup.\n    </p>\n\n    <p>\n      In traditional retail a designer shirt is marked up 8x by the time it reaches the customer.\n      By being online only, we eliminate brick-and-mortar expenses and pass these savings on to you.\n    </p>\n\n  </li>\n\n  <li class="copy-column last">\n\n    <div class="sprite-about-icons pencil">&nbsp;</div>\n\n    <h3>Always Ask Why</h3>\n\n    <p>\n      We constantly challenge the status quo. Nothing is worse than complacency,\n      and as&nbsp;a&nbsp;brand our culture is to dissect every single decision we make at every level of the company.\n    </p>\n\n    <p>\n      We know our customers are also rule breakers and questioners, so we hope this\n      philosophy is palpable in the products and choices we make. And by all means, challenge us too.\n    </p>\n\n  </li>\n\n</ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/about/text_columns"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t},n=[].slice
Banana.views.about.IndexView=function(e){function a(){return a.__super__.constructor.apply(this,arguments)}t(a,e)
a.prototype.mainClass="about"
a.prototype.region="content"
a.prototype.template="banana/templates/about/index_view"
a.prototype.regions={"job-column-1":".job-column-1","job-column-2":".job-column-2","job-column-3":".job-column-3"}
a.prototype.partials={"text_columns":"banana/templates/about/text_columns"}
a.prototype.events={"click #about-nav a":"scrollToSection","click .subscribe-button":"subscribeToJobs"}
a.prototype.initialize=function(){return $.getJSON("/jobs",function(e){return function(t){var a,r,i,o,s,l
r=function(){var e,a
a=1<=arguments.length?n.call(arguments,0):[]
e=_.map(a,function(e){return t[e]||[]})
return _.flatten(e)}
i={"job-column-1":r("Production","Design"),"job-column-2":r("Marketing","Creative"),"job-column-3":r("Engineering","Operations")}
e.jobs=i
l=[]
for(a in i){s=i[a]
o=new Banana.views.about.JobsView({"jobs":e.jobs[a],"region":a})
l.push(e.subview(""+a+"Jobs",o))}return l}}(this))}
a.prototype.scrollToSection=function(e){var t
e.preventDefault()
t=$(e.currentTarget)
return $(t.attr("href")).velocity("scroll",{"duration":400,"easing":"easeOutQuad"})}
a.prototype.subscribeToJobs=function(e){var t
e.preventDefault()
if(!E.session.isSignedIn())return new Banana.views.components.ModalView({"view":Banana.views.users.LoginModalView})
t=E.chaplinCollections.user.get("subscribed_to_job_updates")
E.chaplinCollections.user.set("subscribed_to_job_updates",!t)
return E.chaplinCollections.user.save()?this.updateSubscribeButton():void 0}
a.prototype.updateSubscribeButton=function(){var e
e=this.$(".subscribe-button")
if(E.chaplinCollections.user.get("subscribed_to_job_updates")){e.addClass("disabled")
return e.text("Unsubscribe from Updates")}e.removeClass("disabled")
return e.text("Subscribe to Job Updates")}
a.prototype.getTemplateData=function(){var e
return{"jobs":this.jobs,"userIsSubscribed":null!=(e=E.chaplinCollections.user)?e.get("subscribed_to_job_updates"):void 0}}
return a}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/help/faq_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i=""
r=t.unless.call(e,null!=e?e.hidden:e,{"name":"unless","hash":{},"fn":this.program(2,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"2":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l='    <li class="question">\n      <h4>'
r=(i=null!=(i=t.title||(null!=e?e.title:e))?i:s,typeof i===o?i.call(e,{"name":"title","hash":{},"data":a}):i)
null!=r&&(l+=r)
l+="</h4>\n      <p>"
r=(i=null!=(i=t.body||(null!=e?e.body:e))?i:s,typeof i===o?i.call(e,{"name":"body","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+"</p>\n    </li>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l="<h3>"
r=(i=null!=(i=t.subject||(null!=e?e.subject:e))?i:s,typeof i===o?i.call(e,{"name":"subject","hash":{},"data":a}):i)
null!=r&&(l+=r)
l+="</h3>\n<ul>\n"
r=t.each.call(e,null!=e?e.questions:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(l+=r)
return l+"</ul>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/help/faq_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.help.FaqView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="copy-column"
n.prototype.template="banana/templates/help/faq_view"
n.prototype.events={"click h4":"openQuestion","click .usurp-modal":"openForm"}
n.prototype.initialize=function(e){this.subscribeEvent("userSearchesFaqs",function(e){return this.filterQuestions(e)})
this.faq=e.faq
return this.allQuestionsHidden=!1}
n.prototype.getTemplateData=function(){return this.faq}
n.prototype.openQuestion=function(e){$(e.currentTarget).parent().toggleClass("active")
return $(e.currentTarget).next().slideToggle(200)}
n.prototype.openForm=function(e){var t
e.preventDefault()
t=$(e.currentTarget)
return new Banana.views.components.ModalView({"view":{"class":Banana.views.help.ContactEverlaneView,"toAddress":t.data("email")}})}
n.prototype.filterQuestions=function(e){var t
this.resetQuestions()
t=this.$(".question:not(:contains('"+e+"'))")
this.allQuestionsHidden=t.length===this.$(".question").length
return t.hide()}
n.prototype.resetQuestions=function(){this.allQuestionsHidden=!1
return this.$(".question").show()}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/help/contact_everlane"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div id="form-container" class="grid_7">\n  <h3 class="subheader">Contact Everlane</h3>\n\n  <form method="post" action="'+s((r=null!=(r=t.customerInquiryUrl||(null!=e?e.customerInquiryUrl:e))?r:o,typeof r===i?r.call(e,{"name":"customerInquiryUrl","hash":{},"data":a}):r))+'" data-remote="true" class="grid_5" id="customer-inquiry-form" novalidate>\n\n    <div class="status-message"></div>\n\n    <ul>\n\n      <li>\n        <label for="to-address">To</label>\n        <span id="to-address">\n          '+s((r=null!=(r=t.toAddress||(null!=e?e.toAddress:e))?r:o,typeof r===i?r.call(e,{"name":"toAddress","hash":{},"data":a}):r))+'\n        </span>\n        <input type="hidden" id="hidden-to-address" name="to_address" value="'+s((r=null!=(r=t.toAddress||(null!=e?e.toAddress:e))?r:o,typeof r===i?r.call(e,{"name":"toAddress","hash":{},"data":a}):r))+'">\n      </li>\n\n      <li>\n        <label for="from-address">From</label>\n        <input type="email" id="from-address" name="from_address" value="'+s((r=null!=(r=t.userEmail||(null!=e?e.userEmail:e))?r:o,typeof r===i?r.call(e,{"name":"userEmail","hash":{},"data":a}):r))+'" tab-index="1" placeholder="e.g., t.yorke@gmail.com">\n      </li>\n\n      <li>\n        <label for="category">Subject</label>\n        <select id="category" name="category" tab-index="-2" data-placeholder="Select Category" style="width: 100%">\n          <option></option>\n          <option value="comments">General Comments</option>\n          <option value="issues">Website Issues</option>\n          <option value="sizing">Sizing &amp; Product</option>\n        </select>\n      </li>\n\n      <li>\n        <label for="inquiry-comment">Comments</label>\n        <textarea id="inquiry-comment"  tab-index="3" placeholder="e.g., didn\'t fit, not my color, damaged goods." name="message"></textarea>\n      </li>\n\n    </ul>\n\n    <input id="contact-submit" type="submit" class="disabled fancy-button orange large" value="Send" data-disable-with="Sending&hellip;">\n\n  </form>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/help/contact_everlane"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.help.ContactEverlaneView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/help/contact_everlane"
n.prototype.events={"submit #customer-inquiry-form":"checkForErrors","change #category":"letUserContinue"}
n.prototype.initialize=function(e){null==e&&(e={})
n.__super__.initialize.apply(this,arguments)
this.toAddress=e.toAddress||"support@everlane.com"
return this.placeholderText={"comments":"General comments, good or bad.","issues":"Issues you have with your order or the website.","sizing":"Any questions about fit and sizing.","returns":"Oops, I actually wanted a medium.","other":"Anything goes."}}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$("#category").select2({"minimumResultsForSearch":-1})}
n.prototype.close=function(){this.$("#category").select2("close")
return n.__super__.close.apply(this,arguments)}
n.prototype.getTemplateData=function(){E.session.isSignedIn()&&{"userEmail":E.chaplinCollections.user.get("email")}
return{"customerInquiryUrl":"/customer_inquiry","toAddress":this.toAddress}}
n.prototype.letUserContinue=function(){var e
e=this.$("#category").val()
this.$("textarea","#customer-inquiry-form").attr("placeholder",this.placeholderText[e])
this.$("#contact-submit").removeClass("disabled")
return this.$("#inquiry-comment").focus()}
n.prototype.checkForErrors=function(){var e,t,n
n=function(e){return this.$(".status-message").html(e).velocity("fadeIn").effect("highlight",{},1500)}
t=this.$("#from-address").val()
e=$.trim($("#inquiry-comment").val())
if(E.lib.helpers.isEmail(t)){if(e.length)return!0
n("Please give us some context to your inquiry.<br>We will be glad to help.")
return!1}n("Hmm, looks like an incorrect email&hellip;")
return!1}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/help/info_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l='<section class="intro clearfix">\n\n  <h2>We&rsquo;re here to help.</h2>\n\n  <div class="email grid_4">\n    <a id="form-activator" class="support-email" href="mailto:support@everlane.com">Email us</a><br>\n    <a href="mailto:support@everlane.com">support@everlane.com</a>\n    <p><span>Estimated response time:</span> <span class="time-estimate">'
r=(i=null!=(i=t.responseTime||(null!=e?e.responseTime:e))?i:s,typeof i===o?i.call(e,{"name":"responseTime","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+'</span></p>\n    <div class="inquiries">\n      <p>For press inquiries: <a href="mailto:press@everlane.com">press@everlane.com</a></p>\n      <p>For bulk order inquiries: <a href="mailto:bulk@everlane.com">bulk@everlane.com</a></p>\n    </div>\n  </div>\n\n</section>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/help/info_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.help.InfoView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="pre_content"
n.prototype.template="banana/templates/help/info_view"
n.prototype.events={"click #form-activator":"openForm"}
n.prototype.initialize=function(){var e,t,n,a,r,i,o,s,l,c,p
c=-8
e=(new Date).getTime()+3600*c*1e3
p=new Date(e).toUTCString().replace(/GMT$/,"")
n=new Date(p)
a=n.getDay()
s=n.getHours()
r=n.getDate()
o=["11-24","11-25","11-26","11-27","11-28","11-29","11-30","11-31","0-1","10-28","10-29"]
t=""+n.getUTCMonth()+"-"+n.getUTCDate()
i=this.$(".intro").find(".email").find("time")
l=function(){return $.inArray(t,o)>-1}
this.responseHours=E.lib.helpers.inRange(a,1,5)?E.lib.helpers.inRange(s,17,22)?"3 hr":E.lib.helpers.inRange(s,5,16)?"40 min":"6 hr":o?"12 hr":E.lib.helpers.inRange(s,11,17)?"3 hr":"6 hr"
return this.emailResponseTime="<time>"+this.responseHours+"</time>"}
n.prototype.openForm=function(e){e.preventDefault()
return new Banana.views.components.ModalView({"view":Banana.views.help.ContactEverlaneView})}
n.prototype.getTemplateData=function(){return{"responseTime":this.emailResponseTime}}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/help/index_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<h3 class="section-divider">Frequently Asked Questions</h3>\n\n<div class="faq-search">\n  <p>What can we help you with?</p>\n  <span class="deletable-field">\n    <input type="text" id="faq-search-box" class="fancy-input" size="45" placeholder="eg: returns, exchanges, shipping">\n    <span class="clear-icon">\xd7</span>\n  </span>\n  <h5 class="no-result">No Results Found</h5>\n</div>\n\n<div class="faq-container">\n  <ul class="faq clearfix"></ul>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/help/index_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.help.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.mainClass="help"
n.prototype.region="content"
n.prototype.template="banana/templates/help/index_view"
n.prototype.events={"click .clear-icon":"clearSearchField","input #faq-search-box":"updateSearchResults"}
n.prototype.initialize=function(){var e
e=[{"subject":"Everlane","questions":[{"title":"Who are we?","body":"We&rsquo;re a motley group held together by a shared passion for pushing boundaries and challenging conventions. We work in an airy office in San Francisco&rsquo;s Mission District, and have recently opened a small office in New York."},{"title":"What is your design philosophy?","body":"All of our pieces are created in-house by a<br> small team of designers who obsess over every detail. We do not outsource and we do not overproduce. Instead of offering 20 options<br> for a shirt, we try to make only the best in each category."},{"title":"How do you ensure quality?","body":'A great design is nothing without a great factory that can bring the design to life. We spend months seeking out the best ones, the very same factories producing your favorite designer labels. We take pride in creating high-quality products and are continuously working to improve our methods for quality control.<br> If ever you receive a damaged or defective item, please let us know at <a class="usurp-modal" data-email="support@everlane.com" data-category="issues" href="mailto:support@everlane.com">support@everlane.com</a> and we will help you out with a replacement item or refund.'},{"title":"Why is your selection so small?","body":"As part of our commitment to quality and minimalist design, we intentionally pare down our collections, producing only what we believe to be essential pieces in each product category. Instead of overwhelming the customer with limitless choice, we focus on offering our members tightly edited collections of key<br> pieces that people can build a wardrobe around."},{"title":"What size am I?","body":'We know online shopping can be tricky for fit and sizing. If you have a question about a particular style, send your measurements to <a class="usurp-modal" data-email="fit@everlane.com" data-category="sizing" href="mailto:fit@everlane.com">fit@everlane.com</a>. We&rsquo;ll help you find the right size.<br><br> You can also reference the detailed sizing charts on each product page (just look for the orange &rdquo;What Size am I?&rdquo; button).'}]},{"subject":"My Orders & Shipping","questions":[{"title":"How much does US shipping cost?","body":"Standard shipping is free for orders of 2+ items. Single-item orders are $4.95."},{"title":"Can I get my order expedited?","body":"Yes. We offer expedited shipping at checkout. Our expedited shipping is handled through UPS 2-Day Air and costs $15 for 2+ items and $20 for single-item orders."},{"title":"When will I get my order?","body":"We process orders within 1-2 days, then ship them to you via UPS or USPS. All in all, standard shipping takes 3-5 business days."},{"title":"What if I need to change my order?","body":'After placing an order, you have a one-hour window in which you can edit the shipping address or cancel the order. Just click &rdquo;Edit Address&rdquo; or &rdquo;Cancel Order&rdquo; on the My Orders page. If you don\u2019t see a link, reach out to <a class="usurp-modal" data-email="support@everlane.com" data-category="issues" href="mailto:support@everlane.com">support@everlane.com</a> &mdash; we\u2019ll see what we can do!'},{"title":"Do you ship to Canada?","body":"Yes, we do! Shipping to Canada costs a flat fee of $9.95, and typically takes around 10-14 business days (sometimes longer if the package is waylaid at customs). Final delivery is handled via UPS or Canada Post."},{"title":"Do you ship internationally?","body":'For now, we only ship to US and Canada.  We&rsquo;ve found that <a href="http://hopshopgo.com" target="_blank">HopShopGo</a> offers an effective workaround, shipping from the US to anywhere in the world.<br><br> We accept credit cards from the US, Canada, and 12 other countries. You can see the full list when you add your credit card at checkout.<br><br> Please note: Because <a href="http://hopshopgo.com" target="_blank">HopShopGo</a> is a third-party vendor, sales under this method are final.'}]},{"subject":"Returns & Exchanges","questions":[{"title":"What&rsquo;s your return and exchange policy?","body":'We have a 90-day return policy. The items should be unworn, unwashed, and undamaged. We charge $5 to cover return shipping and restocking and deduct it from your refund. Exchanges are free. <br><br> Visit our <a href="/account/returns">returns page</a> to start a return or exchange.'},{"title":"How do I return a gift?","body":'We offer store credit for gift returns. Send a message to <a class="usurp-modal" data-email="support@everlane.com" data-category="issues" href="mailto:support@everlane.com">support@everlane.com</a> and include any information you have (order number, item, name of person who ordered, etc). Feel free to include a photo if that makes it easier!'},{"title":"When can I expect my refund?","body":"Please allow 2-3 weeks for your refund. We&rsquo;ll send an email once the item(s) have been received and your refund has been processed."},{"title":"What if my items are damaged or defective?","body":'If you have a damaged or defective item, contact <a class="usurp-modal" data-email="support@everlane.com" data-category="issues" href="mailto:support@everlane.com">support@everlane.com</a> with a photo of the damage and your order number. We&rsquo;ll take care of you!'}]}]
return this.faqs=_.reject(e,function(e){return e.hidden})}
n.prototype.render=function(){var e,t,a,r,i
n.__super__.render.apply(this,arguments)
i=this.faqs
for(a=0,r=i.length;r>a;a++){e=i[a]
t=new Banana.views.help.FaqView({"faq":e,"container":this.$(".faq")})
this.subview(e.subject,t)}return this.subview("infoView",new Banana.views.help.InfoView)}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.updateSearchResults()}
n.prototype.updateSearchResults=function(){var e
e=this.$("#faq-search-box").val()
e.length>0?this.$(".clear-icon").show():this.$(".clear-icon").hide()
this.publishEvent("userSearchesFaqs",e)
return this.checkForEmptyResults()}
n.prototype.clearSearchField=function(){this.$("#faq-search-box").val("")
return this.updateSearchResults()}
n.prototype.checkForEmptyResults=function(){var e
this.hideMessage()
e=_.filter(this.subviews,function(e){return e.faq})
return _.every(e,function(e){return e.allQuestionsHidden})?this.showMessage():void 0}
n.prototype.showMessage=function(){this.$(".no-result").show()
return this.$(".faq").hide()}
n.prototype.hideMessage=function(){this.$(".no-result").hide()
return this.$(".faq").show()}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/invite/index_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<div class="rewards-container">\n\n  <h2>Tell your friends<sup>*</sup></h2>\n\n  <p>Receive $25 of credit when friends make their first purchase.</p>\n\n  <div class="link-container">\n    <input value="'+s((r=null!=(r=t.link||(null!=e?e.link:e))?r:o,typeof r===i?r.call(e,{"name":"link","hash":{},"data":a}):r))+'" autocomplete="off" readonly class="share-url fancy-input" title="copy me" name="share">\n\n    <a class="fancy-button twitter" href="https://twitter.com/intent/tweet?'+s((r=null!=(r=t.twitterShareLink||(null!=e?e.twitterShareLink:e))?r:o,typeof r===i?r.call(e,{"name":"twitterShareLink","hash":{},"data":a}):r))+'"><img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,"referral_tw.png",{"name":"staticImageUrl","hash":{},"data":a}))+'"> Tweet</a>\n    <a class="fancy-button facebook"><img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,"referral_fb.png",{"name":"staticImageUrl","hash":{},"data":a}))+'"> Share</a>\n  </div>\n\n  <div class="user-messaging">\n    <p>'+s((r=null!=(r=t.invitedUsers||(null!=e?e.invitedUsers:e))?r:o,typeof r===i?r.call(e,{"name":"invitedUsers","hash":{},"data":a}):r))+" friends have joined. "+s((r=null!=(r=t.invitedPurchasers||(null!=e?e.invitedPurchasers:e))?r:o,typeof r===i?r.call(e,{"name":"invitedPurchasers","hash":{},"data":a}):r))+" friends have purchased. </p>\n    <p>You have <em>"+s((r=null!=(r=t.total||(null!=e?e.total:e))?r:o,typeof r===i?r.call(e,{"name":"total","hash":{},"data":a}):r))+"</em> in credit</p>\n  </div>\n\n  <aside>\n    *As we've grown we've learned that our best <br/>\n    customers are the ones who hear about us through friends. <br/>\n    So we wanted to reward people for sharing.\n  </aside>\n\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/invite/index_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.invite.IndexView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/invite/index_view"
n.prototype.region="content"
n.prototype.mainClass="rewards"
n.prototype.requireLogin=!0
n.prototype.initialize=function(){var e,t,n,a
this.total=(null!=(e=E.chaplinCollections.user)?e.get("credits_total"):void 0)||"$0.00"
this.invitedPurchasers=(null!=(t=E.chaplinCollections.user)?t.get("invited_purchasers"):void 0)||"0"
this.invitedUsers=(null!=(n=E.chaplinCollections.user)?n.get("invited_users"):void 0)||"0"
return this.shareURL=(null!=(a=E.chaplinCollections.user)?a.get("invite_url"):void 0)||"https://everlane.com"}
n.prototype.getTemplateData=function(){return{"total":this.total,"link":this.shareURL,"invitedPurchasers":this.invitedPurchasers,"invitedUsers":this.invitedUsers,"twitterShareLink":$.param({"url":this.shareURL,"text":"Check out Everlane - high quality basics without traditional retail markups."})}}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
this.$(".facebook").on("click",$.proxy(this.onFacebookShare,this))
twttr.ready(function(e){return function(t){return t.events.bind("tweet",$.proxy(e.onTwitterShare,e))}}(this))
return E.pub(E.Event.Invite.PAGE_VIEW,{"source":$.getURLParameter("source")||"unknown"})}
n.prototype.onFacebookShare=function(){return E.lib.FB.share({"link":this.shareURL,"name":"Everlane","description":"Luxury basics without traditional retail markups. Join now. "+this.shareURL,"actions":[{"name":"Join Everlane","link":this.shareURL}],"display":"popup"},function(e){if(e){E.lib.FlashMessage("Shared")
return E.pub(E.Event.Invite.SHARE,{"network":"facebook","context":"rewards"})}})}
n.prototype.onTwitterShare=function(){E.lib.FlashMessage("Tweeted")
return E.pub(E.Event.Invite.SHARE,{"network":"twitter","context":"rewards"})}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.controllers.PagesController=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.about=function(){E.pub(E.Event.About.PAGE_VIEW)
this.adjustTitle("About")
return this.view=new Banana.views.about.IndexView}
n.prototype.help=function(){this.adjustTitle("Help")
return this.view=new Banana.views.help.IndexView}
n.prototype.invite=function(){this.adjustTitle("Refer Your Friends")
return this.view=new Banana.views.invite.IndexView}
return n}(Banana.Controller)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.models.ProductPage=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.validate=function(){return this.variant()?void 0:["size","Please select a size"]}
n.prototype.variant=function(){var e,t,n,a,r,i
n=this.get("product").get("variants")
t=(e=this.get("size"))?_.find(n,function(t){return t.short_name.toLowerCase()===e}):1===n.length?n[0]:void 0
a=null!=(r=E.chaplinCollections.user)?r.get("waitlisted_variants"):void 0
t&&("waitlistable"===(i=t.orderable_state)||"sold_out"===i)&&_.contains(a,t.id)&&(t.orderable_state="waitlisted")
return t}
n.prototype.populate=function(){var e,t,n,a
n=this.get("product")
a=this.get("quantity")
e=E.currentOrder.get("lineItems")
t={}
this.get("giftcard_reservation")&&(t.giftcard_reservation=this.get("giftcard_reservation"))
return e.addLineItem(a,this.variant(),n.attributes,t)}
return n}(Banana.Model)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/slideshow_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="slideshow-slide-previous"> <div class="arrow"></div> </div>\n  \n<div class="swipe">\n  <ol class="swipe-wrap"></ol>\n</div>\n\n<div class="slideshow-slide-next"> <div class="arrow"></div> </div>\n'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/slideshow_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.SlideshowView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.container="#page"
n.prototype.template="banana/templates/components/slideshow_view"
n.prototype.listSelector=".swipe-wrap"
n.prototype.className="slideshow__slides"
n.prototype.events={"click .slideshow-slide-previous":"slidePrevious","click .slideshow-slide-next":"slideNext"}
n.prototype.options={"openIndex":0}
n.prototype.slidePrevious=function(){return this.swipe.prev()}
n.prototype.slideNext=function(){return this.swipe.next()}
n.prototype.handleKeyPresses=function(e){switch(e.which){case 37:return this.slidePrevious()
case 39:return this.slideNext()}}
n.prototype.initialize=function(e){null==e&&(e={})
this.options=_.extend({},this.options,e)
this.keypressProxy=function(e){return function(t){return e.handleKeyPresses(t)}}(this)
this.positionProxy=function(e){return function(t){return e.positionArrows(t)}}(this)
$(document).on("keydown",this.keypressProxy)
return $(window).on("resize",this.positionProxy)}
n.prototype.dispose=function(){n.__super__.dispose.apply(this,arguments)
$(document).off("keydown",this.keypressProxy)
return $(window).off("resize",this.positionProxy)}
n.prototype.attach=function(){var e,t,a
n.__super__.attach.apply(this,arguments)
e=_.values(this.getItemViews())[this.options.openIndex].$el
null==(t=this.options).width&&(t.width=e.width())
null==(a=this.options).height&&(a.height=e.height())
_.each(_.values(this.getItemViews()),function(e){return function(t){t.$el.css({"width":e.options.width,"height":e.options.height})
e.listenTo(t,"slideshow:next",e.slideNext)
return e.listenTo(t,"slideshow:previous",e.slidePrevious)}}(this))
this.$el.css({"width":this.options.width,"height":this.options.height})
this.positionArrows()
return this.swipe=this.$(".swipe").Swipe({"startSlide":this.options.openIndex}).data("Swipe")}
n.prototype.positionArrows=function(){var e,t,n,a
t=_.values(this.getItemViews())[this.options.openIndex].$el
e=t.find(this.options.alignEl)
0===e.length&&(e=t)
a=(e.height()-this.$(".slideshow-slide-previous").height())/2
n=50
this.options.width>.9*this.container.parent().width()&&(n=-20)
this.$(".slideshow-slide-previous").css({"top":a,"left":-n})
return this.$(".slideshow-slide-next").css({"top":a,"right":-n})}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/thumbnail_list_item_view"]=Handlebars.template({"1":function(e,t,n,a){var r=t.helperMissing,i=this.escapeExpression
return'  <img class="play-thumb" data-video="true" src="'+i((t.staticImageUrl||e&&e.staticImageUrl||r).call(e,"product_video_play_thumb_darker.png",{"name":"staticImageUrl","hash":{},"data":a}))+'">\n'},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'  <img data-src="'+s((r=null!=(r=t.src||(null!=e?e.src:e))?r:o,typeof r===i?r.call(e,{"name":"src","hash":{},"data":a}):r))+'" src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.src:e,{"name":"staticImageUrl","hash":{"size":70},"data":a}))+'">\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r
r=t["if"].call(e,null!=e?e.video:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.program(3,a),"data":a})
return null!=r?r:""},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/thumbnail_list_item_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.ThumbnailListItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.tagName="li"
n.prototype.className="thumb"
n.prototype.template="banana/templates/products/thumbnail_list_item_view"
n.prototype.listen={"change:selected model":"onChange"}
n.prototype.events={"click img":"onClick"}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.video="video"===this.model.get("type")
return e}
n.prototype.onClick=function(){this.model.set("selected",!0)
return this.$el.addClass("active")}
n.prototype.onChange=function(e,t){return this.$el.toggleClass("active",t)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/thumbnail_list_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<ul class="thumbnail-list"></ul>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/thumbnail_list_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.ThumbnailListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.itemView=Banana.views.products.ThumbnailListItemView
n.prototype.listSelector=".thumbnail-list"
n.prototype.template="banana/templates/products/thumbnail_list_view"
n.prototype.listen={"change:selected collection":"onSelectedChange"}
n.prototype.animationDuration=0
n.prototype.onSelectedChange=function(e,t){return t?this.collection.each(function(t){return t!==e?t.set("selected",!1):void 0}):void 0}
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/images_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i=""
r=t["if"].call(e,null!=e?e.sdd_banner:e,{"name":"if","hash":{},"fn":this.program(2,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"2":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'      <img class="product-image__sdd-banner" src="'+s((r=null!=(r=t.sdd_banner||(null!=e?e.sdd_banner:e))?r:o,typeof r===i?r.call(e,{"name":"sdd_banner","hash":{},"data":a}):r))+'">\n'},"4":function(){return'    <div class="watch-video-wrap">\n      <a href="javascript:;" class="watch-video">watch video</a>\n    </div>\n'},"6":function(e,t,n,a){var r,i=t.helperMissing,o=this.escapeExpression
return'    <video height="448" width="800" class="primary-video" loop="loop" preload="auto" poster="'+o((t.staticImageUrl||e&&e.staticImageUrl||i).call(e,"product_video_loading_animation.gif",{"name":"staticImageUrl","hash":{},"data":a}))+'">\n      <source src="'+o((t.videoUrl||e&&e.videoUrl||i).call(e,null!=(r=null!=e?e.videoUrls:e)?r.mp4:r,{"name":"videoUrl","hash":{},"data":a}))+'" type=\'video/mp4; codecs="avc1.42E01E, mp4a.40.2"\'></source>\n      <source src="'+o((t.videoUrl||e&&e.videoUrl||i).call(e,null!=(r=null!=e?e.videoUrls:e)?r.ogg:r,{"name":"videoUrl","hash":{},"data":a}))+'" type=\'video/ogg; codecs="theora, vorbis"\'></source>\n    </video>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="primary-asset-container">\n\n'
r=t["if"].call(e,null!=e?e.deliverable:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+="\n"
r=t["if"].call(e,null!=e?e.videoUrls:e,{"name":"if","hash":{},"fn":this.program(4,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
c+='\n  <img class="product-image__primary-image" alt="'+l((i=null!=(i=t.displayName||(null!=e?e.displayName:e))?i:s,typeof i===o?i.call(e,{"name":"displayName","hash":{},"data":a}):i))+' - Everlane" data-src="'+l((i=null!=(i=t.first_image||(null!=e?e.first_image:e))?i:s,typeof i===o?i.call(e,{"name":"first_image","hash":{},"data":a}):i))+'" src="'+l((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=e?e.first_image:e,{"name":"staticImageUrl","hash":{"size":442},"data":a}))+'" />\n\n'
r=t["if"].call(e,null!=e?e.videoUrls:e,{"name":"if","hash":{},"fn":this.program(6,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+'\n</div>\n\n<div class="product-thumbs"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/images_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.ImagesView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/images_view"
n.prototype.events={"click .watch-video":"toggleVideo"}
n.prototype.listen={"change:product model":"render","change:size model":"render"}
n.prototype.render=function(){var e,t
this.assets=new Banana.Collection(this.model.get("product").get("assets").first(5))
n.__super__.render.apply(this,arguments)
this.listenTo(this.assets,"change",$.proxy(this.updateMainAsset,this))
e=this.assets.findWhere({"selected":!0})||this.assets.first().set("selected",!0)
t=new Banana.views.products.ThumbnailListView({"container":this.$(".product-thumbs"),"containerMethod":"prepend","collection":this.assets})
this.subview("thumbnails",t)
return this.$(".product-image__primary-image").hoverZoom({"zoomParent":".product-sidebar","getZoomUrl":function(e){return E.lib.ImageHelper.imageUrl(e)},"in":function(){return $(".product-title, .product-options").velocity({"opacity":0})},"out":function(){return $(".product-title, .product-options").velocity({"opacity":1})}})}
n.prototype.getTemplateData=function(){var e,t,a
e=n.__super__.getTemplateData.apply(this,arguments)
if(this.model.get("size")){t=_.findWhere(this.model.get("product").get("variants"),{"short_name":this.model.get("size").toUpperCase()})
e.deliverable=E.delivery.isDeliverable({"variant":t})&&E.delivery.isActive()}else e.deliverable=E.delivery.isDeliverable({"product":this.model.get("product")})&&E.delivery.isActive()
e.sdd_banner=E.delivery.bannerForCurrentLocation()
e.videoUrls=null!=(a=this.assets.findWhere({"type":"video"}))?a.get("urls"):void 0
e.first_image=this.assets.first().get("src")
e.product.images=this.assets.where({"type":"image"})
e.displayName=this.model.get("product").get("display_name")
return e}
n.prototype.toggleVideo=function(e){var t
null==e&&(e=!0)
if(t=this.$(".primary-video").get(0)){this.$(".primary-video").toggle(e)
this.$(".watch-video-wrap").toggle(!e)
return e?t.play():t.pause()}}
n.prototype.updateMainAsset=function(e){var t
if(e.get("selected")){if("video"===e.get("type"))return this.toggleVideo(!0)
this.toggleVideo(!1)
t=E.lib.ImageHelper.imageUrl(e.get("src"),{"size":442})
return this.$(".product-image__primary-image").attr("src",t).data("src",e.get("src"))}}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/quantity_selector_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<h4 class="label">Choose Quantity</h4>\n<input class="minus disabled" type=\'button\' name=\'subtract\' value=\'&minus;\'>\n<input type="text" name="quantity" id="quantity" class="quantity serif thirteen" value="1" maxlength="1" size="1">\n<input class="plus" type=\'button\' name=\'add\' value=\'+\'>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/quantity_selector_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.QuantitySelectorView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/quantity_selector_view"
n.prototype.events={"click .plus":"quantityAdd","click .minus":"quantitySubtract","change .quantity":"quantityChange"}
n.prototype.listen={"valid model":"valid","invalid model":"invalid"}
n.prototype.valid=function(){return this.$("label").removeClass("error")}
n.prototype.invalid=function(e,t){var n,a
n=t[0],a=t[1]
return"quantity"===n?this.$el.effect("shake").find("label").addClass("error"):void 0}
n.prototype.quantityAdd=function(){var e
return e=this.setQuantity(this.model.get("quantity")+1||1)}
n.prototype.quantitySubtract=function(){var e
return e=this.setQuantity(this.model.get("quantity")-1)}
n.prototype.quantityChange=function(){return this.setQuantity(this.$(".quantity").val())}
n.prototype.setQuantity=function(e){var t
t=this.validateQuantity(e)
this.model.set("quantity",t)
this.$(".quantity").val(t)
E.pub(E.Event.Product.QUANTITY_CHANGE,_.extend({},this.model.attributes,this.model.get("product").attributes))
return t}
n.prototype.validateQuantity=function(e){var t,n,a,r
t=5
n=1
a=this.$(".minus")
r=this.$(".plus")
e=parseInt(e,10)
e=Math.min(e,t)
e=Math.max(e,n)
isNaN(e)&&(e=n)
a.toggleClass("disabled",e===n)
r.toggleClass("disabled",e===t)
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/color_selector_view"]=Handlebars.template({"1":function(){return"Select"},"3":function(){return"monochrome"},"5":function(e){var t,n=this.lambda,a=this.escapeExpression
return'          <div class="second-color" style="border-color: transparent transparent #'+a(n(null!=(t=null!=(t=null!=e?e.product:e)?t.color:t)?t.hex_value_2:t,e))+' transparent;">\n          </div>\n'},"7":function(){return'<div class="drop-down-indicator"></div>'},"9":function(e,t,n,a){var r,i='    <ul class="color-button-set">\n'
r=t.each.call(e,null!=(r=null!=e?e.grouping:e)?r.products:r,{"name":"each","hash":{},"fn":this.program(10,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"    </ul>\n"},"10":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c=this.lambda,p='        <li data-id="'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'" class="swatch-container '
r=t["if"].call(e,null!=e?e.active:e,{"name":"if","hash":{},"fn":this.program(11,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='" title="'+l(c(null!=(r=null!=e?e.color:e)?r.name:r,e))+'">\n          <a class="swatch" href="/collections/'+l((i=null!=(i=t.collection_permalink||(null!=e?e.collection_permalink:e))?i:s,typeof i===o?i.call(e,{"name":"collection_permalink","hash":{},"data":a}):i))+"/products/"+l((i=null!=(i=t.permalink||(null!=e?e.permalink:e))?i:s,typeof i===o?i.call(e,{"name":"permalink","hash":{},"data":a}):i))+'" style="background-color: #'+l(c(null!=(r=null!=e?e.color:e)?r.hex_value:r,e))+'">'+l((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:s,typeof i===o?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+"\n"
r=t["if"].call(e,null!=(r=null!=e?e.color:e)?r.hex_value_2:r,{"name":"if","hash":{},"fn":this.program(13,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
return p+"          </a>\n        </li>\n"},"11":function(){return"active"},"13":function(e){var t,n=this.lambda,a=this.escapeExpression
return'              <div class="second-color" style="border-color: transparent transparent #'+a(n(null!=(t=null!=e?e.color:e)?t.hex_value_2:t,e))+' transparent;"></div>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o=this.lambda,s=this.escapeExpression,l="function",c=t.helperMissing,p='<h4 class="label color-selector-title">\n  '
r=t["if"].call(e,null!=e?e.monochrome:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+=' color\n</h4>\n\n<div class="color-dropdown '
r=t["if"].call(e,null!=e?e.monochrome:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='">\n\n  <div class="product-color">\n    <div class="swatch-container active">\n      <a class="swatch" href="javascript:;" style="background-color: #'+s(o(null!=(r=null!=(r=null!=e?e.product:e)?r.color:r)?r.hex_value:r,e))+'">'+s((i=null!=(i=t.display_name||(null!=e?e.display_name:e))?i:c,typeof i===l?i.call(e,{"name":"display_name","hash":{},"data":a}):i))+"\n"
r=t["if"].call(e,null!=(r=null!=(r=null!=e?e.product:e)?r.color:r)?r.hex_value_2:r,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='      </a>\n    </div>\n    <div class="name-container">\n      <span class="name">'+s(o(null!=(r=null!=(r=null!=e?e.product:e)?r.color:r)?r.name:r,e))+"</span>\n    </div>\n    "
r=t.unless.call(e,null!=e?e.monochrome:e,{"name":"unless","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+="\n  </div>\n\n"
r=t.unless.call(e,null!=e?e.monochrome:e,{"name":"unless","hash":{},"fn":this.program(9,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
return p+"\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/color_selector_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.ColorSelectorView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/color_selector_view"
n.prototype.className="product-color-selector-wrap"
n.prototype.events={"click li":"clickColor","touchstart li":"clickColor"}
n.prototype.initialize=function(e){this.hover_preview=e.hover_preview
this.activeProduct=this.model.get("product")
return E.lib.helpers.isMobile()?_.extend(this.events,{"click .product-color":"toggleOptions"}):_.extend(this.events,{"mouseenter li":"selectMouseEnter","mouseenter .color-dropdown":"showOptions","mouseleave .color-dropdown":"hideOptions"})}
n.prototype.render=function(){n.__super__.render.apply(this,arguments)
this.colorDropdown=this.$(".color-dropdown")
this.selectedProductColor=this.$(".product-color")
this.selectedLabel=this.selectedProductColor.find(".name")
this.selectedSwatch=this.selectedProductColor.find(".swatch")
return this.colorSet=this.$(".color-button-set")}
n.prototype.clickColor=function(e){e.stopPropagation()
e.preventDefault()
clearTimeout(this.hoverRevertTimer)
this.selectProduct(this.getProductFromEvent(e))
this.colorSet.find(".active").removeClass("active")
$(e.currentTarget).addClass("active")
this.selectedProductColor.find(".swatch-container").effect("bounce")
return this.colorSet.hide()}
n.prototype.selectMouseEnter=function(e){var t
t=this.getProductFromEvent(e)
this.selectedSwatch.toggleClass("active",t===this.activeProduct)
this.setLabel(t)
E.pub(E.Event.Product.COLOR_HOVER,t.attributes)
if(this.hover_preview){clearTimeout(this.hoverRevertTimer)
return this.model.set("product",this.getProductFromEvent(e))}}
n.prototype.selectProduct=function(e){this.activeProduct=e
this.model.set("product",e)
this.setLabel(e)
return E.pub(E.Event.Product.COLOR_CHANGE,e.attributes)}
n.prototype.showOptions=function(){return this.colorSet.show()}
n.prototype.hideOptions=function(){this.colorSet.hide()
this.model.set("product",this.activeProduct)
return this.setLabel(this.activeProduct)}
n.prototype.toggleOptions=function(){return this.colorSet.toggle()}
n.prototype.setLabel=function(e){var t,n
n=e.get("color")
t=n.hex_value_2?"#"+n.hex_value_2:"transparent"
this.selectedLabel.text(n.name)
this.selectedSwatch.css("background-color","#"+n.hex_value)
return this.selectedSwatch.find(".second-color").css({"borderColor":"transparent","borderBottomColor":t})}
n.prototype.getProductFromEvent=function(e){var t
t=this.$(e.currentTarget).data("id")
return this.model.get("grouping").get("products").findWhere({"id":t})}
n.prototype.getTemplateData=function(){var e,t,a,r,i,o,s
e=n.__super__.getTemplateData.apply(this,arguments)
i=e.grouping.products
a=i.length
for(t=o=0,s=i.length;s>o;t=++o){r=i[t]
r.active=r.id===e.product.id}return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/denomination_selector_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='    <li class="denominations__denomination '
r=t["if"].call(e,null!=e?e.active:e,{"name":"if","hash":{},"fn":this.program(2,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+'" data-id="'+l((i=null!=(i=t.id||(null!=e?e.id:e))?i:s,typeof i===o?i.call(e,{"name":"id","hash":{},"data":a}):i))+'">$'+l((i=null!=(i=t.price||(null!=e?e.price:e))?i:s,typeof i===o?i.call(e,{"name":"price","hash":{},"data":a}):i))+"</li>\n"},"2":function(){return"denominations__denomination--active"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i='<div class="denomination-selector">\n  <ul class="denominations">\n'
r=t.each.call(e,null!=(r=null!=e?e.grouping:e)?r.products:r,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"  </ul>\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/denomination_selector_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.DenominationSelectorView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/denomination_selector_view"
n.prototype.events={"click li":"changeProduct"}
n.prototype.getProductFromEvent=function(e){var t
t=this.$(e.currentTarget).data("id")
return this.model.get("grouping").get("products").findWhere({"id":t})}
n.prototype.changeProduct=function(e){var t
t=this.getProductFromEvent(e)
this.$(".denomination-selector").find(".denominations__denomination--active").removeClass("denominations__denomination--active")
$(e.currentTarget).addClass("denominations__denomination--active")
return this.model.set("product",t)}
n.prototype.getTemplateData=function(){var e,t,a,r,i,o
e=n.__super__.getTemplateData.apply(this,arguments)
r=e.grouping.products
for(t=i=0,o=r.length;o>i;t=++i){a=r[t]
a.active=a.id===e.product.id}return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/size_chart_modal_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="      <tr>\n"
r=t.each.call(e,e,{"name":"each","hash":{},"fn":this.program(2,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"      </tr>\n"},"2":function(e){var t,n=this.lambda,a="          <td>"
t=n(e,e)
null!=t&&(a+=t)
return a+"</td>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<div class="caption">'
r=(i=null!=(i=t.caption||(null!=e?e.caption:e))?i:s,typeof i===o?i.call(e,{"name":"caption","hash":{},"data":a}):i)
null!=r&&(c+=r)
c+='</div>\n<img src="'+l((t.staticImageUrl||e&&e.staticImageUrl||s).call(e,null!=e?e.main_image:e,{"name":"staticImageUrl","hash":{},"data":a}))+'" class="reserve-space">\n<div class="size-table">\n  <table>\n'
r=t.each.call(e,null!=e?e.tableData:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"  </table>\n</div>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/size_chart_modal_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.SizeChartModalView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/size_chart_modal_view"
n.prototype.className="size-chart-modal"
n.prototype.initialize=function(e){return this.sizeChart=e}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.$el.find("img").reserveSpace()}
n.prototype.getTemplateData=function(){var e,t
e=this.sizeChart
t=JSON.parse(this.sizeChart.content)
t=_.map(t,function(e){return _.map(e,function(e){return parseInt(e)?e.replace(/((\d*)\/(\d*))/g,function(e,t,n,a){return"<span class='numerator'>"+n+"</span><span class='denominator'>"+a+"</span>"}):e})})
e.tableData=t
return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/size_selector_view"]=Handlebars.template({"1":function(){return"  Select Type\n"},"3":function(e,t,n,a){var r,i="  Select Size\n"
r=t["if"].call(e,null!=e?e.hasSizeChart:e,{"name":"if","hash":{},"fn":this.program(4,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i},"4":function(){return"    <a href=\"#\" class='size-chart-link'>(What size am I?)</a>\n"},"6":function(){return'  <span class="serif">Runs large, please order one size down.</span>\n'},"8":function(e,t,n,a,r){var i,o,s="function",l=t.helperMissing,c=this.escapeExpression,p='    <li title="'+c((o=null!=(o=t.name||(null!=e?e.name:e))?o:l,typeof o===s?o.call(e,{"name":"name","hash":{},"data":a}):o))+'"\n      class="'
i=t.unless.call(e,null!=r[1]?r[1].giftCard:r[1],{"name":"unless","hash":{},"fn":this.program(9,a,r),"inverse":this.noop,"data":a})
null!=i&&(p+=i)
p+=" serif thirteen "
i=t["if"].call(e,null!=e?e.active:e,{"name":"if","hash":{},"fn":this.program(11,a,r),"inverse":this.noop,"data":a})
null!=i&&(p+=i)
p+=" "
i=t.unless.call(e,null!=e?e.available:e,{"name":"unless","hash":{},"fn":this.program(13,a,r),"inverse":this.noop,"data":a})
null!=i&&(p+=i)
p+='"\n      data-size-name="'+c((t.lowerCase||e&&e.lowerCase||l).call(e,null!=e?e.short_name:e,{"name":"lowerCase","hash":{},"data":a}))+'">\n        '+c((o=null!=(o=t.short_name||(null!=e?e.short_name:e))?o:l,typeof o===s?o.call(e,{"name":"short_name","hash":{},"data":a}):o))+"\n    </li>\n"
i=(t.ifCond||e&&e.ifCond||l).call(e,a&&a.index,null!=r[1]?r[1].breakIndex:r[1],{"name":"ifCond","hash":{},"fn":this.program(15,a,r),"inverse":this.noop,"data":a})
null!=i&&(p+=i)
return p},"9":function(){return"tooltip"},"11":function(){return"active"},"13":function(){return"sold-out"},"15":function(){return"      <br>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a,r){var i,o='<h4 class="label">\n'
i=t["if"].call(e,null!=e?e.giftCard:e,{"name":"if","hash":{},"fn":this.program(1,a,r),"inverse":this.program(3,a,r),"data":a})
null!=i&&(o+=i)
o+="</h4>\n\n"
i=t["if"].call(e,null!=e?e.sizedown:e,{"name":"if","hash":{},"fn":this.program(6,a,r),"inverse":this.noop,"data":a})
null!=i&&(o+=i)
o+='\n<ul id="size-button-set">\n'
i=t.each.call(e,null!=(i=null!=e?e.product:e)?i.variants:i,{"name":"each","hash":{},"fn":this.program(8,a,r),"inverse":this.noop,"data":a})
null!=i&&(o+=i)
return o+"</ul>"},"useData":!0,"useDepths":!0})
return this.HandlebarsTemplates["banana/templates/products/size_selector_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.SizeSelectorView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/size_selector_view"
n.prototype.listen={"valid model":"valid","invalid model":"invalid","change:size model":"onChangeSize","change:product model":"onChangeProduct"}
n.prototype.events={"click li":"selectSize","click .size-chart-link":"openSizingModal"}
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
return this.product=this.model.get("product")}
n.prototype.valid=function(){return this.$("label").removeClass("error")}
n.prototype.invalid=function(e,t){var n,a
n=t[0],a=t[1]
return"size"===n?this.$el.effect("shake").find("label").addClass("error"):void 0}
n.prototype.openSizingModal=function(e){var t
e.preventDefault()
t=_.extend({},{"class":Banana.views.products.SizeChartModalView},this.product.get("size_chart"))
return new Banana.views.components.ModalView({"view":t})}
n.prototype.selectSize=function(e){var t
t=this.$(e.target).data("size-name").toString()
this.model.set("size",t)
return E.pub(E.Event.Product.SIZE_CHANGE,_.extend({},this.model.attributes,this.product.attributes))}
n.prototype.onChangeSize=function(e,t){var n,a,r,i,o
i=this.$("li")
o=[]
for(a=0,r=i.length;r>a;a++){n=i[a]
o.push(this.$(n).data("size-name").toString()===t?this.$(n).addClass("active"):this.$(n).removeClass("active"))}return o}
n.prototype.onChangeProduct=function(e,t){var n,a,r,i,o,s
o=t.get("variants")
s=[]
for(r=0,i=o.length;i>r;r++){a=o[r]
n='li[data-size-name="'+a.short_name.toLowerCase()+'"]'
s.push(this.$(n).toggleClass("sold-out",!a.available))}return s}
n.prototype.getTemplateData=function(){var e,t,a,r,i
e=n.__super__.getTemplateData.apply(this,arguments)
e.sandal=null!=e.product.permalink.match(/sandal/)
e.sizedown=null!=e.product.permalink.match(/womens-shirt/)
e.hasSizeChart=this.product.get("size_chart")&&this.model.get("product").get("apparel")
e.giftCard=this.product.isGiftCard()
a=e.product.variants
e.breakIndex=a.length>7?Math.ceil(a.length/2)-1:void 0
for(r=0,i=a.length;i>r;r++){t=a[r]
t.active=1===a.length||t.short_name.toLowerCase()===e.size}return e}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/giftcard_reservation_form"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<form class="fancy-form" novalidate>\n  <div class="form_fields clearfix">\n    <div class="field half_width">\n      <label for="reservation_recipient_name">Recipient Name <br></label>\n      <input name="recipient_name" type="text" id="reservation_recipient_name" placeholder="Jonny Greenwood">\n    </div>\n\n    <div class="field half_width">\n      <label for="reservation_recipient_email">Recipient Email <br></label>\n      <input name="recipient_email" type="email" id="reservation_recipient_email" placeholder="jonny@radiohead.com">\n    </div>\n\n    <div class="field half_width">\n      <label for="reservation_notify_at">Delivery Date (optional)</label>\n      <input name="notify_at" type="text" id="reservation_notify_at" class="giftcard-reservation__form__date-field" placeholder="'+s((r=null!=(r=t.placeHolderTime||(null!=e?e.placeHolderTime:e))?r:o,typeof r===i?r.call(e,{"name":"placeHolderTime","hash":{},"data":a}):r))+'">\n\n      <div class="picker__container"></div>\n    </div>\n\n    <div class="field half_width">\n      <p class="giftcard-reservation__date-explanation">Gift card will be sent instantly unless you specify a date.</p>\n    </div>\n\n    <div class="field">\n      <label for="reservation_message">Message (optional)</label>\n      <textarea name="message" id="reservation_message" rows="3" class="giftcard-reservation__form__message-field" placeholder="Didn\'t know your size. Happy Birthday, Jonny!"></textarea>\n    </div>\n\n    <div class="field">\n      <button type="submit" class="fancy-button orange large full-width">Add Card</button>\n    </div>\n  </div>\n</form>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/giftcard_reservation_form"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.GiftcardReservationFormView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/giftcard_reservation_form"
n.prototype.className="giftcard-reservation__form"
n.prototype.events={"submit form":"attemptSubmit"}
n.prototype.validations={"#reservation_recipient_name":{"presence":!0},"#reservation_recipient_email":{"presence":!0,"pattern":"email"},"#reservation_message":{"maxLength":160}}
n.prototype.initialize=function(){n.__super__.initialize.apply(this,arguments)
this.bodyClickProxy=function(e){return function(t){return e.handleBodyClick(t)}}(this)
return $("*").on("click.giftcard_reservation_form",this.bodyClickProxy)}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
this.pickerElement=this.$("#reservation_notify_at").pickadate({"min":Date.today(),"container":this.$(".picker__container"),"format":"dddd, mmm dd, yyyy"})
this.picker=this.pickerElement.pickadate("picker")
return this.picker.on("open",$.proxy(this.positionPicker,this))}
n.prototype.remove=function(){$("*").off("click.giftcard_reservation_form",this.bodyClickProxy)
return n.__super__.remove.apply(this,arguments)}
n.prototype.positionPicker=function(){var e,t,n,a
t=this.$("#reservation_notify_at")
a=this.$(".picker__container")
e=this.$(".picker__holder")
n=25
return a.css({"top":t.position().top+t.height()/2-129,"left":t.position().left+t.width()+n})}
n.prototype.handleBodyClick=function(e){var t
t=this.$(e.target)
return!t.parents(".picker__container").length&&this.picker.get("open")&&"reservation_notify_at"!==t.attr("id")?this.picker.close():void 0}
n.prototype.attemptSubmit=function(e){var t
e.preventDefault()
if(this.validate()){t={"recipient_email":this.$("#reservation_recipient_email").val(),"recipient_name":this.$("#reservation_recipient_name").val(),"notify_at":this.$("#reservation_notify_at").val(),"message":this.$("#reservation_message").val()}
return this.trigger("form:submit",t)}}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.placeHolderTime=(new Date).addWeeks(1).toString("dddd, MMM dd, yyyy")
return e}
return n}(Banana.View)
E.mix(Banana.views.products.GiftcardReservationFormView,E.mixins.Form)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/giftcard_reservation"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<h2 class="giftcard-reservation__header">Who\'s the lucky recipient?</h2>\n\n<div class="giftcard-reservation__form-container"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/giftcard_reservation"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.GiftcardReservationView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/giftcard_reservation"
n.prototype.className="giftcard-reservation"
n.prototype.render=function(){var e
n.__super__.render.apply(this,arguments)
e=new Banana.views.products.GiftcardReservationFormView({"container":this.$(".giftcard-reservation__form-container")})
this.subview("form",e)
return this.listenTo(e,"form:submit",function(e){this.trigger("form:submit",e)
return this.trigger("modal:close")})}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/purchase_area"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div id="cart_error" class="error clearfix"></div>\n\n<div class="email-form-container" style="display: none;">\n  <form>\n    <label>Please enter your email:</label>\n    <input type="email" size=\'35\' class=\'email-field fancy-input\' placeholder="user@example.com">\n    <input type="submit" style="display: none;">\n  </form>\n</div>\n\n<a class="fancy-button orange shippable large order-button" href="/checkout/review">Add to Bag</a>\n\n<div class="sku-notification">\n  <div class="sku-notification-messages"></div>\n</div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/purchase_area"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.PurchaseAreaView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/purchase_area"
n.prototype.className="product__purchase-area"
n.prototype.listen={"valid model":"valid","invalid model":"invalid","change:size model":"updateButton","change:product model":"updateButton"}
n.prototype.events={"click .add-to-cart":"attemptAddToCart","click .add-to-waitlist":"addToWaitlist","submit .email-form-container form":"addToWaitlist"}
n.prototype.types={"sold_out":{"value":"Sold Out","disabled":!0,"class":"grey inactive"},"waitlistable":{"value":"Notify Me","disabled":!1,"class":"default add-to-waitlist"},"shippable":{"value":"Add to Bag","disabled":!1,"class":"orange  add-to-cart"},"preorderable":{"value":"Add to Bag","disabled":!1,"class":"orange add-to-cart"},"waitlisted":{"value":"Waitlisted","disabled":!0,"class":"default inactive"}}
n.prototype.attemptAddToCart=function(e){var t
e.preventDefault()
return this.model.isValid()?(null!=(t=this.model.variant())?t.is_digital_giftcard:void 0)?this.showGiftcardReservationModal():this.addToCart():!1}
n.prototype.showGiftcardReservationModal=function(){var e
e=new Banana.views.components.ModalView({"view":{"class":Banana.views.products.GiftcardReservationView,"events":{"form:submit":function(e){return function(t){e.addGiftcardReservation(t)
return e.addToCart()}}(this),"modal:close":function(){return this.dismiss()}}}})
this.subview("giftcard_reservation_form",e)
return!1}
n.prototype.addToCart=function(){var e,t,n,a,r
this.model.trigger("valid")
this.$(".add-to-cart").text("Adding...").addClass("disabled")
if(this.model.populate()){n=this.$(".add-to-cart").attr("href")
e=null!=(a=this.model.get("collection"))?a.get("permalink"):void 0
t={"items_added":this.model.get("quantity"),"collection":e,"sku":null!=(r=this.model.variant())?r.sku:void 0}
return E.banana.router.route({"url":n},{"query":t})}}
n.prototype.addGiftcardReservation=function(e){var t
t={"amount":this.model.get("product").get("price")}
_.extend(t,e)
return this.model.set("giftcard_reservation",t)}
n.prototype.addToWaitlist=function(e){var t,n
e.preventDefault()
e.stopPropagation()
if(this.model.isValid()){n=this.$(".email-field")
t=n.val()
if(E.session.isSignedIn())return this.submitWaitlist()
if(t)return this.submitWaitlist({"email":t,"success":function(e){return function(){e.hideEmailForm()
return n.val("")}}(this)})
this.showEmailForm()
return n.focus()}}
n.prototype.submitWaitlist=function(e){var t,n,a
null==e&&(e={})
t=this.model.get("product")
a=this.model.variant()
n=new E.models.StockedItem({"variant":a,"email":e.email})
return n.addToWaitlist({"success":function(n){return function(){var a
null!=(a=e.success)&&a.call()
return n.showWaitlistConfirm(t)}}(this)})}
n.prototype.showWaitlistConfirm=function(e){var t
t=new Banana.views.products.WaitlistModalView({"model":e})
return $(".order-button").text("Waitlisted").removeClass("add-to-waitlist").addClass("inactive")}
n.prototype.valid=function(){return this.$("#cart_error").hide()}
n.prototype.invalid=function(e,t){var n,a
n=t[0],a=t[1]
return this.$("#cart_error").html(a).show()}
n.prototype.showEmailForm=function(){return this.$(".email-form-container").is(":visible")?void 0:this.$(".email-form-container").velocity("slideAndFadeIn")}
n.prototype.hideEmailForm=function(){return this.$(".email-form-container").velocity("slideUp")}
n.prototype.updateButton=function(e){var t,n,a,r,i,o,s,l,c,p
null==e&&(e=this.model)
l=e.variant()
this.notifications||(this.notifications=[])
r=[]
o=(null!=l?l.deliverable_state:void 0)&&E.delivery.isActive()&&e.get("product").get("is_sdd")
s=l?l.orderable_state:e.get("product").get("orderable_state")
this.hideEmailForm()
this.$(".order-button").text(this.types[s].value).attr("disabled",this.types[s].disabled).removeClass("blue orange grey inactive add-to-cart add-to-waitlist").addClass(this.types[s]["class"]);(null!=l?l.inventory_count:void 0)<20&&"shippable"===(null!=l?l.orderable_state:void 0)&&r.push("Only "+l.inventory_count+" left in stock");(null!=l?l.annotation:void 0)&&r.push(l.annotation)
i=null!=l?l.orderable_state:void 0
!o||"shippable"!==i&&"preorderable"!==i||(E.delivery.isDeliverable({"variant":l})?r.push('<span class="sku-notifications__message--sdd-available">1-hour delivery available</span>'):E.delivery.isDeliverable({"variant":l})||r.push("This size is not available for<br>1-hour delivery"))
if("US"===E.session.getCountry()){t=E.chaplinCollections.user
t||r.push("Free Shipping");((null!=t?t.get("has_free_shipping_coupon"):void 0)||(null!=t?t.get("has_free_shipping_badge"):void 0))&&r.push("You have free shipping")}l&&"preorderable"===s&&!o?r.push("<strong>Ships On</strong><br>"+l.restock_date):l&&"waitlistable"===s&&r.push("<strong>Restock Expected</strong><br>"+l.restock_date)
if(!(r.length>0))return this.$(".sku-notification").velocity("stop").velocity("slideAndFadeOut")
n=""
for(c=0,p=r.length;p>c;c++){a=r[c]
n+="<p>"+a+"</p>"}if(n!==this.$(".sku-notification-messages").html()){this.$(".sku-notification-messages").html(n)
return this.$(".sku-notification").velocity("stop").velocity("slideAndFadeIn")}}
n.prototype.attach=function(){n.__super__.attach.apply(this,arguments)
return this.updateButton(this.model)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/subhero_item_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return"<!-- Chaplin collection items inserted here -->"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/subhero_item_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.SubheroItemView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.className="subhero"
n.prototype.template="banana/templates/products/subhero_item_view"
n.prototype.types={"video":Banana.views.subheros.VideoView,"elastic":Banana.views.subheros.ElasticView,"lookbook":Banana.views.subheros.LookbookView}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
e=new(this.types[this.model.get("subhero_type")])({"container":this.$el,"model":this.model})
return this.subview("subheroView",e)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/subheros_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<div class="subheros"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/subheros_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.SubherosListView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="post_content"
n.prototype.itemView=Banana.views.products.SubheroItemView
n.prototype.listSelector=".subheros"
n.prototype.template="banana/templates/products/subheros_view"
return n}(Banana.CollectionView)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/components/vertical_slides_view"]=Handlebars.template({"1":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'    <li class="'+s((r=null!=(r=t.type||(null!=e?e.type:e))?r:o,typeof r===i?r.call(e,{"name":"type","hash":{},"data":a}):r))+'">\n      <img src="'+s((t.staticImageUrl||e&&e.staticImageUrl||o).call(e,null!=e?e.url:e,{"name":"staticImageUrl","hash":{},"data":a}))+'" class="slide-image"></img>\n    </li>\n'},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c='<ul class="slide-container" style="max-width: '+l((i=null!=(i=t.width||(null!=e?e.width:e))?i:s,typeof i===o?i.call(e,{"name":"width","hash":{},"data":a}):i))+'px;">\n'
r=t.each.call(e,null!=e?e.slides:e,{"name":"each","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(c+=r)
return c+"</ul>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/components/vertical_slides_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.components.VerticalSlidesView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/components/vertical_slides_view"
n.prototype.events={"click .slide-container li:not(.vimeo_video)":"clickSlide"}
n.prototype.initialize=function(e){null==e&&(e={})
this.width=e.width
return this.slides=e.slides}
n.prototype.clickSlide=function(e){var t
t=$(e.currentTarget)
return t.next().length?t.next().velocity("scroll",{"offset":E.config.fixedNavHeight,"duration":500}):void 0}
n.prototype.getTemplateData=function(){return{"width":this.width,"slides":this.slides}}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/poplin_details_view"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(){return'<style>\n  #cross-sell {\n    margin-top: 0;\n  }\n  .slide-container {\n    margin-top: 20px;\n  }\n</style>\n\n<div class="slides"></div>'},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/poplin_details_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.PoplinDetailsView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.region="post_content"
n.prototype.template="banana/templates/products/poplin_details_view"
n.prototype.render=function(){var e,t,a
n.__super__.render.apply(this,arguments)
t=this.model.get("product").get("permalink")
a=function(){var n,a,r,i,o,s
if(t.match(/ls/)){i=[]
for(e=n=1;6>=n;e=++n)i.push({"type":"image","url":"poplin_product_page_photos/poplin-longsleeve_"+e+".jpg"})
return i}if(t.match(/ss/)){o=[]
for(e=a=1;5>=a;e=++a)o.push({"type":"image","url":"poplin_product_page_photos/poplin-shortsleeve_"+e+".jpg"})
return o}if(t.match(/sl/)){s=[]
for(e=r=1;4>=r;e=++r)s.push({"type":"image","url":"poplin_product_page_photos/poplin-sleeveless_"+e+".jpg"})
return s}}()
a=new Banana.views.components.VerticalSlidesView({"container":this.$(".slides"),"width":1400,"slides":a})
return this.subview("slides",a)}
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/instagram_slide"]=Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'<img class="instagram-image" src='+s((r=null!=(r=t.url||(null!=e?e.url:e))?r:o,typeof r===i?r.call(e,{"name":"url","hash":{},"data":a}):r))+'>\n\n<p class="instagram-username">\n  @'+s((r=null!=(r=t.handle||(null!=e?e.handle:e))?r:o,typeof r===i?r.call(e,{"name":"handle","hash":{},"data":a}):r))+'\n</p>\n\n<p class="instagram-description">\n  '+s((r=null!=(r=t.description||(null!=e?e.description:e))?r:o,typeof r===i?r.call(e,{"name":"description","hash":{},"data":a}):r))+"\n</p>"},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/instagram_slide"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.InstagramSlideView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.template="banana/templates/products/instagram_slide"
n.prototype.tagName="li"
n.prototype.className="instagram-slide"
return n}(Banana.View)}).call(this);(function(){this.HandlebarsTemplates||(this.HandlebarsTemplates={})
this.HandlebarsTemplates["banana/templates/products/product_page_view"]=Handlebars.template({"1":function(e){var t,n=this.lambda,a=this.escapeExpression
return'          <div class="traditional-price">\n            <a href="#post_content">\n              Traditional Retail: $'+a(n(null!=(t=null!=e?e.product:e)?t.traditional_price:t,e))+"\n            </a>\n          </div>\n"},"3":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return'          <span class="price-equation serif" title="'+s((r=null!=(r=t.explanation||(null!=e?e.explanation:e))?r:o,typeof r===i?r.call(e,{"name":"explanation","hash":{},"data":a}):r))+'">\n          '+s((r=null!=(r=t.equation||(null!=e?e.equation:e))?r:o,typeof r===i?r.call(e,{"name":"equation","hash":{},"data":a}):r))+"\n          </span>\n"},"5":function(){return'        <li class="variant-selector"></li>\n'},"7":function(){return'        <li class="quantity-selector"></li>\n'},"9":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l=this.escapeExpression,c="    <section>\n      <b>"+l((i=null!=(i=t.heading||(null!=e?e.heading:e))?i:s,typeof i===o?i.call(e,{"name":"heading","hash":{},"data":a}):i))+"</b>\n"
r=t.each.call(e,null!=e?e.content:e,{"name":"each","hash":{},"fn":this.program(10,a),"inverse":this.program(12,a),"data":a})
null!=r&&(c+=r)
return c+"    </section>\n"},"10":function(e){var t,n=this.lambda,a="        <ul>\n          <li>"
t=n(e,e)
null!=t&&(a+=t)
return a+"</li>\n        </ul>\n"},"12":function(e,t,n,a){var r,i,o="function",s=t.helperMissing,l="        <p>"
r=(i=null!=(i=t.content||(null!=e?e.content:e))?i:s,typeof i===o?i.call(e,{"name":"content","hash":{},"data":a}):i)
null!=r&&(l+=r)
return l+"</p>\n"},"14":function(e,t,n,a){var r,i='  <div class="product-instagram-photos border-top">\n    <h2>From The People</h2>\n\n    <ol class="instagram-thumbnails">\n'
r=t.each.call(e,null!=e?e.first_five_instagram_images:e,{"name":"each","hash":{},"fn":this.program(15,a),"inverse":this.noop,"data":a})
null!=r&&(i+=r)
return i+"    </ol>\n\n  </div>\n"},"15":function(e,t,n,a){var r,i="function",o=t.helperMissing,s=this.escapeExpression
return"        <li>\n          <img src='"+s((r=null!=(r=t.url||(null!=e?e.url:e))?r:o,typeof r===i?r.call(e,{"name":"url","hash":{},"data":a}):r))+"'>\n        </li>\n"},"compiler":[6,">= 2.0.0-beta.1"],"main":function(e,t,n,a){var r,i,o=this.lambda,s=this.escapeExpression,l="function",c=t.helperMissing,p='<div class="product">\n  <section class="images"></section>\n\n  <section class="product-sidebar" itemscope itemtype="http://schema.org/Product">\n\n    <hgroup class="product-title">\n      <h1 class="name" itemprop="name">'+s(o(null!=(r=null!=e?e.grouping:e)?r.name:r,e))+'</h1>\n\n      <meta itemprop="sku" content="'+s((i=null!=(i=t.sku||(null!=e?e.sku:e))?i:c,typeof i===l?i.call(e,{"name":"sku","hash":{},"data":a}):i))+'" />\n      <meta itemprop="brand" content="Everlane" />\n      <meta itemprop="color" content="'+s(o(null!=(r=null!=(r=null!=e?e.product:e)?r.color:r)?r.name:r,e))+'" />\n      <meta itemprop="url" content="https://everlane.com/collections/'+s(o(null!=(r=null!=e?e.product:e)?r.collection_permalink:r,e))+"/products/"+s(o(null!=(r=null!=e?e.product:e)?r.permalink:r,e))+'" />\n      <meta itemprop="image" content="https:'+s((t.staticImageUrl||e&&e.staticImageUrl||c).call(e,null!=(r=null!=e?e.product:e)?r.main_image:r,{"name":"staticImageUrl","hash":{},"data":a}))+'" />\n\n      <h3 class="price" itemprop="offers" itemscope itemtype="schema.org/Offer">\n        <meta itemprop="availability" itemtype="http://schema.org/ItemAvailability" content="http://schema.org/'+s((i=null!=(i=t.availability_for_microformat||(null!=e?e.availability_for_microformat:e))?i:c,typeof i===l?i.call(e,{"name":"availability_for_microformat","hash":{},"data":a}):i))+'" />\n        <span itemprop="price">'+s((t.formatPrice||e&&e.formatPrice||c).call(e,null!=(r=null!=e?e.product:e)?r.price:r,{"name":"formatPrice","hash":{},"data":a}))+"</span>\n\n"
r=t["if"].call(e,null!=e?e.showTradPrice:e,{"name":"if","hash":{},"fn":this.program(1,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='\n        <meta itemprop="priceCurrency" content="USD" />\n\n'
r=t["if"].call(e,null!=e?e.hasEquation:e,{"name":"if","hash":{},"fn":this.program(3,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='      </h3>\n\n    </hgroup>\n\n    <ul class="product-options">\n        <li class="product-selector"></li>\n\n'
r=t["if"].call(e,null!=e?e.showVariantSelector:e,{"name":"if","hash":{},"fn":this.program(5,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+="\n"
r=t["if"].call(e,null!=e?e.showQuantitySelector:e,{"name":"if","hash":{},"fn":this.program(7,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='\n      <li class="button-container"></li>\n\n    </ul>\n\n  </section>\n\n</div>\n\n<div class="product-description">\n'
r=t.each.call(e,null!=(r=null!=e?e.product:e)?r.description:r,{"name":"each","hash":{},"fn":this.program(9,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
p+='</div>\n<div class="product-details"></div>\n\n'
r=t["if"].call(e,null!=e?e.instagram_images:e,{"name":"if","hash":{},"fn":this.program(14,a),"inverse":this.noop,"data":a})
null!=r&&(p+=r)
return p},"useData":!0})
return this.HandlebarsTemplates["banana/templates/products/product_page_view"]}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.views.products.ProductPageView=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.mainClass="products"
n.prototype.region="content"
n.prototype.template="banana/templates/products/product_page_view"
n.prototype.listen={"change:product model":"updateDetails"}
n.prototype.events={"click .traditional-price a":"scrollToInfographic","click .instagram-thumbnails li":"openInstagramSlideshow"}
n.prototype.initialize=function(){return this.product=this.model.get("product")}
n.prototype.priceIsRight=function(e){var t
t=function(t){var n
n=function(e){return _.isObject(e)?e[E.session.getCountry()]:e}
return n(e.get("product").get(t))}
return this.setPrice(this.helpers.formatPrice(t("base_price")))}
n.prototype.setPrice=function(e){return this.$("span[itemprop=price], span.base-price").html(e)}
n.prototype.updateDetails=function(){var e,t,n,a,r
n=this.model.get("product")
t=this.helpers.formatPrice(this.model.get("product").get("price"))
this.setPrice(t)
this.$(".price-equation").html(this.priceEquation())
e=window.location.pathname
e=e.replace(/\/[^\/]*$/,"/"+n.get("permalink"))
a=Banana.Application.prototype.titleTemplate({"subtitle":n.get("display_name"),"title":"Everlane"})
Chaplin.mediator.execute("adjustTitle",a)
return"function"==typeof(r=window.history).replaceState?r.replaceState({},a,e):void 0}
n.prototype.attach=function(){var e
n.__super__.attach.apply(this,arguments)
this.preloadImages()
this.$(".price-equation").tipTip({"cssClass":"large","maxWidth":"250px"})
E.lib.helpers.isMobile()||$(".tooltip").tipTip()
e=new Banana.views.products.PurchaseAreaView({"model":this.model,"container":this.$(".button-container")})
return this.subview("purchase_area",e)}
n.prototype.render=function(){var e,t,a,r,i,o,s,l
n.__super__.render.apply(this,arguments)
r=new Banana.views.products.ImagesView({"model":this.model,"container":this.$(".images")})
if("female"===this.model.get("product").get("gender")){e=E.lib.ab.test("autoplay-product-video",["control","autoplay"])
"autoplay"===e&&r.toggleVideo()}this.subview("images",r)
i=Banana.views.products.QuantitySelectorView
this.subview("quantity",new i({"model":this.model,"container":this.$(".quantity-selector")}))
o=this.product.isGiftCard()?Banana.views.products.DenominationSelectorView:Banana.views.products.ColorSelectorView
this.subview("productSelector",new o({"model":this.model,"hover_preview":!0,"container":this.$(".product-selector")}))
l=new Banana.views.products.SizeSelectorView({"model":this.model,"container":this.$(".variant-selector")})
this.subview("size",l)
if(this.product.get("subheros")){s=new Banana.views.products.SubherosListView({"collection":new Banana.Collection(this.product.get("subheros"))})
this.subview("product_subheros",s)}if(a=this.details()){t=new a({"model":this.model,"container":this.$(".product-details")})
this.subview("details",t)}this.preloadImages()
this.$(".price-equation").tipTip({"cssClass":"large","maxWidth":"250px"})
return E.lib.helpers.isMobile()?void 0:$(".tooltip").tipTip()}
n.prototype.preloadImages=function(){var e,t,n,a,r,i,o
e=this.product
t=this.model.get("grouping").get("products").without(e)
n=function(e){var t,n,a,r,i
n=[]
i=e.get("images")
for(a=0,r=i.length;r>a;a++){t=i[a]
n.push(E.lib.ImageHelper.imageUrl(t,{"size":100}))
n.push(E.lib.ImageHelper.imageUrl(t,{"size":442}))
n.push(E.lib.ImageHelper.imageUrl(t))}return E.lib.ImageLoader.loadImages(n)}
n(e)
o=[]
for(r=0,i=t.length;i>r;r++){a=t[r]
o.push(n(a))}return o}
n.prototype.details=function(){var e
e=this.product.get("permalink")
return e.match(/womens-poplin-(ls|ss|sl)/)?Banana.views.products.PoplinDetailsView:void 0}
n.prototype.priceEquation=function(){var e
e=this.model.get("product")
return"("+this.helpers.formatPrice(e.get("price"))+" base + "+this.helpers.formatPrice(e.get("duties"))+" duties + "+this.helpers.formatPrice(e.get("taxes"))+" taxes)"}
n.prototype.scrollToInfographic=function(e){e.preventDefault()
return $("#post_content").velocity("scroll")}
n.prototype.openInstagramSlideshow=function(e){var t,n
t=$(e.currentTarget).index()
this.instagramCollection||(this.instagramCollection=new Banana.Collection(this.product.get("instagram_images")))
n=new Banana.views.components.ModalView({"closeButtonInSubview":!1,"backgroundClass":"slideshow__bg","view":{"class":Banana.views.components.SlideshowView,"collection":this.instagramCollection,"openIndex":t,"alignEl":"img","itemView":Banana.views.products.InstagramSlideView}})
this.subview("instagramSlideshow",n)
return E.pub(E.Event.Product.INSTAGRAM_CLICK,{"imageIndex":t})}
n.prototype.getTemplateData=function(){var e
e=n.__super__.getTemplateData.apply(this,arguments)
e.sku=e.product.variants[0].sku
e.availability_for_microformat=function(){switch(e.product.orderable_state){case"shippable":return"InStock"
case"preorderable":return"PreOrder"
default:return"OutOfStock"}}()
e.hasEquation="CA"===E.session.getCountry()
e.hasEquation&&(e.equation=this.priceEquation())
e.showTradPrice=this.product.get("traditional_price")&&"CA"!==E.session.getCountry()&&this.product.get("subheros")
e.showVariantSelector=this.product.attributes.variants.length>1
e.showQuantitySelector=!1
e.instagram_images=this.product.get("instagram_images")
null!=e.instagram_images&&(e.first_five_instagram_images=e.instagram_images.slice(0,5))
return e}
return n}(Banana.views.application.TopLevelView)}).call(this);(function(){var e,t={}.hasOwnProperty,n=function(e,n){function a(){this.constructor=e}for(var r in n)t.call(n,r)&&(e[r]=n[r])
a.prototype=n.prototype
e.prototype=new a
e.__super__=n.prototype
return e}
e=null
Banana.controllers.ProductsController=function(t){function a(){return a.__super__.constructor.apply(this,arguments)}n(a,t)
a.prototype.beforeAction=function(t){a.__super__.beforeAction.apply(this,arguments)
null==t.permalink&&(t.permalink="SINGLE_PRODUCT_WRAPPER")
return Banana.models.Collection.get(t.permalink).then(function(){return function(t){return e=t}}(this))}
a.prototype.show=function(t,n,a){var r,i,o
o=e.get("products").get(t.id)
r=_.findWhere(o.get("description"),{"heading":"Quick Description"})
r=null!=r?r.content.replace(/(<([^>]+)>)/gi,""):void 0
this.adjustTitle(o.get("display_name"))
this.adjustMetaTag("og:title",""+o.get("display_name")+" - Everlane")
this.adjustMetaTag("og:description",r)
this.adjustMetaTag("og:url","https://"+E.env.getUrlBase()+"#/{product.get('collection_permalink')}/product.get('permalink')")
this.adjustMetaTag("og:image",E.lib.ImageHelper.imageUrl(o.get("main_image")))
o.get("permalink").match("shopstyle")&&!$.cookie("show_shopstyle")&&$.cookie("show_shopstyle",!0,{"expires":new Date(Date.parse("2015-01-01"))})
i=e.grouping_for(o.get("id"),"product_group")
this.reuse("product",{"compose":function(t){return function(){var n
t.model=new Banana.models.ProductPage({"size":null!=(n=a.query)?n.size:void 0,"product":o,"quantity":1,"grouping":i,"monochrome":1===i.get("products").length,"collection":e})
return t.view=new Banana.views.products.ProductPageView({"model":t.model})}}(this),"check":function(e){return e.key===this.options.key},"options":{"key":i.get("name")}})
return E.pub(E.Event.Product.PAGE_VIEW,{"product":o.attributes,"collection":e.attributes})}
return a}(Banana.Controller)}).call(this);(function(){var e={}.hasOwnProperty,t=function(t,n){function a(){this.constructor=t}for(var r in n)e.call(n,r)&&(t[r]=n[r])
a.prototype=n.prototype
t.prototype=new a
t.__super__=n.prototype
return t}
Banana.Application=function(e){function n(){return n.__super__.constructor.apply(this,arguments)}t(n,e)
n.prototype.title="Everlane"
n.prototype.titleTemplate=_.template("<@= subtitle @> \u2013 <@= title @>")
n.prototype.initDispatcher=function(){n.__super__.initDispatcher.apply(this,arguments)
return this.dispatcher.loadController=function(e,t){var n;(n=E.lib).LoadingIndicator||(n.LoadingIndicator=new E.lib.ProgressBar)
E.lib.LoadingIndicator.start(150)
return t(Banana.controllers[""+_.str.capitalize(_.str.camelize(e))+"Controller"])}}
n.prototype.initRouter=function(){var e,t
e=Chaplin.Router.prototype
e.oldRoute=e.route
t=this
e.route=function(e,n,a){var r
t.dispatcher.currentRoute||$("#content").empty()
try{return this.oldRoute(e,n,a)}catch(i){r=i
if("Router#route: request was not routed"!==r.message)throw r
return window.location=e.url}}
return n.__super__.initRouter.call(this,Banana.routes,{"hashChange":!1})}
n.prototype.initLayout=function(){E.session.isSignedIn()&&(E.chaplinCollections.user=new Banana.models.User(E.session.getCurrentUser()))
return this.layout=new Banana.views.application.Layout({"title":this.title,"titleTemplate":this.titleTemplate})}
n.prototype.initControllers=function(){return n.__super__.initControllers.apply(this,arguments)}
n.prototype.initMediator=function(){return n.__super__.initMediator.apply(this,arguments)}
n.prototype.start=function(){var e
E.chaplinCollections.purchased_orders=new Banana.collections.PurchasedOrders
E.chaplinCollections.credit_cards=new Banana.collections.CreditCards
E.chaplinCollections.addresses=new Banana.collections.Addresses
E.chaplinCollections.factory_meta_data=new Banana.collections.FactoryMetaData
E.chaplinCollections.homepages=new Banana.collections.Homepages(E.data.homepages)
e=[]
E.data.initial_collection&&e.push(new Banana.models.Collection(E.data.initial_collection,{"parse":!0}))
E.chaplinCollections.collections=new Banana.collections.Collections(e)
E.chaplinCollections.collections.fetchOnce({"remove":!1})
Chaplin.mediator.subscribe("dispatcher:dispatch",function(e,t,n){return E.pub(E.Event.App.ROUTE,"/"+n.path,e.model)})
E.showAlert=function(e){return E.chaplinCollections.alerts.add(e)}
_.each(Banana.initializers,function(e){return e()})
return n.__super__.start.apply(this,arguments)}
return n}(Chaplin.Application)}).call(this);(function(){var e
e=function(e){e.fn.disableWith=function(e){this.addClass("grey inactive")
this.data("disable-with-old",this.val())
return this.val(e)}
return e.fn.clearDisableWith=function(){this.val(this.data("disable-with-old"))
return this.removeClass("grey inactive")}}
e(jQuery)}).call(this);(function(){$(function(){return E.banana=new Banana.Application})}).call(this)
