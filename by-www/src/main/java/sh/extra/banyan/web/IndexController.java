package sh.extra.banyan.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("")
public class IndexController {

    @RequestMapping("/index.html")
    public String index(HttpServletRequest request,HttpServletResponse response){
//        if(RequestUtils.isLogin(request)){
            return "/index";
//        }
//        return "/login";
    }
    
    @RequestMapping("/main.html")
    public String main(HttpServletRequest request,HttpServletResponse response){
        return "/main";
    }
}