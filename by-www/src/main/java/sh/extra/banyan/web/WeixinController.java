package sh.extra.banyan.web;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import sh.extra.banyan.service.WXService;

import com.gson.WeChat;
import com.gson.model.AccessTokenResult;
import com.gson.util.OauthWeChat;
import com.gson.util.Tools;

@Controller
@RequestMapping("/weixin")
public class WeixinController {

	private static final Logger logger = Logger.getLogger(WeixinController.class);

	@Inject
	private WXService weixinService;
	
	@RequestMapping("/extra1qaz2wsx")
	public void weixin(HttpServletRequest request, HttpServletResponse response) throws IOException{
		Boolean isGet = request.getMethod().equals("GET");

        if (isGet) {
            doGet(request, response);
        } else {
            doPost(request, response);
        }
	}
	
	private void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/xml");
        ServletInputStream in = request.getInputStream();
        String xmlMsg = Tools.inputStream2String(in);
        logger.debug("输入消息:[" + xmlMsg + "]");
        String xml = WeChat.processing(xmlMsg, weixinService);//weixinService.processing(xmlMsg);
        response.getWriter().write(xml);
    }

    private void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String path = request.getServletPath();
        String pathInfo = path.substring(path.lastIndexOf("/"));
        String _token;
        String outPut = "error";
        if (pathInfo != null) {
            _token = pathInfo.substring(1);
            String signature = request.getParameter("signature");// 微信加密签名
            String timestamp = request.getParameter("timestamp");// 时间戳
            String nonce = request.getParameter("nonce");// 随机数
            String echostr = request.getParameter("echostr");//
            // 验证
            if (WeChat.checkSignature(_token, signature, timestamp, nonce)) {
                outPut = echostr;
            }
        }
        response.getWriter().write(outPut);
    }
    
    /**
     * 微信授权回掉页面
     * @param request
     * @param response
     * @param code
     * @return
     */
    @RequestMapping("/auth")
    public String auth(HttpServletRequest request,HttpServletResponse response,Model model,String code){
        logger.info("code:"+code);
        if(StringUtils.isBlank(code)){
            throw new RuntimeException("用户未授权!");
        }
        AccessTokenResult result = OauthWeChat.getToken(code);
        if(result == null || result.getErrcode() != 0){
            throw new RuntimeException("用户未授权!");
        }
        return result.getOpenid();
    }
}
