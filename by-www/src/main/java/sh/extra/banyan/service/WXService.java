package sh.extra.banyan.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.gson.MessageProcessingHandlerImpl;
import com.gson.bean.Articles;
import com.gson.bean.InMessage;
import com.gson.bean.NewsOutMessage;
import com.gson.bean.OutMessage;
import com.gson.bean.TextOutMessage;
import com.gson.oauth.Menu;

@Service
public class WXService extends MessageProcessingHandlerImpl {

	private static final Logger logger = Logger.getLogger(WXService.class);
	
	@PostConstruct
	public void init(){
//		Menu.createMenu("");
	}
	
	public static void main(String[] args) {
		System.out.println(publishMenu());
	}
	public static boolean publishMenu(){
		String menu = "{\"button\":[" +
					"{\"type\":\"view\",\"name\":\"抢车位\",\"url\":\"http://m.api.parkmecn.com/m/activity/apenny/\"}," +
					"{\"type\":\"view\",\"name\":\"我的订单\",\"url\":\"http://m.api.parkmecn.com/m/activity/apenny/order/list.html\"}," +
					"{\"type\":\"click\",\"name\":\"关于我们\",\"key\":\"menu_abount_us\"}" +
						"]}";
		return Menu.createMenu(menu);
	}
	
	@Override
	public OutMessage textTypeMsg(InMessage msg) {
		TextOutMessage oms = new TextOutMessage();
		if("免费停车".equals(msg.getContent())){
			return subscribeEvent(msg);
		}else{
			oms.setContent("欢迎关注停车宝!我们的微信服务还在紧急的开发中，敬请期待!");
		}
		return oms;
	}
	
	private OutMessage helloworld(){
		TextOutMessage oms = new TextOutMessage();
		oms.setContent("欢迎关注停车宝!我们的微信服务还在紧急的开发中，敬请期待!");
		return oms;
	}
	/**
	 * 首次关注回复的消息
	 * @return
	 */
	private OutMessage subscribeEvent(InMessage msg){
		NewsOutMessage out = new NewsOutMessage();
		Articles article = new Articles();
		article.setDescription("停车宝免费停车券限量大派送，先抢先得！");
		article.setPicUrl("http://m.parkmecn.com/activity/apenny/images/pic.png");
		article.setTitle("免费停车券，每天限量领取");
		article.setUrl("http://m.api.parkmecn.com/m/activity/apenny/");
		List<Articles> list = new ArrayList<Articles>();
		list.add(article);
		out.setArticles(list);
		return out;
	}

	@Override
	public OutMessage eventTypeMsg(InMessage msg) {
		logger.info(msg.getEvent());
		if("subscribe".equals(msg.getEvent())){
			return subscribeEvent(msg);
		}else if("unsubscribe".equals(msg.getEvent())){
			//取消关注
		}
//		else if("CLICK".equals(msg.getEvent())){
			
		if("menu_abount_us".equals(msg.getEventKey())){
			TextOutMessage oms = new TextOutMessage();
			oms.setContent("停车宝将逐步开通停车场可预订空位查询、车位预订，让您的爱车告别无家可归的\"流浪\"生活；" +
					"空闲车位出租发布，让您的空闲车位能赚钱；" +
					"停车费在线支付、实现免取卡免排队快进快出。\n" +
					"\n" +
					"有了停车宝，停车不再难！"+	
					"\n" +
					"\n" +
					"客服电话:4008881362\n"+
					"停车场合作电话:18607313332"
					);
			return oms;
		}
//		}
		
		return super.eventTypeMsg(msg);
	}
    
    
    
}
